DELETE FROM transactions;
UPDATE balance_bank SET balance = 0;
UPDATE balance_savings SET balance = 0;
UPDATE balance_holding SET balance = 0;
UPDATE balance_loans SET balance = 0;
UPDATE balance_dcf SET balance = 0;
UPDATE balance_school_fees SET balance = 0;
UPDATE balance_facility_provisions SET balance = 0;
