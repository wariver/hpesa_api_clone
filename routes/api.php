<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api"f middleware group. Enjoy building your API!
|
*/
Route::get('test', 'Api@test');
Route::post('crud_test', 'Api@crud_test');
/*Route::post( 'login', function ( Request $request ) {

	$email    = $request->get( 'email' );
	$password = $request->get( 'userpass' );
	$password = md5( $password );
	$user = DB::table( 'users' )->$this->where( 'email', $email )->where( 'password', $password )->first();
	return json_encode( $user );
} );*/

Route::post('login', 'Api@login');
Route::post('sign_up', 'Api@sign_up');
Route::post('register', 'Api@register');
Route::post('create_ac', 'Api@create_target_account');
Route::post('create_school_fees_account', 'Api@create_school_fees_account');
Route::post('my_savings_balance', 'Api@get_my_savings_balance');
Route::post('mpesa_payment', 'Api@mpesa_payment');
Route::post('mpesa_dcfs', 'Api@mpesa_payment_dcfs');


Route::post('user_s_accounts', 'Api@user_school_accounts');
Route::post('list_school_accounts', 'Api@list_school_accounts');
Route::post('withdraw_bal', 'Api@withadrawable_balance');
Route::post('user_profile', 'Api@get_user_profile');
Route::post('mpesa_wthdrw_req', 'Api@mpesa_withdrawal_request');

Route::post('send_cash_savings', 'Api@transfer_to_savings');
Route::post('send_member_cash', 'Api@transfer_to_member');
Route::post('confirm_cash_transfer_member', 'Api@confirm_transfer_to_other');
//Route::post( 'bank_dcf', 'Api@transfer_to_dcf' );

Route::post('savings', 'Api@savings');

Route::post('sys_scoring', 'Api@instant_financing');


Route::post('airtime', 'Api@buy_airtime');

Route::post('group_members', 'Api@group_members');
Route::post('user_groups', 'Api@user_groups');
Route::post('group_action', 'Api@change_group_membership_status');

Route::post('bank_to_memberships', 'Api@bank_to_memberships');

Route::post('request_count', 'Api@get_requests_count');
Route::post('memberships', 'Api@user_memberships');

Route::post('grp_members', 'Api@grp_members');
Route::post('borrow_guarantors', 'Api@add_list_group_loan_guarantors');
Route::post('borrow_guarantors_main', 'Api@add_list_main_guarantors');

Route::post('group_savings', 'Api@group_savings');

Route::post('member_group_savings', 'Api@member_group_savings');
Route::post('group_loan_eligibility', 'Api@chk_group_loan_eligibility');
Route::post('outstanding_group_loans', 'Api@list_outstanding_group_loans');
Route::post('group_loan_balances', 'Api@group_loan_balances');
Route::post('loan_guarantee_requests', 'Api@loan_guarantee_request');
Route::post('loan_guarantee_request_ct', 'Api@loan_guarantee_request_count');
Route::post('ad_guarantorship', 'Api@ad_gurantee_request');

Route::post('dcfs_bal', 'Api@account_balance');
Route::post('dcfs_total_bal', 'Api@getTotalsDCFS');
Route::post('groups_total_balance', 'Api@getTotalsGroups');

Route::post('school_fees_limit', 'Api@getFinancingNTimes');
Route::post('business_facility_limit', 'Api@getTotalsFunding');
Route::post('instant_financing_limit', 'Api@selfScoring');

Route::post('select_funding_ac', 'Api@funding_params');

Route::post('instant_financing_limit_old', 'Api@getInstantLoanLimit');

Route::post('user_loans', 'Api@list_loans');
Route::post('loan_types', 'Api@get_loan_types');

Route::post('withdrawal_report', 'Api@get_withdrawals_report');
Route::post('airtime_report', 'Api@get_airtime_report');
Route::post('deposits_report', 'Api@get_deposits_report');
Route::post('banking_report', 'Api@get_banking_reports');

Route::post('loan_eligible_main', 'Api@get_users_eligibility_for_loan');
Route::post('business_eligible_main', 'Api@business_eligibility_for_loan');
Route::post('loan_eligible_main_confirm', 'Api@get_users_eligibility_for_loan_and_apply');
Route::post('business_eligible_main_confirm', 'Api@business_eligibility_for_loan_and_apply');
Route::post('school_fees_elig_confirm', 'Api@users_eligibility_for_school_fees_and_apply');
Route::post('user_school_fees_financing', 'Api@getSchoolFeesFinancingBalance');

Route::post('user_reports', 'Api@user_reports');

Route::post('bank_loan_repayment', 'Api@repay_loan_withdrawable_account');


Route::get('balance/{id}/{ac_type}', 'Api@withdraw_balance');
Route::get('user_accounts', 'Api@get_user_accounts');


Route::post('metro_pol', 'Api@metropol');

Route::get('user_details/{id}', 'Api@get_user_details_for_loan');
Route::get('fetch_loans_report', 'Api@fetchLoansReport');

Route::get('get_bank_tx/{account_id}/{count}', 'Abi@get_last_account_transactions');


/*NEW API ROUTES*/
Route::post('loans_test', 'Facility@test');
Route::get('function_tester', 'Facility@function_tester');
/*BANK TRANSACTIONS*/
Route::post('withdraw_to_phone', 'Bank@withdraw_to_phone');
Route::post('bank_savings', 'Bank@bank_savings');
Route::post('bank_member', 'Bank@bank_bank');
Route::post('bank_airtime', 'Bank@bank_airtime');
Route::post('bank_dcf', 'Bank@bank_dcf');
Route::post('c2b_wallet', 'Bank@C2B_bank');

//Route::get('is_member/{group_id}/{member_id}', 'Groups@isGroupMember');
/*SAVINGS TRANSACTIONS*/
Route::post('c2b_savings', 'Savings@C2B_savings');
Route::post('savings_reports', 'Savings@expose_reports');
Route::get('create_accounts/{us_name}/{us_id}', 'Start@create_accounts');
Route::post('savings_bank', 'Savings@savings_bank');

/*REGISTRATION NEW*/
Route::post('registration', 'Start@sign_up');
Route::post('balance', 'Balances@user_balance');

/*BALANCE*/
Route::post('user_rpt_bank', 'Reports@user_bank_reports');
Route::post('user_rpt_savings', 'Reports@user_savings_reports');
Route::post('user_rpt_facility', 'Reports@user_facility_reports');
Route::get('user_rpt_cbk', 'Reports@cbk_facility_reports');

/*DCF ROUTES*/
Route::post('c2b_dcf', 'DCF@c2b_dcf');
Route::post('dcf_bank', 'DCF@dcf_bank');
Route::post('dcf_list', 'DCF@dcf_list');
Route::post('create_dcf', 'DCF@create_dcf');
Route::post('create_school_fees', 'DCF@create_school_fees');
Route::post('balance_dcf', 'Balances@user_balance_dcf');
Route::post('dcf_details', 'DCF@dcf_details');

/*FACILITY ROUTES*/
Route::post('apply_facility', 'Facility@check_and_apply');
Route::post('eligibility', 'Facility@eligibility');
Route::get('facility_balances/{user_id}', 'Facility@get_user_facility_balance');
Route::get('facility_limits/{user_id}', 'Abi@get_user_facility_limit');
Route::post('repayment', 'Facility@repayment');

Route::get('facility_status/{status}', 'Facility@get_defaulted_loans');


Route::get('user_ac/{user_id}/{ac_type}', 'Abi@get_user_account');

Route::post('balance_dcfs_group', 'DCF@balance_sum_dcfs');
Route::post('reports', 'Abi@get_reports');


/*FACILITIES AND DATA CLEANING OPERATIONS*/
Route::get('clean_up_serviced', 'Facility@archive_serviced_loans');
Route::get('facilities_stats/{defaulted}', 'Facility@get_defaulted_loans');


Route::get('detach_account/{user_id}/{account_id}', 'Abi@remove_account_to_user_holder');
Route::get('disbursed_today', 'Facility@total_assets_disbursed_today');
Route::get('disbursed_month', 'Facility@total_assets_disbursed_month');
Route::get('disbursed/{start}/{end}', 'Facility@total_assets_disbursed');
Route::get('disbursed', 'Facility@total_assets_disbursed');
Route::get('repayments_today', 'Facility@total_repayments_today');
Route::get('repayments_yesterday', 'Facility@total_repayments_yesterday');
Route::get('repayments_month', 'Facility@total_repayments_month');


Route::get('admin_numbers', 'Facility@facility_admin_numbers');


Route::get('last_user_transactions/{user_id}/{count}', 'Reports@user_last_transactions');
Route::get('tx_user/{user_id}/{count}', 'Reports@transactions_by_users_accounts');
Route::get('dashboard_graph/{user_id}', 'Reports@user_dashboard_graph');


//OTHERS
Route::get('test_q', 'Abi@test_queuing');
Route::post('registration_messages', 'Abi@registration_sms_mail');

//FACILITY AGING
Route::get('get_age/{account}', 'Provisioning@get_loan_age');
Route::get('provision/{account}/{user_id}', 'Provisioning@should_provision_or_reverse');
Route::get('provision_all', 'Provisioning@provision_zote');

Route::get('get_overdue_facilities', 'Provisioning@get_facilities_overdue');
Route::get('get_facility_provisioning/{id}', 'Provisioning@get_facility_provisioning');

Route::get('do_penas', 'Provisioning@do_penalties');




//MEMBERSHIPS
Route::post('invite_member', 'Groups@invite_to_group');

Route::get('all_members/{group_id}','Groups@get_all_group_members');
Route::get('pending_members/{group_id}','Groups@get_pending_group_members');
Route::get('active_members/{group_id}','Groups@get_active_group_members');
Route::get('declined_members/{group_id}','Groups@get_declined_group_members');




