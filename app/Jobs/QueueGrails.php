<?php

namespace App\Jobs;

use App\Http\Controllers\Api;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class QueueGrails implements ShouldQueue {
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	protected $job_type;
	protected $param1;
	protected $api;


	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct( $_job_type, $_param1 ) {
		$this->job_type = $_job_type;
		$this->param1   = $_param1;
		$this->api      = new Api();
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle() {
		//
		switch ( $this->job_type ) {
//			    sms queue
			case 1:
				$phone   = $this->param1['phone'];
				$message = $this->param1['message'];
				$this->api->sendMobileSasa( $phone, $message );
				break;
//				email queue

			case 2:
				$subject = $this->param1['subject'];
				$message = $this->param1['message'];
				$email = $this->param1['email'];
				$files = $this->param1['files'];

				$this->api->sendEmail( $subject, $message, $email, $files );
				break;
//				iprs checking
			case 3:
				break;
//				giving CRB
			case 4:
				break;
//				posting to flex cube
			case 5:

				break;
//
		}
	}
}
