<?php
/**
 * Created by Warui.
 * User: root
 * Date: 8/22/18
 * Time: 5:37 PM
 */

namespace App\Http\Controllers;

use AfricasTalkingGateway;
use AfricasTalkingGatewayException;
use DateTime;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use PHPMailer;
use phpmailerException;
use stdClass;

class Api extends Controller
{
    public $link;
    public $now;

    /**
     * Api constructor.
     */
    public function __construct()
    {
		$this->link = mysqli_connect( '127.0.0.1', 'root', 'DeeEyeB33Wave51234$$', 'hpesa_clone' );
//        $this->link = mysqli_connect('127.0.0.1', 'root', 'pass', 'hpesa_clone');
        date_default_timezone_set('Africa/Nairobi');
        $this->now = date('Y-m-d H:i:s');
    }

    public function test()
    {
        echo 'mths';
    }

    public function crud_test(Request $request)
    {
        $value = $request->get('key');
        $user_id = 37;

        if (DB::table('users')->where('id', $user_id)->update(['idpass' => $value])) {
            echo 'done';
        } else {
            echo 'failed';
        }
    }

    function crud_update($user_id, $idno)
    {
        if (DB::table('users')->where('id', $user_id)->update(['idpass' => $idno])) {
            return true;
        } else {
            echo false;
        }

        return false;
    }

//	1
    public function login(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');
        $longitude = $request->get('longitude');
        $latitude = $request->get('latitude');
//		$password  = md5( $password );

        $user = DB::table('users')->where('email', $email)->where('c_password', $password)->first();
        if (!$user) {
            $response = array(
                'status' => 504,
                'error' => 'You have entered the wrong email and password combination'
            );
        } else {
            $userId = $user->id;
            $notiText = "";
            $notiTime = "";
            $user->notif_text = $notiText;
            $user->notif_time = $notiTime;
            $user->savings_balance = $this->getSavingsBalance($userId);

            $this->checkAdminNotifications($userId);

            if (!$noti = DB::table('usernotifications')->where('userId', $userId)->first()) {
                $response = array('status' => 504, 'error' => 'database notifications error');
            } else {
                if (!empty($noti)) {

                    $notiText = $noti->messagetext;
                    $notiTime = $this->formatDateTime($noti->timeupdated);

                    $user->notif_text = $notiText;
                    $user->notif_time = $notiTime;

                    $notiId = $noti->notificationId;

                    //update notification status
                    if (!DB::update('UPDATE usernotifications SET readflag = ? WHERE notificationId = ?', [
                        1,
                        $notiId
                    ])) {
                        $response = array('status' => 501, 'error' => 'database notifications error');
                    }
                }
                date_default_timezone_set('Africa/Nairobi');
                $now = date('Y-m-d H:i:s');
                //update location
                if ($latitude != 0 && $longitude != 0) {
                    if (!DB::insert("INSERT INTO locationupdates(latitude, longitude, timeupdated, userId) VALUES($latitude, $longitude, $now, $userId)")) {
                        $response = array('status' => 501, 'error' => 'database notifications error');
                    }
                }
            }
            $response = $user;
        }
        echo json_encode($response);
    }

//	2
    public function sign_up(Request $request)
    {

        $name = $request->get('username');
        $actype = $request->get('actype');
        $idpass = $request->get('idpass');
        $phone = $request->get('phone');
        $email = $request->get('useremail');
        $dob = $request->get('dob');
        $country = $request->get('country');
        $town = $request->get('town');
        $password = $request->get('password');
        $referal = $request->get('referal');

        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');

        //format phone
        if (substr($phone, 0, 2) === "07")//if phone starts with 07
        {
            $phone = substr_replace($phone, "254", 0, 1);
        } elseif (substr($phone, 0, 1) === "7")//if starts with 7
        {
            $phone = substr_replace($phone, "254", 0, 0);
        } elseif (substr($phone, 0, 4) === "+254") {
            $phone = substr_replace($phone, "", 0, 1);
        }
        if ($this->validatePhone($phone) == 1) {
            $response['phone'] = $phone;
            if ($this->validateIdPass($idpass, $actype) == 1) {
                $response['id'] = $idpass;
                $response['actype'] = $actype;

                if (!$query = DB::table('users')->where('email', $email)->get()) {
                    $response = array('status' => 501, 'error' => 'Failed to fetch email');
                } else {

                    if (count($query) > 0)//if email is in use
                    {
                        $response = array('status' => 501, 'error' => 'This email is already registered');
                    } else {

                        //check if ID/pas is registered
                        $idpass = $actype == 4 ? time() . rand(1000, 9999) : $idpass;//generate random number for group


                        if (!$checkidpass = DB::table('users')->where('idpass', $idpass)->get()) {
                            $response = array('status' => 501, 'error' => 'Could not find id number in database');
                        } else {

                            if (count($checkidpass) == 0) {
                                if ($actype == 4) {
                                    $acStatusThis = 2;
                                } elseif ($actype == 5) {
                                    $acStatusThis = 2;
                                } elseif ($actype == 6) {
                                    $acStatusThis = 2;
                                } else {
                                    $acStatusThis = 0;
                                }

                                if (!$user = DB::insert("INSERT INTO users (fullname,idpass,phone,email,password,actype,acstatus,signuptime,passrecoverycode,signupoption,regfee,membershipfee,timepaid,timeactivated,merchantId,passkey,blacklist,phoneconfirmed,mpesaconfirmed,referredby,phone_iprs,gender,physicaladdress,dateofbirth,country,town) VALUES('$name','$idpass','$phone','$email','$password',$actype,$acStatusThis,'$now','',2,0,0,'$now','$now','','',0,0,0,'','',0,'','$dob','$country','$town')"))//insert record
                                {
                                    $response = array('status' => 501, 'error' => 'Failed to insert user ');
                                } else {
                                    $userId = DB::table('users')->select('id')->where('email', $email)->get();
                                    //if group generate registration number
                                    $userId = $userId[0]->id;

                                    if ($actype == 4) {
                                        $idno = "Group" . $userId;

                                        if (!$this->crud_update($userId, $idno)) {

                                            echo json_encode(array(
                                                'status' => 501,
                                                'error' => 'Failed to update gid'
                                            ));
                                        } else {

                                            $message = "$name's registration number is " . $idno;
                                            $this->sendMobileSasa($phone, $message);
                                        }
                                    } elseif ($actype == 5) {
                                        $idno = "Business" . $userId;

                                        if (!$this->crud_update($userId, $idno)) {

                                            echo json_encode(array(
                                                'status' => 501,
                                                'error' => 'Failed to update gid'
                                            ));
                                        } else {

                                            $message = "You have registered $name successfully as a business, registration number is " . $idno;
                                            $this->sendMobileSasa($phone, $message);
                                        }
                                    } elseif ($actype == 6) {
                                        $idno = "Sacco" . $userId;

                                        if (!$this->crud_update($userId, $idno)) {

                                            echo json_encode(array(
                                                'status' => 501,
                                                'error' => 'Failed to update gid'
                                            ));
                                        } else {

                                            $message = "Your Sacco $name has been created successfully. Your registration number is " . $idno;
                                            $this->sendMobileSasa($phone, $message);
                                        }
                                    }

//									if ( $actype == 1 && ! empty( $referal ) )//if individual - add referal
//									{
                                    /*	if ( ! DB::insert( "INSERT INTO referals(idphone,dateadded,userId,status) VALUES('$referal','$now',$userId,0)" ) ) {
											$response = array( 'status' => 501, 'error' => 'Failed to insert referal' );
										}*/
//									}

                                    //send email
                                    $message = $this->getSignUpEmailMessage($name);
                                    $subject = "Welcome to DIB WAVES";
                                    $files = "Loan_policy.pdf";
                                    $this->sendEmail($subject, $message, $email, $files);

                                    $response['user_id'] = $userId;
                                    $response['actype'] = $actype;
                                    $response['ac_status'] = $acStatusThis;
                                }
                            } else {
                                $response = array(
                                    'status' => 501,
                                    'error' => 'The Id/Registration number you entered is already registered'
                                );
                            }
                        }
                    }
                }
            } else {
                $response = array('status' => 501, 'error' => 'enter a valid / passport / registration number');
            }
        } else {
            $response = array('status' => 501, 'error' => 'phone number is invalid');
        }
        echo json_encode($response);
    }

//	4
    public function savings(Request $request)
    {
        $user_id = $request->get('userId');
        $response = 0;

        if (isset($user_id)) {
            $savings = 0;
            if (!$savvals = DB::table('savings')->where('userId', $user_id)->orderBy('savingsId', 'DESC')->get()) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                $lastTwenty = "<h4>Last 50 transactions</h4>";
                $num = 1;
                $collection = array();
                for ($i = 0; $i < count($savvals); $i++) {
                    $savings += $savvals[$i]->amount;

                    if ($num <= 50) {
                        $obj = new stdClass();
                        $lastTwenty .= "<i><b>Transaction $num</b></i><br/><b>Amount:</b> Ksh " . number_format($savvals[$i]->amount, 2)
                            . " <br/><b>Date: </b>" . $this->formatDateTime($savvals[$i]->timemade)
                            . "<br/><b>Method:</b> " . $this->savingsSource($savvals[$i]->source) . "<br/><br/>";

                        $obj->i = $num;
                        $obj->amount = number_format($savvals[$i]->amount, 2);
                        $obj->date = $this->formatDateTime($savvals[$i]->timemade);
                        $obj->method = $this->savingsSource($savvals[$i]->source);
                    }
                    $num++;
                    array_push($collection, $obj);
                }
            }
            //get registration fee and membership fee paid
            if (!$regfee = DB::table('users')->where('id', $user_id)->get()) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                $regfee = $regfee[0];
                //$regmembership = "<p><b>Activation Fees Paid:</b> Ksh ".number_format($regfeerow['regfee'],2)."</p><p><b>Membership Fees Paid:</b> Ksh ".number_format($regfeerow['membershipfee'],2)."</p>";
                $regmembership = number_format($regfee->regfee, 2);
            }
            $response = array(
                'status' => 200,
                'savings' => $collection,
                'count' => count($collection),
                'registration' => $regmembership
            );
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

    public function withdraw_balance($user_id, $ac_type)
    {
        $withAmount = 0;
        /*if ( $ac_type == 1 ) {
			$withAmount = $this->getWithdrawableHoldingAmount( $user_id );
		} elseif ( $ac_type == 2 ) {*/
        if ($ac_type == 1) {
            $withAmount = $this->getWithdrawableAmount($user_id);
        }
//		}

        echo json_encode(array('balance' => $withAmount));
    }

//	31
    public function transfer_to_savings(Request $request)
    {
        $user_id = $request->get('user_id');
        $amount = (int)$request->get('amount');
        $holdingorwallet = $request->get('holdingorwallet');

        if (isset($user_id) && isset($amount) && isset($holdingorwallet)) {
            if (is_numeric($amount) && $amount > 0) {
                //check if user has enough amount on withdrawable account
                if ($holdingorwallet == 1)//from holding account
                {
                    $withAmount = $this->getWithdrawableHoldingAmount($user_id);
                } else {
                    $withAmount = $this->getWithdrawableAmount($user_id);
                }

                if ($withAmount >= $amount) {
                    //handle operation locking - only one withdrawable operation can take place at a time
                    $lockFile = '/var/www/html/dibwaves/admin/attachments/' . $user_id . '.txt';
                    if (!file_exists($lockFile)) {
                        //create lock file
                        $handle = fopen($lockFile, 'a');
                        fwrite($handle, 'operation underway');
                        fclose($handle);

                        $now = date('Y-m-d H:i:s');

                        $purpose = 3;
                        $savingsSource = 3;
                        $description = "Transfer to savings";

                        if ($holdingorwallet == 1)//from holding account
                        {
                            $savingsSource = 6;
                            $transResult = $this->withdrawHoldingFunds($amount, $user_id, $purpose, $description);
                        } else {
                            $savingsSource = 3;
                            $transResult = $this->withdrawFunds($amount, $user_id, $purpose, $description);
                        }

                        if ($transResult == 1)//success
                        {
                            //send to savings
                            $currency = 'KES';
                            $savResults = $this->makeDirectSavings($amount, $user_id, $savingsSource);
                            if ($savResults == 1)     //success
                            {
                                $response = array(
                                    'status' => 200,
                                    'amount' => $amount,
                                    'msg' => 'You have transfered ' . $amount . ' ' . $currency . ' to savings. Please check your balance'
                                );
                            } else {
                                $response = array(
                                    'status' => 501,
                                    'error' => 'Transfer to savings failed. Please try again later.'
                                );
                            }
                        } else {
                            $response = array(
                                'status' => 501,
                                'error' => 'Transfer to savings failed. Please try again later.'
                            );
                        }
                        //delete lock file
                        unlink($lockFile);
//						File::delete($lockFile);

                    } else {
                        $response = array(
                            'status' => 501,
                            'error' => 'Another operation is being performed on this account. Please try again later.'
                        );
                    }
                } else {
                    $response = array(
                        'status' => 505,
                        'amt' => number_format($withAmount, 2),
                        'error' => 'You have insufficient balance. Your account balance is Ksh '
                    );
                }
            } else {
                $response = array(
                    'status' => 501,
                    'error' => 'Amount must be greater than 0'
                );
            }
        } else {
            $response = array(
                'status' => 501,
                'error' => 'Request could not complete'
            );
        }
        echo json_encode($response);
    }

//	32
    public function transfer_to_member(Request $request)
    {
        $idpass = $request->get('idpass');
        $amount = $request->get('amount');
        $description = $request->get('description');
        $userId = $request->get('user_id');
        if (isset($idpass) && isset($amount) && isset($description) && isset($userId)) {
            $txnId = 0;

            if (is_numeric($amount)) {

                //check if id/pass exists
                if (!$check = DB::table('users')->where('idpass', $idpass)->first()) {
                    $response = array(
                        'status' => 501,
                        'error' => 'user was not found'
                    );
                } else {
                    if (empty($check)) {
                        $response = array(
                            'status' => 501,
                            'error' => 'The national id/passport does not exist'
                        );
                    } else {
                        $toId = $check->id;

                        if ($toId == $userId) {
                            $response = array(
                                'status' => 501,
                                'error' => 'You cannot transfer to your own account.'
                            );
                        } else {
                            $charge = 50;//our profit

                            //check if user has enough amount on withdrawable account
                            $withAmount = $this->getWithdrawableAmount($userId);
                            if ($withAmount >= ($amount + $charge)) {
                                //insert into transfers

                                $approvalCode = strtoupper(substr(md5(rand()), 0, 7));

                                if (!DB::insert("INSERT INTO transfers(fromId,toId,idpass,amount,transfertime,approvalstatus,approvalcode,remarks,charge) VALUES($userId,$toId,$idpass,$amount,'$this->now',0,'$approvalCode','$description',$charge)")) {
                                    $response = array(
                                        'status' => 501,
                                        'error' => 'Failed to transfer funds'
                                    );
                                } else {
                                    $txnId = DB::select("SELECT transferId FROM transfers ORDER BY transferId DESC LIMIT 1");
                                    $txnId = $txnId[0]->transferId;
                                    //send sms
                                    $phoneNo = $this->getUserRegisteredPhone($userId);

                                    if (strlen($phoneNo) >= 9) {
                                        $message = "Your transfer code is $approvalCode";
                                        $smsResult = $this->sendMobileSasa($phoneNo, $message);

                                        if ($smsResult == 1)//success
                                        {
                                            $response = array(
                                                'status' => 200,
                                                'tx_id' => $txnId,
                                                'recipient' => $this->getUserName($toId),
                                                'msg' => 'confirm transfer to '
                                            );
                                        } else {
                                            $response = array(
                                                'status' => 501,
                                                'error' => 'SMS with activation code cannot be sent at the moment. Please try again later.'
                                            );
                                        }
                                    } else {
                                        $response = array(
                                            'status' => 501,
                                            'error' => 'Your phone number was not properly captured during registration. Contact customer care for more guidance'
                                        );
                                    }
                                }
                            } else {
                                $response = array(
                                    'status' => 505,
                                    'amt' => number_format($withAmount, 2),
                                    'error' => 'Insufficient balance: Your account balance is Ksh'
                                );
                            }
                        }
                    }
                }

            } else {
                $response = array(
                    'status' => 503,
                    'msg' => 'Enter a valid amount'
                );
            }
        } else {
            $response = array(
                'status' => 501,
                'msg' => 'Request could not complete'
            );
        }
        echo json_encode($response);
    }

//	32.5
    public function transfer_to_dcf(Request $request)
    {
        $user_id = $request->get('user_id');
        $amount = $request->get('amount');
        $dcfs_id = $request->get('account_id');
        $dcfs_type = $request->get('dcfs_type');
        $dcfs_option = $request->get('lipakaro');

        switch ($dcfs_type) {
            case 1:
                $dcfs_type = 'Development';
                break;
            case 2:
                $dcfs_type = 'Car Finance';
                break;
            case 3:
                $dcfs_type = 'Fund Raising';
                break;
            case 4:
                $dcfs_type = 'School Fees';
                break;
        }
        $response = array();

        if (isset($user_id) && isset($dcfs_id) && isset($amount)) {
            if (is_numeric($amount) && $amount > 0) {
                //check if user has enough amount on withdrawable account
                $withAmount = $this->getWithdrawableAmount($user_id);
                if ($withAmount >= $amount) {
                    //handle operation locking - only one withdrawable operation can take place at a time
                    $lockFile = '/var/www/html/dibwaves/admin/attachments/' . $user_id . '.txt';
                    if (!file_exists($lockFile)) {
                        //create lock file
                        $handle = fopen($lockFile, 'a');
                        fwrite($handle, 'operation underway');
                        fclose($handle);

                        $now = date('Y-m-d H:i:s');

                        $purpose = 3;
                        $savingsSource = 3;
                        $description = "Transfer to savings";
                        $transResult = 1;
                        $savingsSource = 3;
                        if ($dcfs_option == 1) {
                            $transResult = $this->withdrawLipaKaro($amount, $user_id, 'lipakaro to school fees', $description);
                        } else {
                            $transResult = $this->withdrawFunds($amount, $user_id, $purpose, $description);
                        }

                        if ($transResult == 1)//success
                        {
                            //send to savings
                            $currency = 'KES';
                            $savResults = $this->makeDirectDCF($dcfs_id, $amount, $dcfs_type);
                            if ($savResults == 1)     //success
                            {
                                $response = array(
                                    'status' => 200,
                                    'amount' => $amount,
                                    'currency' => $currency,
                                    'msg' => 'You have transferred ' . $currency . ' ' . $amount . ' to ' . $dcfs_type . ' account. Please check your balance'
                                );
                            } else {
                                $response = array(
                                    'status' => 501,
                                    'error' => 'Transfer failed. Please try again later.'
                                );
                            }
                        } else {
                            $response = array(
                                'status' => 501,
                                'error' => 'Transfer to savings failed. Please try again later.'
                            );
                        }
                        //delete lock file
                        unlink($lockFile);
                    } else {
                        $response = array(
                            'status' => 501,
                            'error' => 'Another operation is being performed on this account. Please try again later.'
                        );
                    }
                }
            }
        }
        echo json_encode($response);
    }

//	33
    public function confirm_transfer_to_other(Request $request)
    {
        $userId = $request->get('userId');
        $transactionid = $request->get('transactionid');
        $confirmationcode = $request->get('confirmationcode');

        if (isset($userId) && isset($transactionid) && isset($confirmationcode)) {
            //check if transaction code is activated
            if (!$check = mysqli_query($this->link, "SELECT * FROM transfers WHERE transferId=$transactionid AND approvalcode='$confirmationcode' AND fromId=$userId AND approvalstatus=0")) {
                $response = array('status' => 504, 'error' => mysqli_error($this->link));

            } else {
                if (mysqli_num_rows($check) == 0) {
                    $response = array('status' => 502, 'error' => 'This code seems to have expired');
                } else {
                    $checkrow = mysqli_fetch_array($check, MYSQLI_ASSOC);
                    //change approvar status
                    if (!mysqli_query($this->link, "UPDATE transfers SET approvalstatus=1 WHERE transferId=$transactionid")) {
                        $response = "0~Update error " . mysqli_error($this->link);
                    } else {
                        //withdraw funds
                        $amount = $checkrow['amount'];
                        $charge = $checkrow['charge'];
                        $idpass = $checkrow['idpass'];
                        $toId = $checkrow['toId'];

                        $description = "Transfer to " . $idpass;

                        //check if there are enough funds to withdraw
                        $withAmount = $this->getWithdrawableAmount($userId);
                        if ($withAmount >= $amount) {
                            //handle operation locking - only one withdrawable operation can take place at a time
                            $lockFile = '/var/www/html/dibwaves/admin/attachments/' . $userId . '.txt';
                            if (!file_exists($lockFile)) {
                                //create lock file
                                $handle = fopen($lockFile, 'a');
                                fwrite($handle, 'operation underway');
                                fclose($handle);

                                $res1 = $this->withdrawFunds($amount, $userId, 4, $description);

                                if ($res1 == 1) {
                                    //deduct transfer charge
                                    $description2 = "Transaction charge for transfer to $idpass";
                                    $res2 = $this->withdrawFunds($charge, $userId, 4, $description2);

                                    //credit to account
                                    $res3 = $this->makeWithdrawableDeposit($amount, $toId, 3);

                                    if ($res3 == 1)//success
                                    {
                                        $this->recordCommision(2, $userId, $charge, 0);//record commision for report and tax purposes
                                        $response = array('status' => 200, 'msg' => 'Funds transfered to ');
                                    } else {
                                        //reverse amount
                                        $this->recordCommision(2, $userId, -$charge, 0);//record negative commision for report and tax purposes
                                        $this->makeWithdrawableDeposit($amount, $userId, 4);//reverse amount
                                        $this->makeWithdrawableDeposit($charge, $userId, 4);//reverse charge

                                        $response = array(
                                            'status' => 501,
                                            'error' => 'Transfer failed. Please try again'
                                        );
                                    }
                                } else {
                                    $response = array(
                                        'status' => 501,
                                        'error' => 'Transfer failed. Please try again'
                                    );
                                }
                                //delete lock file
                                unlink($lockFile);
                            } else {
                                $response = array(
                                    'status' => 501,
                                    'error' => 'Another operation is being performed on this account. Please try again later.'
                                );
                            }
                        } else {
                            $response = array(
                                'status' => 501,
                                'error' => 'You have insufficient balance. Your account balance is Ksh ' . number_format($withAmount, 2)
                            );
                        }
                    }
                }
            }

        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');

        }
        echo json_encode($response);
    }

//  35
    public function mpesa_withdrawal_request(Request $request)
    {
        $userId = $request->get('userId');
        $amount = $request->get('amount');
        $description = $request->get('description');
        $phone = $request->get('phone');

        if (isset($userId) && isset($amount) && isset($description) && isset($phone)) {
            $tumaSMS = 0;
            $approvalCode = 0;
            $formatPhoneVals = json_decode($this->phoneFormat($phone), true);

            if ($formatPhoneVals['status'] == true) {
                $phone = $formatPhoneVals['formatedPhone'];

                $withAmount = $this->getWithdrawableAmount($userId);
                $txIdWith = 0;
                $chargeRaw = json_decode($this->getMpesaB2CCharges($amount), true);
                $commision = $chargeRaw['commision'];
                $otherDeductions = $chargeRaw['othercharges'];

                if ($withAmount >= ($amount + $commision + $otherDeductions)) {

                    $ttlCharge = $commision + $otherDeductions;

                    /* generate random code - if there's a code that was generated today do not generate a new one - should call office if they did not receive SMS */
                    //check if the last code is used up
                    $todayCheck = date('Y-m-d');
                    $now = date('Y-m-d H:i:s');
                    $getLastApprovalCode = DB::select(DB::raw("SELECT * FROM mpesawithdrawals WHERE processingstatus = 0 AND transactiontime LIKE  :daycheck"), array(
                        'daycheck' => '$todayCheck'
                    ));
//					dd( $getLastApprovalCode );
//					DB::table('mpesawithdrawals')
                    if (!empty($getLastApprovalCode)) {
                        echo json_encode(array('status' => 501, 'error' => 'failed to fetch mpesa withdrawals'));
                    } else {
//						$getLastApprovalCode = $getLastApprovalCode[0];

                        if ($getLastApprovalCode) {
                            $tumaSMS = 1;
                            $approvalCode = $getLastApprovalCode->authorizationcode;
                        } else {
                            $tumaSMS = 1;
                            $approvalCode = strtoupper(substr(md5(rand()), 0, 7));
                        }
                    }
                    /* end generate code*/
                    $tid = 0;

                    if (!$check = DB::table('mpesawithdrawals')->where('transactionId', $tid)->get()) {
                        $response = array('status' => 501, 'error' => 'failed to fetch mpesa withdrawals');
                    } else {

                        if (count((array)$check) == 1) {
                            //insert record in table and send sms

                            if (!DB::insert("INSERT INTO mpesawithdrawals(amount,remarks,phone,transactiontime,status,authorizationcode,charges,transactiondescription,userId,timeconfirmed,timesent,processingstatus,processingtxnid,mpesacode,orgbalabce) VALUES($amount,'$description','$phone','$now',0,'$approvalCode',$ttlCharge,'Created',$userId,'$now','$now',0,'','',0)")) {
                                echo json_encode(array(
                                    'status' => 501,
                                    'error' => 'failed to insert into mpesawithdrawals'
                                ));
                            } else {
                                //send sms
                                $phoneNo = $this->getUserRegisteredPhone($userId);

                                if (strlen($phoneNo) >= 9) {
                                    $message = "Your transfer code is $approvalCode";

                                    if ($tumaSMS == 1) {
//										TODO: SMS SUCCESS RETURNS 1 - SHUTDOWN FOR TESTING
                                        $smsResult = $this->sendMobileSasa($phoneNo, $message);
//										$smsResult = 1;
                                    } else {
                                        $smsResult = 1;
                                    }

                                    if ($smsResult == 1)//success
                                    {

                                        $response = array(
                                            'status' => 200,
                                            'msg' => 'Request was succesful',
                                            'action' => 'Wait for transfer code'
                                        );
                                    } else {
                                        $response = array(
                                            'status' => 501,
                                            'error' => 'There was a problem sending the activation code',
                                        );
                                    }
                                } else {
                                    $response = array(
                                        'status' => 501,
                                        'error' => 'Your phone is not properly registered. Please contact customer care',
                                    );
                                }
                            }
                        }
                    }
                } else {
                    $response = array(
                        'status' => 502,
                        'error' => 'You have insufficient balance.',
                        'amt' => ($commision + $otherDeductions)
                    );
                }
            } else {
                $response = array('status' => 501, 'error' => 'Enter a valid Mpesa phone number');
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }

        echo json_encode($response);
    }
    //  MORE PARAMS

//  14
    public function recover_password(Request $request)
    {
        $user_id = $request->get('userId');
        $email = $request->get('email');
        if (isset($user_id) && isset($email)) {
            $response = "";
            //check if email exists
            if (!$check = DB::table('users')->where('email', $email)->first()) {
                echo json_encode(array('status' => 501, 'error' => 'failed to fetch email'));
            } else {
                if (DB::table('users')->where('email', $email)->count() > 0) {
                    $userId = $check->id;
                    //generate a new recovery code
                    $passRecoveryCode = strtoupper(substr(md5(rand()), 0, 7));

                    //update table
                    if (!DB::update('UPDATE users SET passrecoverycode = ? WHERE id = ?',
                        [$passRecoveryCode, $userId])) {
                        echo json_encode(array('status' => 501, 'error' => 'failed to update pass recovery code'));
                    } else {
                        //send email to user
                        $this->link = "https://homepesasacco.com/recoverpassmy.php?id=$userId&authorization=$passRecoveryCode";
                        $subject = "Homepesa Sacco Password Recovery";
                        $message = "
								<p>Dear " . ucwords(strtolower($check->fullname)) . ",</p>
								<p>We are happy to be at your service. You have requested to recover your password. To do so click <a href='" . $this->link . "'>here</></p>
								<p>To enhance the security of your account, please ensure that your password is atleast 8 characters and a mixture of numbers, letters and symbols. Also avoid using common words and phrases.</p>
								<p>For further inquiries please feel free to call or email us back. Also visit dibwaves.com for more information.</p>
								<p>Regards</p>
								<p>DIB WAVES</p>
							";
                        $files = "";

                        $recoverResult = $this->sendEmail($subject, $message, $_POST['email'], $files);

                        if ($recoverResult == '1') {
                            $response = array(
                                'status' => 200,
                                'msg' => 'An email with the recovery link has been sent to' . $email . 'Click on the link to recover your password.'
                            );
                        } else {
                            $response = array(
                                'status' => 501,
                                'error' => 'Password recovery cannot be completed at the moment. Please try again later.'
                            );
                        }
                    }
                } else {
                    $response = array(
                        'status' => 501,
                        'error' => 'The email you have entered is not registered.'
                    );
                }
            }
        } else {
            $response = array(
                'status' => 501,
                'error' => 'There are missing parameters in your request.'
            );
        }
        echo json_encode($response);
    }

//  9
    public function mpesa_payment(Request $request)
    {
        $phoneno = (int)$request->get('phoneno');
        $amount = (int)$request->get('amount');
        $identifier = $request->get('identifier');
        $userId = $request->get('userId');
        $paymenttype = (int)$request->get('paymenttype');
        /*
				Payment Type: 1 = Savings; 2 = Loap repayment; (3 = Savings other-deprecated); 4 = Withdrawable
				Source: 1 = Mpesa; 2 = Card
				For savings to own account otherid is always 0
			*/
        if (is_numeric($amount) && $amount >= 10) {
            //format phone
            $phoneVals = json_decode($this->phoneFormat($phoneno), true);

            if ($phoneVals['status'] == true) {
                $phone = $phoneVals['formatedPhone'];

                //check if the phone transacting is the phone registered
                $userIdVals = json_decode($this->getUserDetails($userId), true);
                //format registered phone too
                $regPhoneVals = json_decode($this->phoneFormat($userIdVals['phone']), true);

                if ($phone == $regPhoneVals['formatedPhone']) {
                    $reference = $this->getReference($paymenttype, $userId);//get the merchant id and passkey of entity

                    $paymentRequestResult = $this->stkPayment($amount, $phone, $paymenttype, $userId, $identifier, $reference);

                    $resultVals = json_decode($paymentRequestResult, true);

                    if ($resultVals['status'] == 1) {
                        $response = array(
                            'status' => 200,
                            'msg' => 'A request has been sent to your phone.'
                        );
                    } else {
                        $response = array(
                            'status' => 501,
                            'msg' => 'Request failed. Please try again after a few minutes. Alternatively you can send money to paybill number 667872 with ' . $userIdVals['idno'] . ' as account number'
                        );
                    }
                } else {
                    $response = array(
                        'status' => 501,
                        'error' => 'For security purposes, you can only transact from your registered phone ' . $userIdVals['phone']
                    );
                }
            } else {
                $response = array(
                    'status' => 501,
                    'error' => 'You have entered a wrong phone format.'
                );
            }
        } else {
            $response = array(
                'status' => 501,
                'error' => 'Amount must be at least 10.'
            );
        }
        echo json_encode($response);
    }

//	9.5
    public function mpesa_payment_dcfs(Request $request)
    {
        $phoneno = (int)$request->get('phoneno');
        $amount = (int)$request->get('amount');
        $identifier = $request->get('identifier');
        $userId = $request->get('userId');
        $paymenttype = (int)$request->get('paymenttype');
        $account_id = (int)$request->get('account_id');
        /*
				Payment Type: 1 = Savings; 2 = Loap repayment; (3 = Savings other-deprecated); 4 = Withdrawable
				Source: 1 = Mpesa; 2 = Card
				For savings to own account otherid is always 0
			*/
        if (is_numeric($amount) && $amount >= 10) {
            //format phone
            $phoneVals = json_decode($this->phoneFormat($phoneno), true);

            if ($phoneVals['status'] == true) {
                $phone = $phoneVals['formatedPhone'];

                //check if the phone transacting is the phone registered
                $userIdVals = json_decode($this->getUserDetails($userId), true);

                //format registered phone too
                $regPhoneVals = json_decode($this->phoneFormat($userIdVals['phone']), true);

                if ($phone == $regPhoneVals['formatedPhone']) {
                    $reference = $this->getReference($paymenttype, $userId);//get the merchant id and passkey of entity

                    $paymentRequestResult = $this->stkPayment($amount, $phone, $paymenttype, $userId, $identifier, $reference);

                    $resultVals = json_decode($paymentRequestResult, true);

//					add to dcfs account
//					$holder = $this->makeDirectDCF( $account_id, $amount );

                    if ($resultVals['status'] == 1) {
                        $response = array(
                            'status' => 200,
                            'msg' => 'A request has been sent to your phone.'
                        );
                    } else {
                        $response = array(
                            'status' => 501,
                            'msg' => 'Request failed. Please try again after a few minutes. Alternatively you can send money to paybill number 667872 with ' . $userIdVals['idno'] . ' as account number'
                        );
                    }
                } else {
                    $response = array(
                        'status' => 501,
                        'error' => 'For security purposes, you can only transact from your registered phone ' . $userIdVals['phone']
                    );
                }
            } else {
                $response = array(
                    'status' => 501,
                    'error' => 'You have entered a wrong phone format.'
                );
            }
        } else {
            $response = array(
                'status' => 501,
                'error' => 'Amount must be at least 10.'
            );
        }
        echo json_encode($response);
    }

//  15
    public function add_new_card_debit(Request $request)
    {
        $cardno = $request->get('cardno');
        $expirymonth = $request->get('expirymonth');
        $expiryyear = $request->get('expiryyear');
        $security = $request->get('security');
        $userId = $request->get('userId');
        $cardDisplay = $request->get('cardDisplay');

        if (isset($cardno) && isset($expirymonth) && isset($expiryyear) && isset($security) && isset($userId) && isset($cardDisplay)) {
            //validate month
            if (is_numeric($expirymonth) && strlen($expirymonth) == 2 && $expirymonth > 0) {
                if (is_numeric($expiryyear) && strlen($expiryyear) == 4 && $expiryyear >= 2017) {
                    if (is_numeric($cardno) && strlen($cardno) == 16) {
                        if (is_numeric($security)) {
                            //check if card had already been added by someone else
                            if (!$check = mysqli_query($this->link, "SELECT * FROM cards WHERE cardno='$cardno' AND cardstatus <> 3 AND userId <> $userId")) {
                                echo json_encode(array(
                                    'status' => 501,
                                    'error' => 'Failed to fetch cards'
                                ));
                            } else {
                                if (count($check) > 0) {
                                    echo json_encode(array(
                                        'status' => 501,
                                        'error' => 'This card is already in use by someone else. You cannot add the same card to your account.'
                                    ));
                                } else {
                                    //check if card had already been added by this user
                                    if (!$check2 = mysqli_query($this->link, "SELECT * FROM cards WHERE cardno='$cardno' AND cardstatus <> 3 AND userId = $userId")) {
                                        echo json_encode(array('status' => 501, 'error' => 'database error'));
                                        $response = array(
                                            'status' => 501,
                                            'error' => 'For security purposes, you can only transact from your registered phone ' . $userIdVals['phone']
                                        );
                                    } else {
                                        if (count($check2) > 0) {
                                            echo "0~You have already added this card to your profile";
                                            $response = array(
                                                'status' => 501,
                                                'error' => 'You have already added this card to your profile'
                                            );
                                        } else {
                                            $randomAmount = $this->getRandomFloatAmount();
                                            $deductedRandomAmount = $randomAmount * 100;//in cents

                                            $cardVals = json_decode($this->liveCreditCardPayment(0, $userId, $_POST['cardno'], $_POST['expirymonth'], $_POST['expiryyear'], $_POST['security'], $deductedRandomAmount), true);

                                            if ($cardVals['status'] == 1)//success
                                            {
                                                if (!mysqli_query($this->link, "INSERT INTO cards(cardno,carddisplayno,crv,expirymonth,expiryyear,cardstatus,approvalamount,dateadded,approvaldate,userId) VALUES('$cardno','$cardDisplay','$security','$expiryMonth','$expiryYear',0,$randomAmount,'$now','$now',$userId)")) {
                                                    echo json_encode(array(
                                                        'status' => 501,
                                                        'error' => 'Failed to add card transaction'
                                                    ));
                                                } else {
                                                    $response = array(
                                                        'status' => 200,
                                                        'msg' => 'Card has been successfully added. To prove card ownership, a random amount has been deducted from your card for confirmation. Please check your card statement for the amount and confirm the card next.'
                                                    );
                                                }
                                            } else {
                                                $response = array(
                                                    'status' => 501,
                                                    'error' => 'Your card cannot be added at the moment. Please try again later. Confirm that your card has been enabled by the bank for online transactions. Also ensure that you have at least $3 (Ksh 300) in your card. A random amount is deducted from your card which is used to confirm card ownership. The amount is refunded after confirmation.'
                                                );
                                            }
                                        }

                                    }
                                }
                            }
                        } else {
                            $response = array(
                                'status' => 501,
                                'error' => 'Enter a valid CVC number'
                            );
                        }
                    } else {
                        $response = array(
                            'status' => 501,
                            'error' => 'Enter a valid card number'
                        );
                    }
                } else {
                    $response = array(
                        'status' => 501,
                        'error' => 'Enter a valid expiry year as per the instructions'
                    );
                }
            } else {
                $response = array(
                    'status' => 501,
                    'error' => 'Enter a valid expiry month as per the instructions'
                );
            }
        } else {
            $response = array(
                'status' => 501,
                'error' => 'There are missing parameters in your request'
            );
        }
        echo json_encode($response);
    }

//	17
    public function activate_card(Request $request)
    {
        $userId = $request->get('userId');
        $activationamount = $request->get('activationamount');
        $cardId = $request->get('cardId');
        $cardDisplay = $request->get('cardDisplay');

        if (isset($_POST['userId'])) {
            $response = "";
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $activationamount = htmlspecialchars($_POST['activationamount'], ENT_QUOTES);
            $cardId = htmlspecialchars($_POST['cardId'], ENT_QUOTES);
            $cardDisplay = htmlspecialchars($_POST['cardDisplay'], ENT_QUOTES);

            //check if the amount is correct
            if (!$check = mysqli_query($this->link, "SELECT * FROM cards WHERE cardId = $cardId AND approvalamount=$activationamount")) {
                echo "0~Error activate " . mysqli_error($this->link);
            } else {
                if (count($check) > 0) {
                    //change card status
                    if (!mysqli_query($this->link, "UPDATE cards SET cardstatus=1,approvaldate='$now' WHERE cardId = $cardId AND approvalamount=$activationamount")) {
                        echo json_encode(array('status' => 501, 'error' => 'database error'));
                    } else {
                        //send amount to withdrawable once charges have been deducted
                        $nowTime = date('Y-m-d H:i:s');

                        $dolarRateTable = 0;
                        $bankRateTable = 0;
                        $homepesaRateTable = 0;

                        if (!$cardSettings = mysqli_query($this->link, "SELECT * FROM cardsettings")) {
                            echo json_encode(array('status' => 501, 'error' => 'query execution error'));
                        } else {
                            $cardSettingsrow = mysqli_fetch_array($cardSettings, MYSQLI_ASSOC);

                            $dolarRateTable = $cardSettingsrow['dollarrate'];
                            $bankRateTable = $cardSettingsrow['bankrate'];
                            $homepesaRateTable = $cardSettingsrow['homepesarate'];
                        }

                        $amountInKsh = $activationamount * $dolarRateTable;
                        $toKCB = $bankRateTable / 100 * $amountInKsh;
                        $toHomepesa = $homepesaRateTable / 100 * $amountInKsh;
                        $toCustomer = $amountInKsh - $toKCB - $toHomepesa;
                        $totalCharge = $homepesaRateTable + $bankRateTable;

                        if (makeWithdrawableDeposit($toCustomer, $userId, 2) == 1)//success
                        {
                            $response = "1~Card has been successfully confirmed. Ksh " . number_format($toCustomer, 2) . " has been credited to your Homepesa Wallet. (Dollar rate: " . number_format($dolarRateTable, 2) . "; gateway charge " . $totalCharge . "%)";
                        } else {
                            $response = "0~Card has been activated but amount could not be added to your Homepesa Wallet. Please contact customer care for assistance.";
                        }
                    }
                } else {
                    $response = "0~The amount you have entered is wrong";
                }
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

//  19
    public function remove_card(Request $request)
    {
        $cardId = $request->get('cardId');
        $userId = $request->get('userId');
        if (isset($cardId) && isset($userId)) {
            $response = "";
            //check if user owns card
            if (!$check = DB::table('cards')->where('userId', $userId)->where('cardId', $cardId)->get()) {
                echo "0~Error remove " . mysqli_error($this->link);
            } else {
                if (count($check) > 0)//remove card - change status
                {
                    if (!mysqli_query($this->link, "DELETE FROM cards WHERE cardId=$cardId")) {
                        echo "0~Error remove " . mysqli_error($this->link);
                    } else {
                        $response = "0~Card has been successfully removed";
                    }
                } else {
                    $response = "0~This card is not on your profile";
                }
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

    public function update_profile(Request $request)
    {
        $fullname = $request->get('fullname');
        $idpass = $request->get('idpass');
        $email = $request->get('email');
        $phone = $request->get('phone');
        $password = $request->get('password');
        $userId = $request->get('userId');
        if (isset($_POST['fullname']) && isset($_POST['idpass']) && isset($_POST['email']) && isset($_POST['phone']) && isset($_POST['password']) && isset($_POST['userId'])) {
            $fullname = htmlspecialchars($_POST['fullname'], ENT_QUOTES);
            $idpass = htmlspecialchars($_POST['idpass'], ENT_QUOTES);
            $email = htmlspecialchars($_POST['email'], ENT_QUOTES);
            $phone = htmlspecialchars($_POST['phone'], ENT_QUOTES);
            $password = htmlspecialchars($_POST['password'], ENT_QUOTES);
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);

            //none of the above except password should contain ~
            if (strpos($fullname, '~') === false && strpos($idpass, '~') === false && strpos($email, '~') === false && strpos($phone, '~') === false) {
                //check if email is already registered by someone else
                if (!$register = mysqli_query($this->link, "SELECT * FROM users WHERE email='$email' AND id<>$userId")) {
                    echo json_encode(array('status' => 501, 'error' => 'query execution error'));
                } else {
                    if (count($register) > 0) {
                        echo "0~This email is already registered by someone else with a different account";
                    } else {
                        //insert into table
                        if (!mysqli_query($this->link, "UPDATE users SET email='$email',password='$password' WHERE id=$userId")) {
                            echo json_encode(array('status' => 501, 'error' => 'database error'));
                        } else {
                            $response = "1~Profile has been successfully updated";
                        }
                    }
                }
            } else {
                $response = "0~Please ensure that you do not use ~ anywhere";
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

//	24
    public function check_card_status(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($user_id)) {
            //get card records
            if (!$addedCards = DB::table('cards')->where('userId', $user_id)->where('cardstatus', '!=', 3)->get()) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                $totalNoofCards = count($addedCards);
                if ($totalNoofCards == 0)//no card added
                {
                    $response = "1~Dear " . getUserName($user_id) . ", you have not added any debit/credit card. Would you like to add a new card?";
                } else {
                    //check if there are any inconfirmed cards
                    if (!$confirmed = DB::table('cards')->where('userId', $user_id)->where('cardstatus', 1)->get()) {
                        echo json_encode(array('status' => 501, 'error' => 'database error'));
                    } else {
                        $confirmedCards = count($confirmed);

                        if ($confirmedCards == $totalNoofCards)//all cards added are confirmed
                        {
                            $dolarRateTable = 0;
                            $bankRateTable = 0;
                            $homepesaRateTable = 0;

                            if (!$cardSettings = DB::table('cardsettings')->get()) {
                                echo json_encode(array('status' => 501, 'error' => 'database error'));
                            } else {
                                $cardSettings = $cardSettings[0];

                                $dolarRateTable = $cardSettings->dollarrate;
                                $bankRateTable = $cardSettings->bankrate;
                                $homepesaRateTable = $cardSettings->homepesarate;
                            }

                            $rateString = "$1 = Ksh $dolarRateTable (Gateway charge " . ($bankRateTable + $homepesaRateTable) . "%)";

                            $response = array(
                                'status' => 200,
                                'msg' => $this->getUserName($user_id),
                                'val' => $rateString
                            );
                        } elseif ($confirmedCards != $totalNoofCards && $confirmedCards > 0)//some cards not confirmed
                        {
                            $response = array(
                                'status' => 201,
                                'msg' => 'Dear ' . $this->getUserName($user_id) . ', there is an unconfirmed card. Would you like to confirm card?'
                            );

                        } else//all cards not confirmed
                        {
                            $response = array(
                                'status' => 202,
                                'msg' => 'Dear ' . $this->getUserName($user_id) . ', you have not confirmed any card. Would you like to confirm card?'
                            );
                        }
                    }
                }
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }

        echo json_encode($response);
    }

    public function pay_with_debit_credit_card(Request $request)
    {
        $cardno = $request->get('cardno');
        $expirymonth = $request->get('expirymonth');
        $expiryyear = $request->get('expiryyear');
        $security = $request->get('security');
        $amount = $request->get('amount');
        $identifier = $request->get('identifier');
        $userId = $request->get('userId');
        $paymenttype = $request->get('paymenttype');
        if (isset($_POST['cardno']) && isset($_POST['expirymonth']) && isset($_POST['expiryyear']) && isset($_POST['security']) && isset($_POST['amount']) && isset($_POST['identifier']) && isset($_POST['userId']) && isset($_POST['paymenttype'])) {
            $cardno = md5(htmlspecialchars($_POST['cardno'], ENT_QUOTES));
            $expirymonth = md5(htmlspecialchars($_POST['expirymonth'], ENT_QUOTES));
            $expiryyear = md5(htmlspecialchars($_POST['expiryyear'], ENT_QUOTES));
            $security = md5(htmlspecialchars($_POST['security'], ENT_QUOTES));
            $amount = htmlspecialchars($_POST['amount'], ENT_QUOTES);
            $paymenttype = htmlspecialchars($_POST['paymenttype'], ENT_QUOTES);
            $id = htmlspecialchars($_POST['identifier'], ENT_QUOTES);
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);

            //check if this card has been allowed to do other transactions
            if (cardTransactionAllowed($cardno, $paymenttype) == 1)//card has been allowed
            {
                if ($amount >= 1) {
                    $dolarRateTable = 0;

                    if (!$cardSettings = mysqli_query($this->link, "SELECT * FROM cardsettings")) {
                        echo json_encode(array('status' => 501, 'error' => 'query execution error'));
                    } else {
                        $cardSettingsrow = mysqli_fetch_array($cardSettings, MYSQLI_ASSOC);

                        $dolarRateTable = $cardSettingsrow['dollarrate'];
                    }

                    if (($amount * $dolarRateTable) <= checkDailyCardLimit($userId)) {
                        //step 1 check if card is added
                        if (!$check = mysqli_query($this->link, "SELECT * FROM cards WHERE cardno='$cardno' AND userId=$userId")) {
                            echo json_encode(array('status' => 501, 'error' => 'database error'));
                        } else {
                            if (count($check) == 0)//card not added
                            {
                                $response = "2~You have not added this card to your profile. Would you like to add card?";
                            } else {
                                //check if card is activated
                                $checkrow = mysqli_fetch_array($check, MYSQLI_ASSOC);

                                $cardStatus = $checkrow['cardstatus'];

                                if ($cardStatus == 0)//card not activated
                                {
                                    $response = "3~You have not confirmed this card. Would you like to confirm card?";
                                } elseif ($cardStatus == 1)//card activated
                                {
                                    $amountCents = $amount * 100;
                                    $cardVals = json_decode(liveCreditCardPayment($paymenttype, $userId, $_POST['cardno'], $_POST['expirymonth'], $_POST['expiryyear'], $_POST['security'], $amountCents), true);

                                    if ($cardVals['status'] == 1)//success
                                    {
                                        echo json_encode(array('status' => 200, 'msg' => 'Payment is successful'));
                                    } else {
                                        echo json_encode(array(
                                            'status' => 501,
                                            'error' => 'Payment failed. (' . $cardVals['description'] . '). Ensure that online payment is enabled by your bank'
                                        ));
                                    }
                                } else {
                                    $response = array('status' => 501, 'error' => 'Payment using card failed');
                                }

                            }
                        }
                    } else {
                        $response = array(
                            'status' => 501,
                            'error' => 'Maximum total card deposits per day is Ksh 4,000. You can deposit up to Ksh ' . number_format($this->checkDailyCardLimit($userId), 2) . ' today'
                        );
                    }
                } else {
                    $response = array('status' => 501, 'error' => 'Amount must be atleast $1');
                }
            } else {
                $response = array(
                    'status' => 501,
                    'error' => 'For security purposes, you are required to use this card to only deposit to Akiba account for at least 3 months after which you will be able to deposit to Homepesa wallet, make savings and repay facility among many other options. Go to Akiba account option to know more about it'
                );
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

    public function buy_airtime(Request $request)
    {
        $phone = $request->get('phone');
        $amount = $request->get('amount');
        $user_id = $request->get('user_id');
        $operator = $request->get('operator');
        if (isset($user_id) && isset($phone) && isset($amount)) {
            $formatPhoneVals = json_decode($this->phoneFormat($phone), true);
            if ($formatPhoneVals['status'] == true) {
                $phone = $formatPhoneVals['formatedPhone'];

                if (is_numeric($amount) && $amount >= 10) {
                    //check if user has enough amount on withdrawable account
                    $withAmount = $this->getWithdrawableAmount($user_id);
                    if ($withAmount >= $amount) {
                        //handle operation locking - only one withdrawable operation can take place at a time
                        $lockFile = '/var/www/html/dibwaves/admin/attachments/' . $user_id . '.txt';
                        if (!file_exists($lockFile)) {
                            //create lock file
                            $handle = fopen($lockFile, 'a');
                            fwrite($handle, 'operation underway');
                            fclose($handle);

                            $now = date('Y-m-d H:i:s');

                            $result = $this->buyAirtime($operator, $phone, $amount);
                            $resultVals = json_decode($result, true);

//							dd( $result );
                            //results
                            $status = htmlspecialchars($resultVals['status'], ENT_QUOTES);
                            $discount = htmlspecialchars($resultVals['discount'], ENT_QUOTES);
                            $requestId = htmlspecialchars($resultVals['requestId'], ENT_QUOTES);
                            $errorMessage = htmlspecialchars($resultVals['errorMessage'], ENT_QUOTES);

                            if (strtolower($status) == 'success' || strtolower($status) == 'sent') {
                                $discountVals = explode(' ', $discount);
                                $discount = trim($discountVals[1]);
                            } else {
                                $discount = 0;
                            }

                            //insert into table
                            if (!DB::insert("INSERT INTO airtime (operator,amount,discount,phone,transactiontime,status,requestId,errormessage,userId) VALUES('$operator',$amount,$discount,'$phone','$now',$status,$requestId,'$errorMessage',$user_id)")) {
                                $response = array('status' => 501, 'error' => 'database error');
                            } elseif (strtolower($status) == 'success' || strtolower($status) == 'sent') {
                                //deduct from user account
                                if (!DB::insert("INSERT INTO withdrawals(amount,withdrawaldate,purpose,description,userId) VALUES($amount,'$now',2,'Airtime purchase',$user_id)")) {
                                    $response = json_encode(array('status' => 501, 'error' => 'database error'));
                                } else {
                                    $this->recordCommision(1, $user_id, $discount, 0);//record commision for report and tax purposes
                                    $response = array(
                                        'status' => 200,
                                        'msg' => 'Airtime request has been successfully sent. Please wait for response. Please note that you cannot buy airtime to the same number within 5 minutes.'
                                    );
                                }
                            } else {
                                $response = array(
                                    'status' => 501,
                                    'error' => 'Airtime purchase failed. Please try again later.'
                                );
                            }

                            //delete lock file
                            unlink($lockFile);
                        } else {
                            $response = array(
                                'status' => 501,
                                'error' => 'Another operation is being performed on this account. Please try again later.'
                            );
                        }
                    } else {
                        $response = array(
                            'status' => 501,
                            'error' => 'You have insufficient balance. Your account balance is Ksh ' . number_format($withAmount, 2)
                        );
                    }
                } else {
                    $response = array(
                        'status' => 501,
                        'error' => 'Enter a valid amount. Amount must be at least Ksh 10.'
                    );
                }
            } else {
                $response = array('status' => 501, 'error' => 'Enter a valid phone number');
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

    public function transfer_to_other(Request $request)
    {
        $idpass = $request->get('idpass');
        $amount = $request->get('amount');
        $description = $request->get('description');
        $userId = $request->get('userId');

        if (isset($_POST['idpass']) && isset($_POST['amount']) && isset($_POST['description']) && isset($_POST['userId'])) {
            $idpass = htmlspecialchars($_POST['idpass'], ENT_QUOTES);
            $amount = htmlspecialchars($_POST['amount'], ENT_QUOTES);
            $description = htmlspecialchars($_POST['description'], ENT_QUOTES);
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);

            $txnId = 0;

            if (is_numeric($amount)) {
                //check if id/pass exists
                if (!$check = mysqli_query($this->link, "SELECT * FROM users WHERE idpass='$idpass'")) {
                    $response = json_encode(array('status' => 501, 'error' => 'database error'));
                } else {
                    if (count($check) == 0)//no id
                    {
                        $response = "0~The national id/passport does not exist";
                    } else//check id id does not belong to this user
                    {
                        $checkrow = mysqli_fetch_array($check, MYSQLI_ASSOC);
                        $toId = $checkrow['userId'];

                        if ($toId == $userId) {
                            $response = "0~You cannot transfer to your own account.";
                        } else {
                            $charge = 50;//our profit

                            //check if user has enough amount on withdrawable account
                            $withAmount = getWithdrawableAmount($userId);
                            if ($withAmount >= ($amount + $charge)) {
                                //insert into transfers
                                date_default_timezone_set('Africa/Nairobi');
                                $now = date('Y-m-d H:i:s');

                                $approvalCode = strtoupper(substr(md5(rand()), 0, 7));

                                if (!mysqli_query($this->link, "INSERT INTO transfers(fromId,toId,idpass,amount,transfertime,approvalstatus,approvalcode,remarks,charge) VALUES($userId,$toId,'$idpass',$amount,'$now',0,'$approvalCode','$description',$charge)")) {
                                    $response = json_encode(array(
                                        'status' => 501,
                                        'error' => 'query execution error'
                                    ));
                                } else {
                                    $txnId = mysqli_insert_id($this->link);

                                    //send sms
                                    $phoneNo = getUserRegisteredPhone($userId);

                                    if (strlen($phoneNo) >= 9) {
                                        $message = "Your transfer code is $approvalCode";
                                        $smsResult = $this->sendMobileSasa($phoneNo, $message, "HOMEPESA");

                                        if ($smsResult == 1)//success
                                        {
                                            $response = "1~" . $txnId . "~" . getUserName($toId);
                                        } else {
                                            $response = "0~SMS with activation code cannot be sent at the moment. Please try again later.";
                                        }
                                    } else {
                                        $response = "0~Your phone number was not properly captured during registration. Contact customer care for more guidance";
                                    }
                                }
                            } else {
                                $response = "0~You have insufficient balance. Your account balance is Ksh " . number_format($withAmount, 2);
                            }
                        }
                    }
                }

            } else {
                $response = "0~Enter a valid amount";
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

//  36
    public function confirm_mpesa_withdrawal(Request $request)
    {
        $userId = $request->get('userId');
        $transactionid = $request->get('transactionid');
        $confirmationcode = $request->get('confirmationcode');

        if (isset($userId) && isset($transactionid) && isset($confirmationcode)) {

            //check if code is still valid for transaction
            if (!$check = mysqli_query($this->link, "SELECT * FROM mpesawithdrawals WHERE transactionId=$transactionid AND authorizationcode='$confirmationcode' AND userId=$userId AND status=0")) {
                $response = "0~Transfer error " . mysqli_error($this->link);
            } else {
                $checkrow = mysqli_fetch_array($check, MYSQLI_ASSOC);
                if (count($check) == 0) {
                    $response = "0~This code seems to have expired";
                } else {
                    date_default_timezone_set('Africa/Nairobi');
                    $time = date('Y-m-d H:i:s');
                    //change approvar status
                    if (!mysqli_query($this->link, "UPDATE mpesawithdrawals SET status=1,transactiondescription='Confirmed',timeconfirmed='$time' WHERE transactionId=$transactionid")) {
                        $response = "0~Update error " . mysqli_error($this->link);
                    } else {
                        //get user's phone, email and name
                        if (!$usr = mysqli_query($this->link, "SELECT * FROM users WHERE id=$userId")) {
                            $response = "0~Transfer error1 " . mysqli_error($this->link);
                        } else {
                            $usrrow = mysqli_fetch_array($usr, MYSQLI_ASSOC);
                            $name = $usrrow['fullname'];
                            $phone = $usrrow['phone'];
                            $email = $usrrow['email'];

                            $processVals = json_decode($this->processMpesaWithdrawal($transactionid, $phone, $email, $name, $userId), true);

                            if ($processVals['status'] === 1)//send cash
                            {
                                $resultVals = json_decode($this->sendMpesaCash($userId, $transid, $checkrow['amount'], $checkrow['phone']), true);

                                if ($resultVals['status'] == 0 || count($resultVals) == 0)//failed
                                {
                                    $commision = -($processVals['commision']);
                                    $otherCharges = -($processVals['otherCharges']);
                                    $amount = -($processVals['amount']);
                                    $totalDeductions = $commision + $otherCharges;

                                    date_default_timezone_set('Africa/Nairobi');
                                    $time = date('Y-m-d H:i:s');
                                    //reverse withdrawal transaction
                                    if (!mysqli_query($this->link, "INSERT INTO withdrawals(amount,withdrawaldate,purpose,description,userId) VALUES($amount,'$time',7,'Reversal',$userId),($totalDeductions,'$time',7,'Reversal',$userId)")) {
                                        $response = "0~Error2 " . mysqli_error($this->link);
                                    }

                                    //reverse commision
                                    $this->recordCommision(3, $userId, $commision, $otherCharges);

                                    //alert user
                                    $memberPhone = $this->getUserRegisteredPhone($userId);
                                    $message = "Your Mpesa withdrawal of Ksh " . number_format($processVals['amount'], 2) . " failed and has been reversed. Please try again after a few minutes";
                                    $this->sendMobileSasa($memberPhone, $message);

                                }
                            }
                            $response = "1~" . $processVals['description'];
                        }
                    }
                }
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

//  37
    public function bank_withdrawal_request(Request $request)
    {
        $userId = $request->get('userId');
        $amount = $request->get('amount');
        $description = $request->get('description');
        $bankname = $request->get('bankname');
        $branch = $request->get('branch');
        $acname = $request->get('acname');
        $acno = $request->get('acno');

        if (isset($_POST['userId']) && isset($_POST['amount']) && isset($_POST['description']) && isset($_POST['bankname']) && isset($_POST['branch']) && isset($_POST['acname']) && isset($_POST['acno'])) {
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $amount = htmlspecialchars($_POST['amount'], ENT_QUOTES);
            $description = htmlspecialchars($_POST['description'], ENT_QUOTES);
            $bankname = htmlspecialchars($_POST['bankname'], ENT_QUOTES);
            $branch = htmlspecialchars($_POST['branch'], ENT_QUOTES);
            $acname = htmlspecialchars($_POST['acname'], ENT_QUOTES);
            $acno = htmlspecialchars($_POST['acno'], ENT_QUOTES);

            $bankTxnId = 0;

            if ($amount >= 1000) {
                //check if user has enough funds
                $withAmount = getWithdrawableAmount($userId);

                $charge = getBankCharges();

                if ($withAmount >= ($amount + $charge)) {
                    //generate random code
                    $approvalCode = strtoupper(substr(md5(rand()), 0, 7));

                    //insert record in table and send sms
                    if (!mysqli_query($this->link, "INSERT INTO bankwithdrawals(amount,description,bankname,bankbranch,accountname,accountno,status,approvalcode,transactiontime,timeconfirmed,timesent,charges,transactiondescription,userId) VALUES($amount,'$description','$bankname','$branch','$acname','$acno',0,'$approvalCode','$now','0000-00-00 00:00:00','0000-00-00 00:00:00',$charge,'Created',$userId)")) {
                        $response = json_encode(array('status' => 501, 'error' => 'database error'));
                    } else {
                        $bankTxnId = mysqli_insert_id($this->link);

                        //send sms
                        $phoneNo = getUserRegisteredPhone($userId);

                        if (strlen($phoneNo) >= 9) {
                            $message = "Your bank transfer code is $approvalCode";
                            $smsResult = $this->sendMobileSasa($phoneNo, $message, "HOMEPESA");

                            if ($smsResult == 1)//success
                            {
                                $response = "1~" . $bankTxnId;
                            } else {
                                $response = "0~There was a problem sending the activation code";
                            }
                        } else {
                            $response = "0~Your phone is not properly registered. Please contact customer care";
                        }
                    }
                } else {
                    $response = "0~You have insufficient balance. A transaction charge of Ksh " . ($charge) . " is required";
                }
            } else {
                $response = "0~Amount must be atleast Ksh 1000";
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

//	38
    public function confirm_bank_withdrawal(Request $request)
    {
        $userId = $request->get('userId');
        $transactionID = $request->get('transactionid');
        $confirmatioCode = $request->get('confirmationcode');
        if (isset($_POST['userId']) && isset($_POST['transactionid']) && isset($_POST['confirmationcode'])) {
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $transid = htmlspecialchars($_POST['transactionid'], ENT_QUOTES);
            $concode = htmlspecialchars($_POST['confirmationcode'], ENT_QUOTES);

            //check if code is still valid for transaction
            if (!$check = mysqli_query($this->link, "SELECT * FROM bankwithdrawals WHERE withdrawalId=$transid AND approvalcode='$concode' AND userId=$userId AND status=0")) {
                $response = "0~Transfer error " . mysqli_error($this->link);
            } else {
                if (count($check) == 0) {
                    $response = "0~This code seems to have expired.";
                } else {
                    //change approvar status
                    if (!mysqli_query($this->link, "UPDATE bankwithdrawals SET status=1,transactiondescription='Confirmed',timeconfirmed='$now' WHERE withdrawalId=$transid")) {
                        $response = "0~Update error " . mysqli_error($this->link);
                    } else {
                        $response = "1~Your withdraw request is being processed.";
                    }
                }
            }

        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

//	39
    public function cheque_request(Request $request)
    {
        $userId = $request->get('userId');
        $amount = $request->get('amount');
        $description = $request->get('description');
        $bankname = $request->get('bankname');
        $branch = $request->get('branch');
        $acname = $request->get('acname');
        $acno = $request->get('acno');

        if (isset($userId) && isset($amount) && isset($description) && isset($bankname) && isset($branch) && isset($acname) && isset($acno)) {
            $chequeTxnId = 0;

            //amount should be greater than 1000
            if ($amount < 1000) {
                $response = "0~For cheques, amount must be at least Ksh 1000.00";
            } else {
                //check if user has enough funds
                $withAmount = $this->getWithdrawableAmount($userId);

                $charge = $this->getChequeCharges();

                if ($withAmount >= ($amount + $charge)) {
                    //generate random code
                    $approvalCode = strtoupper(substr(md5(rand()), 0, 7));

                    if (!$check = mysqli_query($this->link, "SELECT * FROM chequewithdrawals WHERE withdrawalId=$tid")) {
                        $response = json_encode(array('status' => 501, 'error' => 'database error'));
                    } else {
                        if (count($check) == 0) {
                            //insert record in table and send sms
                            if (!mysqli_query($this->link, "INSERT INTO chequewithdrawals(amount,description,bankname,bankbranch,accountname,accountno,status,approvalcode,transactiontime,timeconfirmed,timesent,charges,transactiondescription,userId) VALUES($amount,'$description','$bankname','$branch','$acname','$acno',0,'$approvalCode','$now','0000-00-00 00:00:00','0000-00-00 00:00:00',$charge,'Created',$userId)")) {
                                $response = json_encode(array('status' => 501, 'error' => 'database error'));
                            } else {
                                $chequeTxnId = mysqli_insert_id($this->link);

                                //send sms
                                $phoneNo = $this->getUserRegisteredPhone($userId);

                                if (strlen($phoneNo) >= 9) {
                                    $message = "Your bank transfer code is $approvalCode";
                                    $smsResult = $this->sendMobileSasa($phoneNo, $message, "HOMEPESA");

                                    if ($smsResult == 1)//success
                                    {
                                        $response = "1~" . $chequeTxnId;
                                    } else {
                                        $response = "0~There was a problem sending the activation code";
                                    }
                                } else {
                                    $response = "0~Your phone is not properly registered. Please contact customer care";
                                }
                            }
                        }
                    }
                } else {
                    $response = "0~You have insufficient balance. A transaction charge of Ksh " . ($charge) . " is required";
                }
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

//	40
    public function get_users_eligibility_for_loan(Request $request)
    {
        $userId = $request->get('userId');
        $loantype = $request->get('loantype');
        $security = $request->get('security');
        $amount = $request->get('amount');

        if (isset($userId) && isset($loantype) && isset($security) && isset($amount)) {
            $loanLimit = $this->getFacilityLimit($loantype);
            $minLoanAmount = $this->getMinimumLoanAmount($loantype);
            $memberSavings = $this->getMemberSavings($userId);
            $outstandingLoanBalance = $this->getLoanBalance($userId);
            $blackList = $this->checkBlacklist($userId);

            $maxPeriod = $this->getLoanRepaymentPeriod($loantype);
            $loanTypenm = $this->getLoanType($loantype);

            $guarantableAmount = $this->getGuarantableAmount($userId);
//			SECURITY 1 IS SAVINGS: 2. IS GUARANTORS  3. secured by title deed/car log book
            if ($security == 1)//secured by my savings
            {
                $accessibleLoan = $guarantableAmount - $outstandingLoanBalance;
                $accessibleLoan += $this->getInstantLoanLimit($userId);

                $savings = 0;

                if ($amount <= $loanLimit && $amount >= $minLoanAmount) {
                    if ($amount >= 10) {
                        if ($accessibleLoan >= $amount)//some more loan available
                        {
                            //get user savings
                            if (!$mySavings = mysqli_query($this->link, "SELECT SUM(amount) AS yote FROM savings WHERE userId=$userId")) {
//								$response = "0~Query error " . mysqli_error( $this->link );
                                $response = array('status' => 501, 'error' => 'There was an error adding up savings');
                            } else {
                                $mySavingsrow = mysqli_fetch_array($mySavings, MYSQLI_ASSOC);
                                $savings = isset($mySavingsrow['yote']) ? $mySavingsrow['yote'] : 0;

                                if ($amount <= ($savings + $this->getInstantLoanLimit($userId))) {
                                    if ($blackList == false) {
                                        $response = array(
                                            'status' => 200,
                                            'msg' => 'continue?',
                                            'amount' => number_format($amount, 2),
                                            'loan_type' => $loanTypenm
                                        );
                                    } else {
                                        $response = array(
                                            'status' => 502,
                                            'info' => 'You cannot access a finance due to a default in a previous loan.'
                                        );
                                    }
                                } else {
                                    $response = array(
                                        'status' => 502,
                                        'info' => "Your current loan limit is Ksh " . number_format(($savings + $this->getInstantLoanLimit($userId)), 2)
                                    );
                                }
                            }
                        } else {
                            $response = array(
                                'status' => 502,
                                'info' => "You can access at most Ksh " . number_format($accessibleLoan, 2) . ". Increase your savings or pay any outstanding finances to increase your limit."
                            );
                        }
                    } else {
                        $response = array(
                            'status' => 502,
                            'info' => 'Finance amount must be at least Ksh 10.'
                        );
                    }
                } else {
                    $response = array(
                        'status' => 502,
                        'info' => "$loanTypenm amount must be at least Ksh " . number_format($minLoanAmount, 2) . " and at most Ksh " . number_format($loanLimit, 2)
                    );
                }
            } elseif ($security == 2) {
                if ($amount > $this->getSavingsBalance($userId)) {
                    if ($amount <= $loanLimit && $amount >= $minLoanAmount) {
                        if ($amount >= 10) {
                            //check if amount is less than 3 times of member's savings
                            $maxLimit = ($memberSavings * 3);
                            if ($amount <= $maxLimit) {
                                //check if member is seven days old since activation
                                if ($this->getMembershipDuration($userId) >= 7) {
                                    if ($outstandingLoanBalance <= 0)//no loan balance
                                    {
                                        if ($blackList == false)//not blacklisted
                                        {
                                            $response = array(
                                                'status' => 200,
                                                'msg' => "Apply for an $loanTypenm finance of Ksh " . number_format($amount, 2) . " for a maximum period of $maxPeriod months?",
                                                'loan_type' => $loanTypenm,
                                                'period' => $maxPeriod

                                            );
                                        } else {
                                            $response = array(
                                                'status' => 502,
                                                'info' => 'You cannot access a finance due to a default in a previous finance.'
                                            );
                                        }
                                    } else {
                                        $response = array(
                                            'status' => 502,
                                            'info' => "You have an outstanding finance balance of Ksh " . number_format($outstandingLoanBalance, 2)
                                                . ". Please pay the amount to continue. Thank you."
                                        );
                                    }
                                } else {
                                    $response = array(
                                        'status' => 502,
                                        'info' => 'Your account must have been active for at least 7 days.'
                                    );
                                }
                            } else {
                                $response = array(
                                    'status' => 502,
                                    'info' => "Your maximum finance limit is Ksh " . number_format($maxLimit, 2)
                                );
                            }
                        } else {
                            $response = array(
                                'status' => 502,
                                'info' => 'Finance amount must be at least Ksh 10.'
                            );
                        }
                    } else {
                        $response = array(
                            'status' => 502,
                            'info' => "$loanTypenm amount must be at least Ksh " . number_format($minLoanAmount, 2) . " and at most Ksh " . number_format($loanLimit, 2)
                        );
                    }
                } else {
                    $response = array(
                        'status' => 502,
                        'info' => 'This finance can be secured by your savings. Kindly select your savings as security.'
                    );
                }
            } elseif ($security == 3) {
                if ($amount > $this->getSavingsBalance($userId)) {
                    if ($amount <= $loanLimit && $amount >= $minLoanAmount) {
                        if ($amount >= 10) {
                            //check if amount is less than 3 times of member's savings
                            $maxLimit = ($memberSavings * 3);
                            if ($amount <= $maxLimit) {
                                //check if member is three months since activation
                                if ($this->getMembershipDuration($userId) >= 7) {
                                    if ($outstandingLoanBalance <= 0)//no loan balance
                                    {
                                        if ($blackList == false)//not blacklisted
                                        {
                                            $response = array(
                                                'status' => 502,
                                                'info' => "Apply for an $loanTypenm finance of Ksh " . number_format($amount, 2) . " for a maximum period of $maxPeriod months?"
                                            );
                                        }
                                    } else {
                                        $response = array(
                                            'status' => 502,
                                            'info' => "You have an outstanding finance balance of Ksh " . number_format($outstandingLoanBalance, 2) . ". Please pay the amount to continue. Thank you."
                                        );
                                    }
                                } else {
                                    $response = array(
                                        'status' => 502,
                                        'info' => 'Your account must have been active for atleast 7 days'
                                    );
                                }
                            } else {
                                $response = array(
                                    'status' => 502,
                                    'info' => "Your maximum loan limit is Ksh " . number_format($maxLimit, 2)
                                );
                            }
                        } else {
                            $response = array(
                                'status' => 502,
                                'info' => 'Finance amount must be at least Ksh 10'
                            );
                        }
                    } else {
                        $response = array(
                            'status' => 502,
                            'info' => $loanTypenm . " amount must be at least Ksh " . number_format($minLoanAmount, 2) . " and at most Ksh " . number_format($loanLimit, 2)
                        );
                    }
                } else {
                    $response = array(
                        'status' => 502,
                        'info' => 'This finance can be secured by your savings. Kindly select your savings as security.'
                    );
                }
            }
        } else {
            $response = array(
                'status' => 501,
                'error' => 'There are missing parameters in your finance request.'
            );
        }
        echo json_encode($response);
    }

//	41
    public function get_users_eligibility_for_loan_and_apply(Request $request)
    {
        $userId = $request->get('userId');
        $loantype = $request->get('loantype');
        $security = $request->get('security');
        $amount = $request->get('amount');

        if (isset($userId) && isset($loantype) && isset($security) && isset($amount)) {

//			$period                 = $this->getLoanRepaymentPeriodBasedOnAmount( $amount );
            $period = $this->getRepaymentPeriodBasedOnLoanType($loantype, 'hours');
            $loanLimit = $this->getFacilityLimit($loantype);
            $minLoanAmount = $this->getMinimumLoanAmount($loantype);
            $memberSavings = $this->getMemberSavings($userId);
            $outstandingLoanBalance = $this->getLoanBalance($userId);
            $blackList = $this->checkBlacklist($userId);
            date_default_timezone_set('Africa/Nairobi');
            $time = date('Y-m-d H:i:s');
            if ($this->getUserCurrentActiveLoans($userId) <= 0) {
                $loanDueDate = date('Y-m-d H:i:s', strtotime($time) + ($period));

                //save loan application
                if (!mysqli_query($this->link, "INSERT INTO loanapplications(loantype,securitytype,amount,dateapplied,approvalflag,userId,period) VALUES ($loantype,$security,$amount,'$time',0,$userId,$period)")) {
                    echo json_encode(array('status' => 501, 'error' => 'query execution error'));
                }

                $lnId = mysqli_insert_id($this->link);

                $guarantableAmount = $this->getGuarantableAmount($userId);

                if ($security == 1)//secured by my savings
                {
                    //get the amount of loan that the member can access. Should be less any amount guaranteed and loan balance
                    $accessibleLoan = ($guarantableAmount - $outstandingLoanBalance) > 0 ? ($guarantableAmount - $outstandingLoanBalance) : 0;
                    $accessibleLoan += $this->getInstantLoanLimit($userId);
                    $savings = 0;

                    if ($amount <= $loanLimit && $amount >= $minLoanAmount) {
                        if ($amount >= 100) {
                            if ($accessibleLoan >= $amount) {
                                //get user savings
                                if (!$mySavings = mysqli_query($this->link, "SELECT SUM(amount) AS yote FROM savings WHERE userId=$userId")) {
                                    $response = "0~Query error " . mysqli_error($this->link);
                                } else {
                                    $mySavingsrow = mysqli_fetch_array($mySavings, MYSQLI_ASSOC);
                                    $savings = isset($mySavingsrow['yote']) ? $mySavingsrow['yote'] : 0;

                                    if ($amount <= ($savings + $this->getInstantLoanLimit($userId))) {
                                        //process loan
                                        $loanLockfile = '/var/www/html/dibwaves/docs/loanapplication' . $userId . '.txt';
                                        if (!file_exists($loanLockfile))//only process if there's no other operation underway
                                        {
                                            if ($blackList == false) {
                                                //write lock file
                                                $handle = fopen($loanLockfile, 'w');
                                                fwrite($handle, 'Finance processing');
                                                fclose($handle);
                                                /*
													interestflag
													0: not interest
													1: interest
													2: loan transfer fee
												*/
                                                $loanInterestVals = json_decode($this->getLoanInterest($loantype, $amount, $period), true);
                                                $loanInterest = $loanInterestVals['interest'];
                                                $monthlyrepayment = $loanInterestVals['monthlyrepayment'];

                                                //update loan application as approved
                                                if (!mysqli_query($this->link, "UPDATE loanapplications SET approvalflag=1, period=$period WHERE applicationid=$lnId")) {
                                                    echo json_encode(array(
                                                        'status' => 501,
                                                        'error' => 'query execution error'
                                                    ));
                                                }
                                                date_default_timezone_set('Africa/Nairobi');
                                                $time = date('Y-m-d H:i:s');

                                                if (!mysqli_query($this->link, "INSERT INTO loans(loantype,amount,applidescription,dateapplied,dateapproved,userId,approvedflag,interestflag,period,monthlyrepayment,loanapplicationId,correspondingloanId,defaultstatus,defaultdate,useralert,guarantoralert) VALUES($loantype,$amount,'Instant Finance','$time','$time',$userId,1,0,$period,$monthlyrepayment,0,0,0,'$loanDueDate',0,0)")) {
                                                    $response = "0~Query error " . mysqli_error($this->link);
                                                } else {
                                                    $loadId = mysqli_insert_id($this->link);

                                                    //insert interest for the first month

                                                    $extraMsg = "";
                                                    if ($monthlyrepayment > 0) {
                                                        $extraMsg = "Your monthly repayment is Ksh " . number_format($monthlyrepayment, 2) . ".";
                                                    }

                                                    /*TOTAL FOR LAST PROFIT AND LAST ASSET*/
                                                    // total asset
                                                    if (!$totalAssetResult = mysqli_query($this->link, "SELECT total_asset FROM user_loans ORDER BY total_asset DESC LIMIT 1")) {

                                                    } else {
                                                        $assetsrow = mysqli_fetch_array($totalAssetResult, MYSQLI_ASSOC);
                                                        $total_assets = $assetsrow['total_asset'];
                                                    }
                                                    // total profit
                                                    if (!$totalProfitResult = mysqli_query($this->link, "SELECT total_profit FROM user_loans ORDER BY total_profit DESC LIMIT 1")) {

                                                    } else {
                                                        $profitrow = mysqli_fetch_array($totalProfitResult, MYSQLI_ASSOC);
                                                        $total_profit = $profitrow['total_profit'];
                                                    }
                                                    $userDetails = $this->get_user_details_for_loan($userId);
                                                    $overDueStartDate = $loanDueDate;
                                                    $dateOfNonPerformance = date('Y-m-d H:i:s', strtotime($loanDueDate) + (60));

                                                    $total_profit += $loanInterest;
                                                    $total_assets += $amount;
                                                    if (!mysqli_query($this->link, "INSERT INTO user_loans(loan_id,customer_id,phone_number,user_id_number,finance_account_balance, total_asset, total_profit,  principle_and_profit,customer_type,applicant_name,address,dob,branch_name,product_description,current_profit_rate,book_date,maturity_date,amount_financed,current_principle,principle_paid,overdue_principle,overdue_profit,profit_paid,total_outstanding,number_of_installments,provisions_held,installments_amount,overdue_start_date,overdue_days,date_of_none_performance,credit_grade) 
															VALUES($loadId, $userDetails->customer_id,'$userDetails->phone_number','$userDetails->user_id_number',$amount,$total_assets,$total_profit,$amount,1,'$userDetails->applicant_name','$userDetails->address','$userDetails->dob','Upperhill','Instant Finance',0.05,'$time','$loanDueDate',$amount,$amount,0,0,0,$loanInterest,$amount,6,0,0,'$overDueStartDate',0,'$dateOfNonPerformance',0)")) {
                                                        $response = "0~Query error " . mysqli_error($this->link);
                                                    } else {
                                                        //credit withdrawable a/c
                                                        /*
															deposit method
															1: Mpesa
															2: Credit Card
															3: Savings
															4: Loan
														*/

                                                        $amount -= $loanInterest;
                                                        if (!mysqli_query($this->link, "INSERT INTO deposits(amount,depositdate,depositmethod,userId) VALUES($amount,'$this->now',5,$userId)")) {
                                                            $response = "0~Query error " . mysqli_error($this->link);
                                                        } else {
                                                            //send sms to a/c owner
                                                            $smsMessage = "Dear " . $this->getUserName($userId) . ", your finance application of Ksh " . number_format($amount, 2) . " is successful." . $extraMsg . " Check your Bank Account balance. Your finance is due on $loanDueDate.";
                                                            $userPhone = $this->getUserRegisteredPhone($userId);
//															$this->sendMobileSasa( $userPhone, $smsMessage, '' );
//														$response = "1~Your finance application is successful. Ksh " . number_format( $amount, 2 ) . " has been credited to your Bank Account.~" . $loadId;
                                                            $response = array(
                                                                'status' => 200,
                                                                'msg' => 'Your finance application is successful. Ksh ' . number_format($amount, 2) . ' has been credited to your Bank Account.',
                                                                'loan_id' => $loadId
                                                            );
                                                        }
                                                    }
                                                }
                                                //delete lock file after processing
                                                unlink($loanLockfile);
                                            } else {
                                                $response = array(
                                                    'status' => 501,
                                                    'error' => 'You cannot access a finance due to a default in a previous finance.'
                                                );
                                            }
                                        } else {
                                            $response = array(
                                                'status' => 501,
                                                'error' => 'There\'s another finance application underway. Please try again later'
                                            );
                                        }
                                    } else {
                                        $response = array(
                                            'status' => 501,
                                            'error' => 'Your current finance limit is Ksh ' . number_format(($savings + $this->getInstantLoanLimit($userId)), 2)
                                        );
                                    }
                                }
                            } else {
                                $response = array(
                                    'status' => 501,
                                    'error' => 'You can access at most Ksh ' . number_format($accessibleLoan, 2) . '. Increase your savings or pay any outstanding finances to increase your limit.'
                                );
                            }
                        } else {
                            $response = array('status' => 501, 'error' => 'Finance amount must be at least Ksh 100');
                        }
                    } else {
                        $response = array(
                            'status' => 501,
                            'error' => 'Finance amount must be at least Ksh ' . number_format($minLoanAmount, 2) . ' and at most Ksh ' . number_format($loanLimit, 2)
                        );
                    }
                } elseif ($security == 2)//secured by guarantors
                {
                    if ($amount > $this->getSavingsBalance($userId)) {
                        if ($amount <= $loanLimit && $amount >= $minLoanAmount) {
                            if ($amount >= 100) {
                                //check if amount is less than 3 times of member's savings
                                $maxLimit = ($memberSavings * 3);
                                if ($amount <= $maxLimit) {
                                    //check if member is three months since activation
                                    if ($this->getMembershipDuration($userId) >= 7) {
                                        if ($outstandingLoanBalance <= 0)//no loan balance
                                        {
                                            if ($blackList == false)//not blacklisted
                                            {
                                                $response = array(
                                                    'status' => 200,
                                                    'loan_id' => $lnId,
                                                    'msg' => 'Your finance application of Ksh ' . number_format($amount, 2) . ' has been received. Proceed to add guarantors.'
                                                );
                                            } else {
                                                $response = array(
                                                    'status' => 501,
                                                    'error' => 'You cannot access a finance due to a default in a previous finance.'
                                                );
                                            }
                                        } else {
                                            $response = array(
                                                'status' => 501,
                                                'error' => 'You have an outstanding finance balance of Ksh ' . number_format($outstandingLoanBalance, 2) . '. Please pay the amount to continue. Thank you.'
                                            );
                                        }
                                    } else {
                                        $response = array(
                                            'status' => 501,
                                            'error' => 'Your account must have been active for atleast 7 days'
                                        );
                                    }
                                } else {
                                    $response = array(
                                        'status' => 501,
                                        'error' => 'Your maximum finance limit is Ksh ' . number_format($maxLimit, 2)
                                    );
                                }
                            } else {
                                $response = array(
                                    'status' => 501,
                                    'error' => 'Finance amount must be at least Ksh 100'
                                );
                            }
                        } else {
                            $response = array(
                                'status' => 501,
                                'error' => 'Finance amount must be at least Ksh ' . number_format($minLoanAmount, 2) . ' and at most Ksh ' . number_format($loanLimit, 2)
                            );
                        }
                    } else {
                        $response = array(
                            'status' => 501,
                            'error' => 'This finance can be secured by your savings. Kindly select your savings as security.'
                        );
                    }
                } elseif ($security == 3)//secured by title deed/car log book
                {
                    if ($amount > $this->getSavingsBalance($userId)) {
                        if ($amount <= $loanLimit && $amount >= $minLoanAmount) {
                            if ($amount >= 100) {
                                //check if amount is less than 3 times of member's savings
                                $maxLimit = ($memberSavings * 3);
                                if ($amount <= $maxLimit) {
                                    //check if member is three months since activation
                                    if ($this->getMembershipDuration($userId) >= 7) {
                                        if ($outstandingLoanBalance <= 0) {
                                            if ($blackList == false) {
                                                $response = "1~Your finance application of Ksh " . number_format($amount, 2) . " has been received. Kindly bring an original document of your security to our office at Skyview Park, Kilimani, Lenana Road (Nairobi).";
                                            } else {
                                                $response = "0~You cannot access a finance due to a default in a previous finance.";
                                            }
                                        } else {
                                            $response = "0~You have an outstanding finance balance of Ksh " . number_format($outstandingLoanBalance, 2) . ". Please pay the amount to continue. Thank you.";
                                        }
                                    } else {
                                        $response = "0~Your account must have been active for atleast 7 days";
                                    }
                                } else {
                                    $response = "0~Your maximum finance limit is Ksh " . number_format($maxLimit, 2);
                                }
                            } else {
                                $response = "0~Finance amount must be at least Ksh 100";
                            }
                        } else {
                            $response = "0~Finance amount must be at least Ksh " . number_format($minLoanAmount, 2) . " and at most Ksh " . number_format($loanLimit, 2);
                        }
                    } else {
                        $response = "0~This finance can be secured by your savings. Kindly select your savings as security.";
                    }
                }
            } else {
                $response = array(
                    'status' => 501,
                    'error' => 'Kindly pay your outstanding finance balance to access more finance.'
                );
            }
        } else {
            $response = "0~There are missing parameters in your finance request";
        }
        echo json_encode($response);
    }

//  41.6
    public function business_eligibility_for_loan(Request $request)
    {
        $userId = $request->get('userId');
        $loantype = $request->get('loantype');
        $security = $request->get('security');
        $amount = $request->get('amount');
        $bankAccountInvolved = $request->get('bank_checked');

        if (isset($userId) && isset($loantype) && isset($security) && isset($amount)) {
            $loanLimit = $this->getFacilityLimit($loantype);
            $minLoanAmount = $this->getMinimumLoanAmount($loantype);
            $memberSavings = $this->getMemberSavings($userId);
            $outstandingLoanBalance = $this->getLoanBalance($userId);
            $blackList = $this->checkBlacklist($userId);
            $memberSavings = $this->getMemberSavings($userId);
            $totalUserBusinessFunding = $this->total_business_funding($userId);
            $maxPeriod = $this->getLoanRepaymentPeriod($loantype);
            $loanTypenm = $this->getLoanType($loantype);

            if ($security == 1)//secured by my savings
            {
                $savings = 0;

                if ($amount <= $loanLimit && $amount >= $minLoanAmount) {
                    if ($amount >= 10) {
                        //get user savings
                        if (!$mySavings = mysqli_query($this->link, "SELECT SUM(amount) AS yote FROM savings WHERE userId=$userId")) {
//								$response = "0~Query error " . mysqli_error( $this->link );
                            $response = array('status' => 501, 'error' => 'There was an error adding up savings');
                        } else {
                            $mySavingsrow = mysqli_fetch_array($mySavings, MYSQLI_ASSOC);
                            $savings = isset($mySavingsrow['yote']) ? $mySavingsrow['yote'] : 0;

                            if ($bankAccountInvolved == 1) {
                                if ($amount > $memberSavings && $amount < $totalUserBusinessFunding) {
                                    $transferrableAmount = $amount - $memberSavings;
                                    $savings += $transferrableAmount;
                                }
                            }

                            if ($amount <= ($savings)) {
                                if ($blackList == false) {
                                    $response = array(
                                        'status' => 200,
                                        'msg' => 'continue?',
                                        'amount' => number_format($amount, 2),
                                        'loan_type' => $loanTypenm
                                    );
                                } else {
                                    $response = array(
                                        'status' => 502,
                                        'info' => 'You cannot access a finance due to a default in a previous loan.'
                                    );
                                }
                            } else {
                                $response = array(
                                    'status' => 502,
                                    'info' => "Your current loan limit is Ksh " . number_format(($savings + $this->getInstantLoanLimit($userId)), 2)
                                );
                            }
                        }

                    } else {
                        $response = array(
                            'status' => 502,
                            'info' => 'Finance amount must be at least Ksh 10.'
                        );
                    }
                } else {
                    $response = array(
                        'status' => 502,
                        'info' => "$loanTypenm amount must be at least Ksh " . number_format($minLoanAmount, 2) . " and at most Ksh " . number_format($loanLimit, 2)
                    );
                }
            }
        } else {
            $response = array(
                'status' => 501,
                'error' => 'There are missing parameters in your finance request.'
            );
        }
        echo json_encode($response);
    }

//	41.5
    public function business_eligibility_for_loan_and_apply(Request $request)
    {
        $userId = $request->get('userId');
        $loantype = $request->get('loantype');
        $amount = $request->get('amount');
        $bankAccountInvolved = $request->get('bank_checked');
        $savingsAccountInvolved = $request->get('savings_checked');

        if (isset($userId) && isset($loantype) && isset($amount)) {
            $period = $this->getRepaymentPeriodBasedOnLoanType($loantype, 'hours');
            $loanLimit = $this->getFacilityLimit($loantype);
            $minLoanAmount = $this->getMinimumLoanAmount($loantype);
            $memberSavings = $this->getMemberSavings($userId);
            $outstandingLoanBalance = $this->getLoanBalance($userId);
            $blackList = $this->checkBlacklist($userId);
            $totalUserBusinessFunding = $this->total_business_funding($userId);

            if ($this->getUserCurrentActiveLoans($userId) <= 0) {
                $loanDueDate = date('Y-m-d H:i:s', strtotime($this->now) + ($period));

                $maxRepaymentPeriod = $this->getLoanRepaymentPeriod($loantype);

                if ($bankAccountInvolved == 1) {
                    if ($amount > $memberSavings && $amount < $totalUserBusinessFunding) {
                        $transferrableAmount = $amount - $memberSavings;

                        if ($this->bank_savings_transaction($userId, $transferrableAmount, 2) == 1) {
                        } else {
                            die;
                        }
                    }
                }
                //save loan application
                if (!mysqli_query($this->link, "INSERT INTO loanapplications(loantype,securitytype,amount,dateapplied,approvalflag,userId,period) VALUES ($loantype,1,$amount,'$this->now',0,$userId,$period)")) {
                    echo json_encode(array('status' => 501, 'error' => 'query execution error'));
                }

                $lnId = mysqli_insert_id($this->link);
                $savings = 0;

                if ($amount <= $loanLimit && $amount >= $minLoanAmount) {

                    if (!$mySavings = mysqli_query($this->link, "SELECT SUM(amount) AS yote FROM savings WHERE userId=$userId")) {
                        $response = "0~Query error " . mysqli_error($this->link);
                    } else {
                        $mySavingsrow = mysqli_fetch_array($mySavings, MYSQLI_ASSOC);
                        $savings = isset($mySavingsrow['yote']) ? $mySavingsrow['yote'] : 0;

                        if ($amount <= ($savings + $this->getInstantLoanLimit($userId))) {
                            //process loan
                            $loanLockfile = '/var/www/html/dibwaves/docs/loanapplication' . $userId . '.txt';
                            if (!file_exists($loanLockfile))//only process if there's no other operation underway
                            {
                                if ($blackList == false) {
                                    //write lock file
                                    $handle = fopen($loanLockfile, 'w');
                                    fwrite($handle, 'Finance processing');
                                    fclose($handle);

                                    $loanInterestVals = json_decode($this->getLoanInterest($loantype, $amount, $period), true);
                                    $loanInterest = $loanInterestVals['interest'];
                                    $monthlyrepayment = $loanInterestVals['monthlyrepayment'];

                                    //update loan application as approved
                                    if (!mysqli_query($this->link, "UPDATE loanapplications SET approvalflag=1,period=$period WHERE applicationid=$lnId")) {
                                        echo json_encode(array(
                                            'status' => 501,
                                            'error' => 'query execution error'
                                        ));
                                    }

                                    if (!mysqli_query($this->link, "INSERT INTO loans(loantype,amount,applidescription,dateapplied,dateapproved,userId,approvedflag,interestflag,period,monthlyrepayment,loanapplicationId,correspondingloanId,defaultstatus,defaultdate,useralert,guarantoralert) 
															VALUES($loantype,$amount,'Business Financing','$this->now','$this->now',$userId,1,0,$period,$monthlyrepayment,0,0,0,'$loanDueDate',0,0)")) {
                                        $response = "0~Query error " . mysqli_error($this->link);
                                    } else {
                                        $loadId = mysqli_insert_id($this->link);

                                        //insert interest for the first month

                                        $extraMsg = "";
                                        if ($monthlyrepayment > 0) {
                                            $extraMsg = "Your monthly repayment is Ksh " . number_format($monthlyrepayment, 2) . ".";
                                        }

                                        $userDetails = $this->get_user_details_for_loan($userId);
                                        $overDueStartDate = $loanDueDate;
                                        $dateOfNonPerformance = date('Y-m-d H:i:s', strtotime($loanDueDate) + (60));
                                        /*TOTAL FOR LAST PROFIT AND LAST ASSET*/
                                        // total asset
                                        if (!$totalAssetResult = mysqli_query($this->link, "SELECT total_asset FROM user_loans ORDER BY total_asset DESC LIMIT 1")) {

                                        } else {
                                            $assetsrow = mysqli_fetch_array($totalAssetResult, MYSQLI_ASSOC);
                                            $total_assets = $assetsrow['total_asset'];
                                        }
                                        // total profit
                                        if (!$totalProfitResult = mysqli_query($this->link, "SELECT total_profit FROM user_loans ORDER BY total_profit DESC LIMIT 1")) {

                                        } else {
                                            $profitrow = mysqli_fetch_array($totalProfitResult, MYSQLI_ASSOC);
                                            $total_profit = $profitrow['total_profit'];
                                        }

                                        $total_profit += $loanInterest;
                                        $total_assets += $amount;

                                        if (!mysqli_query($this->link, "INSERT INTO user_loans(loan_id,customer_id,phone_number,user_id_number,finance_account_balance, total_asset, total_profit,principle_and_profit,customer_type,applicant_name,address,dob,branch_name,product_description,current_profit_rate,book_date,maturity_date,amount_financed,current_principle,principle_paid,overdue_principle,overdue_profit,profit_paid,total_outstanding,number_of_installments,provisions_held,installments_amount,overdue_start_date,overdue_days,date_of_none_performance,credit_grade) 
															VALUES($loadId, $userDetails->customer_id,'$userDetails->phone_number','$userDetails->user_id_number',$total_assets,$total_profit,$amount,$amount,1,'$userDetails->applicant_name','$userDetails->address','$userDetails->dob','Upperhill','Business Financing',0.05,'$this->now','$loanDueDate',$amount,$amount,0,0,0,$loanInterest,$amount,6,0,0,'$overDueStartDate',0,'$dateOfNonPerformance',0)")) {
                                            $response = "0~Query error " . mysqli_error($this->link);
                                        } else {
                                            //credit withdrawable a/c
                                            /*
												deposit method
												1: Mpesa
												2: Credit Card
												3: Savings
												4: Loan
											*/

                                            $amount -= $loanInterest;
                                            if (!mysqli_query($this->link, "INSERT INTO deposits(amount,depositdate,depositmethod,userId) VALUES($amount,'$this->now',5,$userId)")) {
                                                $response = "0~Query error " . mysqli_error($this->link);
                                            } else {
                                                //send sms to a/c owner
                                                $smsMessage = "Dear " . $this->getUserName($userId) . ", your finance application of Ksh " . number_format($amount, 2) . " is successful." . $extraMsg . " Check your Bank Account balance. Your finance is due on $loanDueDate.";
                                                $userPhone = $this->getUserRegisteredPhone($userId);
//															$this->sendMobileSasa( $userPhone, $smsMessage, '' );
//														$response = "1~Your finance application is successful. Ksh " . number_format( $amount, 2 ) . " has been credited to your Bank Account.~" . $loadId;
                                                $response = array(
                                                    'status' => 200,
                                                    'msg' => 'Your finance application is successful. Ksh ' . number_format($amount, 2) . ' has been credited to your Bank Account.',
                                                    'loan_id' => $loadId
                                                );
                                            }
                                        }
                                    }
                                    //delete lock file after processing
                                    unlink($loanLockfile);
                                } else {
                                    $response = array(
                                        'status' => 501,
                                        'error' => 'You cannot access a finance due to a default in a previous finance.'
                                    );
                                }
                            } else {
                                $response = array(
                                    'status' => 501,
                                    'error' => 'There\'s another finance application underway. Please try again later'
                                );
                            }
                        } else {
                            $response = array(
                                'status' => 501,
                                'error' => 'Your current finance limit is Ksh ' . number_format(($savings + $this->getInstantLoanLimit($userId)), 2)
                            );
                        }
                    }
                } else {
                    $response = array(
                        'status' => 501,
                        'error' => 'Finance amount must be at least Ksh ' . number_format($minLoanAmount, 2) . ' and at most Ksh ' . number_format($loanLimit, 2)
                    );
                }

            } else {
                $response = array(
                    'status' => 501,
                    'error' => 'Kindly pay your outstanding finance balance to access more finance.'
                );
            }
        } else {
            $response = "0~There are missing parameters in your finance request";
        }
        echo json_encode($response);
    }

//  41.6
    public function users_eligibility_for_school_fees_and_apply(Request $request)
    {
        $userId = $request->get('userId');
        $loantype = $request->get('loantype');
        $security = $request->get('security');
        $amount = $request->get('amount');
        $period = $request->get('period');

        if (isset($userId) && isset($loantype) && isset($security) && isset($amount)) {

            $period = $this->getRepaymentPeriodBasedOnLoanType($loantype, 'hours');
            $loanLimit = $this->getFacilityLimit($loantype);
            $minLoanAmount = $this->getMinimumLoanAmount($loantype);
            $memberSavings = $this->getMemberSavings($userId);
            $outstandingLoanBalance = $this->getLoanBalance($userId);
            $blackList = $this->checkBlacklist($userId);
            $school_fees_account_id = $this->getSchoolFeesFinancing($userId);

            if ($this->getUserCurrentActiveLoans($userId) <= 0) {
                $loanDueDate = date('Y-m-d H:i:s', strtotime($this->now) + ($period));

                $maxRepaymentPeriod = $this->getLoanRepaymentPeriod($loantype);

                //save loan application
                if (!mysqli_query($this->link, "INSERT INTO loanapplications(loantype,securitytype,amount,dateapplied,approvalflag,userId,period) VALUES ($loantype,$security,$amount,'$this->now',0,$userId,$period)")) {
                    echo json_encode(array('status' => 501, 'error' => 'query execution error'));
                }

                $lnId = mysqli_insert_id($this->link);

                $guarantableAmount = $this->getGuarantableAmount($userId);

                if ($security == 1)//secured by my savings
                {
                    //get the amount of loan that the member can access. Should be less any amount guaranteed and loan balance
                    $accessibleLoan = ($guarantableAmount - $outstandingLoanBalance) > 0 ? ($guarantableAmount - $outstandingLoanBalance) : 0;
                    $accessibleLoan += $this->getInstantLoanLimit($userId);
                    $savings = 0;

                    if ($amount <= $loanLimit && $amount >= $minLoanAmount) {
                        if ($amount >= 100) {
                            if ($accessibleLoan >= $amount) {
                                //get user savings
                                if (!$mySavings = mysqli_query($this->link, "SELECT SUM(amount) AS yote FROM savings WHERE userId=$userId")) {
                                    $response = "0~Query error " . mysqli_error($this->link);
                                } else {
                                    $mySavingsrow = mysqli_fetch_array($mySavings, MYSQLI_ASSOC);
                                    $savings = isset($mySavingsrow['yote']) ? $mySavingsrow['yote'] : 0;

                                    if ($amount <= ($savings + $this->getInstantLoanLimit($userId))) {
                                        //process loan
                                        $loanLockfile = '/var/www/html/dibwaves/docs/loanapplication' . $userId . '.txt';
                                        if (!file_exists($loanLockfile))//only process if there's no other operation underway
                                        {
                                            if ($blackList == false) {
                                                //write lock file
                                                $handle = fopen($loanLockfile, 'w');
                                                fwrite($handle, 'Finance processing');
                                                fclose($handle);

                                                //insert into loans
                                                /*
													approvedflag
													0: not approved
													1: approved
												*/

                                                /*
													interestflag
													0: not interest
													1: interest
													2: loan transfer fee
												*/

                                                $loanInterestVals = json_decode($this->getLoanInterest($loantype, $amount, $period), true);
                                                $loanInterest = $loanInterestVals['interest'];
                                                $monthlyrepayment = $loanInterestVals['monthlyrepayment'];

                                                //update loan application as approved
                                                if (!mysqli_query($this->link, "UPDATE loanapplications SET approvalflag=1,period=$period WHERE applicationid=$lnId")) {
                                                    echo json_encode(array(
                                                        'status' => 501,
                                                        'error' => 'Facility Application Error'
                                                    ));
                                                }

                                                /*TOTAL FOR LAST PROFIT AND LAST ASSET*/
                                                // total asset
                                                if (!$totalAssetResult = mysqli_query($this->link, "SELECT total_asset FROM user_loans ORDER BY total_asset DESC LIMIT 1")) {

                                                } else {
                                                    $assetsrow = mysqli_fetch_array($totalAssetResult, MYSQLI_ASSOC);
                                                    $total_assets = $assetsrow['total_asset'];
                                                }
                                                // total profit
                                                if (!$totalProfitResult = mysqli_query($this->link, "SELECT total_profit FROM user_loans ORDER BY total_profit DESC LIMIT 1")) {

                                                } else {
                                                    $profitrow = mysqli_fetch_array($totalProfitResult, MYSQLI_ASSOC);
                                                    $total_profit = $profitrow['total_profit'];
                                                }

                                                if (!mysqli_query($this->link, "INSERT INTO loans(loantype,amount,applidescription,dateapplied,dateapproved,userId,approvedflag,interestflag,period,monthlyrepayment,loanapplicationId,correspondingloanId,defaultstatus,defaultdate,useralert,guarantoralert) 
													VALUES($loantype,$amount,'School Fees','$this->now','$this->now',$userId,1,0,$period,$monthlyrepayment,0,0,0,'$loanDueDate',0,0)")) {
                                                    $response = "0~Query error " . mysqli_error($this->link);
                                                } else {
                                                    $loadId = mysqli_insert_id($this->link);

                                                    //insert interest for the first month
                                                    $extraMsg = "";
                                                    if ($monthlyrepayment > 0) {
                                                        $extraMsg = "Your monthly repayment is Ksh " . number_format($monthlyrepayment, 2) . ".";
                                                    }

                                                    $userDetails = $this->get_user_details_for_loan($userId);
                                                    $overDueStartDate = $loanDueDate;
                                                    $dateOfNonPerformance = date('Y-m-d H:i:s', strtotime($loanDueDate) + (60));

                                                    $total_profit += $loanInterest;
                                                    $total_assets += $amount;

                                                    if (!mysqli_query($this->link, "INSERT INTO user_loans(loan_id,customer_id,phone_number,user_id_number,finance_account_balance,total_asset,total_profit,principle_and_profit,customer_type,applicant_name,address,dob,branch_name,product_description,current_profit_rate,book_date,maturity_date,amount_financed,current_principle,principle_paid,overdue_principle,overdue_profit,profit_paid,total_outstanding,number_of_installments,provisions_held,installments_amount,overdue_start_date,overdue_days,date_of_none_performance,credit_grade) 
															VALUES($loadId, $userDetails->customer_id,'$userDetails->phone_number','$userDetails->user_id_number',$amount,$total_assets,$total_profit,$amount,1,'$userDetails->applicant_name','$userDetails->address','$userDetails->dob','Upperhill','School Fees',0.05,'$this->now','$this->now',$amount,$amount,0,0,0,$loanInterest,$amount,6,0,0,'$overDueStartDate',0,'$dateOfNonPerformance',0)")) {
                                                        $response = "0~Query error " . mysqli_error($this->link);
                                                    } else {
                                                        //credit withdrawable a/c
                                                        /*
															deposit method
															1: Mpesa
															2: Credit Card
															3: Savings
															4: Loan
														*/

                                                        $amount -= $loanInterest;
                                                        if (!mysqli_query($this->link, "INSERT INTO accounttransactions(accountId, drcrflag, amount, source_id, reference, txntime, savingsoption) VALUES ($school_fees_account_id, 1, $amount, 5, 'School Fees', '$this->now', 0)")) {
                                                            $response = "0~Query error " . mysqli_error($this->link);
                                                        } else {
                                                            //send sms to a/c owner
                                                            $smsMessage = "Dear " . $this->getUserName($userId) . ", your school fees financing application of Ksh " . number_format($amount, 2) . " is successful." . $extraMsg . " Check your Bank Account balance. Your finance is due on $loanDueDate.";
                                                            $userPhone = $this->getUserRegisteredPhone($userId);
//															$this->sendMobileSasa( $userPhone, $smsMessage, '' );
//														$response = "1~Your finance application is successful. Ksh " . number_format( $amount, 2 ) . " has been credited to your Bank Account.~" . $loadId;
                                                            $response = array(
                                                                'status' => 200,
                                                                'msg' => 'Your finance application is successful. Ksh ' . number_format($amount, 2) . ' has been credited to your School Account.',
                                                                'loan_id' => $loadId
                                                            );
                                                        }
                                                    }
                                                }
                                                //delete lock file after processing
                                                unlink($loanLockfile);
                                            } else {
                                                $response = array(
                                                    'status' => 501,
                                                    'error' => 'You cannot access a finance due to a default in a previous finance.'
                                                );
                                            }
                                        } else {
                                            $response = array(
                                                'status' => 501,
                                                'error' => 'There\'s another finance application underway. Please try again later'
                                            );
                                        }
                                    } else {
                                        $response = array(
                                            'status' => 501,
                                            'error' => 'Your current finance limit is Ksh ' . number_format(($savings + $this->getInstantLoanLimit($userId)), 2)
                                        );
                                    }
                                }
                            } else {
                                $response = array(
                                    'status' => 501,
                                    'error' => 'You can access at most Ksh ' . number_format($accessibleLoan, 2) . '. Increase your savings or pay any outstanding finances to increase your limit.'
                                );
                            }
                        } else {
                            $response = array('status' => 501, 'error' => 'Finance amount must be at least Ksh 100');
                        }
                    } else {
                        $response = array(
                            'status' => 501,
                            'error' => 'Finance amount must be at least Ksh ' . number_format($minLoanAmount, 2) . ' and at most Ksh ' . number_format($loanLimit, 2)
                        );
                    }
                } elseif ($security == 2)//secured by guarantors
                {
                    if ($amount > $this->getSavingsBalance($userId)) {
                        if ($amount <= $loanLimit && $amount >= $minLoanAmount) {
                            if ($amount >= 100) {
                                //check if amount is less than 3 times of member's savings
                                $maxLimit = ($memberSavings * 3);
                                if ($amount <= $maxLimit) {
                                    //check if member is three months since activation
                                    if ($this->getMembershipDuration($userId) >= 7) {
                                        if ($outstandingLoanBalance <= 0)//no loan balance
                                        {
                                            if ($blackList == false)//not blacklisted
                                            {
                                                $response = array(
                                                    'status' => 200,
                                                    'loan_id' => $lnId,
                                                    'msg' => 'Your finance application of Ksh ' . number_format($amount, 2) . ' has been received. Proceed to add guarantors.'
                                                );
                                            } else {
                                                $response = array(
                                                    'status' => 501,
                                                    'error' => 'You cannot access a finance due to a default in a previous finance.'
                                                );
                                            }
                                        } else {
                                            $response = array(
                                                'status' => 501,
                                                'error' => 'You have an outstanding finance balance of Ksh ' . number_format($outstandingLoanBalance, 2) . '. Please pay the amount to continue. Thank you.'
                                            );
                                        }
                                    } else {
                                        $response = array(
                                            'status' => 501,
                                            'error' => 'Your account must have been active for atleast 7 days'
                                        );
                                    }
                                } else {
                                    $response = array(
                                        'status' => 501,
                                        'error' => 'Your maximum finance limit is Ksh ' . number_format($maxLimit, 2)
                                    );
                                }
                            } else {
                                $response = array(
                                    'status' => 501,
                                    'error' => 'Finance amount must be at least Ksh 100'
                                );
                            }
                        } else {
                            $response = array(
                                'status' => 501,
                                'error' => 'Finance amount must be at least Ksh ' . number_format($minLoanAmount, 2) . ' and at most Ksh ' . number_format($loanLimit, 2)
                            );
                        }
                    } else {
                        $response = array(
                            'status' => 501,
                            'error' => 'This finance can be secured by your savings. Kindly select your savings as security.'
                        );
                    }
                } elseif ($security == 3)//secured by title deed/car log book
                {
                    if ($amount > $this->getSavingsBalance($userId)) {
                        if ($amount <= $loanLimit && $amount >= $minLoanAmount) {
                            if ($amount >= 100) {
                                //check if amount is less than 3 times of member's savings
                                $maxLimit = ($memberSavings * 3);
                                if ($amount <= $maxLimit) {
                                    //check if member is three months since activation
                                    if ($this->getMembershipDuration($userId) >= 7) {
                                        if ($outstandingLoanBalance <= 0) {
                                            if ($blackList == false) {
                                                $response = "1~Your finance application of Ksh " . number_format($amount, 2) . " has been received. Kindly bring an original document of your security to our office at Skyview Park, Kilimani, Lenana Road (Nairobi).";
                                            } else {
                                                $response = "0~You cannot access a finance due to a default in a previous finance.";
                                            }
                                        } else {
                                            $response = "0~You have an outstanding finance balance of Ksh " . number_format($outstandingLoanBalance, 2) . ". Please pay the amount to continue. Thank you.";
                                        }
                                    } else {
                                        $response = "0~Your account must have been active for atleast 7 days";
                                    }
                                } else {
                                    $response = "0~Your maximum finance limit is Ksh " . number_format($maxLimit, 2);
                                }
                            } else {
                                $response = "0~Finance amount must be at least Ksh 100";
                            }
                        } else {
                            $response = "0~Finance amount must be at least Ksh " . number_format($minLoanAmount, 2) . " and at most Ksh " . number_format($loanLimit, 2);
                        }
                    } else {
                        $response = "0~This finance can be secured by your savings. Kindly select your savings as security.";
                    }
                }
            } else {
                $response = array(
                    'status' => 501,
                    'error' => 'Kindly pay your outstanding finance balance to access more finance.'
                );
            }
        } else {
            $response = "0~There are missing parameters in your finance request";
        }
        echo json_encode($response);
    }

    public function getSchoolFeesFinancingBalance(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($user_id)) {
            $accountId = $this->getSchoolFeesHoldingBalance($user_id);
            $currency = 'KES';
            $account_balance = $this->getTargetSavingsBalance($accountId);
            if ($account_balance > 0) {
                $response = array(
                    'status' => 200,
                    'msg' => 'Account balance: Ksh ',
                    'data' => $currency . ' ' . number_format($account_balance, 2)
                );
            } else {
                $response = array(
                    'status' => 501,
                    'error' => 'Your lipakaro balance is 0.00'
                );
            }

        } else {
            $response = array('status' => 501, 'error' => 'missing params in request');
        }

        echo json_encode($response);
    }

    public function fetchLoansReport()
    {
        $servicing = array();
        $defaulted_collection = array();
        $non_perfoming_collection = array();

        if (!$servicing_loans = mysqli_query($this->link, "SELECT * FROM user_loans WHERE total_outstanding > 0 AND maturity_date > now()")) {

        } else {
            while ($row = mysqli_fetch_array($servicing_loans, MYSQLI_ASSOC)) {
                $row['status'] = 'servicing';
                array_push($servicing, $row);
            }
        }
        if (!$defaulted = mysqli_query($this->link, "SELECT * FROM user_loans WHERE total_outstanding > 0 AND maturity_date < now() AND date_of_none_performance > now()")) {

        } else {
            while ($row = mysqli_fetch_array($defaulted, MYSQLI_ASSOC)) {
                $row['status'] = 'defaulted';
                array_push($defaulted_collection, $row);
            }
        }
        if (!$non_perfoming = mysqli_query($this->link, "SELECT * FROM user_loans WHERE total_outstanding > 0 AND maturity_date < now() AND date_of_none_performance < now()")) {

        } else {
            while ($row = mysqli_fetch_array($non_perfoming, MYSQLI_ASSOC)) {
                $row['status'] = 'non performing';
                array_push($non_perfoming_collection, $row);
            }
        }
        $filename = 'userData.csv';

        header("Content-type: text/csv");

        header("Content-Disposition: attachment; filename=$filename");

        $output = fopen("php://output", "w");
        if (count($servicing) > 0) {
            $header = array_keys($servicing[0]);
        } elseif (count($defaulted_collection) > 0) {
            $header = array_keys($defaulted_collection[0]);
        } elseif (count($non_perfoming_collection) > 0) {
            $header = array_keys($non_perfoming_collection[0]);
        }

        fputcsv($output, $header);

        foreach ($servicing as $row) {
            fputcsv($output, $row);
        }
        foreach ($defaulted_collection as $row) {
            fputcsv($output, $row);
        }
        foreach ($non_perfoming_collection as $row) {
            fputcsv($output, $row);
        }
        fclose($output);


//		echo json_encode( $response );
    }

    function total_business_funding($userId)
    {
        //		BANK ACCOUNT TOTALS
        $main_account_balance = $this->getWithdrawableAmount($userId);
        $savings_balance = 0;
//		SAVINGS ACCOUNT TOTALS
        if (!$savvals = DB::table('savings')->where('userId', $userId)->orderBy('savingsId', 'DESC')->get()) {
            echo json_encode(array('status' => 501, 'error' => '0.00'));
            die;
        } else {
            for ($j = 0; $j < count($savvals); $j++) {
                $savings_balance += $savvals[$j]->amount;
            }
        }

        return ($savings_balance + $main_account_balance);
    }

    function bank_savings_transaction($user_id, $amount, $holdingorwallet)
    {
        if (isset($user_id) && isset($amount) && isset($holdingorwallet)) {
            if (is_numeric($amount) && $amount > 0) {
                //check if user has enough amount on withdrawable account
                if ($holdingorwallet == 1)//from holding account
                {
                    $withAmount = $this->getWithdrawableHoldingAmount($user_id);
                } else {
                    $withAmount = $this->getWithdrawableAmount($user_id);
                }

                if ($withAmount >= $amount) {
                    //handle operation locking - only one withdrawable operation can take place at a time
                    $lockFile = '/var/www/html/dibwaves/admin/attachments/' . $user_id . '.txt';
                    if (!file_exists($lockFile)) {
                        //create lock file
                        $handle = fopen($lockFile, 'a');
                        fwrite($handle, 'operation underway');
                        fclose($handle);

                        $now = date('Y-m-d H:i:s');

                        $purpose = 3;
                        $savingsSource = 3;
                        $description = "Transfer to savings";

                        if ($holdingorwallet == 1)//from holding account
                        {
                            $savingsSource = 6;
                            $transResult = $this->withdrawHoldingFunds($amount, $user_id, $purpose, $description);
                        } else {
                            $savingsSource = 3;
                            $transResult = $this->withdrawFunds($amount, $user_id, $purpose, $description);
                        }

                        if ($transResult == 1)//success
                        {
                            //send to savings
                            $currency = 'KES';
                            $savResults = $this->makeDirectSavings($amount, $user_id, $savingsSource);
                            if ($savResults == 1)     //success
                            {
                                $response = 1;
                            } else {
                                $response = 0;
                            }
                        } else {
                            $response = 0;
                        }
                        //delete lock file
                        unlink($lockFile);
//						File::delete($lockFile);

                    } else {
                        $response = 0;
                    }
                } else {
                    $response = 0;
                }
            } else {
                $response = 0;
            }
        } else {
            $response = 0;
        }

        return $response;
    }
//	finish up
//	42
    public function fetch_guarantor_details(Request $request)
    {
        $userId = $request->get('userId');
        $searchby = $request->get('searchby');
        $ippassphone = $request->get('ippassphone');
        $amount = $request->get('amount');

        if (isset($userId) && isset($searchby) && isset($ippassphone) && isset($amount) && isset($_POST['guarantorId']) && isset($_POST['loanId'])) {
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $amount = htmlspecialchars($_POST['amount'], ENT_QUOTES);
            $guarantorId = htmlspecialchars($_POST['guarantorId'], ENT_QUOTES);
            $loanId = htmlspecialchars($_POST['loanId'], ENT_QUOTES);

            if ($guarantorId == $userId) {
                $response = "0~You cannot add yourself as a guarantor";
            } else {
                //check if a guarantor has an outstanding loan
                if (getUserLoanBalance($guarantorId) <= 0) {
                    //check if guarantor has enough unguaranteid savings
                    if ($amount <= getGuarantableAmount($guarantorId)) {
                        //insert into guarantors table
                        if (!mysqli_query($this->link, "INSERT INTO guarantors(guarantorId,guaranteeId,loanapplicationId,guaranteeamount,datesent,dateapproved,status) VALUES($guarantorId,$userId,$loanId,$amount,'$now','$now',0)")) {
                            echo "0~Error guar" . mysqli_error($this->link);
                        } else {
                            //insert into guarantor messages
                            $notificationMsg = htmlspecialchars(getUserName($userId) . " has requested you to guarantee Ksh " . number_format($amount, 2) . " of a finance. To do so, go to 'Finances' then select 'Guarantee a Finance' and confirm. Thank you.", ENT_QUOTES);
                            if (!mysqli_query($this->link, "INSERT INTO usernotifications(userId,messagetext,alertId,timeupdated,readflag,notified) VALUES($guarantorId,'$notificationMsg',0,'$now',0,0)")) {
                                echo "0~Error noti " . mysqli_error($this->link);
                            } else {
                                $response = "0~Your request has been submitted. Please wait for confirmation from " . getUserName($guarantorId);
                            }
                        }
                    } else {
                        $response = "0~" . getUserName($guarantorId) . " does not have enough savings to guarantee Ksh " . number_format($amount, 2);
                    }
                } else {
                    $response = "0~" . getUserName($guarantorId) . " has an outstanding loan. Kindly add another guarantor.";
                }
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

//	43.5
    /*	things to get before this function
	applicant - userid
	amountSaved - savings
	guarantorId - add_list_main_guarantors
	loanID

	*/
    function use_guarantor_for_loan()
    {
        /*
		 * add savings to total amount
		 * send savings to holding account
		 *
		 * calculate guarantorable amount
		 *  criteria    -   has no outstanding loan, has enough savings
		 *  get list of users and send them gurarantorship requests
		 */
    }

    public function add_guarantor(Request $request)
    {
        $userId = $request->get('userId');
        $ippassphone = $request->get('ippassphone');
        $amount = $request->get('amount');
        $guarantorId = $request->get('guarantorId');
        $loanId = $request->get('loanId');

        if (isset($userId) && isset($ippassphone) && isset($amount) && isset($guarantorId) && isset($loanId)) {

            if ($guarantorId == $userId) {
                $response = array('status' => 501, 'error' => 'You cannot add yourself as a guarantor');
            } else {
                //check if a guarantor has an outstanding loan
                if ($this->getUserLoanBalance($guarantorId) <= 0) {
                    //check if guarantor has enough unguaranteid savings
                    if ($amount <= $this->getGuarantableAmount($guarantorId)) {
                        //insert into guarantors table
                        if (!mysqli_query($this->link, "INSERT INTO guarantors(guarantorId,guaranteeId,loanapplicationId,guaranteeamount,datesent,dateapproved,status) VALUES($guarantorId,$userId,$loanId,$amount,'$now','$now',0)")) {
                            echo "0~Error guar" . mysqli_error($this->link);
                        } else {
                            //insert into guarantor messages
                            $notificationMsg = htmlspecialchars($this->getUserName($userId) . " has requested you to guarantee Ksh " . number_format($amount, 2) . " of a finance. To do so, go to 'Financess' then select 'Guarantee a Finance' and confirm. Thank you.", ENT_QUOTES);
                            if (!mysqli_query($this->link, "INSERT INTO usernotifications(userId,messagetext,alertId,timeupdated,readflag,notified) VALUES($guarantorId,'$notificationMsg',0,'$now',0,0)")) {
                                echo "0~Error noti " . mysqli_error($this->link);
                            } else {
                                $response = "0~Your request has been submitted. Please wait for confirmation from " . $this->getUserName($guarantorId);
                            }
                        }
                    } else {
                        $response = "0~" . $this->getUserName($guarantorId) . " does not have enough savings to guarantee Ksh " . number_format($amount, 2);
                    }
                } else {
                    $response = "0~" . $this->getUserName($guarantorId) . " has an outstanding loan. Kindly add another guarantor.";
                }
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

//	48
    public function change_loan_status(Request $request)
    {
        $userId = $request->get('userId');
        $status = $request->get('status');
        $requestId = $request->get('requestId');
        if (isset($_POST['userId']) && isset($_POST['status']) && isset($_POST['requestId'])) {
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $status = htmlspecialchars($_POST['status'], ENT_QUOTES);
            $requestId = htmlspecialchars($_POST['requestId'], ENT_QUOTES);

            $response = "";
            $guaranteePhone = "";
            $guarAmount = 0;

            //get guarantee details
            if (!$guaraee = mysqli_query($this->link, "SELECT * FROM guarantors WHERE guarantorId=$userId AND guarantorshipId=$requestId")) {
                echo json_encode(array('status' => 501, 'error' => 'query execution error'));
            } else {
                $guaraeerow = mysqli_fetch_array($guaraee, MYSQLI_ASSOC);
                $guaranteePhone = $this->getUserRegisteredPhone($guaraeerow['guaranteeId']);
                $guarAmount = $guaraeerow['guaranteeamount'];
            }

            if ($status == "1")//accepted
            {
                $this->processGuarantorship($requestId);//checks if full amount has been guaranteed and credits loan

                $response = "1~Request has been accepted and processed.";
            } elseif ($status == "2")//declined
            {
                //change status
                if (!mysqli_query($this->link, "UPDATE guarantors SET status=2,dateapproved='$now' WHERE guarantorId=$userId AND status=0 AND guarantorshipId=$requestId")) {
                    echo json_encode(array('status' => 501, 'error' => 'query execution error'));
                } else {
                    //notify user of the decline
                    $message = getUserName($userId) . " declined your guarantee request of Ksh " . number_format($guarAmount, 2) . ". Please add another guarantor.";
                    $this->sendMobileSasa($guaranteePhone, $message);
                }
                $response = "1~Request has been declined.";
            } else {
                $response = "0~Finance guarantee request failed. Please try again later.";
            }

        } else {
            $response = "There are missing parameters in your request";
        }
        echo json_encode($response);
    }

//	53
    public function confirm_cheque_withdrawal(Request $request)
    {
        $userId = $request->get('userId');
        $transactionid = $request->get('transactionid');
        $confirmationcode = $request->get('confirmationcode');
        if (isset($userId) && isset($transactionid) && isset($confirmationcode)) {
            //check if code is still valid for transaction
            if (!$check = DB::table('chequewithdrawals')->where('withdrawalId', $transid)->where('approvalcode', $concode)->where('userId', $userId)->where('status', 0)->get()) {
                $response = array('status' => 501, 'error' => 'Failed to transfer');
            } else {
                if (count($check) == 0) {
                    $response = "0~This code seems to have expired.";
                } else {
                    //change approvar status
                    if (!DB::update('UPDATE chequewithdrawals SET status = ?, transactiondescription = ?, timeconfirmed = ? WHERE withdrawalId = ?', [
                        1,
                        'Confirmed',
                        $now,
                        $transid
                    ])) {
                        $response = array('status' => 501, 'error' => 'Update error');
                    } else {
                        $response = array('status' => 200, 'msg' => 'Your request is being processed');
                    }
                }
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

//	54
    public function buy_shares_withdrawable_account(Request $request)
    {
        $userId = $request->get('userId');
        $amount = $request->get('amount');
        $holdingorwallet = $request->get('holdingorwallet');

        if (isset($_POST['userId']) && isset($_POST['amount']) && isset($_POST['holdingorwallet'])) {
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $amount = htmlspecialchars($_POST['amount'], ENT_QUOTES);
            $holdingorwallet = htmlspecialchars($_POST['holdingorwallet'], ENT_QUOTES);
            $response = "";

            if (is_numeric($amount) && $amount > 0) {
                $sharesSourceTransfer = 3;
                if ($holdingorwallet == 1)//holding
                {
                    $sharesSourceTransfer = 6;
                    $bal = getWithdrawableHoldingAmount($userId);
                } else {
                    $sharesSourceTransfer = 3;
                    $bal = getWithdrawableAmount($userId);
                }

                if ($amount <= $bal)//has enough balance
                {
                    //buy shares
                    if (buyShares($amount, $userId, $sharesSourceTransfer) == 1)//success
                    {
                        //withdraw funds
                        if ($holdingorwallet == 1)//holding
                        {
                            $description = "Buy Shares from Mpesa Holding a/c";
                            $withdrawResult = withdrawHoldingFunds($amount, $userId, 5, $description);
                        } else {
                            $description = "Buy Shares from Homepesa Wallet";
                            $withdrawResult = withdrawFunds($amount, $userId, 5, $description);
                        }

                        if ($withdrawResult == 1) {
                            $response = "1~You have successfully purchased shares worth Ksh " . number_format($amount, 2);
                        } else {
                            $response = "0~Share purchase failed. Please try again later.";
                        }
                    } else {
                        $response = "0~Share purchase failed. Please try again later.";
                    }
                } else {
                    $response = "0~You have insufficient balance. Your current balance is Ksh " . number_format($bal, 2) . ".";
                }
            } else {
                $response = "0~Amount must be greater than 0.";
            }

        } else {
            $response = "0~There are missing parameters";
        }
        echo json_encode($response);
    }

//	55
    public function repay_loan_withdrawable_account(Request $request)
    {
        $userId = $request->get('userId');
        $amount = $request->get('amount');
        $loanidtorepay = $request->get('loanidtorepay');
        $holdingorwallet = $request->get('holdingorwallet');

        if (isset($userId) && isset($amount) && isset($loanidtorepay) && isset($holdingorwallet)) {

            $response = "";
            $repaymentSource = 3;

            if ($holdingorwallet == 1)//from holding
            {
                $repaymentSource = 8;
                $bal = $this->getWithdrawableHoldingAmount($userId);
            } else {
                $repaymentSource = 3;
                $bal = $this->getWithdrawableAmount($userId);
            }

            if (is_numeric($amount) && $amount > 0) {
                if ($amount <= $bal)//has enough balance
                {
                    if ($this->repayLoan($amount, $userId, $loanidtorepay, $repaymentSource) == 1)//success
                    {
                        //withdraw amount
                        if ($holdingorwallet == 1)//from holding
                        {
                            $description = "Repay finance from Mpesa Holding a/c";
                            $repayResult = $this->withdrawHoldingFunds($amount, $userId, 6, $description);
                        } else {
                            $description = "Repay finance from Homepesa Wallet";
                            $repayResult = $this->withdrawFunds($amount, $userId, 6, $description);
                        }

                        if ($repayResult == 1) {
                            $response = "1~";
                            $response = array(
                                'status' => 200,
                                'msg' => 'Finance repayment is successful. Check your finance balance to confirm.'
                            );
                        } else {
                            $response = array(
                                'status' => 501,
                                'error' => 'Finance repayment failed. Please try again later.'
                            );
                        }
                    } else {
                        $response = array(
                            'status' => 501,
                            'error' => 'Finance repayment failed. Please try again later.'
                        );
                    }
                } else {
                    $response = array(
                        'status' => 501,
                        'error' => 'You have insufficient balance. Your current balance is Ksh ' . number_format($bal, 2)
                    );
                }
            } else {
                $response = array('status' => 501, 'error' => 'Amount must be greater than 0');
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters');
        }
        echo json_encode($response);
    }

//	56
    public function confirm_add_member_name(Request $request)
    {
        $groupId = $request->get('groupId');
        $idno = $request->get('idno');
        if (isset($_POST['groupId']) && isset($_POST['idno'])) {
            $groupId = htmlspecialchars($_POST['groupId'], ENT_QUOTES);
            $idno = htmlspecialchars($_POST['idno'], ENT_QUOTES);

            //get member details
            if (!$getDetails = mysqli_query($this->link, "SELECT * FROM users WHERE idpass='$idno'")) {
                $response = json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                if (count($getDetails) > 0)//exists
                {
                    $getDetailsrow = mysqli_fetch_array($getDetails, MYSQLI_ASSOC);
                    $response = "1~Add " . htmlspecialchars_decode($getDetailsrow['fullname'], ENT_QUOTES) . " to group?~$idno";
                } else {
                    $response = "0~The id number $idno does not exist";
                }
            }

        } else {
            $response = "0~There are missing parameters";
        }
    }

//  57
    public function add_member_to_group(Request $request)
    {
        $groupId = $request->get('groupId');
        $idno = $request->get('idno');
        if (isset($_POST['groupId']) && isset($_POST['idno'])) {
            $groupId = htmlspecialchars($_POST['groupId'], ENT_QUOTES);
            $idno = htmlspecialchars($_POST['idno'], ENT_QUOTES);

            //get member details
            if (!$getDetails = mysqli_query($this->link, "SELECT * FROM users WHERE idpass='$idno'")) {
                $response = json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                if (count($getDetails) > 0)//exists
                {
                    $getDetailsrow = mysqli_fetch_array($getDetails, MYSQLI_ASSOC);
                    $userId = $getDetailsrow['userId'];

                    //check if user is already added
                    if (!$check = mysqli_query($this->link, "SELECT * FROM groupmembers WHERE groupId=$groupId AND userId = $userId")) {
                        $response = json_encode(array('status' => 501, 'error' => 'database error'));
                    } else {
                        if (count($check) == 0)//does not exist
                        {
                            if (!mysqli_query($this->link, "INSERT INTO groupmembers(groupId,userId,dateadded,dateaccepted,status) VALUES($groupId,$userId,'$now','$now',0)")) {
                                $response = json_encode(array('status' => 501, 'error' => 'database error'));
                            } else {
                                $smsMessage = "You have been added to the group " . getUserName($groupId) . ". Log into your account to confirm or decline membership.";
                                $smsPhone = getUserRegisteredPhone($userId);
                                $this->sendMobileSasa($smsPhone, $smsMessage, '');
                                $response = "0~Member has been successfully added";
                            }
                        } else {
                            $response = "0~Member has already been added in group";
                        }
                    }
                } else {
                    $response = "0~The id number does not exist";
                }
            }
        } else {
            $response = "0~There are missing parameters";
        }
    }

//	58
    public function add_member(Request $request)
    {
        $groupId = $request->get('groupId');
        $username = $request->get('username');
        $idpass = $request->get('idpass');
        $phone = $request->get('phone');
        $useremail = $request->get('useremail');

        if (isset($_POST['groupId']) && isset($_POST['useremail']) && isset($_POST['username']) && isset($_POST['idpass']) && isset($_POST['phone'])) {
            $groupId = htmlspecialchars($_POST['groupId'], ENT_QUOTES);
            $email = htmlspecialchars($_POST['useremail'], ENT_QUOTES);
            $name = htmlspecialchars($_POST['username'], ENT_QUOTES);
            $idno = htmlspecialchars($_POST['idpass'], ENT_QUOTES);
            $phone = htmlspecialchars($_POST['phone'], ENT_QUOTES);

            //get member details
            if (!$getDetails = mysqli_query($this->link, "SELECT * FROM users WHERE idpass='$idno'")) {
                $response = json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                if (count($getDetails) == 0)// does not exists
                {
                    //check if email is registered
                    if (!$getEmailDetails = mysqli_query($this->link, "SELECT * FROM users WHERE email='$email'")) {
                        $response = json_encode(array('status' => 501, 'error' => 'database error'));
                    } else {
                        if (count($getEmailDetails) == 0)//does not exists
                        {
                            //generate password
                            $random = substr(md5(rand()), 0, 7);
                            $encrypted = md5($random);

                            //insert member and get their id
                            $userId = 0;
                            if (!mysqli_query($this->link, "INSERT INTO users(fullname,idpass,phone,email,password,actype,acstatus,regfee,membershipfee,signuptime,timepaid,timeactivated,merchantId,passkey,passrecoverycode,signupoption,blacklist,phoneconfirmed,mpesaconfirmed,referredby) VALUES('$name','$idno','$phone','$email','$encrypted',1,0,0,0,'$now','$now','$now','','','',1,0,0,0,'')")) {
                                $response = json_encode(array('status' => 501, 'error' => 'query execution error'));
                            } else {
                                $userId = mysqli_insert_id($this->link);
                            }

                            //add new user to group
                            if (!mysqli_query($this->link, "INSERT INTO groupmembers(groupId,userId,dateadded,dateaccepted,status) VALUES($groupId,$userId,'$now','$now',0)")) {
                                $response = json_encode(array('status' => 501, 'error' => 'query execution error'));
                            } else {
                                $smsMessage = "You have been added to the group " . getUserName($groupId) . ". Your temporary password is $random. Change it to enhance the security of your account. you will be required to approve or decline request. Log in at www.homepesasacco.com to complete request.";
                                $smsPhone = getUserRegisteredPhone($userId);
                                $this->sendMobileSasa($smsPhone, $smsMessage, '');

                                $response = "1~Member has been successfully added. Inform the member the email registered";
                            }
                        } else {
                            $response = "0~This email is already in use. Kindly choose a different email";
                        }
                    }
                } else {
                    $response = "0~The member is already registered by Homepesa Sacco. Select Existing Option";
                }
            }
        } else {
            $response = "0~There are missing parameters";
        }
        echo json_encode($response);
    }

//  59
    public function group_member(Request $request)
    {
        $groupId = $request->get('groupId');
        $memberstatus = $request->get('memberstatus');

        if (isset($_POST['groupId']) && isset($_POST['memberstatus'])) {
            $groupId = htmlspecialchars($_POST['groupId'], ENT_QUOTES);
            $status = htmlspecialchars($_POST['memberstatus'], ENT_QUOTES);
            $response = "";

            if (!$result = mysqli_query($this->link, "SELECT * FROM groupmembers WHERE groupId=$groupId AND status=$status")) {
                echo "Error1 " . mysqli_error($this->link);
            } else {
                while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                    $response .= 'Name: ' . getUserName($row['userId']) . '~' .
                        'ID No: ' . getUserIdNo($row['userId']) . '~' .
                        ($status == 1 ? 'Group Savings: ' . number_format(getGroupSavingsBalance($row['userId']), 2) . '~' : '') .
                        ($status == 1 ? 'Group Finance: ' . number_format(memberGroupLoanBalance($row['userId'], $groupId), 2) . '~' : '') .
                        'Date: ' . formatDateTime($row['dateadded']) . '~`';
                }
            }

            $response = trim($response, '`');

        } else {
            $response = "0~There are missing parameters";
        }
        echo json_encode($response);
    }

//	60
    public function remove_group_member(Request $request)
    {
        $groupId = $request->get('groupId');
        $idno = $request->get('idno');

        if (isset($_POST['groupId']) && isset($_POST['idno'])) {
            $groupId = htmlspecialchars($_POST['groupId'], ENT_QUOTES);
            $idno = htmlspecialchars($_POST['idno'], ENT_QUOTES);

            //get member details
            if (!$getDetails = mysqli_query($this->link, "SELECT * FROM users WHERE idpass='$idno'")) {
                $response = json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                if (count($getDetails) > 0)//exists
                {
                    $getDetailsrow = mysqli_fetch_array($getDetails, MYSQLI_ASSOC);
                    $userId = $getDetailsrow['userId'];

                    //check if there's a pending group loan application
                    if (!$checkLoan = mysqli_query($this->link, "SELECT * FROM admingrouploanapplication WHERE groupId=$groupId AND status=0")) {
                        $response = json_encode(array('status' => 501, 'error' => 'query execution error'));
                    } else {
                        if (count($checkLoan) == 0)//no pending loan application
                        {
                            //check if user still exists
                            if (!$checkUserId = mysqli_query($this->link, "SELECT * FROM groupmembers WHERE groupId=$groupId AND userId=$userId")) {
                                $response = json_encode(array('status' => 501, 'error' => 'query execution error'));
                            } else {
                                //group or member should not have any outstanding loans in the group
                                if (memberGroupLoanBalance($userId, $groupId) > 0) {
                                    $response = "0~The member has a group finance balance";
                                } elseif (getAdminGroupLoanBalance($groupId) > 0) {
                                    $response = "0~The group has a finance balance. You cannot remove a member.";
                                } else {
                                    if (count($checkUserId) > 0) {
                                        if (!mysqli_query($this->link, "DELETE FROM groupmembers WHERE groupId=$groupId AND userId=$userId")) {
                                            $response = json_encode(array(
                                                'status' => 501,
                                                'error' => 'query execution error'
                                            ));
                                        } else {
                                            $response = "1~Member has been successfully removed";
                                        }
                                    } else {
                                        $response = "0~Member has already been removed";
                                    }
                                }
                            }
                        } else {
                            $response = "0~There's a pending group finance application. The finance must be approved/declined first to proceed";
                        }
                    }

                } else {
                    $response = "0~The id number does not exist";
                }
            }

        } else {
            $response = "0~There are missing parameters";
        }
        echo json_encode($response);
    }

//  62
    public function group_loan_application(Request $request)
    {
        $groupId = $request->get('groupId');
        $amount = $request->get('amount');
        $description = $request->get('description');
        if (isset($_POST['groupId']) && isset($_POST['amount']) && isset($_POST['description'])) {
            $userId = htmlspecialchars($_POST['groupId'], ENT_QUOTES);
            $amount = htmlspecialchars($_POST['amount'], ENT_QUOTES);
            $description = htmlspecialchars($_POST['description'], ENT_QUOTES);

            //validate amount
            if (is_numeric($amount) && $amount >= 100) {
                //check if there is any pending group loan application
                if (!$checkPending = mysqli_query($this->link, "SELECT * FROM admingrouploanapplication WHERE groupId=$userId AND status=0")) {
                    $response = json_encode(array('status' => 501, 'error' => 'database error'));
                } else {
                    if (count($checkPending) == 0) {
                        $totalGroupMemberLoans = 0;
                        $totalGroupMemberLoanPayments = 0;
                        $totalThisGroupLoans = 0;
                        $totalThisGroupLoanPayments = 0;
                        $totalThisGroupSavings = 0;

                        //get total member group loans
                        if (!$groupLoans = mysqli_query($this->link, "SELECT SUM(amount) AS mkopo FROM grouploans WHERE groupId=$userId")) {
                            $response = json_encode(array('status' => 501, 'error' => 'database error'));
                        } else {
                            $groupLoansrow = mysqli_fetch_array($groupLoans, MYSQLI_ASSOC);
                            $totalGroupMemberLoans = isset($groupLoansrow['mkopo']) ? $groupLoansrow['mkopo'] : 0;
                        }

                        //get total member group loan repayments
                        if (!$groupLns = mysqli_query($this->link, "SELECT * FROM grouploans WHERE groupId=$userId")) {
                            $response = json_encode(array('status' => 501, 'error' => 'database error'));
                        } else {
                            while ($groupLnsrow = mysqli_fetch_array($groupLns, MYSQLI_ASSOC)) {
                                $loanId = $groupLnsrow['loanId'];
                                if (!$groupPayments = mysqli_query($this->link, "SELECT SUM(amount) AS lipa FROM grouploanpayments WHERE loanId=$loanId")) {
                                    $response = json_encode(array('status' => 501, 'error' => 'database error'));
                                } else {
                                    $groupPaymentsrow = mysqli_fetch_array($groupPayments, MYSQLI_ASSOC);
                                    $totalGroupMemberLoanPayments += isset($groupPaymentsrow['lipa']) ? $groupPaymentsrow['lipa'] : 0;
                                }
                            }
                        }

                        //get loans taken by this group
                        if (!$adminGroupLoans = mysqli_query($this->link, "SELECT SUM(amount) AS adminmkopo FROM admingrouploanapplication WHERE groupId=$userId AND status=1")) {
                            $response = json_encode(array('status' => 501, 'error' => 'database error'));
                        } else {
                            $adminGroupLoansrow = mysqli_fetch_array($adminGroupLoans, MYSQLI_ASSOC);
                            $totalThisGroupLoans = isset($adminGroupLoansrow['adminmkopo']) ? $adminGroupLoansrow['adminmkopo'] : 0;
                        }

                        //get loan repayments for this group
                        if (!$adminGroupLns = mysqli_query($this->link, "SELECT * FROM admingrouploanapplication WHERE groupId=$userId AND status=1")) {
                            $response = json_encode(array('status' => 501, 'error' => 'database error'));
                        } else {
                            while ($adminGroupLnsrow = mysqli_fetch_array($adminGroupLns, MYSQLI_ASSOC)) {
                                $adminLoanId = $adminGroupLnsrow['applicationId'];
                                if (!$adminGroupPayments = mysqli_query($this->link, "SELECT SUM(amount) AS adminlipa FROM admingrouploanpayments WHERE loanId=$adminLoanId")) {
                                    $response = json_encode(array('status' => 501, 'error' => 'database error'));
                                } else {
                                    $adminGroupPaymentsrow = mysqli_fetch_array($adminGroupPayments, MYSQLI_ASSOC);
                                    $totalThisGroupLoanPayments += isset($adminGroupPaymentsrow['adminlipa']) ? $adminGroupPaymentsrow['adminlipa'] : 0;
                                }
                            }
                        }

                        //get total group savings
                        if (!$groupSavings = mysqli_query($this->link, "SELECT SUM(amount) AS adminakiba FROM groupsavings WHERE groupId=$userId")) {
                            $response = json_encode(array('status' => 501, 'error' => 'database error'));
                        } else {
                            $groupSavingsrow = mysqli_fetch_array($groupSavings, MYSQLI_ASSOC);
                            $totalThisGroupSavings = isset($groupSavingsrow['adminakiba']) ? $groupSavingsrow['adminakiba'] : 0;
                        }

                        //check amount that group can access
                        $totalLoanBalances = ($totalGroupMemberLoans - $totalGroupMemberLoanPayments) + ($totalThisGroupLoans - $totalThisGroupLoanPayments);
                        $accessibleLoan = $totalThisGroupSavings - $totalLoanBalances;

                        //deny if group has a balance
                        if (($totalThisGroupLoans - $totalThisGroupLoanPayments) <= 0) {
                            if ($amount <= $accessibleLoan) {
                                $period = getLoanRepaymentPeriodBasedOnAmount($amount);

                                //insert into applications
                                if (!mysqli_query($this->link, "INSERT INTO admingrouploanapplication(amount,dateapplied,dateapproved,status,groupId,interestflag,correspondingloanId,period,defaultstatus,useralerted,description) VALUES($amount,'$this->now','$this->now',0,$userId,0,0,$period,0,0,'$description')")) {
                                    $response = json_encode(array('status' => 501, 'error' => 'database error'));
                                } else {
                                    //send notification to all members
                                    $notiMessage = htmlspecialchars(getUserName($userId) . " has requested approval for the finance of Ksh " . number_format($amount, 2), ENT_QUOTES);
                                    if (!$allGroupMembers = mysqli_query($this->link, "SELECT * FROM groupmembers WHERE groupId=$userId")) {
                                        $response = json_encode(array(
                                            'status' => 501,
                                            'error' => 'database error'
                                        ));
                                    } else {
                                        while ($allGroupMembersrow = mysqli_fetch_array($allGroupMembers, MYSQLI_ASSOC)) {
                                            $groupMemberId = $allGroupMembersrow['userId'];

                                            if (!mysqli_query($this->link, "INSERT INTO usernotifications(userId,messagetext,alertId,timeupdated,readflag,notified) VALUES($groupMemberId,'$notiMessage',0,'$now',0,0)")) {
                                                $response = json_encode(array(
                                                    'status' => 501,
                                                    'error' => 'database error'
                                                ));
                                            }
                                        }
                                    }

                                    $response = "1~Finance application is successful, finance will be processed once all members approve the finance";

                                }
                            } else {
                                $response = "0~You can access at most Ksh " . number_format($accessibleLoan, 2);
                            }
                        } else {
                            $response = "0~Kindly pay your outstanding finance balance of Ksh " . number_format(($totalThisGroupLoans - $totalThisGroupLoanPayments), 2);
                        }
                    } else {
                        $response = "0~There's a pending application. Ask members to approve/decline";
                    }
                }
            } else {
                $response = "0~Enter a valid amount";
            }

        } else {
            $response = "0~There are missing parameters";
        }
        echo json_encode($response);
    }

//	68


//	68.5
    public function get_requests_count(Request $request)
    {
        $userId = $request->get('user_id');
        $account_type = $request->get('ac_type');

        if (isset($userId)) {
            $response = 0;
//			if ( ! $result = DB::table( 'groupmembers' )->where( 'userId', $userId )->where( 'status', $status )->get() ) {
            if (!$result = DB::table('groupmembers')->where('userId', $userId)->where('status', 0)->get()) {
                echo json_encode(array('status' => 501, 'error' => 'failed to fetch user groups'));
            } else {
                $count_result = count($result);
                for ($i = 0; $i < $count_result; $i++) {


                    $id = $result[$i]->groupId;

                    if ($this->getUserById($id)->actype == $account_type) {
                        $response += 1;
                    }

                }
            }
            $response = array('status' => 200, 'count' => $response);

        } else {
            $response = array('status' => 501, 'error' => 'missing params in request');
        }
        echo json_encode($response);
    }

//	68 v2
    public function user_memberships(Request $request)
    {
        $userId = $request->get('user_id');
        $group_type = $request->get('group_type');
//
        if (isset($userId) && isset($group_type)) {
            $response = "";
            $compile = array();
//			if ( ! $result = DB::table( 'groupmembers' )->where( 'userId', $userId )->where( 'status', $status )->get() ) {
            if (!$result = DB::table('groupmembers')->where('userId', $userId)->get()) {
                echo json_encode(array('status' => 501, 'error' => 'failed to fetch user groups'));
            } else {
                $count_result = count($result);
                for ($i = 0; $i < $count_result; $i++) {
                    $id = $result[$i]->groupId;
                    $group = new stdClass();
                    $group->pid = $id;
                    $group->username = $this->getUserName($id);
                    $group->actype = $this->getUserById($id)->actype;
                    $group->grp_idno = $this->getUserIdNo($id);
                    $group->us_status = $result[$i]->status;

                    if (!$user_details = DB::table('users')->where('id', $id)->get()) {
                    } else {
                        $user_details = $user_details[0];
                        if ($group_type == $user_details->actype) {
                            $compile[] = $group;
                        }
                    }
                }
                if (count($result) > 0) {
                    $response = array(
                        'status' => 200,
                        'count' => (int)count($compile),
                        'data' => $compile
                    );
                } else {
                    switch ($group_type) {
                        case 4:
                            $response = array('status' => 501, 'error' => 'YOU HAVEN\'T JOINED ANY GROUP YET');

                            break;
                        case 5:
                            $response = array('status' => 501, 'error' => 'YOU HAVEN\'T JOINED ANY SACCO YET');

                            break;
                        case 6:
                            $response = array('status' => 501, 'error' => 'YOU HAVEN\'T JOINED ANY BUSINESS YET');

                            break;
                    }
                }
//				$response['count'] = count($response);
            }

        } else {
            $response = array('status' => 501, 'error' => 'missing params in request');
        }
        echo json_encode($response);
    }

//	70
    public function member_group_savings(Request $request)
    {
        $userId = $request->get('userId');
        $groupId = $request->get('groupId');
        if (isset($groupId) && isset($userId)) {
            $savings = 0;

            if (!$savvals = DB::table('groupsavings')->where('groupId', $groupId)->where('savedby', $userId)->orderBy('savingId', 'DESC')->get()) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                $count_sav = count($savvals);
                foreach ($savvals as $iValue) {
                    $savings += $iValue->amount;
                }
            }
            $response = array('status' => 200, 'total_savings' => $savings, 'transactions' => $savvals);
        } else {
            $response = array('status' => 501, 'error' => 'looks like there are missing params');
        }
        echo json_encode($response);
    }

//	73

//	75
    public function change_group_loan_status(Request $request)
    {
        $loanId = $request->get('loanId');
        $status = $request->get('status');
        $userId = $request->get('userId');

        if (isset($_POST['loanId']) && isset($_POST['status'])) {
            $id = htmlspecialchars($_POST['loanId'], ENT_QUOTES);
            $status = htmlspecialchars($_POST['status'], ENT_QUOTES);
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $response = "";

            $adminPhone = "";
            $groupId = 0;

            //get group details
            if (!$groupDetails = mysqli_query($this->link, "SELECT * FROM admingrouploanapplication WHERE applicationId=$id")) {
                $response = json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                $groupDetailsrow = mysqli_fetch_array($groupDetails, MYSQLI_ASSOC);
            }

            $adminPhone = getUserRegisteredPhone($groupDetailsrow['groupId']);
            $groupId = $groupDetailsrow['groupId'];

            //check if user had already reacted to the loan
            if (!$checkReactionStatus = mysqli_query($this->link, "SELECT * FROM admingrouploanapprovers WHERE approverId=$userId AND loanId=$id")) {
                $response = json_encode(array('status' => 501, 'error' => 'database error'));
            } elseif (count($checkReactionStatus) == 0) {
                if ($status == 1)//approved
                {
                    $noOfmembers = 0;//all members in the group
                    $noOfmembersReacted = 0;//all members who have responded to loan
                    $noOfMembersApproved = 0;//all members who have approved

                    //add approver
                    if (!mysqli_query($this->link, "INSERT INTO admingrouploanapprovers(loanId,approverId,dateapproved,approvalflag) VALUES($id,$userId,'$now',1)")) {
                        $response = json_encode(array('status' => 501, 'error' => 'database error'));
                    }

                    //get total number of members in the group
                    if (!$groupMemberCount = mysqli_query($this->link, "SELECT * FROM groupmembers WHERE groupId=$groupId")) {
                        $response = json_encode(array('status' => 501, 'error' => 'database error'));
                    } else {
                        $noOfmembers = count($groupMemberCount);
                    }

                    //get total number of members who have reacted to the loan
                    if (!$reactedMembers = mysqli_query($this->link, "SELECT * FROM admingrouploanapprovers WHERE loanId=$id")) {
                        $response = json_encode(array('status' => 501, 'error' => 'database error'));
                    } else {
                        $noOfmembersReacted = count($reactedMembers);
                    }

                    //get total number of members who have approved
                    if (!$approvedLoanMembers = mysqli_query($this->link, "SELECT * FROM admingrouploanapprovers WHERE loanId=$id AND approvalflag=1")) {
                        $response = json_encode(array('status' => 501, 'error' => 'database error'));
                    } else {
                        $noOfMembersApproved = count($approvedLoanMembers);
                    }

                    //check if all members have approved
                    if ($noOfMembersApproved == $noOfmembers)//all members approved
                    {
                        //process loan
                        processAdminGroupLoan($id);
                    } elseif ($noOfmembers == $noOfmembersReacted)//all members reacted but not all approved
                    {
                        //decline loan
                        if (!mysqli_query($this->link, "UPDATE admingrouploanapplication SET dateapproved='$now',status=2 WHERE applicationId=$id")) {
                            $response = json_encode(array('status' => 501, 'error' => 'database error'));
                        }
                        $response = "1~You have successfully approved finance";
                    } else {
                        $response = "1~You have successfully approved finance";
                    }
                } elseif ($status == 2)//declined
                {
                    //mark loan as declined
                    if (!mysqli_query($this->link, "UPDATE admingrouploanapplication SET dateapproved='$now',status=2 WHERE applicationId=$id")) {
                        $response = json_encode(array('status' => 501, 'error' => 'database error'));
                    } else {
                        //insert into approvers
                        if (!mysqli_query($this->link, "INSERT INTO admingrouploanapprovers(loanId,approverId,dateapproved,approvalflag) VALUES($id,$userId,'$now',2)")) {
                            $response = json_encode(array('status' => 501, 'error' => 'database error'));
                        }

                        //send sms to admin
                        $smsMessage = getUserName($userId) . " declined " . getUserName($groupId) . "'s finance application. Finance will not be processed.";
                        $this->sendMobileSasa($adminPhone, $smsMessage, "");

                        $response = "1~Request has been successfully declined. Finance will not be processed";
                    }
                }
            } else {
                $response = "0~You have already reacted to this finance";
            }
        } else {
            $response = "0~There are missing parameters";
        }
        echo json_encode($response);
    }

//	77
    public function ad_gurantee_request(Request $request)
    {
        $requestId = $request->get('guaranteeId');
        $status = $request->get('status');
        $userId = $request->get('user_id');

        if (isset($requestId) && isset($status) && isset($userId)) {
            $response = "";
            $guaranteePhone = "";
            $guarAmount = 0;

            //get guarantee details
            if (!$guaraee = DB::table('groupguarantors')->where('guarantorId', $userId)->where('guarantorshipId', $requestId)->get()) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                $guaraee = $guaraee[0];
                $guaranteePhone = $this->getUserRegisteredPhone($guaraee->guaranteeId);
                $guarAmount = $guaraee->guaranteeamount;
            }

            if ($status == "1")//accepted
            {
                $this->processGroupGuarantorship($requestId);//checks if full amount has been guaranteed and credits loan
                $response = array('status' => 200, 'msg' => 'Request has been accepted and processed.');
            } elseif ($status == "2")//declined
            {
                //change status
                if (!DB::update('UPDATE groupguarantors SET status = ?,dateapproved = ? WHERE guarantorId = ? AND status = ? AND guarantorshipId= ?', [
                    2,
                    '$now',
                    $userId,
                    0,
                    $requestId
                ])) {
                    echo json_encode(array('status' => 501, 'error' => 'database error'));
                } else {
                    //notify user of the decline
                    $message = $this->getUserName($userId)
                        . " declined your guarantee request of Ksh "
                        . number_format($guarAmount, 2)
                        . ". Please add another guarantor.";
//					$this->sendMobileSasa( $guaranteePhone, $message);
                }
                $response = array('status' => 501, 'error' => 'Request has been declined.');
            } else {
                $response = array(
                    'status' => 501,
                    'error' => 'Finance guarantee request failed. Please try again later.'
                );
            }

        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters.');
        }
        echo json_encode($response);
    }

//	78
    public function chk_group_loan_eligibility(Request $request)
    {
        $userId = $request->get('userId');
        $security = $request->get('security');
        $amount = $request->get('amount');
        $remarks = $request->get('remarks');
        $groupId = $request->get('groupId');

        if (isset($security) && isset($amount) && isset($userId) && isset($remarks) && isset($groupId)) {

            if (is_numeric($amount) && $amount >= 10 && is_numeric($security)) {
                //check if there is any pending loan application
                if (!$checkPendingLoans = DB::table('admingrouploanapplication')->where('groupId', $groupId)->where('status', 0)->get()) {
                    $response = array('status' => 501, 'error' => 'database error');
                } elseif (count($checkPendingLoans) == 0) {
                    //check if group has a balance

                    $thisGrpBalance = $this->getAdminGroupLoanBalance($groupId);

                    if ($thisGrpBalance > 0)//group has a loan balance
                    {
                        $response = array(
                            'status' => 501,
                            'error' => 'Your group has an outstanding loan balance of Ksh ' . number_format($thisGrpBalance, 2)
                        );
                    } else {

                        //check if user has unpaid loans
                        if ($this->memberGroupLoanBalance($userId, $groupId) <= 0) {
//							dd($this->getLoanRepaymentPeriodBasedOnAmount( $amount ));
                            $period = $this->getLoanRepaymentPeriodBasedOnAmount($amount);
                            $responseVals = json_decode($this->processMemberGroupLoan($security, $amount, $period, $userId, $groupId, $remarks), true);

                            if ($responseVals['status'] == 0)//failed
                            {
                                $response = array('status' => 501, 'error' => $responseVals['description']);
                            } elseif ($responseVals['status'] == 1)//savings security success
                            {
                                $response = array(
                                    'status' => 200,
                                    'msg' => $responseVals['description'],
                                    'type' => 1
                                );
                            } elseif ($responseVals['status'] == 2)//guarantors security success
                            {
                                $response = array(
                                    'status' => 200,
                                    'desc' => $responseVals['description'],
                                    'type' => 2,
                                    'loan_id' => $responseVals['loanId']
                                );
                            }
                        } else {
                            $response = array(
                                'status' => 501,
                                'error' => 'Kindly pay your outstanding finance balance to access more finance'
                            );
                        }
                    }
                } else {
                    $response = array('status' => 501, 'error' => 'This group has a pending finance application');
                }
            } else {
                $response = array('status' => 501, 'error' => 'Enter all details. Amount must be at least Ksh 100');
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters');
        }
        echo json_encode($response);
    }

//	79
    public function add_group_loan_guarantors(Request $request)
    {
        $userId = $request->get('userId');
        $amount = $request->get('amount');
        $loanId = $request->get('loanId');
        $guarantorId = $request->get('guarantorId');
        if (isset($userId) && isset($amount) && isset($loanId) && isset($guarantorId)) {

            $messageNoti = $this->getUserName($userId) . " has requested you to guarantee Ksh " . number_format($amount, 2) . " of a group finance.";

            if ($amount >= 10) {
                if ($userId == $guarantorId) {
                    $response = array('status' => 501, 'error' => 'You cannot add yourself as a guarantor');
                } else {
                    if (!$check1 = DB::table('groupguarantors')->where('guarantorId', $guarantorId)->where('loanapplicationId', $loanId)->get()) {
                        $response = array('status' => 501, 'error' => 'failed to fetch group guarantors');
                    } else {
                        if (count($check1) > 0) {
                            $response = array(
                                'status' => 501,
                                'error' => 'You have already added member as a guarantor to this finance'
                            );
                        } else {
                            //send user notification

                            //add user as a guarantor
                            $query = "INSERT INTO groupguarantors(guarantorId,guaranteeId,loanapplicationId,guaranteeamount,datesent,dateapproved,status) VALUES($guarantorId,$userId,$loanId,$amount,'$now','$now',0)";
                            if (!DB::insert($query)) {
                                $response = array('status' => 501, 'error' => 'failed to insert group guarantors');
                            } else {
                                $notiQuery = "INSERT INTO usernotifications(userId,messagetext,alertId,timeupdated,readflag,notified) VALUES($guarantorId,'$messageNoti',0,'$now',0,0)";
                                if (!DB::insert($notiQuery)) {
                                    $response = array(
                                        'status' => 501,
                                        'error' => 'failed to insert user notifications'
                                    );
                                } else {
                                    $response = array(
                                        'status' => 200,
                                        'msg' => 'Guarantor has been added. Contact them to guarantee you.'
                                    );
                                }
                            }
                        }
                    }
                }
            } else {
                $response = array(
                    'status' => 501,
                    'error' => 'Amount must be at least Ksh 10'
                );
            }

        } else {
            $response = array(
                'status' => 501,
                'error' => 'There are missing parameters'
            );
        }
        echo json_encode($response);
    }

//	79.5
    public function add_list_group_loan_guarantors(Request $request)
    {
        $contents = json_decode($request->getContent(), true);
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');
//		dd($contents);
        for ($i = 0; $i < count($contents); $i++) {
            $userId = $contents[$i]['userId'];
            $amount = $contents[$i]['amount'];
            $loanId = $contents[$i]['loanId'];
            $guarantorId = $contents[$i]['guarantorId'];
            if (isset($userId) && isset($amount) && isset($loanId) && isset($guarantorId)) {

                $messageNoti = $this->getUserName($userId) . " has requested you to guarantee Ksh " . number_format($amount, 2) . " of a group finance.";

                if ($amount >= 10) {
                    if ($userId == $guarantorId) {
                        $response = array('status' => 501, 'error' => 'You cannot add yourself as a guarantor');
                    } else {
                        if (!$check1 = DB::table('groupguarantors')->where('guarantorId', $guarantorId)->where('loanapplicationId', $loanId)->get()) {
                            $response = array('status' => 501, 'error' => 'failed to fetch group guarantors');
                        } else {
                            if (count($check1) > 0) {
                                $response = array(
                                    'status' => 501,
                                    'error' => 'You have already added member as a guarantor to this finance'
                                );
                            } else {
                                //send user notification

                                //add user as a guarantor
                                $query = "INSERT INTO groupguarantors(guarantorId,guaranteeId,loanapplicationId,guaranteeamount,datesent,dateapproved,status) VALUES($guarantorId,$userId,$loanId,$amount,'$now','$now',0)";
                                if (!DB::insert($query)) {
                                    $response = array(
                                        'status' => 501,
                                        'error' => 'failed to insert group guarantors'
                                    );
                                } else {
                                    $notiQuery = "INSERT INTO usernotifications(userId,messagetext,alertId,timeupdated,readflag,notified) VALUES($guarantorId,'$messageNoti',0,'$now',0,0)";
                                    if (!DB::insert($notiQuery)) {
                                        $response = array(
                                            'status' => 501,
                                            'error' => 'failed to insert user notifications'
                                        );
                                    } else {
                                        $response = array(
                                            'status' => 200,
                                            'msg' => 'Guarantor has been added. Contact them to guarantee you.'
                                        );
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $response = array(
                        'status' => 501,
                        'error' => 'Amount must be at least Ksh 10'
                    );
                }

            } else {
                $response = array(
                    'status' => 501,
                    'error' => 'There are missing parameters'
                );
            }
        }
        echo json_encode($response);
    }

    public function add_list_main_guarantors(Request $request)
    {
        $contents = json_decode($request->getContent(), true);
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');
        dd($contents);
        $loan_id = $contents['loanId'];
        $user_id = $contents['userId'];
        $guarantors_list = $contents['guarantors_list'];
        $collection = array();
        for ($i = 0; $i < count($guarantors_list); $i++) {

            $number = $guarantors_list[$i]['number'];
            $amount = $guarantors_list[$i]['amount'];

            $finalPhone = json_decode($this->phoneFormat($number), true);
            $finalPhone = $finalPhone['formatedPhone'];
//			dd($finalPhone['formatedPhone']);
//			fetch first user with ac_type 1 and same number
            if (!$user = DB::table('users')->where('phone', $finalPhone)->where('actype', 1)->first()) {
                $response = array('status' => 501, 'error' => 'failed to fetch user');
            } else {
                $obj = new stdClass();
                $obj->amount = $amount;
                $obj->guarantorId = $user->id;
                $obj->phone = $user->phone;
                $obj->userId = $user_id;
                $obj->loanId = $loan_id;

                array_push($collection, $obj);
            }
        }
        $this->add_guarantor_bulk($collection);
//		pass to function of sending guarantorship requests
        echo json_encode($response);
    }

    function add_guarantor_bulk($details)
    {

        dd($details);
        for ($i = 0; $i < count($details); $i++) {
            $userId = $details[$i]->userId;
            $amount = $details[$i]->amount;
            $guarantorId = $details[$i]->guarantorId;
            $loanId = $details[$i]->loanId;

            if ($guarantorId == $userId) {
                $response = array('status' => 501, 'error' => 'You cannot add yourself as a guarantor');
            } else {
                //check if a guarantor has an outstanding loan
                if ($this->getUserLoanBalance($guarantorId) <= 0) {
                    //check if guarantor has enough unguaranteid savings
                    if ($amount <= $this->getGuarantableAmount($guarantorId)) {
                        //insert into guarantors table
                        if (!mysqli_query($this->link, "INSERT INTO guarantors(guarantorId,guaranteeId,loanapplicationId,guaranteeamount,datesent,dateapproved,status) VALUES($guarantorId,$userId,$loanId,$amount,'$this->now','$this->now',0)")) {
                            echo "0~Error guar" . mysqli_error($this->link);
                        } else {
                            //insert into guarantor messages
                            $notificationMsg = htmlspecialchars($this->getUserName($userId) . " has requested you to guarantee Ksh " . number_format($amount, 2) . " of a finance. To do so, go to 'Financess' then select 'Guarantee a Finance' and confirm. Thank you.", ENT_QUOTES);
                            if (!mysqli_query($this->link, "INSERT INTO usernotifications(userId,messagetext,alertId,timeupdated,readflag,notified) VALUES($guarantorId,'$notificationMsg',0,'$this->now',0,0)")) {
                                echo "0~Error noti " . mysqli_error($this->link);
                            } else {
                                $response = "0~Your request has been submitted. Please wait for confirmation from " . $this->getUserName($guarantorId);
                            }
                        }
                    } else {
                        $response = "0~" . $this->getUserName($guarantorId) . " does not have enough savings to guarantee Ksh " . number_format($amount, 2);
                    }
                } else {
                    $response = "0~" . $this->getUserName($guarantorId) . " has an outstanding loan. Kindly add another guarantor.";
                }
            }
        }
        echo json_encode($response);
    }

//	80
    public function grp_members(Request $request)
    {
        $groupId = $request->get('groupId');
        $memberstatus = $request->get('memberstatus');
        if (isset($groupId) && isset($memberstatus)) {
            $response = "";

            if (!$result = DB::table('groupmembers')->where('groupId', $groupId)->where('status', $memberstatus)->get()) {
                echo json_encode(array('status' => 501, 'error' => 'failed to fetch active group members'));
            } else {
                $bundle = array();
                $count_result = count($result);
                for ($i = 0; $i < $count_result; $i++) {
                    $id = $result[$i]->userId;
                    $member = new stdClass();

                    $member->id = $id;
                    $member->name = $this->getUserName($id);
                    $member->idno = $this->getUserIdNo($id);

                    array_push($bundle, $member);
                }
                if (count($result) > 1) {
                    $response = array('status' => 200, 'data' => $bundle);
                } else {
                    $response = array('status' => 501, 'error' => 'No other group members');
                }

            }

        } else {
            $response = array('status' => 501, 'error' => 'looks like there are some missing params');
        }
        echo json_encode($response);
    }

//	81
    public function confirm_registered_phone(Request $request)
    {
        $userId = $request->get('userId');
        $confirmationcode = $request->get('confirmationcode');

        if (isset($userId) && isset($confirmationcode)) {
            $phoneNo = $this->getUserRegisteredPhone($userId);

            //check if confirmation code match
            if (!$checkConfirm = mysqli_query($this->link, "SELECT * FROM users WHERE id=$userId AND passrecoverycode='$confirmationcode'")) {
                $response = json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                if (count($checkConfirm) > 0)//match
                {
                    $response = "1~Phone has been confirmed. Enjoy our services.~$phoneNo";
                } else {
                    $response = "0~You have entered a wrong confirmation code. Ensure that you do not confuse 0 with letter 'O'";
                }
            }
        } else {
            $response = "0~There are missing parameters";
        }
        echo json_encode($response);
    }

//	83
    public function authenticate_phone(Request $request)
    {
        $userId = $request->get('userId');
        $phone = $request->get('phone');

        if (isset($userId) && isset($phone)) {
            $phoneNo = $this->getUserRegisteredPhone($userId);

            if ($phone == $phoneNo) {
                $response = "1~Phone confirmed $phone => $phoneNo";
            } else {
                $response = "0~The phone $phone is not registered to this account.";
            }

        } else {
            $response = "0~There are missing parameters";
        }
        echo json_encode($response);
    }

//	85
    public function add_nok(Request $request)
    {
        $name = $request->get('name');
        $id = $request->get('id');
        $relationship = $request->get('relationship');
        $contacts = $request->get('contacts');
        $userId = $request->get('userId');

        if (isset($_POST['userId']) && isset($_POST['name']) && isset($_POST['id']) && isset($_POST['relationship']) && isset($_POST['contacts'])) {
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $name = htmlspecialchars($_POST['name'], ENT_QUOTES);
            $id = htmlspecialchars($_POST['id'], ENT_QUOTES);
            $relationship = htmlspecialchars($_POST['relationship'], ENT_QUOTES);
            $contacts = htmlspecialchars($_POST['contacts'], ENT_QUOTES);

            //check if the user has reached maximum number of kins
            if (!$check = mysqli_query($this->link, "SELECT * FROM nextofkindetails WHERE userId = $userId")) {
                $response = json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                if (count($check) >= 5) {
                    $response = "You have added the maximum number of next of Kins";
                } else {
                    //check if kin had been added
                    if (!$checkKin = mysqli_query($this->link, "SELECT * FROM nextofkindetails WHERE userId = $userId AND identifier = '$id'")) {
                        $response = json_encode(array('status' => 501, 'error' => 'database error'));
                    } else {
                        if (count($checkKin) == 0) {
                            if (!mysqli_query($this->link, "INSERT INTO nextofkindetails(kinname,identifier,relationship,contacts,dateadded,userId) VALUES('$name','$id','$relationship','$contacts','$now',$userId)")) {
                                $response = json_encode(array('status' => 501, 'error' => 'database error'));
                            } else {
                                $response = "1~Next of kin has been added";
                            }
                        } else {
                            $response = "0~You have already added this record";
                        }
                    }
                }
            }

        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

//  87
    public function create_target_account(Request $request)
    {
        $response = 0;
        $userId = (int)$request->get('userId');
        $accountname = $request->get('accountname');
        $targetsavings = (int)$request->get('targetsavings');
        $projectprice = (int)$request->get('projectprice');
        $description = $request->get('description');
        $date = $request->get('date');
        $type = $request->get('type');

        //validate target date
        $today = date('d/m/Y');

        if (strtotime($this->convertDate($date)) - strtotime($this->convertDate($today)) >= (7 * 24 * 60 * 60))//must be at least 1 week
        {
            //validate savings target
            if (is_numeric($targetsavings) && $targetsavings >= 1000) {
                //validate savings target
                if (is_numeric($projectprice) && $projectprice >= 1000) {
                    //check if account name exists for this user
                    if ($check = DB::table('accounts')->where('userId', $userId)->where('accountname', $accountname)->where('status', 1)->get()->first()) {
                        $response = array('status' => 504, 'error' => 'account name does not exist');
                    } else {
                        if ($check != null) {
                            $response = array(
                                'status' => 504,
                                'error' => 'you have an active account with the same name'
                            );
                        } else {
                            date_default_timezone_set('Africa/Nairobi');
                            $now = date('Y-m-d H:i:s');
                            $date = $this->convertDate($date);

                            if (!DB::insert("INSERT INTO accounts(accountname, description, savingstarget, projectprice, targetdate, actype, datecreated, dateclosed, lastalerttime, status, userId) VALUES('$accountname', '$description', $targetsavings, $projectprice, '$date', $type, '$now', '$now', '$now', 1, $userId)")) {
                                $response = array(
                                    'status' => 501,
                                    'error' => 'database error'
                                );
                            } else {
                                $response = array(
                                    'status' => 200,
                                    'msg' => 'Your account has been created'
                                );
                            }
                        }
                    }
                } else {
                    $response = array(
                        'status' => 504,
                        'error' => 'Enter a valid project price. It must be at least Ksh 1,000'
                    );
                }
            } else {
                $response = array(
                    'status' => 504,
                    'error' => 'Enter a valid savings target. It must be at least Ksh 1,000'
                );
            }
        } else {
            $response = array(
                'status' => 506,
                'error' => 'Target date must be at least after one week'
            );

        }

        return json_encode($response);
    }

//	87.5
    public function create_school_fees_account(Request $request)
    {
        $response = 0;
        $user_id = $request->get('user_id');
        $child_name = $request->get('child_name');
        $adm_number = $request->get('adm_number');
        $school_name = $request->get('school_name');
        $bank_name = $request->get('bank_name');
        $branch_name = $request->get('branch_name');
        $cp_phone = $request->get('cp_phone');
        $ac_number = $request->get('account_number');
        $description = 'School fees account';
        //validate target date
        $today = date('d/m/Y');
        //check if account name exists for this user
        $check = DB::table('accounts')->where('userId', $user_id)->where('accountname', $child_name)->where('status', 1)->get()->first();

        if ($check != null) {
            $response = array(
                'status' => 504,
                'error' => 'you have an active account with the same name'
            );
        } else {
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');
            $date = $this->convertDate($today);

            if (!DB::insert("INSERT INTO accounts(accountname, description, savingstarget, projectprice, targetdate, actype, datecreated, dateclosed, lastalerttime, status, userId) VALUES('$child_name', '$description', 0, 0, '$date', 4, '$now', '$now', '$now', 1, $user_id)")) {
                $response = array(
                    'status' => 501,
                    'error' => 'database error'
                );
            } else {
                $last_id = DB::table("accounts")->select(array('accountId'))->orderBy('accountId', 'DESC')->limit(1)->get();
                $last_id = $last_id[0]->accountId;
                if (!$school_fees_account = mysqli_query($this->link, "INSERT INTO school_fees_accounts(account_id, student_name, adm_number, school_name, bank_name, bank_branch, ac_number, status, amount, bursar_phone, user_id, date_created) VALUES( $last_id, '$child_name', '$adm_number', '$school_name', '$bank_name', '$branch_name', $ac_number, 1, 0, '$cp_phone', $user_id, '$now')")) {
                    $response = array(
                        'status' => 501,
                        'error' => 'failed to fill school account record' . mysqli_error($this->link)
                    );
                } else {
                    $response = array(
                        'status' => 200,
                        'msg' => 'Your account has been created!'
                    );
                }
            }
        }

        return json_encode($response);
    }

//	88
    public function get_user_accounts()
    {
        $user_id = Input::get('user_id');
        $ac_type = Input::get('ac_type');

        if (isset($user_id) && isset($ac_type)) {

            if (!$result = DB::table('accounts')->where('userId', $user_id)->where('status', 1)->where('actype', $ac_type)->get()) {
                echo json_encode(array('status' => 501, 'error' => 'no accounts where found'));
            } else {
                echo json_encode($result);
            }

        } else {
            echo json_encode(array('status' => 501, 'error' => 'please contact admin first'));
        }

    }

//	89
    public function account_balance(Request $request)
    {
        $userId = $request->get('userId');
        $accountId = $request->get('accountId');
        if (isset($userId) && isset($accountId)) {
            $response = array(
                'status' => 200,
                'msg' => 'Account balance: Ksh ',
                'data' => number_format($this->getTargetSavingsBalance($accountId), 2)
            );
        } else {
            $response = array('status' => 501, 'error' => 'missing params in request');
        }
        echo json_encode($response);
    }

//	90
    public function withdraw_from_target(Request $request)
    {
        $accountId = $request->get('accountId');
        $amount = $request->get('amount');
        $userId = $request->get('userId');
        $option = $request->get('option');
        if (isset($userId) && isset($accountId) && isset($amount) && isset($option)) {
            $response = "";
            $withdrawto = $option;

            $response = "";

            if (is_numeric($amount) && $amount >= 1000) {
                //check if target date is due

                $qryWith = "SELECT * FROM accounts WHERE accountId = $accountId AND userId = $userId";
                if (!$getAccount = mysqli_query($this->link, $qryWith)) {
                    $response = json_encode(array('status' => 501, 'error' => 'database error')) . " > ";
                } else {
                    if (count($getAccount) > 0) {
                        $getAccountRow = mysqli_fetch_array($getAccount, MYSQLI_ASSOC);

                        date_default_timezone_set('Africa/Nairobi');
                        $today = date('Y-m-d');
                        $now = date('Y-m-d H:i:s');

                        if (strtotime($getAccountRow['targetdate']) <= strtotime($today)) {
                            //check if balance is sufficient
                            $bal = $this->getTargetSavingsBalance($accountId);
                            if ($bal >= $amount) {
                                //lock account to prevent silultaneous transactions
                                $lockFile = 'targetlock_' . $userId . '.txt';
                                if (!file_exists($lockFile)) {
                                    $handle = fopen($lockFile, 'w');
                                    fwrite($handle, $now);
                                    fclose($handle);

                                    if (withdrawFromTarget($accountId, $amount, $withdrawto, $userId) == 1) {
                                        $smsMessage = "";

                                        if ($withdrawto == 1)//withdraw to wallet
                                        {
                                            //deposit to wallet
                                            if (makeWithdrawableDeposit($amount, $userId, 11) == 1) {
                                                $smsMessage = "Ksh " . number_format($amount, 2) . " has been transferred from " . getTargetAccountName($accountId) . " account to your Homepesa Wallet. Thank you.";
                                                $response = "1~Amount has been transferred to wallet";
                                            }
                                        } else//sacco financing
                                        {
                                            if (!mysqli_query($this->link, "INSERT INTO `saccofinance`(`amount`, `dateapplied`, `dateapproved`, `userId`, `accountId`, `status`) VALUES ($amount,'$now','$now',$userId,$accountId,0)")) {
                                                $response = json_encode(array(
                                                    'status' => 501,
                                                    'error' => 'database error'
                                                ));
                                            } else {
                                                $smsMessage = "Ksh " . number_format($amount, 2) . " has been transferred from " . getTargetAccountName($accountId) . " account to your Sacco Financing a/c. We will get in touch with you shortly. Thank you.";
                                                echo "1~Amount has been transferred to wallet";
                                            }
                                        }

                                        //get user phone
                                        $userIdVals = json_decode($this->getUserDetails($userId), true);
                                        $this->sendMobileSasa($userIdVals['phone'], $smsMessage, '');
                                    } else {
                                        $response = "0~Unable to withdraw. Try again later";
                                    }

                                    unlink($lockFile);
                                } else {
                                    $response = "0~Transaction is currently underway. Try again later";
                                }
                            } else {
                                $response = "0~Insufficient balance. Current balance is Ksh " . number_format($bal, 2);
                            }
                        } else {
                            $response = "0~Your target date " . formatDate($getAccountRow['targetdate']) . " is not yet due";
                        }
                    } else {
                        $response = array('status' => 501, 'error' => 'Account not found');
                    }
                }
            } else {
                $response = array('status' => 501, 'error' => 'Amount must be at least Ksh 1,000');
            }
        } else {
            $response = array('status' => 501, 'error' => 'There were missing params in request');
        }
        echo json_encode($response);
    }

//  91
    public function create_fundraising_account(Request $request)
    {
        $userId = $request->get('userId');
        $accountname = $request->get('accountname');
        $targetsavings = $request->get('targetsavings');
        $description = $request->get('description');
        $date = $request->get('date');
        $cause = $request->get('cause');

        if (isset($_POST['userId']) && isset($_POST['accountname']) && isset($_POST['targetsavings']) && isset($_POST['cause']) && isset($_POST['description']) && isset($_POST['date'])) {
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $acname = htmlspecialchars($_POST['accountname'], ENT_QUOTES);
            $cause = htmlspecialchars($_POST['cause'], ENT_QUOTES);
            $targetDate = htmlspecialchars($_POST['date'], ENT_QUOTES);
            $targetamount = htmlspecialchars($_POST['targetsavings'], ENT_QUOTES);
            $description = htmlspecialchars($_POST['description'], ENT_QUOTES);

            //validate target date
            $today = date('Y-m-d');

            if (strtotime($targetDate) - strtotime($today) >= (1 * 24 * 60 * 60))//must be at least 1 week
            {
                //validate savings target
                if (is_numeric($targetamount) && $targetamount >= 1000) {
                    //check if account name exists for this user
                    if (!$check = mysqli_query($this->link, "SELECT * FROM fundraisingaccounts WHERE userId = $userId AND accountname = '$acname' AND status = 1")) {
                        echo json_encode(array('status' => 501, 'error' => 'database error'));
                    } else {
                        if (count($check) > 0) {
                            echo "0~You have an active account with the same name";
                        } else {
                            date_default_timezone_set('Africa/Nairobi');
                            $now = date('Y-m-d H:i:s');

                            if (!mysqli_query($this->link, "INSERT INTO `fundraisingaccounts`(`accountname`, `description`, `targetamount`, `targetdate`, `datecreated`, `cause`, `userId`, `status`) VALUES ('$acname','$description',$targetamount,'$targetDate','$now',$cause,$userId,1)")) {
                                echo json_encode(array('status' => 501, 'error' => 'database error'));
                            } else {
                                //send sms to user
                                $userDetails = json_decode(getUserDetails($userId), true);
                                $message = htmlspecialchars_decode($acname, ENT_QUOTES) . " account has been created. People can send funds to your account using pay bill number 667872 and account number F" . mysqli_insert_id($this->link) . ". Thank you.";
                                $this->sendMobileSasa($userDetails['phone'], $message, 'HOMEPESA');
                                echo "0~Account has been created";
                            }
                        }
                    }
                } else {
                    echo "0~Enter a valid target amount. It must be at least Ksh 1,000";
                }
            } else {
                echo "0~Target date must be at least after one day";
            }
        } else {
            $response = "0~There are missing parameters";
        }

        echo json_encode($response);
    }

//	93
    public function account_balance_fundraising(Request $request)
    {
        $userId = $request->get('userId');
        $accountId = $request->get('accountId');

        if (isset($_POST['userId']) && isset($_POST['accountId'])) {
            $response = "";
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $accountId = htmlspecialchars($_POST['accountId'], ENT_QUOTES);

            $response = "1~Account balance: Ksh " . number_format(fundRaisingBalance($accountId), 2);
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

//  94
    public function withdraw_from_fundraising(Request $request)
    {
        $accountId = $request->get('accountId');
        $amount = $request->get('amount');
        $userId = $request->get('userId');

        if (isset($_POST['userId']) && isset($_POST['accountId']) && isset($_POST['amount'])) {
            $response = "";
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $accountId = htmlspecialchars($_POST['accountId'], ENT_QUOTES);
            $amount = htmlspecialchars($_POST['amount'], ENT_QUOTES);

            if (is_numeric($amount) && $amount >= 100) {
                //check if target date is due
                if (!$getAccount = mysqli_query($this->link, "SELECT * FROM fundraisingaccounts WHERE accountId = $accountId AND userId = $userId")) {
                    echo json_encode(array('status' => 501, 'error' => 'database error'));
                } else {
                    if (count($getAccount) > 0) {
                        $getAccountRow = mysqli_fetch_array($getAccount, MYSQLI_ASSOC);

                        date_default_timezone_set('Africa/Nairobi');
                        $now = date('Y-m-d H:i:s');

                        $chargesLevied = $amount * 3 / 100;

                        //check if balance is sufficient
                        $bal = fundRaisingBalance($accountId);
                        if ($bal >= ($amount + $chargesLevied)) {
                            //lock account to prevent silultaneous transactions
                            $lockFile = 'fundraisinglock_' . $userId . '.txt';
                            if (!file_exists($lockFile)) {
                                $handle = fopen($lockFile, 'w');
                                fwrite($handle, $now);
                                fclose($handle);

                                //withdraw funds plus charges
                                if (!mysqli_query($this->link, "INSERT INTO `fundraisingtransactions`(`accountId`, `drcrflag`, `amount`, `reference`, `txntime`) VALUES ($accountId,2,$amount,'','$now'),($accountId,2,$chargesLevied,'','$now')")) {
                                    echo json_encode(array('status' => 501, 'error' => 'database error'));
                                } else {
                                    //send funds for approval
                                    if (!mysqli_query($this->link, "INSERT INTO `fundraisingwithdrawals`(`amount`, `charges`, `userId`, `status`, `approvedby`, `dateplaced`, `dateapproved`) VALUES ($amount,$chargesLevied,$userId,0,0,'$now','$now')")) {
                                        echo json_encode(array('status' => 501, 'error' => 'database error'));
                                    } else {
                                        $smsMessage = "Ksh " . number_format($amount, 2) . " will be transferred from " . getFundraisingAccountName($accountId) . " account to your Homepesa Wallet after approval. Thank you.";

                                        $userIdVals = json_decode(getUserDetails($userId), true);
                                        $this->sendMobileSasa($userIdVals['phone'], $smsMessage, '');

                                        echo "1~Amount has been withdrawn awaiting approval.";
                                    }
                                }

                                unlink($lockFile);
                            } else {
                                echo "0~Transaction is currently underway. Try again later";
                            }
                        } else {
                            echo "0~Insufficient balance. Current balance is Ksh " . number_format($bal, 2) . " and a charge of Ksh " . (number_format($chargesLevied, 2)) . " will be levied.";
                        }
                    } else {
                        echo "0~Account not found";
                    }
                }
            } else {
                echo "0~Amount must be at least Ksh 1,000";
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

//	95
    public function mpesa_to_target_savings(Request $request)
    {
        $userId = $request->get('userId');
        $amount = $request->get('amount');
        $accountId = $request->get('accountId');

        if (isset($userId) && isset($amount) && isset($accountId)) {

            if (is_numeric($amount) && $amount > 0) {
                //check if user has enough amount on mpesa holding account
                $withAmount = $this->getWithdrawableHoldingAmount($userId);

                if ($withAmount >= $amount) {
                    //handle operation locking - only one withdrawable operation can take place at a time
                    $lockFile = 'admin/attachments/' . $userId . '.txt';
                    if (!file_exists($lockFile)) {
                        //create lock file
                        $handle = fopen($lockFile, 'a');
                        fwrite($handle, 'operation underway');
                        fclose($handle);

                        $now = date('Y-m-d H:i:s');
                        $description = 'Transfer to target savings a/c';

                        $transResult = $this->withdrawHoldingFunds($amount, $userId, 6, $description);

                        if ($transResult == 1)//success
                        {
                            //send to savings
                            if (!mysqli_query($this->link, "INSERT INTO `accounttransactions`(`accountId`, `drcrflag`, `amount`, `reference`, `txntime`, `savingsoption`) VALUES 
									($accountId,1,$amount,'','$now',3)")) {
                                $response = "0~Transfer to savings failed. Please try again later.";
                            } else {
                                $response = "1~Transfer to savings is successful. Please check your balance";
                            }
                        } else {
                            $response = "0~Transfer to savings failed. Please try again later.";
                        }

                        //delete lock file
                        unlink($lockFile);
                    } else {
                        $response = "0~Another operation is being performed on this account. Please try again later.";
                    }
                } else {
                    $response = "0~You have insufficient balance. Your account balance is Ksh " . number_format($withAmount, 2);
                }
            } else {
                $response = "0~Amount must be greater than 0";
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

    /*	public function akiba_withdrawal(){

		}*/
    public function user_reports(Request $request)
    {
        $userId = $request->get('user_id');
//		$this->get_deposits_report();
//		$this->get_airtime_report();
//		$this->get_withdrawals_report();

    }

    public function user_school_accounts(Request $request)
    {
        $user_id = $request->get('user_id');
        $amount = $request->get('amount');

        $times = 3;
        $savings = 0;
        $response = array();

        if (!$savvals = DB::table('savings')->where('userId', $user_id)->orderBy('savingsId', 'DESC')->get()) {
            echo json_encode(array('status' => 501, 'error' => 'database error'));
        } else {
            $num = 1;
            for ($i = 0; $i < count($savvals); $i++) {
                $savings += $savvals[$i]->amount;
                $num++;
            }
        }
//		check if user has any school fees accounts.
        if (!$accounts = mysqli_query($this->link, "SELECT count(userId) AS count FROM accounts WHERE actype = 4 AND userId = $user_id")) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'You don\'t have any school fees accounts with us. To apply for school fees financing, create a school fees account on the app.'
            ));
        }
        $savings = ($savings * $times);

        if ($savings < $amount) {
            $response = array(
                'status' => 501,
                'error' => 'Your school fees account limit is ' . number_format($savings, 2)
            );
        } else if (mysqli_num_rows($accounts) < 1) {
            $response = array(
                'status' => 501,
                'error' => 'You can only borrow a school fees loan on an school fees account you have registered on the main menu. Proceed to register one'
            );
        } else {
            $response = array(
                'status' => 501,
                'error' => 'Please proceed to school fees accounts, select an account and apply for the school fees loan.'
            );
        }

        echo json_encode($response);
    }

    public function list_school_accounts(Request $request)
    {
        $user_id = $request->get('user_id');
        if (!$accounts = mysqli_query($this->link, "SELECT accountId,accountname FROM accounts WHERE actype = 4 AND userId = $user_id")) {
            $response = array('status' => 501, 'error' => 'You don\'t have any school accounts with us');
        } else {
            $response = mysqli_fetch_array($accounts, MYSQLI_ASSOC);
        }
        echo json_encode($response);
    }

    //  ONE
    public function loan_eligibility(Request $request)
    {
        $user_id = $request->get('userId');
        if (isset($_POST['userId'])) {
            //values
            $savings = 0;
            $response = "";

            if (!$savvals = DB::table('savings')->where('userId', $user_id)->orderBy('savingsId', 'DESC')->get()) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                for ($i = 0; count($savvals); $i++) {
                    $savings += $savvals[$i]->amount;
                }
            }

            if ($this->getMembershipDuration($user_id) >= 90) {
                $response = array(
                    'status' => 200,
                    'eligible_instant' => number_format(($savings + $this->getInstantLoanLimit($user_id)), 2),
                    'eligible_secured' => number_format($savings * 3, 2)
                );
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

    public function loan_balance(Request $request)
    {
        $user_id = $request->get('userId');

        if (isset($user_id)) {
            $userid = htmlspecialchars($_POST['userId'], ENT_QUOTES);

            $totalLoans = 0;
            $totalPayments = 0;

            $response = "";

            //get total loans
            if (!$loans = DB::table('loans')->where('approvedflag', 1)->where('userId', $user_id) - get()) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
                die;
            } else {
                while ($loansrow = mysqli_fetch_array($loans, MYSQLI_ASSOC)) {
                    $totalLoans += $loansrow['amount'];
                }
            }

            //get total payments
            if (!$loansPayments = DB::table('loanpayments')->where('userId', $user_id)->get()) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
                die;
            } else {
                while ($loansPaymentsrow = mysqli_fetch_array($loansPayments, MYSQLI_ASSOC)) {
                    $totalPayments += $loansPaymentsrow['amount'];
                }
            }

            /* list loans */
            //get user loans that are approved
            if (!$loans = DB::table('loans')->where('userId', $user_id)->where('approvedflag', 1)->where('interestflag', 0)->get()) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
                die;
            } else {
                $loanNum = 1;
                while ($loansrow = mysqli_fetch_array($loans, MYSQLI_ASSOC)) {
                    /* start get loan balance */
                    $loanId = $loansrow['loanId'];
                    $loanAmount = $loansrow['amount'];

                    //loan type
                    $loanTypeName = $this->getLoanType($loansrow['loantype']);

                    $total = 0;

                    //get total loan repayments
                    if (!$loanRepayment = mysqli_query($this->link, "SELECT SUM(amount) AS totalrep FROM loanpayments WHERE loanId=$loanId")) {
                        echo json_encode(array('status' => 501, 'error' => 'database error'));
                        die;
                    } else {
                        $loanRepaymentRow = mysqli_fetch_array($loanRepayment, MYSQLI_ASSOC);

                        if (!empty($loanRepaymentRow['totalrep'])) {
                            $total = $loanRepaymentRow['totalrep'];
                        }
                    }

                    if ($loanAmount > $total)//if user has not made all the payments
                    {
                        $dueDate = date('Y-m-d H:i:s', strtotime($loansrow['dateapproved']) + ($loansrow['period'] * 30 * 24 * 60 * 60));
                        $response .= $loanNum . ". " . $loanTypeName . ": Ksh " . number_format(getSpecificLoanBalance($loanId), 2) . " (Due date: $dueDate)~~";
                        $loanNum++;
                    }
                    /* end get loan balance */
                }
            }
            $response = array(
                'status' => 501,
                'error' => 'Total: Ksh ' . number_format(($totalLoans - $totalPayments), 2)
            );
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

//	8
    public function list_loans(Request $request)
    {
        $user_id = $request->get('userId');
        if (isset($user_id)) {
            $response = "";

            //get user loans that are approved

            if (!$loans = mysqli_query($this->link, "SELECT * FROM loans WHERE userId=$user_id AND approvedflag=1 AND interestflag=0")) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                $collection = array();
                while ($loansrow = mysqli_fetch_array($loans, MYSQLI_ASSOC)) {
                    /* start get loan balance */
                    $loanId = $loansrow['loanId'];
                    $loanAmount = $loansrow['amount'];
                    //loan type
                    $loanTypeName = $this->getLoanType($loansrow['loantype']);

                    if ($this->getSpecificLoanBalance($loanId) > 0)//if user has not made all the payments
                    {
                        $obj = new stdClass();
                        $obj->id = $loanId;
                        $obj->description = $loansrow['applidescription'];
                        $obj->loan_balance = number_format($this->getSpecificLoanBalance($loanId));
                        $obj->currency = 'KES';

                        if ($obj->loan_balance > 0) {
                            array_push($collection, $obj);
                        }
                    }
                    /* end get loan balance */
                }
                $response = array(
                    'status' => 200,
                    'pending_loans' => $collection,
                    'loans_count' => count($collection)
                );
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

    public function load_unconfirmed_loans(Request $request)
    {
        $user_id = $request->get('userId');
        if (isset($user_id)) {
            $response = "";

            //get card records
            if (!$addedCards = mysqli_query($this->link, "SELECT * FROM cards WHERE userId=$user_id AND cardstatus=0")) {
                echo "0~Error unconfirmed " . mysqli_error($this->link);
            } else {
                while ($addedCardsrow = mysqli_fetch_array($addedCards, MYSQLI_ASSOC)) {
                    $response .= $addedCardsrow['cardId'] . "~" . htmlspecialchars_decode($addedCardsrow['carddisplayno'], ENT_QUOTES) . "`";
                }
            }

            $response = trim($response, "`");
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

    public function load_all_cards(Request $request)
    {
        $user_id = $request->get('userId');
        if (isset($_POST['userId'])) {
            $response = "";
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);

            //get card records
            if (!$addedCards = mysqli_query($this->link, "SELECT * FROM cards WHERE userId=$userId  AND cardstatus <> 3")) {
                echo "0~Error unconfirmed " . mysqli_error($this->link);
            } else {
                while ($addedCardsrow = mysqli_fetch_array($addedCards, MYSQLI_ASSOC)) {
                    $response .= $addedCardsrow['cardId'] . "~" . htmlspecialchars_decode($addedCardsrow['carddisplayno'], ENT_QUOTES) . "`";
                }
            }

            $response = trim($response, "`");
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
    }

//	20
    public function get_user_profile(Request $request)
    {

        $user_id = $request->get('user_id');
        if (isset($user_id)) {
            $profile = DB::table('users')->where('id', $user_id)->first();
            if (!$profile) {
                echo json_encode(array('status' => 501, 'error' => 'failed to fetch user profile'));
            } else {
                echo json_encode($profile);
            }
        } else {
            echo json_encode(array('status' => 501, 'error' => 'server could not find requirements'));
        }
    }

    public function get_mpesa_transactions(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($_POST['userId'])) {
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $response = "1~<h2>Last 50 Mpesa Transactions</h2>";

            //get last 50 transactions
            if (!$transactions = mysqli_query($this->link, "SELECT * FROM mpesatransactions WHERE userId=$userId ORDER BY transactionId DESC")) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                $num = 0;

                while ($transactionsrow = mysqli_fetch_array($transactions, MYSQLI_ASSOC)) {
                    $num++;

                    $transactionstatuscode = $transactionsrow['transactionstatus'];
                    $txStatus = "<b>Failed</b>";
                    switch ($transactionstatuscode) {
                        case '1':
                            if ((strtotime($now) - strtotime($transactionsrow['transactiontime'])) > 86400) {
                                $txStatus = "<b>Failed</b>";
                            } else {
                                $txStatus = "<b>Pending</b>";
                            }
                            break;

                        case '2':
                            $txStatus = "<b>Success</b>";
                            break;

                        default:
                            # code...
                            break;
                    }
                    if ($num <= 50) {
                        $response .= "<i><b>Transaction $num</b></i><br/><b>Phone:</b> " . $transactionsrow['phone'] . "<br/><b>Amount:</b> " . number_format($transactionsrow['amount'], 2) . "<br/><b>Date: </b>" . formatDateTime($transactionsrow['transactiontime']) . "<br/><b>Status:</b> " . $txStatus . "<br/><br/>";
                    } else {
                        break;
                    }
                }
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
    }

    public function get_card_transactions(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($_POST['userId'])) {
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $response = "1~<h2>Last 50 Card Transactions</h2>";

            //get last 50 transactions
            if (!$transactions = mysqli_query($this->link, "SELECT * FROM twoparty WHERE userId=$userId AND responsecode = '0' AND paymenttype <> 0 ORDER BY twopartyId DESC")) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                if (count($transactions) > 0) {
                    $num = 0;
                    while ($transactionsrow = mysqli_fetch_array($transactions, MYSQLI_ASSOC)) {
                        $num++;
                        if ($num <= 50) {
                            $response .= "<b>Transaction $num</b><br/>Amount: " . number_format($transactionsrow['customeramount'], 2) . "<br/>Card: " . $transactionsrow['cardno'] . "<br/>Date: " . formatDateTime($transactionsrow['txnTime']) . "<br/><br/>";
                        } else {
                            break;
                        }
                    }
                } else {
                    $response = "1~<p>There are no card transactions</p>";
                }
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
    }

    public function withadrawable_balance(Request $request)
    {
        $userId = $request->get('userId');
        $holdingorwallet = (int)$request->get('holdingorwallet');

        if (isset($userId) && isset($holdingorwallet)) {
            if ($holdingorwallet == 1) {
                $response = array(
                    'status' => 200,
                    'msg' => 'Your Mpesa Holding a/c balance is Ksh ',
                    'amt' => number_format($this->getWithdrawableHoldingAmount($userId), 2)
                );
            } else {
                $response = array(
                    'status' => 200,
                    'msg' => 'Your account balance is Ksh ',
                    'amt' => number_format($this->getWithdrawableAmount($userId), 2)
                );
            }

        } else {
            $response = array('status' => 501, 'msg' => 'There are missing parameters in your request');
        }

        echo json_encode($response);
    }

//  27
    public function get_withdrawals_report(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($user_id)) {

            //get total withdrawals
            if (!$withdrs = mysqli_query($this->link, "SELECT SUM(amount) as ingia FROM withdrawals WHERE userId=$user_id")) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                $withdrsrow = mysqli_fetch_array($withdrs, MYSQLI_ASSOC);

                //get last 50 transactions
                if (!$withdrawalTrans = mysqli_query($this->link, "SELECT * FROM withdrawals WHERE userId=$user_id ORDER BY withdrawalId DESC")) {
                    echo json_encode(array('status' => 501, 'error' => 'database error'));
                } else {
                    $collection = array();
                    if (mysqli_num_rows($withdrawalTrans) > 0) {
                        $count = 0;

                        while ($withdrawalTransrow = mysqli_fetch_array($withdrawalTrans, MYSQLI_ASSOC)) {
                            $count++;

                            if ($count <= 50) {
                                $obj = new stdClass();
                                $obj->count = $count;
                                $obj->amount = number_format($withdrawalTransrow['amount'], 2);
                                $obj->method = $this->getWithdrawalPurpose($withdrawalTransrow['purpose']);
                                $obj->description = $withdrawalTransrow['description'];
                                $obj->date = $this->formatDateTime($withdrawalTransrow['withdrawaldate']);
                            }
                            array_push($collection, $obj);
                        }

                    }
                    $response = array(
                        'status' => 200,
                        'msg' => 'Your account balance is Ksh ',
                        'tx' => $collection,
                        'tx_count' => count($collection),
                        'amt' => (isset($withdrsrow['ingia']) ? number_format($withdrsrow['ingia'], 2) : '0.00')
                    );
                }
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }

        echo json_encode($response);
    }

//  28
    public function get_deposits_report(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($user_id)) {

            //get total deposits
            if (!$deposts = mysqli_query($this->link, "SELECT SUM(amount) as toa FROM deposits WHERE userId=$user_id")) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                $depostsrow = mysqli_fetch_array($deposts, MYSQLI_ASSOC);

                //get last 50 transactions
                if (!$depositTrans = mysqli_query($this->link, "SELECT * FROM deposits WHERE userId=$user_id ORDER BY depositId DESC")) {
                    echo json_encode(array('status' => 501, 'error' => 'database error'));
                } else {
                    if (mysqli_num_rows($depositTrans) > 0) {
                        $count = 0;
                        $collection = array();
                        while ($depositTransrow = mysqli_fetch_array($depositTrans, MYSQLI_ASSOC)) {
                            $count++;

                            if ($count <= 50) {
                                $obj = new stdClass();
                                $obj->i = $count;
                                $obj->amount = number_format($depositTransrow['amount'], 2);
                                $obj->deposit_method = $this->getDepositMethod($depositTransrow['depositmethod']);
                                $obj->date = $this->formatDateTime($depositTransrow['depositdate']);

                                array_push($collection, $obj);
                            }
                        }
                        $response = array(
                            'status' => 200,
                            'deposit_transactions' => $collection,
                            'count' => count($collection),
                            'total_amount' => (isset($depostsrow['toa']) ? number_format($depostsrow['toa'], 2) : '0.00')
                        );
                    }
                }
            }
        } else {
            $response = array('status' => 501, 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

//  29
    public function get_airtime_report(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($user_id)) {

            $response = "";

            //get total deposits
            if (!$airtime = mysqli_query($this->link, "SELECT SUM(amount) as toa FROM airtime WHERE userId=$user_id AND status='success'")) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                $airtimerow = mysqli_fetch_array($airtime, MYSQLI_ASSOC);

                //get last 50 transactions
                if (!$airtimeTrans = mysqli_query($this->link, "SELECT * FROM airtime WHERE userId=$user_id ORDER BY airtimeId DESC")) {
                    echo json_encode(array('status' => 501, 'error' => 'database error'));
                } else {
                    $tx_collections = array();
                    if (mysqli_num_rows($airtimeTrans) > 0) {
                        $count = 0;
                        while ($airtimeTransrow = mysqli_fetch_array($airtimeTrans, MYSQLI_ASSOC)) {
                            $count++;

                            if ($count <= 50) {
                                $transaction = new stdClass();
                                $transaction->count = $count;
                                $transaction->phone = $airtimeTransrow['phone'];
                                $transaction->status = ucfirst($airtimeTransrow['status']);
                                $transaction->date = formatDateTime($airtimeTransrow['transactiontime']);

                                /*	$response .= "<b><i>Transaction $count</i></b> <br/><b>Phone:</b> "
												 . $airtimeTransrow['phone']
												 . "<br/><b>Amount:</b> ". number_format( $airtimeTransrow['amount'], 2 )
												 . "<br/><b>Status:</b> ". ucfirst( $airtimeTransrow['status'] )
												 . "<br/><b>Date: </b>" . formatDateTime( $airtimeTransrow['transactiontime'] ) . "<br/><br/>";*/
                                array_push($tx_collections, $transaction);
                            }
                        }
                    }
                    $response = array(
                        'status' => 200,
                        'tx' => $tx_collections,
                        'tx_count' => count($tx_collections),
                        'amount' => (isset($airtimerow['toa']) ? number_format($airtimerow['toa'], 2) : '0.00')
                    );
                }

            }

        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

    public function get_shares_report(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($_POST['userId'])) {
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);

            $response = "1~<h2>Last 50 shares transactions</h2>";

            //get total deposits
            if (!$shares = mysqli_query($this->link, "SELECT SUM(amount) as toa FROM shares WHERE userId=$userId")) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                $sharesrow = mysqli_fetch_array($shares, MYSQLI_ASSOC);

                //get last 50 transactions
                if (!$shareTrans = mysqli_query($this->link, "SELECT * FROM shares WHERE userId=$userId ORDER BY shareId DESC")) {
                    echo json_encode(array('status' => 501, 'error' => 'database error'));
                } else {
                    if (mysqli_num_rows($shareTrans) > 0) {
                        $count = 0;
                        while ($shareTransrow = mysqli_fetch_array($shareTrans, MYSQLI_ASSOC)) {
                            $count++;

                            if ($count <= 50) {
                                $response .= "<b><i>Transaction $count</i></b> <br/><b>Amount:</b> " . number_format($shareTransrow['amount'], 2) . "<br/><b>Date:</b> " . formatDateTime($shareTransrow['datetimemade']) . "<br/><b>Purchase Method: </b>" . sharesSource($shareTransrow['source']) . "<br/><br/>";
                            }
                        }
                    } else {
                        $response = "1~";
                    }

                    $response .= "<p><b>TOTAL: " . (isset($sharesrow['toa']) ? number_format($sharesrow['toa'], 2) : '0.00') . "</b></p>";
                }

            }

        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
    }

    public function get_banking_reports(Request $request)
    {

    }

    public function check_for_notifications(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($_POST['userId'])) {
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $response = "";

            if (!$notifications = mysqli_query($this->link, "SELECT * FROM usernotifications WHERE userId=$userId AND notified=0")) {
                echo json_encode(array('status' => 501, 'error' => 'query execution error'));
            } else {
                while ($notificationsrow = mysqli_fetch_array($notifications, MYSQLI_ASSOC)) {
                    $notiId = $notificationsrow['notificationId'];

                    $response .= $notiId . "~" . substr(htmlspecialchars_decode($notificationsrow['messagetext'], ENT_QUOTES), 0, 20) . " ...`";

                    //update notification
                    if (!mysqli_query($this->link, "UPDATE usernotifications SET notified=1 WHERE notificationId=$notiId")) {
                        echo json_encode(array('status' => 501, 'error' => 'query execution error'));
                    }
                }
            }

            $response = trim($response, "`");
        } else {
            $response = "There are missing noti parameters";
        }
    }

//  61
    public function group_savings(Request $request)
    {
        $group_id = $request->get('group_id');
        if (isset($group_id)) {

            $savings = 0;
            if (!$savvals = DB::table('groupsavings')->where('groupId', $group_id)->orderBy('savingId', 'DESC')->get()) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                for ($i = 0; $i < count($savvals); $i++) {
                    $savings += $savvals[$i]->amount;
                }
            }
            $response = array('status' => 200, 'total_savings' => $savings, 'transactions' => $savvals);
        } else {
            $response = array('status' => 501, 'error' => 'looks like there are some missing params');
        }

        echo json_encode($response);
    }

    public function list_group_loans(Request $request)
    {
        $group_id = $request->get('group_id');
        if (isset($_POST['groupId'])) {
            $groupId = htmlspecialchars($_POST['groupId'], ENT_QUOTES);
            $response = "";

            //get user loans that are approved
            if (!$loans = mysqli_query($this->link, "SELECT * FROM admingrouploanapplication WHERE status=1 AND interestflag=0 AND groupId=$groupId")) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                while ($loansrow = mysqli_fetch_array($loans, MYSQLI_ASSOC)) {
                    /* start get loan balance */
                    $loanId = $loansrow['applicationId'];
                    $loanAmount = $loansrow['amount'];
                    $loanBalance = getSpecificAdminGroupLoanBalance($loanId);

                    if ($loanBalance > 0)//if user has not made all the payments
                    {
                        $response .= $loanId . "~" . "Ksh " . number_format($loanBalance, 2) . "`";
                    }
                    /* end get loan balance */
                }
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
    }

//	69
    public function group_members(Request $request)
    {
        $group_id = $request->get('group_id');
        if (isset($group_id)) {
            $response = "";

            if (!$result = DB::table('groupmembers')->where('groupId', $group_id)->where('status', 1)->get()) {
                echo json_encode(array('status' => 501, 'error' => 'query execution error'));
                die;
            } else {
                for ($i = 0; $i < count($result); $i++) {
                    $id = $result[$i]->userId;

                    $user = new stdClass();

                    $user->name = $this->getUserName($id);
                    $user->idno = $this->getUserIdNo($id);
                    $user->phone = $this->getUserRegisteredPhone($id);
                    $user->date = $this->formatDateTime($result[$i]->dateadded);

                    $response[$i] = $user;
                }
            }
        } else {
            $response = array('status' => 501, 'error' => 'missing params in request');
        }
        echo json_encode($response);
    }

//	71
    public function group_loan_balances(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($user_id)) {
            $response = "";

            if (!$result = DB::table('admingrouploanapplication')->where('status', 1)->where('interestflag', 0)->get()) {
                echo json_encode(array('status' => 501, 'error' => 'query execution error'));
            } else {
                //get groups to which member is registered
                $groupArray = array();
                if (!$memberGroups = DB::table('groupmembers')->where('userId', $user_id)->get()) {
                    echo json_encode(array('status' => 501, 'error' => 'query execution error'));
                } else {
                    for ($i = 0; count($memberGroups); $i++) {
                        $groupArray[] = $memberGroups[$i]->groupId;
                    }
                }
                $result_count = count($result);
                for ($j = 0; $j < $result_count; $j++) {

                    $groupId = $result[$j]->groupId;
                    if (in_array($groupId, $groupArray))//if member is in group
                    {
                        $loanBalance = $this->getSpecificAdminGroupLoanBalance($result[$j]->applicationId);
                        if ($loanBalance > 0) {

                            $response = array(
                                'status' => 200,
                                'name' => $this->getUserName($result[$j]->groupId),
                                'applied_amount' => number_format($result[$j]->amount, 2),
                                'balance' => number_format($loanBalance, 2),
                                'description' => $result[$j]->description,
                                'date_applied' => $this->formatDateTime($result[$j]->dateapplied)
                            );
                        }
                    }
                }
            }
        } else {
            $response = array('status' => 501, 'error' => 'Look slike there are some missing params');
        }

        echo json_encode($response);
    }

//	72
    public function list_outstanding_group_loans(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($user_id)) {
            $response = "";

            if (!$loans = DB::table('grouploans')->where('userId', $user_id)->where('interestflag', 0)->get()) {
                $response = json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                if (count($loans) > 0) {
                    $loans = $loans[0];
                    $loanId = $loans->loanId;

                    $thisBal = $this->getSpecificGroupLoanBalance($loanId);

                    $response = array(
                        'status' => 200,
                        'loan_id' => $thisBal,
                        'description' => $loans->applidescription
                    );
                } else {
                    $response = array(
                        'status' => 201,
                        'msg' => 'APPLY LOAN'
                    );
                }

            }
        } else {
            $response = array('status' => 501, 'error' => 'Looks like there are some missing params');
        }

        echo json_encode($response);
    }

    public function pending_group_loan_request(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($_POST['userId'])) {
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $response = "";

            if (!$result = mysqli_query($this->link, "SELECT * FROM admingrouploanapplication WHERE status=0")) {
                $response = "0~Error2 " . mysqli_error($this->link);
            } else {
                //get groups to which member is registered
                $groupArray = array();
                if (!$memberGroups = mysqli_query($this->link, "SELECT * FROM groupmembers WHERE userId=$userId")) {
                    $response = json_encode(array('status' => 501, 'error' => 'query execution error'));
                } else {
                    while ($memberGroupsrow = mysqli_fetch_array($memberGroups, MYSQLI_ASSOC)) {
                        array_push($groupArray, $memberGroupsrow['groupId']);
                    }
                }

                //loop through records
                while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                    $groupId = $row['groupId'];
                    if (in_array($groupId, $groupArray))//if member is in group
                    {
                        $loanId = $row['applicationId'];

                        //check if member had approved/declined loan
                        if (!$checkApprovalStatus = mysqli_query($this->link, "SELECT * FROM admingrouploanapprovers WHERE approverId=$userId AND loanId=$loanId")) {
                            $response = json_encode(array('status' => 501, 'error' => 'query execution error'));
                        } else {
                            if (mysqli_num_rows($checkApprovalStatus) == 0) {
                                $response .= $row['applicationId'] . '~' . getUserName($row['groupId']) . '~Amount: Ksh ' . number_format($row['amount'], 2) . '~Description: ' . $row['description'] . '~Date: ' . formatDateTime($row['dateapplied']) . '`';
                            }
                        }
                    }
                }
            }
            $response = trim($response, '`');
        } else {
            $response = "0~There are missing parameters";
        }
    }

//	76
    public function loan_guarantee_request(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($user_id)) {
            $response = "";
            //get guarabtor where status is 0
            if (!$requests = DB::table('groupguarantors')->where('guarantorId', $user_id)->where('status', 0)->get()) {
                echo json_encode(array('status' => 501, 'error' => 'query execution error'));
            } else {
                $collection = array();
                for ($i = 0; $i < $requests->count(); $i++) {
                    $applicationId = $requests[$i]->loanapplicationId;
                    $loanType = 0;

                    if (!$getLoanType = DB::table('grouploanapplications')->where('applicationid', $applicationId)->get()) {
                        $response = json_encode(array('status' => 501, 'error' => 'database error'));
                    } else {
                        /*$getLoanType->count()
						if($getLoanType->count() > 0){

						}   else {
							$getLoanType = $getLoanType[0];
						}*/
                    }
                    $guarantorMe = new stdClass();

                    $guarantorMe->guarantorshipID = $requests[$i]->guarantorshipId;
                    $guarantorMe->name = $this->getUserName($requests[$i]->guaranteeId);
                    $guarantorMe->phone = $this->getUserRegisteredPhone($requests[$i]->guaranteeId);
                    $guarantorMe->amount = number_format($requests[$i]->guaranteeamount, 2);
                    $guarantorMe->date = $this->formatDateTime($requests[$i]->datesent);

//					$response[ $i ] = $guarantorMe;
                    array_push($collection, $guarantorMe);
                }

                if (count($requests) > 0) {
                    $response = array('status' => 200, 'data' => $collection);
                } else {
                    $response = array('status' => 201, 'data' => 'NO GUARANTOR REQUESTS!');
                }
            }
        } else {
            $response = array('status' => 501, 'error' => 'Looks like there are some missing params');
        }
        echo json_encode($response);
    }

//	76.5
    public function loan_guarantee_request_count(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($user_id)) {
            $response = "";
            //get guarabtor where status is 0
            if (!$requests = DB::table('groupguarantors')->where('guarantorId', $user_id)->where('status', 0)->get()) {
                echo json_encode(array('status' => 501, 'error' => 'query execution error'));
            } else {

                if (count($requests) > 0) {
                    $response = array('status' => 200, 'count' => count($requests));
                } else {
                    $response = array('status' => 201, 'data' => 'NO GUARANTOR REQUESTS!');
                }
            }
        } else {
            $response = array('status' => 501, 'error' => 'Looks like there are some missing params');
        }
        echo json_encode($response);
    }

    public function send_phone_confirmation_message(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($_POST['userId'])) {
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);

            /* generate confirmation code */
            $approvalCode = "";

            //check if there's an existing code
            if (!$getCurrentCode = mysqli_query($this->link, "SELECT * FROM users WHERE id = $userId")) {
                echo json_encode(array('status' => 501, 'error' => 'query execution error'));
            } else {
                $getCurrentCodeRow = mysqli_fetch_array($getCurrentCode, MYSQLI_ASSOC);
                if (empty($getCurrentCodeRow['passrecoverycode'])) {
                    $approvalCode = strtoupper(substr(md5(rand()), 0, 7));
                } else {
                    $approvalCode = $getCurrentCodeRow['passrecoverycode'];
                }
            }
            /* end generate code*/

            //update user table
            if (!mysqli_query($this->link, "UPDATE users SET passrecoverycode='$approvalCode' WHERE id=$userId")) {
                $response = json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                //send sms to user
                $phoneNo = getUserRegisteredPhone($userId);
                $message = "Your phone confirmation code is $approvalCode";
                $smsResult = $this->sendMobileSasa($phoneNo, $message, "HOMEPESA");
                $response = "2~SMS has been sent";
            }
        } else {
            $response = "0~There are missing parameters";
        }
    }

    public function fund_raising_accounts(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($_POST['userId'])) {
            $response = "";
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);

            if (!$result = mysqli_query($this->link, "SELECT * FROM fundraisingaccounts WHERE userId=$userId AND status = 1")) {
                echo json_encode(array('status' => 501, 'error' => 'query execution error'));
            } else {
                while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                    $response .= $row['accountId'] . "~" . $row['accountname'] . "`";
                }
            }

            $response = trim($response, '`');
        } else {
            $response = "";
        }
    }

//	44
    public function get_guarantees(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($user_id)) {
            $response = "1~";

            //get all guarantorships where status = 1
            if (!$guarantees = DB::table('guarantors')->where('guarantorId', $user_id)->where('status', 1)->get()) {
                echo json_encode(array('status' => 501, 'error' => 'query execution error'));
            } else {
                $num = 1;
                for ($i = 0; $i < count($guarantees); $i++) {
                    $loanApplicationId = $guarantees[$i]->loanapplicationId;
                    $guaranteeId = $guarantees[$i]->guaranteeId;
                    $guaranteeamount = $guarantees[$i]->guaranteeamount;
                    $dateapproved = $guarantees[$i]->dateapproved;

                    //check if loan has been fully paid
                    if (!$getLoanId = DB::table('loans')->where('loanApplicationId', $loanApplicationId)->get()) {
                        echo json_encode(array('status' => 501, 'error' => 'query execution error'));
                    } else {
                        $getLoanId = $getLoanId[0];
                        $loanBalance = $this->getSpecificLoanBalance($getLoanId->loanId);

                        if ($loanBalance > 0) {
                            $response .= "<p>$num. " . $this->getUserName($guaranteeId) . ": <b>Ksh " . number_format($guaranteeamount, 2) . "</b></p>";
                            $num++;
                        }
                    }

                }
            }

            if ($num == 1) {
                $response = "0~You do not have finance guarantees";
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
    }

    public function get_guarantors(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($_POST['userId'])) {
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $response = "1~";

            //get all guarantorships where status = 1
            if (!$guarantors = mysqli_query($this->link, "SELECT * FROM guarantors WHERE guaranteeId=$userId AND status=1")) {
                echo json_encode(array('status' => 501, 'error' => 'query execution error'));
            } else {
                $num = 1;
                while ($guarantorsrow = mysqli_fetch_array($guarantors, MYSQLI_ASSOC)) {
                    $loanApplicationId = $guarantorsrow['loanapplicationId'];
                    $guarantorId = $guarantorsrow['guarantorId'];
                    $guaranteeamount = $guarantorsrow['guaranteeamount'];
                    $dateapproved = $guarantorsrow['dateapproved'];

                    //check if loan has been fully paid
                    if (!$getLoanId = mysqli_query($this->link, "SELECT * FROM loans WHERE loanapplicationId=$loanApplicationId")) {
                        echo json_encode(array('status' => 501, 'error' => 'query execution error'));
                    } else {
                        $getLoanIdrow = mysqli_fetch_array($getLoanId, MYSQLI_ASSOC);
                        $loanBalance = getSpecificLoanBalance($getLoanIdrow['loanId']);

                        if ($loanBalance > 0) {
                            $response .= "<p>$num. " . getUserName($guarantorId) . ": <b>Ksh " . number_format($guaranteeamount, 2) . "</b></p>";
                            $num++;
                        }
                    }
                }
            }

            if ($num == 1) {
                $response = "0~You do not have finance guarantors";
            }
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
    }

    public function get_unapproved_loans(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($_POST['userId'])) {
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $response = "";

            if (!$loans = mysqli_query($this->link, "SELECT * FROM loanapplications WHERE userId=$userId AND approvalflag=0")) {
                echo json_encode(array('status' => 501, 'error' => 'query execution error'));
            } else {
                while ($loansrow = mysqli_fetch_array($loans, MYSQLI_ASSOC)) {
                    $response .= $loansrow['applicationid'] . "~" . getLoanType($loansrow['loantype']) . " loan: Ksh " . number_format($loansrow['amount'], 2) . "`";
                }
            }

            $response = trim($response, "`");
        } else {
            $response = "There are missing parameters in your request";
        }
    }

    public function load_requests_guarantee(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($_POST['userId'])) {
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $response = "";

            //get guarabtor where status is 0
            if (!$requests = mysqli_query($this->link, "SELECT * FROM guarantors WHERE guarantorId=$userId AND status=0")) {
                echo json_encode(array('status' => 501, 'error' => 'query execution error'));
            } else {
                while ($requestsrow = mysqli_fetch_array($requests, MYSQLI_ASSOC)) {
                    $applicationId = $requestsrow['loanapplicationId'];
                    $loanType = 0;

                    if (!$getLoanType = mysqli_query($this->link, "SELECT * FROM loanapplications WHERE applicationid=$applicationId")) {
                        echo json_encode(array('status' => 501, 'error' => 'query execution error'));
                    } else {
                        $getLoanTyperow = mysqli_fetch_array($getLoanType, MYSQLI_ASSOC);
                        $loanType = $getLoanTyperow['loantype'];
                    }

                    $response .= $requestsrow['guarantorshipId'] . "~" . getUserName($requestsrow['guaranteeId']) . "~+" . getUserRegisteredPhone($requestsrow['guaranteeId']) . "~" . getLoanType($loanType) . " loan~Ksh " . number_format($requestsrow['guaranteeamount'], 2) . "`";
                }
            }
            $response = trim($response, "`");

        } else {
            $response = "There are missing parameters in your request";
        }
    }

//	50
    public function paybill_instructions(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($_POST['userId'])) {
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);

            if (!$usernationalId = mysqli_query($this->link, "SELECT * FROM users WHERE id=$userId")) {
                echo "Error " . mysqli_query($this->link);
            } else {
                $usernationalIdrow = mysqli_fetch_array($usernationalId, MYSQLI_ASSOC);
                $response = '0~Go to your Mpesa menu and select "Lipa na M-PESA" and then select "Pay Bill". Enter 667872 as the business number and ' . $usernationalIdrow['idpass'] . ' as the account no. Enter amount and your Mpesa Pin and confirm. You will receive an SMS from both MPESA and HOMEPESA confirming your payment. The amount is credited to your Homepesa Wallet. You can then transfer to savings, repay finance, buy shares, buy airtime, transfer to other members. NOTE: you can pay from as low as Ksh 10.';
            }

        } else {
            $response = "0~There are missing noti parameters";
        }
    }

//	51
    public function unconfirmed_transactions(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($_POST['userId'])) {
            $userId = htmlspecialchars($_POST['userId'], ENT_QUOTES);
            $response = "";

            //mpesa
            if (!$details = mysqli_query($this->link, "SELECT * FROM mpesawithdrawals WHERE userId=$userId AND status=0 ORDER BY transactionId DESC")) {
                echo "Error " . mysqli_query($this->link);
            } else {
                while ($detailsrow = mysqli_fetch_array($details, MYSQLI_ASSOC)) {
                    $response .= $detailsrow['transactionId'] . "~1~Amount: Ksh " . number_format($detailsrow['amount'], 2) . "~Method: Mpesa~Date: " . formatDateTime($detailsrow['transactiontime']) . "`";
                }
            }

            //bank transfer
            if (!$bank = mysqli_query($this->link, "SELECT * FROM bankwithdrawals WHERE userId=$userId AND status=0")) {
                echo "Error " . mysqli_query($this->link);
            } else {
                while ($bankrow = mysqli_fetch_array($bank, MYSQLI_ASSOC)) {
                    $response .= $bankrow['withdrawalId'] . "~2~Amount: Ksh " . number_format($bankrow['amount'], 2) . "~Method: Bank`";
                }
            }

            //cheque request
            if (!$cheque = mysqli_query($this->link, "SELECT * FROM chequewithdrawals WHERE userId=$userId AND status=0")) {
                echo "Error " . mysqli_query($this->link);
            } else {
                while ($chequerow = mysqli_fetch_array($cheque, MYSQLI_ASSOC)) {
                    $response .= $chequerow['withdrawalId'] . "~2~Amount: Ksh " . number_format($chequerow['amount'], 2) . "~Method: Cheque`";
                }
            }

            $response = trim($response, "`");
        } else {
            $response = "0~There are missing parameters";
        }
    }

//	52
    public function get_loan_types(Request $request)
    {
        $user_id = $request->get('user_id');

        if (isset($user_id)) {
            $response = "";

            if (!$loanTypes = mysqli_query($this->link, "SELECT * FROM loantypes WHERE status = 1")) {
                echo "Error " . mysqli_query($this->link);
            } else {

                $gather = array();
                while ($loanTypesrow = mysqli_fetch_array($loanTypes, MYSQLI_ASSOC)) {
//					$response .= $loanTypesrow['loantypeId'] . "~" . $loanTypesrow['typename'] . "~" . $loanTypesrow['interesttype'] . "`";
                    $collection = new stdClass();
                    $collection->loan_type = $loanTypesrow['typename'];
                    $collection->interest_type = $loanTypesrow['interesttype'];
                    $collection->loan_type_id = $loanTypesrow['loantypeId'];

                    array_push($gather, $collection);
                }
                $response = array(
                    'status' => 200,
                    'count' => count($gather),
                    'loan_types' => $gather
                );
            }

        } else {
            $response = array('status' => 501, 'error' => 'There are some missing params');
        }

        echo json_encode($response);
    }

    public function bank_to_memberships(Request $request)
    {

        $user_id = $request->get('user_id');
        $amount = (int)$request->get('amount');
        $source = $request->get('source');
        $remarks = $request->get('remarks');
        $group_id = $request->get('group_id');

        if (isset($user_id) && isset($amount) && isset($group_id)) {
            if (is_numeric($amount) && $amount > 0) {
                //check if user has enough amount on withdrawable account
                $withAmount = $this->getWithdrawableAmount($user_id);

                if ($withAmount >= $amount) {
                    //handle operation locking - only one withdrawable operation can take place at a time
                    $lockFile = '/var/www/html/dibwaves/admin/attachments/' . $user_id . '.txt';
                    if (!file_exists($lockFile)) {
                        //create lock file
                        $handle = fopen($lockFile, 'a');
                        fwrite($handle, 'operation underway');
                        fclose($handle);

                        $now = date('Y-m-d H:i:s');

                        $purpose = 3;
                        $savingsSource = 3;
                        $description = "Transfer to account";

                        $savingsSource = 3;
                        $transResult = $this->withdrawFunds($amount, $user_id, $purpose, $description);

                        if ($transResult == 1)//success
                        {
                            //send to savings
                            $currency = 'KES';
                            $savResults = $this->saveToGroup($group_id, $amount, $user_id, $source, $remarks);
                            if ($savResults == 1)     //success
                            {
                                $response = array(
                                    'status' => 200,
                                    'amount' => $amount,
                                    'msg' => 'You have transferred ' . $amount . ' ' . $currency . '. Please check your balance'
                                );
                            } else {
                                $response = array(
                                    'status' => 501,
                                    'error' => 'Transfer to savings failed. Please try again later.'
                                );
                            }
                        } else {
                            $response = array(
                                'status' => 501,
                                'error' => 'Transfer to savings failed. Please try again later.'
                            );
                        }
                        //delete lock file
                        unlink($lockFile);
//						File::delete($lockFile);

                    } else {
                        $response = array(
                            'status' => 501,
                            'error' => 'Another operation is being performed on this account. Please try again later.'
                        );
                    }
                } else {
                    $response = array(
                        'status' => 505,
                        'amt' => number_format($withAmount, 2),
                        'error' => 'You have insufficient balance. Your account balance is Ksh '
                    );
                }
            } else {
                $response = array(
                    'status' => 501,
                    'error' => 'Amount must be greater than 0'
                );
            }
        } else {
            $response = array(
                'status' => 501,
                'error' => 'Request could not complete'
            );
        }
        echo json_encode($response);

    }

    public function get_my_savings_balance(Request $request)
    {
        $user_id = $request->get('user_id');
        if (isset($user_id)) {
            $savings = 0;

            if (!$savvals = DB::table('savings')->where('userId', $user_id)->orderBy('savingsId', 'DESC')->get()) {
                echo json_encode(array('status' => 501, 'error' => '0.00'));
                die;
            } else {
                for ($j = 0; $j < count($savvals); $j++) {
                    $savings += $savvals[$j]->amount;
                }
            }

            //get registration fee and membership fee paid
            if (!$regfee = DB::table('users')->where('id', $user_id)->get()) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                $regfee = $regfee[0];
                $regmembership = number_format($regfee->regfee, 2);
            }

            $response = array(
                'status' => 200,
                'savings' => number_format($savings, 2),
                'registration_fee' => $regmembership
            );
        } else {
            $response = json_encode(array('status' => 501, 'error' => 'not allowed by server'));
        }

        echo json_encode($response);
    }

    public function get_last_bank_transactions($user_id, $count)
    {
        if (!$lastTxResult = mysqli_query($this->link, "SELECT amount, time, method, debit_credit_flag FROM bank_account_tx WHERE user_id = $user_id ORDER BY time DESC LIMIT $count")) {
            echo "Error " . mysqli_query($this->link);
        } else {
            $gather = array();
            while ($row = mysqli_fetch_array($lastTxResult, MYSQLI_ASSOC)) {
                $collection = new stdClass();
                $collection->time = $row['time'];
                $collection->method = $row['method'];
                $collection->dr_cr = $row['debit_credit_flag'];
                if ($row['debit_credit_flag'] == 'DEBIT') {
                    $collection->amount = -1 * $row['amount'];
                } else {
                    $collection->amount = $row['amount'];
                }
                array_push($gather, $collection);
            }

            return json_encode($gather);
        }
    }
//$user_id = $request->get( 'user_id' );
//$group_id = $request->get( 'group_id' );
    /*API FUNCTIONS*/
    function checkAdminNotifications($userId)
    {
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');

        $userDetails = json_decode($this->getUserDetails($userId), true);
        $email = $userDetails['email'];
        $acstatus = $userDetails['acstatus'];

        $lastThirtyDaysAgo = date('Y-m-d H:i:s', strtotime($now) - (30 * 24 * 60 * 60));

        if (!$notifications = DB::table('adminalerts')->where('dateadded', '>=', $lastThirtyDaysAgo)->get()) {
            echo json_encode(array('status' => 501, 'error' => 'failed to fetch admin alerts'));
        } else {
            for ($i = 0; count($notifications); $i++) {

                $alertId = $notifications[$i]->alertId;
                $message = $notifications[$i]->message;
                $recipientgroup = $notifications[$i]->recipientgroup;
                $recipients = $notifications[$i]->recipients;

                //first check if user is in recipients
                $recipientsVals = explode(",", $recipients);

                if (in_array($email, $recipientsVals)) {
                    //check if alert has been inserted
                    $this->insertUserAlert($userId, $alertId, $message);
                }

                //check if user belongs to group
                $correspondingGroup = 0;
                switch ($acstatus) {
                    case '0':
                        $correspondingGroup = 4;
                        break;

                    case '1':
                        $correspondingGroup = 3;
                        break;

                    case '2':
                        $correspondingGroup = 2;
                        break;

                    case '3':
                        $correspondingGroup = 5;

                    default:
                        $correspondingGroup = 0;
                        break;
                }

                if (($correspondingGroup == $recipientgroup) || ($recipientgroup == 1)) {
                    $this->insertUserAlert($userId, $alertId, $message);
                }
            }
        }
    }

    function getWithdrawableAmount($userId)//get amount user has in withdrawable account
    {
        $withdrawableAmount = 0;
        $totalDeposits = 0;
        $totalWithdrawals = 0;
        //get total deposits
        if (!$deposits = DB::select(DB::raw("SELECT SUM(amount) AS weka FROM deposits WHERE userId = :user_id"), array(
            'user_id' => $userId
        ))) {
            echo json_encode(array('error' => 'mysql error', 'status' => '501'));
        } else {
            $deposits = $deposits[0];

            if (!empty($deposits->weka)) {
                $totalDeposits = $deposits->weka;
            }
        }
        //get total withdrawals
        if (!$withdrawals = DB::select(DB::raw("SELECT SUM(amount) AS toa FROM withdrawals WHERE userId = :user_id"), array(
            'user_id' => $userId
        ))) {
            echo json_encode(array('error' => 'mysql error', 'status' => '501'));
        } else {
            $withdrawals = $withdrawals[0];

            if (!empty($withdrawals->toa)) {
                $totalWithdrawals = $withdrawals->toa;
            }
        }

        $withdrawableAmount = $totalDeposits - $totalWithdrawals;

        return $withdrawableAmount;
    }

    function getWithdrawableHoldingAmount($userId)//get amount user has in Mpesa holding account
    {
        $withdrawableAmount = 0;
        $totalDeposits = 0;
        $totalWithdrawals = 0;


        //get total deposits
        if (!$deposits = DB::select(DB::raw("SELECT SUM(amount) AS weka FROM holding_savings WHERE userId =  :user_id"), array(
            'user_id' => $userId
        ))) {
            echo json_encode(array('error' => 'mysql error', 'status' => '501'));
        } else {
            $deposits = $deposits[0];
            if (!empty($deposits->weka)) {
                $totalDeposits = $deposits->weka;
            }
        }

        //get total withdrawals
        if (!$withdrawals = DB::select(DB::raw("SELECT SUM(amount) AS toa FROM holdingwithdrawals WHERE userId = :user_id"), array(
            'user_id' => $userId
        ))) {
            echo json_encode(array('error' => 'mysql error', 'status' => '501'));
        } else {

            if (!empty($withdrawals->toa)) {
                $totalWithdrawals = $withdrawals->toa;
            }
        }

        $withdrawableAmount = $totalDeposits - $totalWithdrawals;

        return $withdrawableAmount;
    }

    function getUserDetails($userId)
    {
        $return = array();

//		DB::table( 'users' )->where( 'id','>=',  $userId );
//		$details = DB::select( DB::raw( "SELECT * FROM users WHERE id = :userId" ), array( 'userId' => $userId ) );
//		$details = DB::table( 'users' )->where( 'id', '>=',  $userId )->get()->first();

        if (!$details = DB::table('users')->where('id', '>=', $userId)->get()->first()) {
            $description = array('error' => 'mysql error', 'status' => '501');
            json_encode($description);
            die;
        } else {
            $return = array
            (
                'name' => $details->fullname,
                'idno' => $details->idpass,
                'phone' => $details->phone,
                'email' => $details->email,
                'actype' => $details->actype,
                'acstatus' => $details->acstatus,
                'regfee' => $details->regfee,
                'created_at' => $details->created_at,
                'timepaid' => $details->timepaid,
                'timeactivated' => $details->timeactivated,
                'signupoption' => $details->signupoption,
                'blacklist' => $details->blacklist,
                'phoneconfirmed' => $details->phoneconfirmed
            );
        }

        return json_encode($return);
    }

    function getFinancingNTimes(Request $request)
    {
        $user_id = $request->get('user_id');
        $times = 3;

        if (isset($user_id)) {
            $savings = 0;
            if (!$savvals = DB::table('savings')->where('userId', $user_id)->orderBy('savingsId', 'DESC')->get()) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                $lastTwenty = "<h4>Last 50 transactions</h4>";
                $num = 1;
                $savings = 0;
                for ($i = 0; $i < count($savvals); $i++) {
                    $savings += $savvals[$i]->amount;
                    $num++;
                }
            }
            $savings = ($savings * $times);
            $response = array(
                'status' => 200,
                'savings' => number_format($savings, 2)
            );
        } else {
            $response = array('status' => 501, 'error' => 'There are missing parameters in your request');
        }
        echo json_encode($response);
    }

    function instant_financing(Request $request)
    {
        $user_id = $request->get('user_id');
        $android_version = $request->get('android_version');
        $android_model_number = $request->get('model_number');

//		$ip_address =
//
//
    }

    function convertDate($slashDate)
    {
        $date = str_replace('/', '-', $slashDate);

        return date('Y-m-d', strtotime($date));
    }

//get savings
    function getSavingsBalance($userId)
    {
        $savingsBalance = 0;

        if (!$savings = DB::select(DB::raw("SELECT SUM(amount) AS yote FROM savings WHERE userId = $userId LIMIT 1"))) {
            echo json_encode(array('error' => 'mysql error', 'status' => '501'));
        } else {
            $savings = $savings[0];

            $savingsBalance = isset($savings->yote) ? $savings->yote : 0;
        }

        return $savingsBalance;
    }

//inserts alert in table
    function insertUserAlert($userId, $alertId, $message)
    {
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');

        //check if alert has been added
        if (!$check = DB::select(DB::raw("SELECT * FROM usernotifications WHERE alertId = :alertID"), array(
            'alertID' => $alertId
        ))) {
            echo json_encode(array('error' => 'mysql error', 'status' => 501));
        } else {
//			DB::insert()
            if (count($check) == 0) {
                if (!DB::insert("INSERT INTO usernotifications(userId,messagetext,alertId,timeupdated,readflag,notified) VALUES($userId,'$message',$alertId,'$now',0,0)")) {
                    echo json_encode(array('error' => 'mysql error', 'status' => 501));
                }
            }
        }
    }

    function sendEmail($subject, $message, $recipient, $files)
    {
        $status = 0;

        try {
            /* CONFIGURATION */
            $crendentials = array(
                'email' => 'info@homepesasacco.com', //Homepesa address
                'password' => 'investment123#' //Email password
            );

            /* SPECIFIC TO GMAIL SMTP */
            $smtp = array(

                'host' => 'smtp.zoho.com',
                'port' => 465,
                'username' => $crendentials['email'],
                'password' => $crendentials['password'],
                'secure' => 'ssl' //SSL or TLS

            );

            $email = new PHPMailer();

            //SMTP Configuration
            $email->isSMTP();
            $email->isHTML(true);
            $email->SMTPAuth = true; //We need to authenticate
            $email->Host = $smtp['host'];
            $email->Port = $smtp['port'];
            $email->Username = $smtp['username'];
            $email->Password = $smtp['password'];
            $email->SMTPSecure = $smtp['secure'];

            $bodytext = $message;

            $email->From = 'info@homepesasacco.com';
            $email->FromName = 'DIB Waves';
            $email->Subject = $subject;
            $email->Body = $bodytext;

            $email->SMTPSecure = 'ssl'; //secure transfer enabled

            $email->AddAddress($recipient);

            //add files
            $files = trim($files, ",");
            $filesVals = explode(",", $files);
            if (count($filesVals) > 0) {
                foreach ($filesVals as $tumayeye) {
                    if ($tumayeye != "") {
                        $email->AddAttachment($tumayeye, $tumayeye);
                    }
                }
            }

            $emailResult = $email->Send();

            if ($emailResult == false) {
                $status = 0;
            } else {
                $status = 1;
            }
        } catch (phpmailerException $e) {
            //echo $e->errorMessage(); //error messages from PHPMailer
        } catch (Exception $e) {
            //echo $e->getMessage();
        }

        return $status;
    }

    function getSignUpEmailMessage($cname)
    {
        /*return '<p>Dear ' . $cname . ', welcome to Homepesa Sacco Limited. We are glad that you chose to be part of us. Using innovation we have brought a wide variety of products and services straight to your living room for you to enjoy.</p>
			<p>Once your account is activated, you will be able to access both a savings/fixed deposit account and Homepesa Wallet.</p>
			<p>We have introduced a new feature that enables you to access loans without guarantors and/or security. On account activation, the system will assign to you the first loan limit of between (Ksh 200 to Ksh 500). Based on how you make savings and repay your loans using this limit, we will review your limit upwards. This enables you to get loans anywhere any time. All our processes are automated with no paper work involved and no human intervention allowing you to access the services instantly.</p>
			<p>The savings account enables you to access a secured loan that is up to a maximum of three times your savings. The loan can be secured by your savings (if the loan is at most your savings), guarantors who are members of the sacco and whose savings add up to the loan amount or securities such as car log book or land title deed. </p>
			<p>You will also be able to access a Homepesa Wallet where loans processed are held. You can also buy Safaricom, Airtel and Orange airtime; send cash to other registered members; transfer to your savings account among many other functions.</p>
			<p>You can make payments using Mpesa or credit card. To make savings, repay loan or buy shares using Mpesa, select the Mpesa option, enter your Mpesa phone number and amount and click Make Payment. You will receive a USSD pop up on your phone asking you to enter your universal Safaricom Pin (formerly known as Bonga Pin). In case you have never created one, you will be guided through the process of creating a Pin. Once done, confirm the payment and the amount will be transferred from your Mpesa to your Homepesa account. You can also pay by using our paybill number. To do so, go to your Mpesa menu and select "Lipa na M-PESA" and then select "Pay Bill". Enter 667872 as the business number, your national ID or passport number as the account no, enter the amount, your Mpesa pin, confirm and send. Your Homepesa Wallet will be updated. You can then transfer to savings, to other members or perform other transactions.</p>
			<p>All transactions come with accurate reports for accounting purposed. You can access your account statistics and generate statements at homepesasacco.com. You can access your account either using the Android application on Google Play Store or via www.homepesasacco.com.</p>
			<p>You are required to keep your account secure by using strong passwords that cannot be easily guessed and by changing your passwords regularly. Please note that you are not supposed to share your account information with anyone. Our staff will not ask for your account Passwords. Our system is highly secured guaranteeing you safety for your money. Also to ensure that you are making payment to your account, every Mpesa transaction send to your phone for confirmation comes with your name and Homepesa Sacco Global as the recipient. Do not respond to any requests without your name.</p>
			<p>For inquiries, please get in touch with us on +254 712 838 486 or info@homepesasacco.com.</p>
			<p>Thank you for choosing Homepesa Sacco.</p>
			<p></p>
			<p>Regards</p>
			<p>Homepesa Sacco Society Limited</p>';*/
        return '<p>Dear ' . $cname . ', welcome to DIB Waves. We are glad that you chose to be part of us. Using innovation we have brought a wide variety of products and services straight to your living room for you to enjoy.</p>
			<p>Once your account is activated, you will be able to access both a savings/fixed deposit account and Waves Account.</p>
			<p>We have introduced a new feature that enables you to access loans without guarantors and/or security. On account activation, the system will assign to you the first loan limit of between (Ksh 200 to Ksh 500). Based on how you make savings and repay your loans using this limit, we will review your limit upwards. This enables you to get loans anywhere any time. All our processes are automated with no paper work involved and no human intervention allowing you to access the services instantly.</p>
			<p>The savings account enables you to access a secured loan that is up to a maximum of three times your savings. The loan can be secured by your savings (if the loan is at most your savings), guarantors who are members of the sacco and whose savings add up to the loan amount or securities such as car log book or land title deed. </p>
			<p>You will also be able to access a Waves Account where loans processed are held. You can also buy Safaricom, Airtel and Orange airtime; send cash to other registered members; transfer to your savings account among many other functions.</p>
			<p>You can make payments using mobile-money or credit card. To make savings, repay loan or buy shares using mobile-money, select the mobile-money option, enter your Mpesa phone number and amount and click Make Payment. You will receive a USSD pop up on your phone asking you to enter your universal Safaricom Pin (formerly known as Bonga Pin). In case you have never created one, you will be guided through the process of creating a Pin. Once done, confirm the payment and the amount will be transferred from your Mpesa to your Homepesa account. You can also pay by using our paybill number. To do so, go to your Mpesa menu and select "Lipa na M-PESA" and then select "Pay Bill". Enter 667872 as the business number, your national ID or passport number as the account no, enter the amount, your Mpesa pin, confirm and send. Your Homepesa Wallet will be updated. You can then transfer to savings, to other members or perform other transactions.</p>
			<p>All transactions come with accurate reports for accounting purposed. You can access your account statistics and generate statements at dibwaves.com. You can access your account either using the Android application on Google Play Store or via www.dibwaves.com.</p>			
			<p>You are required to keep your account secure by using strong passwords that cannot be easily guessed and by changing your passwords regularly. Please note that you are not supposed to share your account information with anyone. Our staff will not ask for your account Passwords. Our system is highly secured guaranteeing you safety for your money. Also to ensure that you are making payment to your account, every mobile-money transaction send to your phone for confirmation comes with your name and Homepesa Sacco Global as the recipient. Do not respond to any requests without your name.</p>
			<p>For inquiries, please get in touch with us on +254 712 838 486 or info@homepesasacco.com.</p>
			<p>Thank you for choosing DIB Waves.</p>
			<p></p>
			<p>Regards</p>
			<p>DIB Waves</p>';
    }

//validate phone
    function validatePhone($phone)
    {
        $return = 0;
        /*if(substr($phone, 0,2) === "07")//if phone starts with 07
		{
			$phone = substr_replace($phone, "254", 0,1);
		}
		elseif(substr($phone, 0,1) === "7")//if starts with 7
		{
			$phone = substr_replace($phone, "254", 0,0);
		}
		elseif(substr($phone, 0,4) === "+254")
		{
			$phone = substr_replace($phone, "", 0,1);
		}*/
        if (strlen($phone) >= 10 && is_numeric($phone))//phone correct
        {
            $return = 1;
        }

        return $return;
    }

//validate id and passport
    function validateIdPass($idpass, $actype)
    {
        $return = 0;
        if ($actype == 1)//individual
        {
            if (strlen($idpass) >= 7 && strlen($idpass) <= 10) {
                if (is_numeric($idpass)) {
                    $return = 1;
                } else {
                    if (is_numeric(substr($idpass, 1))) {
                        $return = 1;
                    }
                }
            }
        } else {
            $return = 1;
        }

        return $return;
    }

    function getTimeStamp()
    {
        date_default_timezone_set('Africa/Nairobi');
        $date = DateTime::createFromFormat('U.u', microtime(true));

        return $date->format('YmdHisu');
    }

    function getPublicKey()
    {
        return 'ptaYdLhAvmbqbzPsuaQmXidwJMTpUg';
    }

    function getPrivateKey()
    {
        return 'aaJyPVGnpyeSayAXMXyUaMARhdAwGnCVKbiptbVpjnZFgEqrCgGtNKUTSicK';
    }

//get user bio-data
    function fetchIdDetails($idNo)
    {
        /* initialize variables */
        $status = 0;
        $names = "";
        $date_of_being = "";
        $gender = 1;
        $phone = "";
        $email = "";
        $postal_address = "";
        $physical_address = "";
        $employmentDate = "";
        $employer = "";
        /* end initialize variables */

        date_default_timezone_set('Africa/Nairobi');
        $publicKey = $this->getPublicKey();
        $privateKey = $this->getPrivateKey();
        $timestamp = $this->getTimeStamp();

        // The data to send to the API
        $postData = array(
            'report_type' => 6,
            'identity_number' => $idNo,
            'identity_type' => '001'
        );

        $requestBody = json_encode($postData);

        $hash = hash('sha256', $privateKey . $requestBody . $publicKey . $timestamp);

        // Setup cURL
        $ch = curl_init('https://api.metropol.co.ke:22225/v2_1/identity/scrub');
        curl_setopt_array($ch, array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'X-METROPOL-REST-API-KEY:' . $publicKey,
                'X-METROPOL-REST-API-HASH:' . $hash,
                'X-METROPOL-REST-API-TIMESTAMP:' . $timestamp
            ),
            CURLOPT_POSTFIELDS => $requestBody
        ));

        // Send the request
        $response = curl_exec($ch);

        // Check for errors
        if ($response === false) {
            echo "Error sending request";
            die(curl_error($ch));
        } else {
            $contentVals = json_decode($response, true);

            $has_error = $contentVals['has_error'];

            if ($has_error == false) {
                $api_code_description = $contentVals['api_code_description'];
                $api_code = $contentVals['api_code'];

                if ($api_code == null) {
                    $status = 1;

                    $identity_type = $contentVals['identity_type'];
                    $trx_id = '';
                    $identity_number = $contentVals['identity_number'];

                    foreach ($contentVals['names'] as $nameVal) {
                        $names .= $nameVal . " ";
                        if (!empty($nameVal)) {
                            break;
                        }
                    }

                    $date_of_being = $contentVals['date_of_being'][0];
                    $gender = (strtolower($contentVals['gender'][0]) == 'm' ? 1 : 2);

                    foreach ($contentVals['phone'] as $phoneVal) {
                        $phone .= $phoneVal . " ";
                        if (!empty($phoneVal)) {
                            break;
                        }
                    }

                    foreach ($contentVals['email'] as $emailVal) {
                        $email = $emailVal . " ";
                    }

                    $postal_address = count($contentVals['postal_address']) > 0 ? ($contentVals['postal_address'][0]['number'] . " - " . $contentVals['postal_address'][0]['code'] . " " . $contentVals['postal_address'][0]['town'] . ", " . $contentVals['postal_address'][0]['country']) : '';
                    $physical_address = count($contentVals['physical_address']) > 0 ? ($contentVals['physical_address'][0]['address'] . ", " . $contentVals['physical_address'][0]['town'] . ", " . $contentVals['physical_address'][0]['country']) : '';
                    $employmentDate = count($contentVals['employment']) > 0 ? $contentVals['employment'][0]['employment_date'] : '';
                    $employer = count($contentVals['employment']) > 0 ? $contentVals['employment'][0]['employer_name'] : '';

                    //save scrub for future reference
                    include 'connectconfig.php';
                    date_default_timezone_set('Africa/Nairobi');
                    $now = date('Y-m-d H:i:s');

                    $qry = "INSERT INTO scrubdetails (api_code_description, api_code, identity_type, trx_id, identity_number, names, date_of_being, gender, phone, email, postal_address, physical_address, employmentDate, employer, scrubdate) VALUES ('$api_code_description', '$api_code', '$identity_type', '$trx_id', '$identity_number', '$names', '$date_of_being', '$gender', '$phone', '$email', '$postal_address', '$physical_address', '$employmentDate', '$employer','$now')";

                    /*if ( ! mysqli_query( $this->link, $qry ) ) {
						echo "Error saving scrub";
					}*/
//					mysqli_close( $this->link );
                }
            }
        }

        $array = array(
            'status' => $status,
            'names' => $names,
            'date_of_being' => $date_of_being,
            'gender' => $gender,
            'phone' => $phone,
            'email' => $email,
            'postal_address' => $postal_address,
            'physical_address' => $physical_address,
            'employmentDate' => $employmentDate,
            'employer' => $employer
        );

        return json_encode($array);
    }

    public function metropol(Request $request)
    {
        $idNo = $request->get('id');
        /* initialize variables */
        $status = 0;
        $names = "";
        $date_of_being = "";
        $gender = 1;
        $phone = "";
        $email = "";
        $postal_address = "";
        $physical_address = "";
        $employmentDate = "";
        $employer = "";
        /* end initialize variables */

        date_default_timezone_set('Africa/Nairobi');
        $publicKey = $this->getPublicKey();
        $privateKey = $this->getPrivateKey();
        $timestamp = $this->getTimeStamp();

        // The data to send to the API
        $postData = array(
            'report_type' => 6,
            'identity_number' => $idNo,
            'identity_type' => '001'
        );

        $requestBody = json_encode($postData);

        $hash = hash('sha256', $privateKey . $requestBody . $publicKey . $timestamp);

        // Setup cURL
        $ch = curl_init('https://api.metropol.co.ke:22225/v2_1/identity/scrub');
        curl_setopt_array($ch, array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'X-METROPOL-REST-API-KEY:' . $publicKey,
                'X-METROPOL-REST-API-HASH:' . $hash,
                'X-METROPOL-REST-API-TIMESTAMP:' . $timestamp
            ),
            CURLOPT_POSTFIELDS => $requestBody
        ));

        // Send the request
        $response = curl_exec($ch);

        // Check for errors
        if ($response === false) {
            echo "Error sending request";
            die(curl_error($ch));
        } else {
            $contentVals = json_decode($response, true);

            $has_error = $contentVals['has_error'];

            if ($has_error == false) {
                $api_code_description = $contentVals['api_code_description'];
                $api_code = $contentVals['api_code'];

                if ($api_code == null) {
                    $status = 1;

                    $identity_type = $contentVals['identity_type'];
                    $trx_id = '';
                    $identity_number = $contentVals['identity_number'];

                    foreach ($contentVals['names'] as $nameVal) {
                        $names .= $nameVal . " ";
                        if (!empty($nameVal)) {
                            break;
                        }
                    }

                    $date_of_being = $contentVals['date_of_being'][0];
                    $gender = (strtolower($contentVals['gender'][0]) == 'm' ? 1 : 2);

                    foreach ($contentVals['phone'] as $phoneVal) {
                        $phone .= $phoneVal . " ";
                        if (!empty($phoneVal)) {
                            break;
                        }
                    }

                    foreach ($contentVals['email'] as $emailVal) {
                        $email = $emailVal . " ";
                    }

                    $postal_address = count($contentVals['postal_address']) > 0 ? ($contentVals['postal_address'][0]['number'] . " - " . $contentVals['postal_address'][0]['code'] . " " . $contentVals['postal_address'][0]['town'] . ", " . $contentVals['postal_address'][0]['country']) : '';
                    $physical_address = count($contentVals['physical_address']) > 0 ? ($contentVals['physical_address'][0]['address'] . ", " . $contentVals['physical_address'][0]['town'] . ", " . $contentVals['physical_address'][0]['country']) : '';
                    $employmentDate = count($contentVals['employment']) > 0 ? $contentVals['employment'][0]['employment_date'] : '';
                    $employer = count($contentVals['employment']) > 0 ? $contentVals['employment'][0]['employer_name'] : '';

                    //save scrub for future reference
                    date_default_timezone_set('Africa/Nairobi');
                    $now = date('Y-m-d H:i:s');

                    $qry = "INSERT INTO scrubdetails (api_code_description, api_code, identity_type, trx_id, identity_number, names, date_of_being, gender, phone, email, postal_address, physical_address, employmentDate, employer, scrubdate) VALUES ('$api_code_description', '$api_code', '$identity_type', '$trx_id', '$identity_number', '$names', '$date_of_being', '$gender', '$phone', '$email', '$postal_address', '$physical_address', '$employmentDate', '$employer','$now')";


                    if (!mysqli_query($this->link, $qry)) {
                        echo "Error saving scrub";
                    }
                    mysqli_close($this->link);
                }
            }
        }

        $array = array(
            'status' => $status,
            'names' => $names,
            'date_of_being' => $date_of_being,
            'gender' => $gender,
            'phone' => $phone,
            'email' => $email,
            'postal_address' => $postal_address,
            'physical_address' => $physical_address,
            'employmentDate' => $employmentDate,
            'employer' => $employer
        );

        return json_encode($array);
    }

    /*
	The function sends SMS to users
	*/
    function sendMobileSasa($phone, $message)
    {
        $alphanumeric = "DIBBank";
        $return = 0;

        $contacts = $phone;

        $data = '{
        "message":"' . $message . '",
        "recipient":"' . $contacts . '",
        "username":"homepesasacco",
        "apikey":"2b5549ca5681c64d459fd9ee567d291466342280",
        "senderId":"' . $alphanumeric . '"
    }';

        $ch = curl_init('http://mobilesasa.com/sendsmsjson.php');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //set this to false if you are getting the error message -SSL certificate problem: unable to get local issuer certificate
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type:application/json',
                'Content-Length:' . strlen($data)
            )
        );

        $result = curl_exec($ch);
        $decodedresult = json_decode($result, true);

        if ($result == false) {
            //echo "Curl Error: ".curl_error($ch)."<br/>";
        }

        /*Uncomment the code below to get any error messages and raw response*/
        /*var_dump($result);
		var_dump(curl_getinfo($ch));
		var_dump(curl_errno($ch));
		var_dump(curl_error($ch));*/

        $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);//used to find out if the response reached the Mobilesasa servers
        if ($resultStatus == 200)//if successful
        {
            foreach ($decodedresult as $itemouter) {
                foreach ($itemouter as $item) {
                    $phoneNumber = $item['phonenumber'];//the SMS phone number
                    $status = $item['status'];//1701 indicates sent and 0 indicates failed
                    $messageId = $item['messageId'];//unique message identifier
                    $cost = $item['cost'];//total SMS cost
                    $response = $item['message'];//the response explaining why status is as it is

                    if ($status == "1701") {
                        $return = 1;
                    }
                }
            }
        } else {
            //echo "My internet is low: ".$resultStatus ;
        }

        return $return;
    }

    function send_email()
    {
        /*$this->load->library('email');

		$this->email->from('email@email.com', 'Name');
		$this->email->to('someone@example.com');
		$this->email->cc('another@example.com');
		$this->email->bcc('and@another.com');

		$this->email->subject('subject');
		$this->email->message('message');

		$this->email->send();

		echo $this->email->print_debugger();*/
    }

    function phoneFormat($phone)
    {
        //initialize valuables
        $status = false;
        $formatedPhone = '';

        //remove white spaces
        $phone = trim($phone);
        $phone = str_replace(" ", "", $phone);

        //remove -, (, and )
        $phone = str_replace("-", "", $phone);
        $phone = str_replace("(", "", $phone);
        $phone = str_replace(")", "", $phone);

        //validate - all should begin with 254
        if (strlen($phone) >= 9 && strlen($phone) <= 13) {
            if (substr($phone, 0, 2) == "07") {
                $phone = substr_replace($phone, "254", 0, 1);
            } elseif (substr($phone, 0, 4) == "+254") {
                $phone = substr_replace($phone, "", 0, 1);
            } elseif (substr($phone, 0, 1) === "7") {
                $phone = substr_replace($phone, "254", 0, 0);
            }

            if (substr($phone, 0, 3) == "254" && strlen($phone) == 12 && is_numeric($phone)) {
                $status = true;
                $formatedPhone = $phone;
            }
        }

        $array = array('status' => $status, 'formatedPhone' => $formatedPhone);

        return json_encode($array);
    }

//function to get reference for mpesa payment
    function getReference($paymentType, $userId)
    {
        $reference = "";

        $accountName = $this->getUserName($userId);

        $accountNameVals = explode(" ", $accountName);
        $newAccountName = $accountNameVals[0];

        $num = 0;
        foreach ($accountNameVals as $namePart) {
            if ($num != 0) {
                $newAccountName .= " " . substr($accountNameVals[$num], 0, 1) . ".";
            }

            $num++;
        }

        return $newAccountName;
    }

//get user name given id
    function getUserName($userid)
    {
        $return = "";
        if (!$users = DB::table('users')->where('id', $userid)->first()) {
            echo json_encode(array('status' => 501, 'error' => 'failed to get username'));
        } else {
            $return = $users->fullname;
        }

        return $return;
    }

    function getUserById($userid)
    {
        $return = "";
        if (!$users = DB::table('users')->where('id', $userid)->first()) {
            echo json_encode(array('status' => 501, 'error' => 'failed to get unique user'));
        } else {
            $return = $users;
        }

        return $return;
    }

//get user name given id
    function getUserNameFromId($idno)
    {
        $return = "";

        if (!$users = DB::table('users')->where('idpass', $idno)->first()) {
            echo json_encode(array('status' => 501, 'error' => 'failed to get username by id'));
        } else {
            $return = $users->fullname;
        }

        return $return;
    }

    public function stkPayment($amount, $phone_number, $paymenttype, $userId, $id, $reference)
    {
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');
        $phone = json_decode($this->phoneFormat($phone_number))->formatedPhone;
        $MerchantRequestID = '';
        $status = 0;
        $ResponseDescription = '';
        $tokenResult = $this->generateAccessToken();

        $accessVals = json_decode($tokenResult, true);

        if ($accessVals['status'] == 1) {
            $accessToken = "Bearer " . $accessVals['token'];

            $shortCode = '610433';
            $passKey = 'cd980657c6b8a63108a9ef55caee1e9e74d004e041754cb952ed4e3089577632';
            $timestamp = date('YmdHis');
            $password = base64_encode($shortCode . $passKey . $timestamp);

            // The data to send to the API
            $postData = array(
                'BusinessShortCode' => $shortCode,
                'Password' => $password,
                'Timestamp' => $timestamp,
                'TransactionType' => 'CustomerPayBillOnline',
                'Amount' => $amount,
                'PartyA' => $phone,
                'PartyB' => $shortCode,
                'PhoneNumber' => $phone,
                'CallBackURL' => 'https://dibwaves.com/stkcallback.php',
                'AccountReference' => $reference,
                'TransactionDesc' => 'STK payment'
            );

            $requestBody = json_encode($postData);

//			dd($requestBody);
            // Setup cURL
            $ch = curl_init('https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest');
            curl_setopt_array($ch, array(
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'Authorization: ' . $accessToken
                ),
                CURLOPT_POSTFIELDS => $requestBody
            ));

            // Send the request
            $response = curl_exec($ch);
            /*echo "<br/>***************************<br/>$amount,$phone,$paymenttype,$userId,$id,$reference<br/>";
			echo "<br/>$requestBody<br/>";
			var_dump($response);
			echo "<br/>***************************<br/>";*/

            // Check for errors
            if ($response === false) {
                die(curl_error($ch));
            } else {
                $requestVals = json_decode($response, true);

                $MerchantRequestID = isset($requestVals['MerchantRequestID']) ? $requestVals['MerchantRequestID'] : '';
                $CheckoutRequestID = isset($requestVals['CheckoutRequestID']) ? $requestVals['CheckoutRequestID'] : '';
                $ResponseCode = isset($requestVals['ResponseCode']) ? $requestVals['ResponseCode'] : '';
                $ResponseDescription = isset($requestVals['ResponseDescription']) ? $requestVals['ResponseDescription'] : '';
                $CustomerMessage = isset($requestVals['CustomerMessage']) ? $requestVals['CustomerMessage'] : '';

                if ($ResponseCode == '0')//success
                {
                    $status = 1;
                }

                $description = ' ';
                //save details in table
                if (!DB::insert("INSERT INTO stkrequests(reqtimestamp,amount,shortcode,phone,reference,txndescription,requesttime,timecompleted,merchantReqId,checkoutReqId,responsecode,responseDesc,custMessage,status,responseResultCode,responseResultDesc,responseamount,receiptNo,responsetxndate,responsephone,userId,transactionType,otherId) VALUES('$timestamp',$amount,'$shortCode','$phone','$reference','$description','$now','$now','$MerchantRequestID','$CheckoutRequestID','$ResponseCode','$ResponseDescription','$CustomerMessage',0,'','',0,'','','',$userId,$paymenttype,$id)")) {
                    echo json_encode(array('status' => 501, 'error' => 'failed to insert stkrequest'));
                }
            }
        }

        $array = array(
            'status' => $status,
            'merchantId' => $MerchantRequestID,
            'response' => $ResponseDescription
        );

        return json_encode($array);
    }

//generate access token to be used for transactions
    function generateAccessToken()
    {
        $accessToken = "";
        $status = 0;
        $description = "";

        $url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        $credentials = base64_encode('nCxwznVLatSHse9FCMptIXQ8azFKEMNu:H1Bd2ihEG1Ckhznj');
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic ' . $credentials)); //setting a custom header
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response != false) {
            $responseVals = json_decode($curl_response, true);

            $responseVals = json_decode($curl_response, true);

            $accessToken = $responseVals['access_token'];
            $status = 1;
        } else {
            $description = "Curl Failed: " . curl_error($curl);
        }

        $array = array('status' => $status, 'token' => $accessToken, 'description' => $description);

        return json_encode($array);
    }

//get Mpesa charges
    function getMpesaB2CCharges($amount)
    {
        $commision = 0;
        $otherCharges = 0;

        //Old rates
        /*
		if($amount >= 50 && $amount <= 1000)
		{
			$commision = 10;
			$otherCharges = 15;
		}
		elseif($amount >= 1001 && $amount <= 1500)
		{
			$commision = 10;
			$otherCharges = 25;
		}
		elseif($amount >= 1501 && $amount <= 70000)
		{
			$commision = 17;
			$otherCharges = 33;
		}*/

        //New rates effective 18th December, 2016
        if ($amount >= 50 && $amount <= 1000) {
            $commision = 10;
            $otherCharges = 15;
        } elseif ($amount >= 1001 && $amount <= 1500) {
            $commision = 13;
            $otherCharges = 22;
        } elseif ($amount >= 1501 && $amount <= 5000) {
            $commision = 28;
            $otherCharges = 22;
        } elseif ($amount >= 5001 && $amount <= 10000) {
            $commision = 78;
            $otherCharges = 22;
        } elseif ($amount >= 10001 && $amount <= 20000) {
            $commision = 128;
            $otherCharges = 22;
        } elseif ($amount >= 20001 && $amount <= 50000) {
            $commision = 178;
            $otherCharges = 22;
        } elseif ($amount >= 50001 && $amount <= 70000) {
            $commision = 228;
            $otherCharges = 22;
        } elseif ($amount > 70000) {
            $commision = 328;
            $otherCharges = 22;
        }

        $array = array('commision' => $commision, 'othercharges' => $otherCharges);

        return json_encode($array);
    }

//get user registsred Mpesa phone
    function getUserRegisteredPhone($userId)
    {
        $return = "";
        if (!$phone = DB::select(DB::raw("SELECT phone FROM users WHERE id = :user_id"), array(
            'user_id' => $userId
        ))) {
            echo json_encode(array('status' => 501, 'error' => 'failed to fetch phone from users'));
        } else {
            $phone = $phone[0];
            $return = $phone->phone;
        }

        return $return;
    }

//direct savings
    function makeDirectSavings($amount, $userId, $source)
    {
        $return = 0;
        /*
			source
			1 = Mpesa
			2 = Card
			3 = Withdrawable account
		*/
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');
        if (!DB::insert("INSERT INTO savings(amount,timemade,source,userId) VALUES($amount,'$now',$source,$userId)")) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'Direct savings error'
            ));
        } else {
            $return = 1;
        }

        return $return;
    }

//	direct cash to dcfs
    function makeDirectDCF($account_id, $amount, $dcfs_type)
    {
        $return = 0;
        /*
			source
			1 = Mpesa
			2 = Card
			3 = Withdrawable account
		*/
        $ref = 'bank -' . $dcfs_type;

        if (!DB::insert("INSERT INTO accounttransactions(accountId,drcrflag,amount,reference, source_id,txntime,savingsoption) VALUES($account_id, 1, '$amount', '$ref', 6,'$this->now', 0)")) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'DCFS error'
            ));
        } else {
            $return = 1;
        }

        return $return;
    }

//withdraw funds
    function withdrawFunds($amount, $userId, $purpose, $description)
    {
        $return = 0;

        /* purpose
			1 = Withdraw to Mpesa
			2 = Buy Airtime
			3 = Transfer to savings
			4 = Transfer other
			5 = Buy shares
			6 = Repay loan
			7 = Reversal
			8 = Account access SMS
			10 = Statement charge
		*/

        if (!DB::insert("INSERT INTO withdrawals(amount,withdrawaldate,purpose,description,userId) VALUES($amount,'$this->now',$purpose,'$description',$userId)")) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'failed to add withdrawals'
            ));
        } else {
            $return = 1;
        }

        return $return;
    }

    function withdrawLipaKaro($amount, $userId, $purpose, $description)
    {
        $return = 0;

        /* purpose
			1 = Withdraw to Mpesa
			2 = Buy Airtime
			3 = Transfer to savings
			4 = Transfer other
			5 = Buy shares
			6 = Repay loan
			7 = Reversal
			8 = Account access SMS
			10 = Statement charge
		*/
        $account_id = $this->getSchoolFeesFinancing($userId);
        if (!DB::insert("INSERT INTO accounttransactions(accountId,drcrflag,amount,reference,source_id, txntime, savingsoption) VALUES($account_id,2,$amount,'$description',11,'$this->now',0)")) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'failed to add transactions'
            ));
        } else {
            $return = 1;
        }

        return $return;
    }

//withdraw funds from holding account
    function withdrawHoldingFunds($amount, $userId, $purpose, $description)
    {
        $return = 0;
        /* purpose
			1 = Savings
			2 = Repay loan
			3 = Buy shares
			4 = Buy shares
			5 = Wallet
			6 = Target savings
		*/

        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');


        if (!DB::insert("INSERT INTO holdingwithdrawals(amount,withdrawaldate,purpose,description,userId) VALUES($amount,'$now',$purpose,'$description',$userId)")) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'failed to add holding withdrawals'
            ));
        } else {
            $return = 1;
        }

        return $return;
    }

//make savings
    function makeSavings($amount, $userId, $otherId, $source)
    {
        $return = 0;

        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');

        /*if($otherId == 0)//if for self account
		{*/

        //check if registration fee is paid
        if (!$check = DB::table('users')->where('id', $userId)->get()) {
            echo json_encode(array('status' => 501, 'error' => 'failed to fetch user'));
        } else {
            $feepaid = $check->regfee;

            if ($feepaid >= 200)//reg fee paid
            {
                //check if membership fee is paid
                $amount = $this->membershipPaid($amount, $userId);

                if ($amount == 0) {
                    $return = 1;
                } else {
                    //check if shares are paid
                    $amount = $this->minimumSharesPaid($amount, $userId, $source);
                    if ($amount == 0) {
                        $return = 1;
                    } else {
                        if (!DB::insert("INSERT INTO savings(amount,timemade,source,userId) VALUES($amount,$now,$source,$userId)")) {
                            echo json_encode(array('error' => 'failed to add into savings', 'status' => '501'));
                        } else {
                            $return = 1;
                        }
                    }
                }
            } else {
                //get reg fee paid and add it to the amount
                $amount += $feepaid;

                if ($amount == 200)//this goes to registration fee
                {
                    /* */
                    if (!DB::update('UPDATE USERS SET regfee = ? WHERE id = ?', [
                        200,
                        $userId
                    ])) {
                        echo json_encode(array('status' => 501, 'error' => 'failed to set registration fee'));
                    } else {
                        //update session
                        $_SESSION['status'] = 1;
                        $this->preActivate($userId);
                        $return = 1;
                    }
                } elseif ($amount > 200)//if excess paid deduct reg fee and the rest as savings
                {

                    if (!DB::update('UPDATE users SET regfee = ? WHERE id = ?', [
                        200,
                        $userId
                    ])) {
                        echo json_encode(array('status' => 501, 'error' => 'failed to set registration fee 2'));
                    } else {
                        //update session
                        $_SESSION['status'] = 1;
                        $this->preActivate($userId);

                        //check if membership fee is paid
                        $amount = $this->membershipPaid($amount - 200, $userId);

                        if ($amount == 0) {
                            $return = 1;
                        } else {
                            //check if shares are paid
                            $amount = $this->minimumSharesPaid($amount, $userId, $source);

                            if ($amount == 0) {
                                $return = 1;
                            } else {
                                //update savings
                                if (!DB::insert("INSERT INTO savings(amount,timemade,source,userId) VALUES( $amount, $now, $source, $userId)")) {
                                    echo json_encode(array(
                                        'status' => 501,
                                        'error' => 'failed to add to savings'
                                    ));
                                } else {
                                    $return = 1;
                                }
                            }
                        }
                    }
                } else {

                    if (!DB::update('UPDATE users SET regfee = ? WHERE id = ?', [
                        $amount,
                        $userId
                    ])) {
                        echo json_encode(array('status' => 501, 'error' => 'failed to set reqistration fee'));
                    } else {
                        $return = 1;
                    }
                }
            }
        }

        /*}
		else
		{
			if(!mysqli_query($this->link,"INSERT INTO savings(amount,timemade,source,userId) VALUES($amount,'$now',$source,$otherId)"))
			{
				echo "Error ".mysqli_error($this->link);
			}
			else
			{
				//insert into other savings table
				$savingsId = mysql_insert_id($this->link);
				if(!mysqli_query($this->link,"INSERT INTO othersavings (savingsId,userId,entity) VALUES($savingsId,$userId,$otherId)"))
				{
					echo "Error ".mysqli_error($this->link);
				}
				else
				{
					$return = 1;
				}
			}
		}*/

        return $return;
    }

    function getRandomFloatAmount()
    {
        $newAmount = 1;
        $prefix = rand(10, 200) / 100;

        $newAmount = $prefix;

        return $newAmount;
    }

    function liveCreditCardPayment($paymentType, $userId, $cardNo, $expiryMonth, $expiryYear, $csc, $amount)
    {
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');
        $status = 0;
        $orderInfo = '';
        $txnRef = '';
        $batch = '';
        $txnNo = '';
        $resposneCode = '';
        $description = '';
        //validate card number
        if ($this->validateCard($cardNo) === true) {
            if ($this->validateMonth($expiryMonth) === true) {
                if (strlen($expiryMonth) == 1) {
                    $expiryMonth = '0' . $expiryMonth;
                }

                if ($this->validateYear($expiryYear) === true) {
                    if ($this->validateAmount($amount) === true) {
                        $theYear = substr($expiryYear, 2, 2);

                        $finalExpiryMonth = $theYear . $expiryMonth;

                        $merchantInfo = substr(md5(rand()), 0, 7) . time();

                        $paramArrays = array(
                            'vpc_Version' => 1,
                            'vpc_Command' => 'pay',
                            'vpc_AccessCode' => '01819F5F',
                            'vpc_MerchTxnRef' => $merchantInfo,
                            'vpc_Merchant' => '61048002',
                            'vpc_OrderInfo' => $merchantInfo,
                            'vpc_Amount' => $amount,
                            'vpc_Locale' => 'en'
                        );

                        ksort($paramArrays);

                        //get card settings
                        $merchantIdTable = '';
                        $cardSecretTable = '';
                        $dolarRateTable = 0;
                        $bankRateTable = 0;
                        $homepesaRateTable = 0;
                        $accessCodeTable = '';
                        if (!$cardSettings = DB::table('cardsettings')->get()) {
                            echo json_encode(array(
                                'status' => 501,
                                'error' => 'failed to fetch card settings'
                            ));
                        } else {

                            $merchantIdTable = $cardSettings->merchantid;
                            $cardSecretTable = $cardSettings->hashkey;
                            $dolarRateTable = $cardSettings->dollarrate;
                            $bankRateTable = $cardSettings->bankrate;
                            $homepesaRateTable = $cardSettings->homepesarate;
                            $accessCodeTable = $cardSettings->accesscode;
                        }

                        $secret = $cardSecretTable;//provided by the bank

                        $appendAmp = 0;
                        $hashString = "";
                        //generate string for getting secure hash
                        foreach ($paramArrays as $key => $value) {
                            if (strlen($value) > 0) {
                                if ($appendAmp == 0) {
                                    $hashString .= urlencode($key) . '=' . urlencode($value);
                                    $appendAmp = 1;
                                } else {
                                    $hashString .= '&' . urlencode($key) . "=" . urlencode($value);
                                }
                            }
                        }
                        $hashString = urldecode($hashString);
                        $hashedvalue = strtoupper(hash_hmac('SHA256', $hashString, pack('H*', $secret)));
                        $saveCardNo = substr($cardNo, 0, 4) . "****" . substr($cardNo, -4, 4);
                        //save in table

                        if (!DB::insert("INSERT INTO twoparty(userId,cardno,amount,hashvalue,orderinfo,vpcTxnRef,batchno,txnNo,responsecode,txnTime,responsemessage,kcbamount,homepesaamount,customeramount,paymenttype) VALUES($userId,$saveCardNo,$amount,$hashedvalue,'',$merchantInfo,'','','',$now,'',0,0,0,$paymentType)")) {
                            echo json_encode(array(
                                'status' => 501,
                                'error' => 'failed to insert into two party'
                            ));
                        } else {
                            $requestId = DB::table('twoparty')->insertGetId();
                            $url = 'https://migs.mastercard.com.au/vpcdps';
                            $fields_string = "";
                            $fields = array(
                                'vpc_Version' => urlencode('1'),
                                'vpc_Command' => urlencode('pay'),
                                'vpc_AccessCode' => urlencode('01819F5F'),
                                'vpc_MerchTxnRef' => urlencode($merchantInfo),
                                'vpc_Merchant' => urlencode('61048002'),
                                'vpc_OrderInfo' => urlencode($merchantInfo),
                                'vpc_Amount' => urlencode($amount),
                                'vpc_CardNum' => urlencode($cardNo),
                                'vpc_CardExp' => urlencode($finalExpiryMonth),
                                'vpc_CardSecurityCode' => urlencode($csc),
                                'vpc_SecureHash' => $hashedvalue,
                                'vpc_SecureHashType' => 'SHA256'
                            );

                            //url-ify the data for the POST
                            foreach ($fields as $key => $value) {
                                $fields_string .= $key . '=' . $value . '&';
                            }
                            rtrim($fields_string, '&');

                            //open connection
                            $ch = curl_init();

                            //set the url, number of POST vars, POST data
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_POST, count($fields));
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                            //execute post
                            $result = curl_exec($ch);
                            //close connection
                            curl_close($ch);

                            if ($result == false) {
                                echo "Failed request";
                            } else {
                                parse_str($result, $output);

                                $KCB_vpc_Amount = $output['vpc_Amount'];
                                $KCB_vpc_Command = $output['vpc_Command'];
                                $KCB_vpc_MerchTxnRef = $output['vpc_MerchTxnRef'];
                                $KCB_vpc_Merchant = $output['vpc_Merchant'];
                                $KCB_vpc_OrderInfo = $output['vpc_OrderInfo'];
                                $KCB_vpc_Version = $output['vpc_Version'];
                                $KCB_vpc_BatchNo = $output['vpc_BatchNo'];
                                $KCB_vpc_Locale = $output['vpc_Locale'];
                                $KCB_vpc_Message = $output['vpc_Message'];
                                $KCB_vpc_TransactionNo = $output['vpc_TransactionNo'];
                                $KCB_vpc_TxnResponseCode = $output['vpc_TxnResponseCode'];

                                $orderInfo = $KCB_vpc_OrderInfo;
                                $txnRef = $KCB_vpc_MerchTxnRef;
                                $batch = $KCB_vpc_BatchNo;
                                $txnNo = $KCB_vpc_TransactionNo;
                                $resposneCode = $KCB_vpc_TxnResponseCode;

                                //update table
                                if (!DB::update('UPDATE twoparty SET orderinfo = ?, batchno = ?, txnNo = ?, responsecode = ?, responsemessage = ?  WHERE twopartyId = ?', [
                                    $orderInfo,
                                    $batch,
                                    $txnNo,
                                    $resposneCode,
                                    $KCB_vpc_Message,
                                    $requestId
                                ])) {
                                    echo json_encode(array(
                                        'status' => 501,
                                        'error' => 'failed to set into two party'
                                    ));
                                }

                                if ($resposneCode == '0')//success
                                {
                                    $status = 1;

                                    $amountInKsh = $amount / 100 * $dolarRateTable;
                                    $toKCB = $bankRateTable / 100 * $amountInKsh;
                                    $toHomepesa = $homepesaRateTable / 100 * $amountInKsh;
                                    $toCustomer = $amountInKsh - $toKCB - $toHomepesa;

                                    //update bank and homepesa amount
                                    if (!DB::update('UPDATE twoparty SET kcbamount = ?, homepesaamount = ?, customeramount = ? WHERE twopartyId = ?', [
                                        $toKCB,
                                        $toHomepesa,
                                        $toCustomer,
                                        $requestId
                                    ])) {
                                        echo json_encode(array(
                                            'status' => 501,
                                            'error' => 'failed to set into two party 2'
                                        ));
                                    }

                                    //save commision
                                    $this->recordCommision(7, $userId, $toHomepesa, $toKCB);

                                    //process transaction
                                    switch ($paymentType) {
                                        case '1'://savings
                                            $this->makeSavings($toCustomer, $userId, 0, 2);
                                            break;

                                        case '2'://loan repayment
                                            $this->repayLoan($toCustomer, $userId, $id, 2);
                                            break;

                                        case '3'://savings to other entity
                                            $this->makeSavingsEntity($toCustomer, $userId, $id, 2);
                                            break;

                                        case '4'://deposit to withdrawable account
                                            $this->makeWithdrawableDeposit($toCustomer, $userId, 2);
                                            break;

                                        case '5'://buy shares
                                            $this->buyShares($toCustomer, $userId, 2);
                                            break;

                                        case '6'://save to group (individual /ac)
                                            $this->saveToGroup($userId, $toCustomer, 0, 2, "Card payment");
                                            break;

                                        case '7'://save to group
                                            $this->saveToGroup($id, $toCustomer, $userId, 2, "Card payment");
                                            break;

                                        case '8'://group loan repayment
                                            $this->repayGroupLoan($toCustomer, $userId, $id, 2);
                                            break;

                                        case '9'://admin group loan repayment
                                            $this->repayAdminGroupLoan($toCustomer, $userId, $id, 2);
                                            break;

                                        case '10'://deposit to akiba account
                                            $this->makeAkibaDeposit($toCustomer, $userId, 2, md5($cardNo));
                                            break;

                                        case '11'://deposit to Mpesa Holding account
                                            $this->makeWithdrawableHoldingDeposit($toCustomer, $userId, 2);
                                            break;

                                        default:
                                            //do nothing
                                            break;
                                    }
                                }

                                $description = $KCB_vpc_Message;
                            }
                        }
                    } else {
                        $status = 0;
                        $description = 'Invalid amount';
                    }
                } else {
                    $status = 0;
                    $description = 'Invalid card expiry year';
                }
            } else {
                $status = 0;
                $description = 'Invalid card expiry month';
            }
        } else {
            $status = 0;
            $description = 'Invalid card number ' . $cardNo;
        }

        $array = array('status' => $status, 'description' => $description);

        return json_encode($array);
    }

    function validateCard($cardNo)//check if card number is correct
    {
        $return = false;

        if (is_numeric($cardNo) && strlen($cardNo) >= 15) {
            $return = true;
        }

        return $return;
    }

    function validateMonth($expiryMonth)//check if expiry month is ok
    {
        $return = false;

        if (is_numeric($expiryMonth) && $expiryMonth >= 1 && $expiryMonth <= 31) {
            $return = true;
        }

        return $return;
    }

    function validateYear($expiryYear)//check if expiry year is ok
    {
        $return = false;

        if (is_numeric($expiryYear) && $expiryYear >= 2005 && $expiryYear <= 2050) {
            $return = true;
        }

        return $return;
    }

    function validateAmount($amount)//check if amount is correct
    {
        $return = false;

        if (is_numeric($amount) && $amount >= 10 && $amount <= 1000000) {
            $return = true;
        }

        return $return;
    }

//record commisions
    function recordCommision($type, $userId, $commision, $otherCharges)
    {
        $return = 0;

        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');

        $taxrate = 0;

        switch ($type) {
            case '1'://Airtime
                $taxrate = 0;
                break;

            case '2'://Interaccount transfer
                $taxrate = 0;
                break;

            case '3'://Withdraw - Mpesa
                $taxrate = 0;
                break;

            case '4'://Withdraw - bank deposit
                $taxrate = 0;
                break;

            case '5'://Withdraw - cheque
                $taxrate = 0;
                break;

            case '6'://Akiba penalty
                $taxrate = 0;
                break;

            case '7'://Card chargs
                $taxrate = 0;
                break;

            case '8'://SMS charges
                $taxrate = 0;
                break;

            case '9'://Statement charges
                $taxrate = 0;
                break;

            default:
                $taxrate = 0;
                break;
        }

        $tax = $commision * ($taxrate / 100);
        //insert record
        if (!DB::insert("INSERT INTO commisions(commisiontype,amount,othercharges,commisiontime,tax,userId) VALUES($type,$commision,$otherCharges,'$now',$tax,$userId)")) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'failed to add commision'
            ));
        } else {
            $return = 1;
        }

        return $return;
    }

//check if membershipfee is paid
    function membershipPaid($amount, $userId)
    {
        $return = $amount;

        /*$return = 0;
		include 'connectconfig.php';
		if(!$membership = mysqli_query($this->link,"SELECT * FROM users WHERE id=$userId"))
		{
			echo "Error ".mysqli_error($this->link);
		}
		else
		{
			$membershiprow = mysqli_fetch_array($membership,MYSQLI_ASSOC);
			$membershipfee = $membershiprow['membershipfee'];

			if($membershipfee >= 1000)//reg fee paid return the whole amount paid for use in next test
			{
				$return = $amount;
			}
			else
			{
				$newAmount = $amount + $membershipfee;

				if($newAmount >= 1000)//resulting amount greater than 1000
				{
					//save fee of 1000 paid
					if(!mysqli_query($this->link,"UPDATE users SET membershipfee = 1000 WHERE id=$userId"))
					{
						echo "Error ".mysqli_error($this->link);
					}
					else
					{
						$return = $newAmount - 1000;
					}
				}
				else
				{
					//save the whole amount and return 0
					if(!mysqli_query($this->link,"UPDATE users SET membershipfee = $newAmount WHERE userId=$userId"))
					{
						echo "Error ".mysqli_error($this->link);
					}
					else
					{
						$return = 0;
					}
				}
			}
		}*/

        return $return;
    }

//check if minimum shares have been paid
    function minimumSharesPaid($amount, $userId, $source)
    {
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');

        //get total shares paid
        $return = 0;
        $shares = 0;

        if (!$savvals = DB::table('shares')->selectRaw('SUM(amount) AS yote')->where('userId', $userId)->get()) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'failed to fetch summed up user shares'
            ));
        } else {
            $savvalsrow = mysqli_fetch_array($savvals, MYSQLI_ASSOC);

            $shares = isset($savvals->yote) ? $savvals->yote : 0;

            if ($shares >= 100) {
                $return = $amount;
            } else {
                $newAmount = $amount + $shares;

                if ($newAmount >= 100)//resulting savings greater than 100
                {
                    $return = $newAmount - 100;

                    $amountToInsertAsShare = 100 - $shares;

                    //insert this into shares table$userId
                    if (!DB::insert("INSERT INTO shares (amount,datetimemade,source,userId) VALUES($amountToInsertAsShare,$now,$source,$userId)")) {
                        echo json_encode(array('status' => 501, 'error' => 'failed to add shares 2'));

                    }
                } else//insert the whole amount as shares
                {

                    //insert this into shares table$userId
                    if (!DB::insert("INSERT INTO shares (amount,datetimemade,source,userId) VALUES($amount,$now,$source,$userId)")) {
                        echo json_encode(array('status' => 501, 'error' => 'failed to add shares'));
                    }
                }
            }
        }

        return $return;
    }

//function preactivate user
    function preActivate($userId)
    {
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');
        if (!DB::update('UPDATE users SET acstatus = ?,timepaid = ? WHERE id = ?', [
            1,
            $now,
            $userId
        ])) {
            echo json_encode(array('status' => 501, 'error' => 'failed to activate account status'));
        }
    }

//function to repay loan
    function repayLoan($amount, $userId, $loanId, $source)
    {
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');
        $total_asset_collection = 0;
        $return = 0;

        $thisLoanBalance = $this->getSpecificLoanBalance($loanId);

        if ($amount <= $thisLoanBalance)//user paid less than or equal to loan balance
        {
            if (!$totalAssetCollectionResult = mysqli_query($this->link, "SELECT asset_collected FROM loanpayments ORDER BY asset_collected DESC LIMIT 1")) {

            } else {
                $assetcollectionrow = mysqli_fetch_array($totalAssetCollectionResult, MYSQLI_ASSOC);
                $total_asset_collection = $assetcollectionrow['asset_collected'];
                $total_asset_collection += $amount;
            }
//			dd($total_asset_collection);

            if (!DB::insert("INSERT INTO loanpayments(amount,timemade,source,userId,loanId,asset_collected) VALUES($amount,'$now',$source,$userId,$loanId,$total_asset_collection)")) {
                echo json_encode(array('status' => 501, 'error' => 'failed to add loan payment'));
            } else {

                $return = 1;
            }
        } elseif ($amount > $thisLoanBalance)//user paid more than loan balance
        {
            if (!DB::insert("INSERT INTO loanpayments(amount,timemade,source,userId,loanId) VALUES($thisLoanBalance,'$now',$source,$userId,$loanId)")) {
                echo json_encode(array('status' => 501, 'error' => 'failed to add loan payment'));
            } else {
                $reversalAmount = $amount - $thisLoanBalance;

                //credit user withdrawable
                $this->makeWithdrawableDeposit($reversalAmount, $userId, 4);

                $return = 1;
            }
        }

        return $return;
    }

//get loan balance of specific loan
    function getSpecificLoanBalance($loanId)
    {
        $loanBalance = 0;
        //get total loan to be repaid
        $loanApplied = 0;
        if (!$loan = DB::select(DB::raw("SELECT SUM(amount) AS deni FROM loans WHERE loanId = :loanId OR correspondingloanId = :correspondingloanId"), array(
            'loanId' => $loanId,
            'correspondingloanId' => $loanId,
        ))) {
            echo json_encode(array('status' => 501, 'error' => 'query execution error'));
        } else {
            $loan = $loan[0];
            $loanApplied = isset($loan->deni) ? $loan->deni : 0;
        }

        //get loan repayments
        $loanRepaid = 0;
        if (!$repaid = DB::select(DB::raw("SELECT SUM(amount) AS lipwa FROM loanpayments WHERE loanId = $loanId"), array(
            'loanId' => $loanId
        ))) {
            echo json_encode(array('status' => 501, 'error' => 'query execution error'));
        } else {
            $repaid = $repaid[0];

            $loanRepaid = isset($repaid->lipwa) ? $repaid->lipwa : 0;
        }

        $loanBalance = $loanApplied - $loanRepaid;

        return round($loanBalance, 2);
    }

    function makeWithdrawableDeposit($amount, $userId, $source)//deposits funds to withdrawable account
    {
        /*
			source
			1 = Mpesa
			2 = Card
			3 = Transfer
			4 = Reversal
			5 = Loan recovery
			6 = From Akiba

		*/
        $status = 0;
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');

        if (!DB::insert("INSERT INTO deposits (amount,depositdate,depositmethod,userId) VALUES($amount,'$now',$source,$userId)")) {
            echo json_encode(array('status' => 501, 'error' => 'query execution error'));
        } else {
            $status = 1;
        }

        return $status;
    }

//get cheque charges
    function getChequeCharges()
    {
        return 500;
    }

//save with other
    function makeSavingsEntity($amount, $userId, $entity, $source)
    {
        $return = 0;
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');
//
        if (!DB::insert("INSERT INTO othersavings (amount,userId,entity,transactiontime,source) VALUES($amount,$userId,$entity,$now,$source)")) {
            echo json_encode(array('status' => 501, 'error' => 'Failed to fetch other savings'));
        } else {
            $return = 1;
        }

        return $return;
    }

//get member loan limit
    function getInstantLoanLimit($userId)
    {
        $return = 0;

        if (!$getUserID = DB::table('users')->where('id', $userId)->get()) {
            echo json_encode(array('status' => 501, 'error' => 'Error in fetching user'));
        } else {
            if (count($getUserID) > 0) {

                $getUserID = $getUserID[0];
                $idNumber = $getUserID->idpass;
//				dd($getUserID);

                if (!$getScore = DB::table('creditdetails')->where('identity_number', $idNumber)->orderBy('creditId', 'DESC')->get()) {
                    echo json_encode(array('status' => 501, 'error' => 'Error in fetching user'));
                } else {
                    if (DB::table('creditdetails')->where('identity_number', $idNumber)->orderBy('creditId', 'DESC')->count() > 0) {
                        $getScore = $getScore[0];
                        $score = $getScore->credit_score;
                    } else {
                        $score = 905;
                    }
                }

                if ($score < 500) {
                    $eligibleAmount = 0;
                } elseif ($score >= 500 && $score <= 599) {
                    $return = 15000;
                } elseif ($score >= 600 && $score <= 650) {
                    $return = 20000;
                } elseif ($score >= 650 && $score <= 699) {
                    $return = 30000;
                } elseif ($score >= 700 && $score <= 749) {
                    $return = 45000;
                } elseif ($score >= 750 && $score <= 799) {
                    $return = 60000;
                } elseif ($score >= 800 && $score <= 849) {
                    $return = 90000;
                } elseif ($score >= 850 && $score <= 900) {
                    $return = 100000;
                } else {
                    $return = 0;
                }
            }
        }

        return $return;
    }

    public function selfScoring(Request $request)
    {
        $userId = $request->get('user_id');
        $return = 0;
        if (isset($userId)) {
            if (!$getUserID = DB::table('users')->where('id', $userId)->get()) {
                echo json_encode(array('status' => 501, 'error' => 'Error in fetching user'));
            } else {
                if (count($getUserID) > 0) {

                    $getUserID = $getUserID[0];
                    $idNumber = $getUserID->idpass;
//				dd($getUserID);

                    if (!$getScore = DB::table('creditdetails')->where('identity_number', $idNumber)->orderBy('creditId', 'DESC')->get()) {
                        echo json_encode(array('status' => 501, 'error' => 'Error in fetching user'));
                    } else {
                        if (DB::table('creditdetails')->where('identity_number', $idNumber)->orderBy('creditId', 'DESC')->count() > 0) {
                            $getScore = $getScore[0];
                            $score = $getScore->credit_score;
                        } else {
                            $score = 0;
                        }
                    }

                    if ($score < 50) {
                        $eligibleAmount = 0;
                    } elseif ($score >= 50 && $score <= 99) {
                        $return = 300;
                    } elseif ($score >= 100 && $score <= 199) {
                        $return = 500;
                    } elseif ($score >= 200 && $score <= 299) {
                        $return = 1000;
                    } elseif ($score >= 300 && $score <= 399) {
                        $return = 1500;
                    } elseif ($score >= 400 && $score <= 499) {
                        $return = 3000;
                    } elseif ($score >= 500 && $score <= 599) {
                        $return = 15000;
                    } elseif ($score >= 600 && $score <= 650) {
                        $return = 20000;
                    } elseif ($score >= 650 && $score <= 699) {
                        $return = 30000;
                    } elseif ($score >= 700 && $score <= 749) {
                        $return = 45000;
                    } elseif ($score >= 750 && $score <= 799) {
                        $return = 60000;
                    } elseif ($score >= 800 && $score <= 849) {
                        $return = 90000;
                    } elseif ($score >= 850 && $score <= 900) {
                        $return = 100000;
                    } else {
                        $return = 0;
                    }
                }
            }
            $response = array('status' => 200, 'instant_financing' => number_format($return, 2));
        } else {
            $response = array('status' => 501, 'error' => ' Missing parameters in your request');
        }
        echo json_encode($response);
    }

//source of savings
    function savingsSource($flag)
    {
        $source = "";
        switch ($flag) {
            case '1':
                $source = "Mpesa";
                break;

            case '2':
                $source = "Card";
                break;

            case '3':
                $source = "DIB My Account";
                break;

            case '4':
                $source = "Loan Recovery";
                break;

            case '5':
                $source = "Transfer to DIB My Account";
                break;

            case '6':
                $source = "Mpesa Holding a/c";
                break;

            default:
                # code...
                break;
        }

        return $source;
    }

    function formatDateTime($dateTime)
    {
        if (count(explode("-", $dateTime)) < 3 && count(explode(":", $dateTime)) < 3) {
            $dateTime == "0000-00-00 00:00:00";
        }

        if ($dateTime == "0000-00-00 00:00:00") {
            return "-";
        } else {
            $dateTimeVals = explode(" ", $dateTime);
            $dateVals = explode("-", $dateTimeVals[0]);

            if (count($dateTimeVals) == 2 && count($dateVals) == 3) {
                return $dateVals[2] . "-" . $dateVals[1] . "-" . $dateVals[0] . " at " . $dateTimeVals[1];
            } else {
                return "-";
            }
        }
    }

    function buyAirtime($operator, $phone, $amount)//function to buy airtime (processing is done outside the function as this could be used by either sacco members or admin)
    {


        $status = "failed";
        $discount = 0;
        $requestId = "";
        $errorMessage = "";

        //validate phone number
        if (substr($phone, 0, 1) == '7' && strlen($phone) == 9)//numbers starting with 7 eg 712123123
        {
            $phone = substr_replace($phone, '+254', 0, 0);
        } elseif (substr($phone, 0, 2) == '07' && strlen($phone) == 10)//numbers starting with 07 eg 0712123123
        {
            $phone = substr_replace($phone, '+254', 0, 1);
        } elseif (substr($phone, 0, 3) == '254' && strlen($phone) == 12)//numbers starting with 254 eg 254712123123
        {
            $phone = substr_replace($phone, '+', 0, 0);
        }

        if (substr($phone, 0, 4) == '+254' && strlen($phone) == 13) {
            //validate phone numbers
            $safaricomArray = array(
                '+254700',
                '+254701',
                '+254702',
                '+254703',
                '+254704',
                '+254705',
                '+254706',
                '+254707',
                '+254708',
                '+254710',
                '+254711',
                '+254712',
                '+254713',
                '+254714',
                '+254715',
                '+254716',
                '+254717',
                '+254718',
                '+254719',
                '+254720',
                '+254721',
                '+254722',
                '+254723',
                '+254724',
                '+254725',
                '+254726',
                '+254727',
                '+254728',
                '+254729',
                '+254790',
                '+254791',
                '+254792'
            );

            $airtelArray = array(
                '+254730',
                '+254731',
                '+254732',
                '+254733',
                '+254734',
                '+254735',
                '+254736',
                '+254737',
                '+254738',
                '+254739',
                '+254750',
                '+254751',
                '+254752',
                '+254753',
                '+254754',
                '+254755',
                '+254756',
                '+254785',
                '+254786',
                '+254787',
                '+254788',
                '+254789'
            );

            $telkomArray = array('+254770', '+254771', '+254772', '+254773', '+254774', '+254775');

            $goon = 0;
            $resp = "";

            switch ($operator) {
                case 'Safaricom':
                    if (in_array(substr($phone, 0, 7), $safaricomArray)) {
                        $goon = 1;
                    } else {
                        $resp = "The phone number you entered is not a valid Safaricom phone number";
                    }
                    break;

                case 'Airtel':

                    if (in_array(substr($phone, 0, 7), $airtelArray)) {
                        $goon = 1;
                    } else {
                        $resp = "The phone number you entered is not a valid Airtel phone number";
                    }
                    break;

                case 'Telkom':
                    if (in_array(substr($phone, 0, 7), $telkomArray)) {
                        $goon = 1;
                    } else {
                        $resp = "The phone number you entered is not a valid Orange phone number";
                    }
                    break;

                default:
                    $resp = "Invalid operator";
                    break;
            }

            if ($goon == 1)//correct phone
            {
                //Specify your credentials
                $username = "homepesasacco";
                $apiKey = "30ee63ad21f5269a7164dea6c3f4d40af731f0be2f4ce1bc093efb93f8b3dcbd";


                $recipients = array(
                    array("phoneNumber" => $phone, "amount" => "KES " . $amount)
                );
                $recipientStringFormat = json_encode($recipients);

                $gateway = new AfricasTalkingGateway($username, $apiKey);

                try {
                    $results = $gateway->sendAirtime($recipientStringFormat);
                    foreach ($results as $result) {
                        $status = $result->status;
                        $discount = $result->discount;
                        $requestId = $result->requestId;

                        //Error message is important when the status is not Success
                        $errorMessage = $result->errorMessage;
                    }
                } catch (AfricasTalkingGatewayException $e) {
                    $errorMessage = $e->getMessage();
                }
            } else {
                $errorMessage = $resp;
            }
        } else {
            echo "<p style='color:red;font-weight:bold;'>Enter a valid phone number</p>";
        }

        $response = array(
            'status' => $status,
            'discount' => $discount,
            'requestId' => $requestId,
            'errorMessage' => $errorMessage
        );

        return json_encode($response);
    }

//get the duration the member has been active in days
    function getMembershipDuration($userId)
    {
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');
        $days = 0;

        if (!$userDetails = DB::table('users')->where('id', $userId)->get()) {
            echo json_encode(array('status' => 501, 'error' => 'Error in fetching user'));
        } else {
            $userDetails = $userDetails[0];
            $dateActivated = $userDetails->timeactivated;
            $days = (strtotime($now) - strtotime($dateActivated)) / (60 * 60 * 24);
        }

        return $days;
    }

    /*

	  # COPYRIGHT (C) 2014 AFRICASTALKING LTD <www.africastalking.com>

	  AFRICAStALKING SMS GATEWAY CLASS IS A FREE SOFTWARE IE. CAN BE MODIFIED AND/OR REDISTRIBUTED
	  UNDER THE TERMS OF GNU GENERAL PUBLIC LICENCES AS PUBLISHED BY THE
	  FREE SOFTWARE FOUNDATION VERSION 3 OR ANY LATER VERSION

	  THE CLASS IS DISTRIBUTED ON 'AS IS' BASIS WITHOUT ANY WARRANTY, INCLUDING BUT NOT LIMITED TO
	  THE IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
	  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
	  OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

	*/
    function saveToGroup($groupId, $amount, $userId, $source, $remarks)
    {
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');

        $return = 0;

        if (!DB::insert("INSERT INTO groupsavings(savedby,amount,timemade,source,groupId,remarks) VALUES($userId,$amount,'$now',$source,$groupId,'$remarks')")) {
            echo json_encode(array('status' => 501, 'error' => 'query execution error'));
        } else {
            $return = 1;
        }

        return $return;
    }

    function checkDailyCardLimit($userId)
    {
        date_default_timezone_set('Africa/Nairobi');
        $today = date('Y-m-d');

        $limit = 0;
        $maximum = 4000;

        if (!$get = DB::select("SELECT SUM(amount) as yote FROM twoparty WHERE userId = $today AND txnTime LIKE '%$today%' AND responsecode='0'")) {
            echo json_encode(array('status' => 501, 'error' => 'Error failed to fetch'));
        } else {
            $get = $get[0];
            $amountDeposited = isset($get->yote) ? $get->yote : 0;
            $limit = $maximum - $amountDeposited;
        }

        return $limit;
    }

//function to buy shares
    function buyShares($amount, $userId, $paymentOption)
    {
        $return = 0;
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');

        //get total shares bought
        if (!$ttl = DB::select(DB::raw("SELECT SUM(amount) AS yote FROM savings WHERE userId = :userId"), array(
            'userId' => $userId
        ))) {
            echo json_encode(array('status' => 501, 'error' => 'share error'));
        } else {
            $ttl = $ttl[0];

            $totalSharesBought = $ttl->yote;

            $shareBalToMax = 20000 - $totalSharesBought;//amount left that they can purchase

            if ($amount <= $shareBalToMax)//insert
            {
                if (!DB::insert("INSERT INTO shares(amount,datetimemade,source,userId) VALUES($amount,$now,$paymentOption,$userId)")) {
                    echo json_encode(array('status' => 501, 'error' => 'Share insert error'));
                } else {
                    $return = 1;
                }
            } else//insert balance and remainder to withdrawable
            {
                //insert the balance
                if (!DB::insert("INSERT INTO shares(amount,datetimemade,source,userId) VALUES($shareBalToMax,$now,$paymentOption,$userId)")) {
                    echo json_encode(array('status' => 501, 'error' => 'Share insert error'));
                } else {
                    //send remainder to withdrawable
                    $withAmount = $amount - $shareBalToMax;
                    $this->makeWithdrawableDeposit($withAmount, $userId, 4);//marked as reversal
                    $return = 1;
                }
            }
        }

        return $return;
    }

//get the loan type
    function getLoanType($loanId)
    {
        $type = "";

        if (!$getTypeName = mysqli_query($this->link, "SELECT * FROM loantypes WHERE loantypeId=$loanId")) {
            echo "Error " . mysqli_error($this->link);
        } else {
            if (mysqli_num_rows($getTypeName) > 0) {
                $getTypeNamerow = mysqli_fetch_array($getTypeName, MYSQLI_ASSOC);
                $type = $getTypeNamerow['typename'];
            }
        }

        return $type;
    }

//validate name
    function validateName($name)
    {
        $return = 1;
        $nameVals = explode(" ", $name);

        if (count($nameVals) >= 2) {
            //check if either name contains numbers
            /*foreach($nameVals as $namepart)
			{
				if(!ctype_alpha($namepart))
				{
					$return = 0;
				}
			}*/
        } else {
            $return = 0;
        }

        return $return;
    }

//get user registsred Mpesa phone
    function getUserIdNo($userId)
    {
        $return = "";

        if (!$phone = DB::table('users')->where('id', $userId)->get()) {
            echo json_encode(array('status' => 501, 'error' => 'could not fetch user'));
        } else {
            $phone = $phone[0];
            $return = $phone->idpass;
        }

        return $return;
    }

    function getAdminGroupLoanBalance($groupId)
    {
        $totalThisGroupLoans = 0;//total loans taken by group
        $totalThisGroupLoanPayments = 0;//total repayments of loans taken by group
        //get loans taken by this group
        if (!$adminGroupLoans = DB::select(DB::raw("SELECT SUM(amount) AS adminmkopo FROM admingrouploanapplication WHERE groupId= :grpID AND status = :status"), array(
            'grpID' => $groupId,
            'status' => 1
        ))) {
            echo json_encode(array('status' => 501, 'error' => 'could not fetch sum amount'));
        } else {
            $adminGroupLoans = $adminGroupLoans[0];
            $totalThisGroupLoans = isset($adminGroupLoans->adminmkopo) ? $adminGroupLoans->adminmkopo : 0;
        }
        //get loan repayments for this group
        if (!$adminGroupLns = DB::table('admingrouploanapplication')->where('groupId', $groupId)->where('status', 1)->get()) {
            echo json_encode(array('status' => 501, 'error' => 'failed to fetch group loan balance'));
        } else {
            for ($i = 0; $i < count($adminGroupLns); $i++) {
                $adminLoanId = $adminGroupLns->applicationId;
                if (!$adminGroupPayments = DB::select(DB::raw("SELECT SUM(amount) AS adminlipa FROM admingrouploanpayments WHERE loanId = :loanId"), array('loanId' => $adminLoanId))) {
                    echo json_encode(array('status' => 501, 'error' => 'failed to fetch sum from admin lipa'));
                } else {
                    $adminGroupPayments = $adminGroupPayments[0];
                    $totalThisGroupLoanPayments += isset($adminGroupPayments->adminlipa) ? $adminGroupPayments->adminlipa : 0;
                }
            }
        }

        $balance = round(($totalThisGroupLoans - $totalThisGroupLoanPayments), 2);

        return $balance;
    }

    function memberGroupLoanBalance($userId, $groupId)//total loan by user in group
    {
        $balance = 0;
        $totalLoan = 0;
        $totalPayments = 0;

        //get total loan applications
        if (!$ttlLoans = DB::select(DB::raw("SELECT SUM(amount) AS mkopo FROM grouploans WHERE userId = :usID AND groupId = :grpID"), array(
            'usID' => $userId,
            'grpID' => $groupId
        ))) {
            echo json_encode(array('status' => 501, 'error' => 'Failed to fetch sum from group loans'));
        } else {
            $ttlLoans = $ttlLoans[0];
            $totalLoan = isset($ttlLoans->mkopo) ? $ttlLoans->mkopo : 0;
        }

        //get total loan payments
        if (!$ttlLoanPayments = DB::table('grouploans')->where('userId', $userId)->where('groupId', $groupId)->get()) {
            echo json_encode(array('status' => 501, 'error' => 'Failed to fetch sum from group loans'));
        } else {

            for ($i = 0; $i < count($ttlLoanPayments); $i++) {
                $loanId = $ttlLoanPayments[$i]->loanId;

                //get total payments for each loanId
                if (!$loanIdPayments = DB::select(DB::raw("SELECT SUM(amount) AS lipa FROM grouploanpayments WHERE userId = :usID AND loanId = :lnID"), array(
                    'usID' => $userId,
                    'lnID' => $loanId
                ))) {

                    echo json_encode(array(
                        'status' => 501,
                        'error' => 'Failed to fetch lipa from group loan payments'
                    ));

                } else {
                    $loanIdPayments = $loanIdPayments[0];
                    $totalPayments += isset($loanIdPayments->lipa) ? $loanIdPayments->lipa : 0;
                }
            }


        }

        return round($totalLoan - $totalPayments, 2);
    }

//get loan repayment period (months)
    function getLoanRepaymentPeriodBasedOnAmount($amount)
    {
        $period = 0;

        if ($amount > 0 && $amount <= 2000)//0 - 2000
        {
            $period = 1;
        } elseif ($amount > 2000 && $amount <= 5000)//2001 - 5000
        {
            $period = 2;
        } elseif ($amount > 5000 && $amount <= 10000)//5001 - 10000
        {
            $period = 3;
        } elseif ($amount > 10000 && $amount <= 20000)//10001 - 20000
        {
            $period = 4;
        } elseif ($amount > 20000 && $amount <= 30000)//20001 - 30000
        {
            $period = 5;
        } elseif ($amount > 30000 && $amount <= 50000)//30001 - 50000
        {
            $period = 6;
        } elseif ($amount > 50000 && $amount <= 100000)//50001 - 100000
        {
            $period = 12;
        } elseif ($amount > 100000 && $amount <= 200000)//100001 - 200000
        {
            $period = 20;
        } elseif ($amount > 200000 && $amount <= 500000)//200001 - 500000
        {
            $period = 30;
        } elseif ($amount > 500000 && $amount <= 1000000)//500001 - 1000000
        {
            $period = 36;
        } elseif ($amount > 1000000 && $amount <= 2000000)//1000001 - 2000000
        {
            $period = 40;
        } elseif ($amount > 2000000 && $amount <= 3000000)//2000001 - 3000000
        {
            $period = 50;
        } elseif ($amount > 3000000 && $amount <= 5000000)//3000001 - 5000000
        {
            $period = 60;
        } elseif ($amount > 5000000)//>5000000
        {
            $period = 72;
        }

        return $period;
    }

    function getRepaymentPeriodBasedOnLoanType($lnType, $hrsDaysMonths)
    {
        $return = "";
        if (!$period = DB::table('loantypes')->where('loantypeId', $lnType)->get()) {
            echo json_encode(array('status' => 501, 'error' => 'could not fetch user'));
        } else {
            $period = $period[0];
            $return = $period->maxtime;
        }
        if ($hrsDaysMonths == 'minutes') {
            $return = ($return * 60);
        } elseif ($hrsDaysMonths == 'hours') {
            $return = ($return * 60 * 60);
        } elseif ($hrsDaysMonths == 'days') {
            $return = ($return * 24 * 60 * 60);
        } elseif ($hrsDaysMonths == 'months') {
            $return = ($return * 30 * 24 * 60 * 60);
        }

        return $return;

    }

    function processMemberGroupLoan($security, $amount, $period, $userId, $groupId, $remarks)
    {
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');

        $appstatus = 0;
        $apploanId = 0;
        $description = "";

        $memberSavings = $this->getMemberGroupSavings($userId, $groupId);
        $blackList = $this->checkBlacklist($userId);

        $loanDueDate = date('Y-m-d H:i:s', strtotime($now) + ($period));

        //save loan application
        if (!DB::insert("INSERT INTO grouploanapplications(securitytype,amount,dateapplied,dateapproved,approvalflag,userId,groupId,period,remarks) VALUES ($security,$amount,'$now','$now',0,$userId,$groupId,$period,'$remarks')")) {
            json_encode(array('status' => 501, 'error' => 'Failed to add group loan application'));
        }

        $lnId = DB::select("SELECT applicationid FROM grouploanapplications ORDER BY applicationid DESC LIMIT 1");
        $lnId = $lnId[0]->applicationid;
//		dd( $lnId );
        $guarantableAmount = $this->getGuarantableAmount($userId);
        $outstandingLoanBalance = $this->memberGroupLoanBalance($userId, $groupId);


        if ($security == 1)//secured by my savings
        {
            //get the amount of loan that the member can access. Should be less any amount guaranteed and loan balance
            $accessibleLoan = ($guarantableAmount - $outstandingLoanBalance) > 0 ? ($guarantableAmount - $outstandingLoanBalance) : 0;

            $savings = $this->getMemberGroupSavings($userId, $groupId);

            if ($amount >= 100) {
                if ($accessibleLoan >= $amount)//some more loan available
                {
                    if ($amount <= $savings) {

                        /*//process loan
						$loanLockfile = 'docs/loanapplication' . $userId . '.txt';
						if ( ! file_exists( $loanLockfile ) )//only process if there's no other operation underway
						{
							if ( $blackList == false ) {
								//write lock file
								$handle = fopen( $loanLockfile, 'w' );
								fwrite( $handle, 'Loan processing' );
								fclose( $handle );*/

                        //insert into loans
                        /*
							approvedflag
							0: not approved
							1: approved
						*/

                        /*
							interestflag
							0: not interest
							1: interest
							2: loan transfer fee
						*/

                        $loanInterest = $amount * (2 / 100);//fix group loans at 2% interest
                        //update loan application as approved

//						$affectedRows = DB::table('grouploanapplications')->where('applicationid', $loanID)->update(array('approvalflag' => 1, 'period' => (double)$period));
                        $affectedRows = DB::statement("UPDATE grouploanapplications SET approvalflag = 1, period = $period WHERE applicationid = $lnId");
                        /*$affectedRows = DB::update( "UPDATE grouploanapplications SET approvalflag = :one, period = :two WHERE applicationid = :three", array(
							'one' => 1,
							'two' => (double)$period,
							'three' => $loanID
						) );*/
//						echo "Affected rows are/is: " . $affectedRows. "<br>";
                        if (!$affectedRows) {
                            echo json_encode(array(
                                'status' => 501,
                                'error' => 'failed to approve loan application'
                            ));
                            die;
                        }
                        if (!DB::insert("INSERT INTO grouploans(amount,applidescription,dateapplied,dateapproved,userId,groupId,interestflag,loanapplicationId,correspondingloanId,period,monthlyrepayment,defaultstatus,useralert,guarantoralert) VALUES($amount,'$remarks','$now','$now',$userId,$groupId,0,$lnId,0,$period,0,0,0,0)")) {
                            echo json_encode(array(
                                'status' => 501,
                                'error' => 'failed to insert group loan'
                            ));
                            die;
                        } else {
                            $loadId = DB::select("SELECT loanId FROM grouploans ORDER BY loanId DESC LIMIT 1");
                            $loadId = $loadId[0]->loanId;
                            //interest
                            $monthName = $this->getMonthName(date('m'));
                            $interestMsg = "Loan Interest (" . substr($monthName, 0, 3) . "," . date('Y') . ")";
                            if (!DB::insert("INSERT INTO grouploans(amount,applidescription,dateapplied,dateapproved,userId,groupId,interestflag,loanapplicationId,correspondingloanId,period,monthlyrepayment,defaultstatus,useralert,guarantoralert) VALUES($loanInterest,'$remarks','$now','$now',$userId,$groupId,1,$lnId,$loadId,$period,0,0,0,0)")) {
                                $description = 'failed to insert group loan';
                            }

                            //credit withdrawable a/c
                            /*
								deposit method
								1: Mpesa
								2: Credit Card
								3: Savings
								4: Loan
								5:
							*/

                            if (!DB::insert("INSERT INTO deposits(amount,depositdate,depositmethod,userId) VALUES($amount,'$now',7,$userId)")) {
                                $description = 'failed to insert deposits';
                            } else {
                                //send sms to a/c owner
                                $smsMessage = "Dear " . $this->getUserName($userId) . ", your loan application of Ksh " . number_format($amount, 2) . " is successful. Check your withdrawable account balance. Your loan is due on $loanDueDate";
                                $userPhone = $this->getUserRegisteredPhone($userId);
//								$this->sendMobileSasa( $userPhone, $smsMessage, '' );

                                $description = "Your loan application is successful. Ksh " . number_format($amount, 2) . " has been credited to your withdrawable account";

                                $appstatus = 1;
                            }
                        }

                        //delete lock file after processing
//								unlink( $loanLockfile );
                        /*} else {
							$description = "You cannot access a loan due to a default in a previous loan.";
						}*/
                        /*} else {
							$description = "There's another loan application underway. Please try again later";
						}*/
                    } else {
                        $description = "You do not have enough savings to secure your loan. Your savings balance is Ksh " . number_format($savings, 2);
                    }
                } else {
                    $description = "You can access at most Ksh " . number_format($accessibleLoan, 2) . ". Increase your savings or pay any outstanding loans to increase your limit.";
                }
            } else {
                $description = "Loan amount must be at least Ksh 100";
            }
        } elseif ($security == 2)//secured by guarantors
        {
            if ($amount >= $memberSavings) {
                if ($amount >= 10) {
                    //check if amount is less than 5 times of member's savings
                    $maxLimit = ($memberSavings * 5);
                    if ($amount <= $maxLimit) {
                        //check if member is three months since activation
                        if ($this->getMembershipDuration($userId) >= 7) {
                            if ($outstandingLoanBalance <= 0)//has loan balance
                            {
                                if ($blackList == false)//not blacklisted
                                {
                                    $apploanId = $lnId;
                                    $description = "Your loan application of Ksh " . number_format($amount, 2) . " has been received. Proceed to add guarantors";
                                    $appstatus = 2;
                                } else {
                                    $description = "You cannot access a loan due to a default in a previous loan.";
                                }
                            } else {
                                $description = "You have an outstanding loan balance of Ksh " . number_format($outstandingLoanBalance, 2) . ". Repay the loan to continue.";
                            }
                        } else {
                            $description = "Your account must have been active for atleast 7 days";
                        }
                    } else {
                        $description = "Your maximum loan limit is Ksh " . number_format($maxLimit, 2);
                    }
                } else {
                    $description = "Loan amount must be at least Ksh 100";
                }
            } else {
                $description = "This loan can be secured by your group savings. Kindly select your savings as security.";
            }
        }

        $arrayReturn = array('status' => $appstatus, 'loanId' => $apploanId, 'description' => $description);

        return json_encode($arrayReturn);
    }

    function getMemberGroupSavings($userId, $groupId)
    {
        $savingsBalance = 0;

        if (!$ttlSavings = DB::select(DB::raw("SELECT SUM(amount) AS akiba FROM groupsavings WHERE savedby = :savedby AND groupId = :grpID"), array(
            'savedby' => $userId,
            'grpID' => $groupId
        ))) {
            echo json_encode(array('status' => 501, 'error' => 'failed to fetch member group savings'));
        } else {
            $ttlSavings = $ttlSavings[0];
            $savingsBalance = isset($ttlSavings->akiba) ? $ttlSavings->akiba : 0;
        }

        return $savingsBalance;
    }

//check if user is blacklisted
    function checkBlacklist($userId)
    {
        $blackList = false;

        if (!$black = DB::table('users')->where('id', $userId)->get()) {
            echo json_encode(array('status' => 501, 'error' => 'failed to user'));
        } else {
            $black = $black[0];
            if ($black->blacklist == 1) {
                $blackList = true;
            }
        }

        return $blackList;
    }

//check a member's unguaranteid savings
    function getGuarantableAmount($userId)
    {
        $guarantableAmount = 0;

        $savings = $this->getMemberSavings($userId);//get a member's savings

        //all loans that member guaranteeid and have not been fully paid paid
        $guaranteeAmountNotFullyPaid = 0;
        if (!$approvedapplicationsPaid = DB::table('guarantors')->where('guarantorId', $userId)->where('status', 1)->get()) {
            echo json_encode(array('status' => 501, 'error' => 'error in fetching guarantors where fulfilled'));
        } else {
            for ($i = 0; $i < count($approvedapplicationsPaid); $i++) {
                $loanapplicationId = $approvedapplicationsPaid[$i]->loanapplicationId;
                //get loan Id to be used in checking loan balance
                if (!$getLianId = DB::table('loans')->where('loanapplicationId', $loanapplicationId)->get()) {
                    echo json_encode(array(
                        'status' => 501,
                        'error' => 'error in fetching loan application identifier'
                    ));
                } else {
                    if (count($getLianId) == 0) {
                        $searchloanId = 0;
                    } else {
                        $searchloanId = $getLianId[0]->loanId;
                    }

                    if ($this->getSpecificLoanBalance($searchloanId) > 0) {
                        $guaranteeAmountNotFullyPaid += $approvedapplicationsPaid[$i]->guaranteeamount;
                    }
                }
            }

        }

        //get amount that can be guaranteeid
        $guarantableAmount = $savings - $guaranteeAmountNotFullyPaid;

        return $guarantableAmount;
    }

    function replaceKey($array, $find, $replace)
    {
        if (array_key_exists($find, $array)) {
            $keys = array_keys($array);
            $i = 0;
            $index = false;
            foreach ($array as $k => $v) {
                if ($find === $k) {
                    $index = $i;
                    break;
                }
                $i++;
            }
            if ($index !== false) {
                $keys[$i] = $replace;
                $array = array_combine($keys, $array);
            }
        }
    }

//get total savings made by member
    function getMemberSavings($userId)
    {
        $savings = 0;
        if (!$sav = DB::select(DB::raw("SELECT SUM(amount) AS akiba FROM savings WHERE userId = :userId"), array('userId' => $userId))) {
            echo json_encode(array('status' => 501, 'error' => 'error in fetching user savings'));
        } else {
            $sav = $sav[0];
            $savings = isset($sav->akiba) ? $sav->akiba : 0;
        }

        return $savings;
    }

    function getMonthName($no)
    {
        $name = "";
        switch ($no) {
            case '1':
                $name = "January";
                break;

            case '2':
                $name = "February";
                break;

            case '3':
                $name = "March";
                break;


            case '4':
                $name = "April";
                break;

            case '5':
                $name = "May";
                break;

            case '6':
                $name = "June";
                break;

            case '7':
                $name = "July";
                break;

            case '8':
                $name = "August";
                break;

            case '9':
                $name = "September";
                break;

            case '10':
                $name = "October";
                break;

            case '11':
                $name = "November";
                break;

            case '12':
                $name = "December";
                break;

            default:
                $name = "";
                break;
        }

        return $name;
    }

//get loan balance of specific loan
    function getSpecificGroupLoanBalance($loanId)
    {
        $loanBalance = 0;
//		dd(  );
        //get total loan to be repaid
        $loanApplied = 0;
        if (!$loan = DB::select(DB::raw("SELECT SUM(amount) AS deni FROM grouploans WHERE loanId = :loan_id OR correspondingloanId = :corre_id"), array(
            'loan_id' => $loanId,
            'corre_id' => $loanId
        ))) {
            echo json_encode(array('status' => 501, 'error' => 'failed to fetch sum from group loans'));
        } else {
//			dd( $loan );
            $loan = $loan[0];
//			dd($loan->deni);
            $loanApplied = isset($loan->deni) ? $loan->deni : 0;
        }

        //get loan repayments
        $loanRepaid = 0;
        if (!$repaid = DB::select(DB::raw("SELECT SUM(amount) AS lipwa FROM grouploanpayments WHERE loanId = :loan_id"), array('loan_id' => $loanId))) {
            echo json_encode(array('status' => 501, 'error' => 'failed to fetch sum from group loan payments'));
        } else {
            $repaid = $repaid[0];
            $loanRepaid = isset($repaid->lipwa) ? $repaid->lipwa : 0;
        }

        $loanBalance = $loanApplied - $loanRepaid;

        return round($loanBalance, 2);
    }

    //get loan balance of specific admin loan
    function getSpecificAdminGroupLoanBalance($loanId)
    {
        $loanBalance = 0;
        //get total loan to be repaid
        $loanApplied = 0;


        if (!$loan = DB::select(DB::raw("SELECT SUM(amount) AS deni FROM admingrouploanapplication WHERE (applicationId = :loanId OR correspondingloanId = :loanId) AND status = :status"), array(
            'loanId' => $loanId,
            'status' => 1
        ))) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'failed to fetch sum from group loan applications'
            ));

        } else {
            $loan = $loan[0];
            $loanApplied = isset($loan->deni) ? $loan->deni : 0;
        }

        //get loan repayments
        $loanRepaid = 0;


        if (!$repaid = DB::select(DB::raw("SELECT SUM(amount) AS lipwa FROM admingrouploanpayments WHERE loanId = :loanId"), array(
            'loanId' => $loanId
        ))) {
            echo json_encode(array('status' => 501, 'error' => 'failed to fetch sum from group loan payments'));

        } else {
            $repaid = $repaid[0];
            $loanRepaid = isset($repaid->lipwa) ? $repaid->lipwa : 0;
        }

        $loanBalance = $loanApplied - $loanRepaid;

        return round($loanBalance, 2);
    }

    //process guarantorship
    function processGroupGuarantorship($requestId)
    {
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');

        //get guarantorship details
        if (!$details = DB::table('groupguarantors')->where('guarantorshipId', $requestId)->get()) {
            echo json_encode(array('status' => 501, 'error' => 'failed to fetch guarantors'));
        } else {
            $details = $details[0];

            $applicant = $details->guaranteeId;
            $loanapplicationId = $details->loanapplicationId;
            $guarantorId = $details->guarantorId;
            $guaranteeId = $details->guaranteeId;
            $guaranteeamount = $details->guaranteeamount;
            $guarantorPhone = $this->getUserRegisteredPhone($guarantorId);
            $guaranteePhone = $this->getUserRegisteredPhone($guaranteeId);

            $guarantableAmt = $this->getGuarantableAmount($guarantorId);
            if ($guarantableAmt >= $guaranteeamount) {


                //get the total amount guaranteed for this loan
                if (!$accepted = DB::select(DB::raw("SELECT SUM(guaranteeamount) AS yote FROM groupguarantors WHERE loanapplicationId= :loanApplicationId AND status = :status"), array(
                    'loanApplicationId' => $loanapplicationId,
                    'status' => 1
                ))) {
                    echo json_encode(array('status' => 501, 'error' => 'failed to sum guaranteeable amount'));
                } else {
                    $accepted = $accepted[0];
                    //get loan details
                    if (!$loanDetails = DB::table('grouploanapplications')->where('applicationid', $loanapplicationId)->get()) {
                        echo json_encode(array('status' => 501, 'error' => 'failed to fetch loan application'));

                    } else {
                        $loanDetailsrow = mysqli_fetch_array($loanDetails, MYSQLI_ASSOC);
                        $amount = $loanDetails->amount;
                        $groupId = $loanDetails->groupId;

                        $acceptedAmount = (isset($accepted->yote) ? $accepted->yote : 0) + $this->getMemberGroupSavings($guaranteeId, $groupId);//take care of guarantee savings

                        $guaranteeBalance = $amount - $acceptedAmount;

                        if ($guaranteeBalance > 0) {
                            //if balance left is less than guaranteed amount --> get difference, update amount guaranteed to new value --> process loan
                            if ($guaranteeBalance < $guaranteeamount) {
                                $realAmountGuaranteed = $guaranteeamount - $guaranteeBalance;

                                //change status to 1 - success
                                if (!DB::update('UPDATE groupguarantors SET status = ?, dateapproved = ?, guaranteeamount = ? WHERE status = ? AND guarantorshipId = ?', [
                                    1,
                                    '$now',
                                    $guaranteeBalance,
                                    0,
                                    $requestId
                                ])) {
                                    echo json_encode(array(
                                        'status' => 501,
                                        'error' => 'failed to sum group guarantors'
                                    ));
                                } else {
                                    //process loan
                                    if ($this->processGroupLoanToAccount($amount, $guaranteeId, $loanapplicationId, $guaranteePhone) == true) {
                                        //update loan application as successful
                                        if (!DB::update('UPDATE grouploanapplications SET approvalflag = ? WHERE applicationid = ?', [
                                            1,
                                            $loanapplicationId
                                        ])) {
                                            echo json_encode(array(
                                                'status' => 501,
                                                'error' => 'failed to approve loan application'
                                            ));
                                        }

                                        //notify applicant that success
                                        $message = $this->getUserName($guarantorId)
                                            . " guaranteed you Ksh "
                                            . number_format($guaranteeBalance, 2)
                                            . ". Your loan has been approved. Thank you.";
                                        $this->sendMobileSasa($guaranteePhone, $message);
                                    } else {
                                        //notify applicant that failed
                                        $message = "Your loan application failed. Please try again.";
                                        $this->sendMobileSasa($guaranteePhone, $message);
                                    }
                                }
                            } elseif ($guaranteeBalance == $guaranteeamount) {
                                //if balance left is equal to the guaranteed --> approve loan --> change guarantorship status
                                if (!DB::update('UPDATE groupguarantors SET status = ?, dateapproved = ? WHERE status = ? AND guarantorshipId = ?', [
                                    1,
                                    '$now',
                                    0,
                                    $requestId
                                ])) {
                                    echo json_encode(array(
                                        'status' => 501,
                                        'error' => 'failed to update loan status'
                                    ));
                                } else {
                                    //process loan
                                    if ($this->processGroupLoanToAccount($amount, $guaranteeId, $loanapplicationId, $guaranteePhone) == true) {

                                        //update loan application as successful
                                        if (!DB::update('UPDATE grouploanapplications SET approvalflag = ? WHERE applicationid = ?', [
                                            1,
                                            $loanapplicationId
                                        ])) {
                                            echo json_encode(array(
                                                'status' => 501,
                                                'error' => 'failed to approve loan'
                                            ));

                                        }

                                        //notify applicant that success
                                        $message = $this->getUserName($guarantorId) . " guaranteed you Ksh " . number_format($guaranteeamount, 2) . ". Your loan has been approved. Thank you.";
                                        $this->sendMobileSasa($guaranteePhone, $message);
                                    } else {
                                        //notify applicant that failed
                                        $message = "Your loan application failed. Please try again.";
                                        $this->sendMobileSasa($guaranteePhone, $message);
                                    }
                                }
                            } elseif ($guaranteeBalance > $guaranteeamount) {

                                //if balance left is more than guaranteed amount
                                if (!DB::update('UPDATE groupguarantors SET status = ?, dateapproved = ? WHERE status = ? AND guarantorshipId = ?', [
                                    1,
                                    '$now',
                                    0,
                                    $requestId
                                ])) {
                                    echo json_encode(array(
                                        'status' => 501,
                                        'error' => 'failed to execute'
                                    ));
                                } else {
                                    //notify applicant that success
                                    $message = $this->getUserName($guarantorId) . " guaranteed you Ksh " . number_format($guaranteeamount, 2) . ". Thank you.";
                                    $this->sendMobileSasa($guaranteePhone, $message);
                                }
                            }
                        } else {
                            //change status to 3 - amount already guaraneed
                            if (!DB::update('UPDATE groupguarantors SET status = ?, dateapproved = ? WHERE status = ? AND guarantorshipId = ?', [
                                3,
                                '$now',
                                0,
                                $requestId
                            ])) {
                                echo json_encode(array(
                                    'status' => 501,
                                    'error' => 'failed to change status'
                                ));
                            } else {
                                //notify user that amount had already been guaranteed
                                $message = "The loan for " . $this->getUserName($guarantorId) . " is already fully guaranteed. Thank you.";
                                $this->sendMobileSasa($guaranteePhone, $message);
                            }
                        }
                    }
                }
            } else {
                //notify guarantor that failed
                $message = "You can guarantee at most Ksh " . number_format($guarantableAmt, 2);
                $this->sendMobileSasa($guarantorPhone, $message);
            }
        }
    }

    function processGroupLoanToAccount($amount, $guaranteeId, $loanapplicationId, $guarantorPhone)
    {
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');

        $dateApplied = "";

        $period = 0;
        $userPhone = "";

        //get date applied
        if (!$applied = DB::table('grouploanapplications')->where('applicationid', $loanapplicationId)->get()) {
            echo json_encode(array('status' => 501, 'error' => 'failed to fetch loan application'));
        } else {
            $dateApplied = $applied->dateapplied;
            $groupId = $applied->groupId;
            $remarks = $applied->remarks;
            $userPhone = $this->getUserRegisteredPhone($applied->userId);
            $period = $this->getLoanRepaymentPeriodBasedOnAmount($applied->amount);
        }

        $return = false;

        $loanDueDate = date('Y-m-d H:i:s', strtotime($now) + ($period));

        if ($this->memberGroupLoanBalance($guaranteeId, $groupId) <= 0)//process
        {
            $interestAmount = $amount * 2 / 100;
            $monthlyrepayment = 0;

            $monthName = $this->getMonthName(date('m'));
            $interestMsg = "Loan Interest (" . substr($monthName, 0, 3) . "," . date('Y') . ")";

            //credit user loan account
            if (!DB::insert("INSERT INTO grouploans(amount,applidescription,dateapplied,dateapproved,userId,groupId,interestflag,loanapplicationId,correspondingloanId,period,monthlyrepayment,defaultstatus,useralert,guarantoralert) VALUES($amount,'$remarks','$dateApplied','$now',$guaranteeId,$groupId,0,$loanapplicationId,0,$period,$monthlyrepayment,0,0,0)")) {
                echo json_encode(array('status' => 501, 'error' => 'failed to add group loan'));
            } else {
                $lastLoanId = DB::select("SELECT loanId FROM grouploans ORDER BY loanId DESC LIMIT 1");
                //add interest
                if (!DB::insert("INSERT INTO grouploans(amount,applidescription,dateapplied,dateapproved,userId,groupId,interestflag,loanapplicationId,correspondingloanId,period,monthlyrepayment,defaultstatus,useralert,guarantoralert) VALUES($interestAmount,'$interestMsg','$dateApplied','$now',$guaranteeId,$groupId,1,$loanapplicationId,$lastLoanId,$period,0,0,0,0)")) {
                    echo json_encode(array('status' => 501, 'error' => 'failed to add interest flag'));
                } else {
                    //credit amount to user a/c
                    $this->makeWithdrawableDeposit($amount, $guaranteeId, 5);
                    $return = true;

                    //alert user of loan due date
                    $message = "Your loan due date is $loanDueDate";
                    $this->sendMobileSasa($guarantorPhone, $message);
                }
            }
        }

        return $return;
    }

    function getTargetSavingsBalance($accountId)
    {
        $deposits = 0;
        $withdrawals = 0;
        //get deposits
        if (!$getDeposits = DB::select(DB::raw("SELECT SUM(amount) AS weka FROM accounttransactions WHERE drcrflag = :drcrFlag AND accountId = :accountId"), array(
            'drcrFlag' => 1,
            'accountId' => $accountId
        ))) {
            echo json_encode(array('status' => 501, 'error' => 'failed to fetch amounts coming from accounts'));
        } else {
            $getDeposits = $getDeposits[0];
            $deposits = isset($getDeposits->weka) ? $getDeposits->weka : 0;
        }

        //get withdrawals
        if (!$getWithdrawals = DB::select(DB::raw("SELECT SUM(amount) AS toa FROM accounttransactions WHERE drcrflag = :drcrFlag AND accountId = :accountId"), array(
            'drcrFlag' => 2,
            'accountId' => $accountId
        ))) {
            echo json_encode(array('status' => 501, 'error' => 'failed to fetch amounts going from accounts'));
        } else {
            $getWithdrawals = $getWithdrawals[0];
            $withdrawals = isset($getWithdrawals->toa) ? $getWithdrawals->toa : 0;
        }

        return $deposits - $withdrawals;
    }

    private function getSchoolFeesHoldingBalance($user_id)
    {
        $accountId = 0;
        if (!$result = mysqli_query($this->link, "SELECT * FROM accounts WHERE userId = $user_id AND actype = 11")) {

        } else {
            $accountId = mysqli_fetch_array($result, MYSQLI_ASSOC);
        }

        return $accountId['accountId'];
    }

    //get withdrawal purpose
    function getWithdrawalPurpose($purposeCode)
    {
        $return = "";

        switch ($purposeCode) {
            case '1':
                $return = "Mpesa";
                break;

            case '2':
                $return = "Buy airtime";
                break;

            case '3':
                $return = "Transfer to savings";
                break;

            case '4':
                $return = "Transfer to other member";
                break;

            case '5':
                $return = "Buy Shares";
                break;

            case '6':
                $return = "Repay Facility";
                break;

            case '7':
                $return = "Reversal";
                break;

            case '8':
                $return = "Account access SMS";
                break;

            case '9':
                $return = "Transfer to Akiba";
                break;

            case '10':
                $return = "Statement charge";
                break;

            default:
                $return = "Unknown";
                break;
        }

        return $return;
    }

    //process Mpesa withdrawal
    function processMpesaWithdrawal($transactionId, $phone, $email, $name, $userId)
    {
        $status = 0;
        $userMessage = "";
        $commision = 0;
        $otherCharges = 0;
        $amount = 0;

        $lockFile = "admin/attachments/mpesawith" . $userId . ".txt";
        if (!file_exists($lockFile)) {
            // ** create lock file
            $handle = fopen($lockFile, 'a');
            fwrite($handle, "Running");
            fclose($handle);
            // ** end create lock file

            //check if user has enough credit
            $withdrawableAmount = $this->getWithdrawableAmount($userId);

            //get transactionn details
            if (!$transDetails = mysqli_query($this->link, "SELECT * FROM mpesawithdrawals WHERE transactionId=$transactionId AND status=1 AND userId=$userId")) {
                echo "Error " . mysqli_error($this->link);
            } else {
                if (mysqli_num_rows($transDetails) > 0) {
                    $transDetailsrow = mysqli_fetch_array($transDetails, MYSQLI_ASSOC);
                    $amount = $transDetailsrow['amount'];
                    $charges = $transDetailsrow['charges'];

                    if ($withdrawableAmount >= $amount + $charges) {
                        //get mpesa charges for amount
                        $mpesaCharges = json_decode($this->getMpesaB2CCharges($amount), true);

                        $mpesaB2CDeduction = $mpesaCharges['othercharges'];
                        $commision = $mpesaCharges['commision'];

                        $otherCharges = $mpesaB2CDeduction;

                        //withdraw amount and transaction charge
                        $this->withdrawFunds($amount, $userId, 1, 'Mpesa withdrawal');
                        $this->withdrawFunds($charges, $userId, 1, 'Mpesa transaction fee');

                        //update commision table
                        $this->recordCommision(3, $userId, $commision, $mpesaB2CDeduction);

                        //mark transaction as rejected
                        if (!mysqli_query($this->link, "UPDATE mpesawithdrawals SET status=2 WHERE transactionId=$transactionId")) {
                            echo "Error1 " . mysqli_error($this->link);
                        }

                        $userMessage = "Withdrawal of Ksh " . number_format($amount, 2) . " is successful. A transaction fee of Ksh " . number_format($charges, 2) . " was applied. Your new balance is Ksh " . number_format(getWithdrawableAmount($userId), 2);

                        $_SESSION['comm'] = "<p style='color:green'>Withdrawal of Ksh " . number_format($amount, 2) . ". is successful. New user balance is Ksh " . number_format(getWithdrawableAmount($userId), 2) . "</p>";
                        $status = 1;
                    } else {
                        //mark transaction as rejected
                        if (!mysqli_query($this->link, "UPDATE mpesawithdrawals SET status=3 WHERE transactionId=$transactionId")) {
                            echo "Error2 " . mysqli_error($this->link);
                        }

                        $userMessage = "You have insufficient balance to withdraw Ksh " . number_format($amount, 2) . ". Your balance is Ksh " . number_format($withdrawableAmount, 2) . " and a transaction fee of Ksh " . number_format($charges, 2) . " is required";

                        $_SESSION['comm'] = "<p style='color:red'>Insufficient balance to withdraw Ksh " . number_format($amount, 2) . ". Balance is Ksh " . number_format($withdrawableAmount, 2) . "</p>";
                    }

                    //send sms
                    sendMobileSasa($phone, $userMessage, "HOMEPESA");

                    //send email
                    $subject = "Mpesa Transaction";
                    $files = "";
                    //sendEmail($subject,$userMessage,$email,$files);
                } else {
                    $userMessage = "The transaction does not exist or not confirmed";
                }

                //delete lock file after running
                unlink($lockFile);
            }
        } else {
            $userMessage = "An operation is currently underway";
        }

        $array = array(
            'status' => $status,
            'description' => $userMessage,
            'commision' => $commision,
            'otherCharges' => $otherCharges,
            'amount' => $amount
        );

        return json_encode($array);

    }

    //sends request for processing
    function sendMpesaCash($userId, $transid, $amount, $phone)
    {

        $phone = str_replace(" ", "", $phone);

        $status = 0;
        $txnId = "";

        $b2cDetails = json_decode(sendB2CRequest($phone, $amount, $transid), true);
        $status = $b2cDetails['status'];
        $txnId = $b2cDetails['txnId'];

        if (!mysqli_query($this->link, "UPDATE mpesawithdrawals SET processingstatus = $status,processingtxnid='$txnId' WHERE transactionId=$transid")) {
            echo "Error " . mysqli_error($this->link);
        }

        $array = array('status' => $status, 'txnId' => $txnId);

        return json_encode($array);
    }

    //function to get maximum loan limit for each loan type
    function getFacilityLimit($loanType)
    {
        $limit = array();
        if (!$getLimit = mysqli_query($this->link, "SELECT * FROM loantypes WHERE loantypeId=$loanType")) {
            echo "Error " . mysqli_error($this->link);
        } else {
            if (mysqli_num_rows($getLimit) > 0) {
                $getLimitrow = mysqli_fetch_array($getLimit, MYSQLI_ASSOC);
                $limit['upper'] = $getLimitrow['maxamount'];
                $limit['lower'] = $getLimitrow['minAmount'];
            }
        }

        return $limit;
    }

//function to get minimum loan amount for each loan type
    function getMinimumLoanAmount($loanType)
    {
        $limit = 0;
        if (!$getLimit = mysqli_query($this->link, "SELECT minAmount FROM loantypes WHERE loantypeId=$loanType")) {
            echo "Error " . mysqli_error($this->link);
        } else {
            if (mysqli_num_rows($getLimit) > 0) {
                $getLimitrow = mysqli_fetch_array($getLimit, MYSQLI_ASSOC);
                $limit = $getLimitrow['minAmount'];
            }
        }

        return $limit;
    }

//function to get maximum loan repayment period
    function getLoanRepaymentPeriod($loanType)
    {
        $period = 0;
        if (!$getLimit = mysqli_query($this->link, "SELECT * FROM loantypes WHERE loantypeId=$loanType")) {
            echo "Error " . mysqli_error($this->link);
        } else {
            if (mysqli_num_rows($getLimit) > 0) {
                $getLimitrow = mysqli_fetch_array($getLimit, MYSQLI_ASSOC);
                if ($getLimitrow['interesttype'] == 2) {
                    $period = $getLimitrow['maxtime'];
                }
            }
        }

        return $period;
    }

    //get loan balance
    function getLoanBalance($userId)
    {
        $loanApplied = 0;
        $loanPaid = 0;

        //get total loan applied
        if (!$loan = mysqli_query($this->link, "SELECT SUM(amount) AS yote FROM loans WHERE userId=$userId")) {
            echo "Error " . mysqli_error($this->link);
        } else {
            $loanrow = mysqli_fetch_array($loan, MYSQLI_ASSOC);
            $loanApplied = isset($loanrow['yote']) ? $loanrow['yote'] : 0;
        }

        //get total loan paid
        if (!$loanPaidQry = mysqli_query($this->link, "SELECT SUM(amount) AS lipa FROM loanpayments WHERE userId=$userId")) {
            echo "Error " . mysqli_error($this->link);
        } else {
            $loanPaidQryrow = mysqli_fetch_array($loanPaidQry, MYSQLI_ASSOC);
            $loanPaid = isset($loanPaidQryrow['lipa']) ? $loanPaidQryrow['lipa'] : 0;
        }

        return round($loanApplied - $loanPaid, 2);
    }

    function getUserCurrentActiveLoans($userId)
    {
        $loans = 0;

        if (!$allLoans = mysqli_query($this->link, "SELECT * FROM loans WHERE userId=$userId AND interestflag=0")) {
            echo "Error " . mysqli_error($this->link);
        } else {
            while ($allLoansrow = mysqli_fetch_array($allLoans, MYSQLI_ASSOC)) {
                if ($this->getSpecificLoanBalance($allLoansrow['loanId']) > 1) {
                    $loans++;
                }
            }
        }

        return $loans;
    }

    //function to calculate loan interest
    function getLoanInterest($loanType, $amount, $period)
    {
        $interest = 0;
        $repayment = 0;
        if (!$getLimit = mysqli_query($this->link, "SELECT * FROM loantypes WHERE loantypeId=$loanType")) {
            echo "Error " . mysqli_error($this->link);
        } else {
            if (mysqli_num_rows($getLimit) > 0) {
                $getLimitrow = mysqli_fetch_array($getLimit, MYSQLI_ASSOC);
                $interestRate = $getLimitrow['interestrate'];
                $interesttype = $getLimitrow['interesttype'];

                if ($interesttype == 1) {
                    $interest = $interestRate / 100 * $amount;
                } else {
                    $interest = $amount * $interestRate / 100 * $period / 12;
                    $repayment = ($interest + $amount) / $period;
                }
            }
        }
        $array = array('interest' => $interest, 'monthlyrepayment' => $repayment);

        return json_encode($array);
    }

    //get deposit method
    function getDepositMethod($code)
    {
        switch ($code) {
            case '1':
                $return = "Mpesa";
                break;

            case '2':
                $return = "Card";
                break;

            case '3':
                $return = "Transfer from member";
                break;

            case '4':
                $return = "Reversal";
                break;

            case '5':
                $return = "Loan";
                break;

            case '6':
                $return = "Savings";
                break;

            case '7':
                $return = "Group Loan";
                break;

            case '8':
                $return = "Akiba a/c";
                break;

            case '9':
                $return = "Mpesa Holding a/c";
                break;

            case '10':
                $return = "Referral Commision";
                break;

            case '11':
                $return = "Target savings a/c";
                break;

            case '12':
                $return = "Fund Raising a/c";
                break;

            default:
                $return = "Unknown";
                break;
        }

        return $return;
    }

    public function getTotalsDCFS(Request $request)
    {
        $userid = $request->get('user_id');
        $dcfs_type = $request->get('dcfs_type');

        if (isset($userid) && isset($dcfs_type)) {

//			how many accounts does the user have of type $dcfs_type
            $ac_totals = 0;
            if (!$how_many_ac = DB::table('accounts')->where('actype', $dcfs_type)->where('userId', $userid)->select('accountId')->get()) {
                echo json_encode(array('status' => 501, 'error' => 'failed to  fetch your accounts'));
            } else {
//				dd( $how_many_ac );
                if ($how_many_ac->count() > 0) {
                    for ($i = 0; $i < $how_many_ac->count(); $i++) {
                        $ac_totals += $this->getTargetSavingsBalance($how_many_ac[$i]->accountId);
                    }
                }
            }
            $currency = 'KES';
            $response = array(
                'status' => 200,
                'msg' => 'Total balance: Ksh ',
                'data' => $currency . ' ' . number_format($ac_totals, 2)
            );
        } else {
            $response = array('status' => 501, 'error' => 'missing params in request');
        }
        echo json_encode($response);
    }

    public function getTotalsGroups(Request $request)
    {
        $userId = $request->get('userId');
        if (isset($userId)) {
            $savings = 0;

            if (!$savvals = DB::table('groupsavings')->where('savedby', $userId)->orderBy('savingId', 'DESC')->get()) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                for ($i = 0; $i < count($savvals); $i++) {
                    $savings += $savvals[$i]->amount;
                }
            }
            $response = array(
                'status' => 200,
                'total_savings' => number_format($savings, 2),
                'transactions' => $savvals
            );
        } else {
            $response = array('status' => 501, 'error' => 'looks like there are missing params');
        }
        echo json_encode($response);
    }

    public function getTotalsFunding(Request $request)
    {
        $user_id = $request->get('user_id');

        if (isset($user_id)) {
//		BANK ACCOUNT TOTALS
            $main_account_balance = $this->getWithdrawableAmount($user_id);
            $savings_balance = 0;
            $fundraised_amount = 0;
            $group_savings_totals = 0;
//		SAVINGS ACCOUNT TOTALS
            if (!$savvals = DB::table('savings')->where('userId', $user_id)->orderBy('savingsId', 'DESC')->get()) {
                echo json_encode(array('status' => 501, 'error' => '0.00'));
                die;
            } else {
                for ($j = 0; $j < count($savvals); $j++) {
                    $savings_balance += $savvals[$j]->amount;
                }
            }

//      FUND RAISED ACCOUNT TOTALS
            if (!$how_many_ac = DB::table('accounts')->where('actype', 3)->where('userId', $user_id)->select('accountId')->get()) {
                echo json_encode(array('status' => 501, 'error' => 'failed to  fetch your accounts'));
            } else {
                if ($how_many_ac->count() > 0) {
                    for ($i = 0; $i < $how_many_ac->count(); $i++) {
                        $fundraised_amount += $this->getTargetSavingsBalance($how_many_ac[$i]->accountId);
                    }
                }
            }

//      GROUP TOTALS
            if (!$savvals = DB::table('groupsavings')->where('savedby', $user_id)->orderBy('savingId', 'DESC')->get()) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                for ($i = 0; $i < count($savvals); $i++) {
                    $group_savings_totals += $savvals[$i]->amount;
                }
            }

            $final_funding_total = $group_savings_totals + $fundraised_amount + $savings_balance + $main_account_balance;

            $response = array(
                'status' => 200,
                'total_funding' => number_format($final_funding_total, 2)
                /*, 'savings' => $savings_balance, 'main_account' => $main_account_balance, 'fundraised' => $fundraised_amount, 'groups_total' => $group_savings_totals*/
            );
        } else {
            $response = array('status' => 501, 'error' => 'Missing params in request');
        }
        echo json_encode($response);

    }

    public function funding_params(Request $request)
    {
        $user_id = $request->get('user_id');
        $amount = $request->get('amount');
        $fundraised = $request->get('fundraised');
        $savings = $request->get('savings');
        $main = $request->get('main');
        $groups = $request->get('groups');

        $status = $this->funding_account_totals($user_id, $fundraised, $savings, $main, $groups);

        if ($amount > $status->full_total) {
            echo "mingi";
//			can't pick loan
        } else {
//			execute loan eleigibility and confirmation

            echo "kidogo";
        }
    }

    function funding_account_totals($user_id, $a_fundraised, $a_savings, $a_main, $a_groups)
    {
        $accounts_and_totals = array();
        $final_funding_total = 0;
        $main = 0;
        $savings_balance = 0;
        $fundraised_amount = 0;
        $group_savings_totals = 0;
        $accounts = new stdClass();


        if ($a_main == 1) {
            $main = $this->getWithdrawableAmount($user_id);
            $accounts->main = $main;
            $final_funding_total += $main;
        }
        if ($a_savings == 1) {
//		SAVINGS ACCOUNT TOTALS
            if (!$savvals = DB::table('savings')->where('userId', $user_id)->orderBy('savingsId', 'DESC')->get()) {
                echo json_encode(array('status' => 501, 'error' => '0.00'));
                die;
            } else {
                for ($j = 0; $j < count($savvals); $j++) {
                    $savings_balance += $savvals[$j]->amount;
                }
            }
            $accounts->savings = $savings_balance;
            $final_funding_total += $savings_balance;
        }

        if ($a_fundraised == 1) {
//      FUND RAISED ACCOUNT TOTALS
            if (!$how_many_ac = DB::table('accounts')->where('actype', 3)->where('userId', $user_id)->select('accountId')->get()) {
                echo json_encode(array('status' => 501, 'error' => 'failed to  fetch your accounts'));
            } else {
                if ($how_many_ac->count() > 0) {
                    for ($i = 0; $i < $how_many_ac->count(); $i++) {
                        $fundraised_amount += $this->getTargetSavingsBalance($how_many_ac[$i]->accountId);
                    }
                }
            }
            $accounts->fundraised_amount = $fundraised_amount;
            $final_funding_total += $fundraised_amount;
        }
        if ($a_groups == 1) {
//      GROUP TOTALS
            if (!$savvals = DB::table('groupsavings')->where('savedby', $user_id)->orderBy('savingId', 'DESC')->get()) {
                echo json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                for ($i = 0; $i < count($savvals); $i++) {
                    $group_savings_totals += $savvals[$i]->amount;
                }
            }
            $accounts->group_savings_totals = $group_savings_totals;
            $final_funding_total += $group_savings_totals;
        }
        $accounts->full_total = $final_funding_total;


        return $accounts;
        /*$response = array(
			'status'        => 200,
			'total_funding' => number_format( $final_funding_total, 2 )
		);*/
    }


    //process guarantorship
    function processGuarantorship($requestId)
    {
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');

        //get guarantorship details
        if (!$details = mysqli_query($this->link, "SELECT * FROM guarantors WHERE guarantorshipId=$requestId")) {
            echo "Error " . mysqli_error($this->link);
        } else {
            $detailsrow = mysqli_fetch_array($details, MYSQLI_ASSOC);
            $applicant = $detailsrow['guaranteeId'];
            $loanapplicationId = $detailsrow['loanapplicationId'];
            $guarantorId = $detailsrow['guarantorId'];
            $guaranteeId = $detailsrow['guaranteeId'];
            $guaranteeamount = $detailsrow['guaranteeamount'];
            $guarantorPhone = $this->getUserRegisteredPhone($guarantorId);
            $guaranteePhone = $this->getUserRegisteredPhone($guaranteeId);

            $guarantableAmt = $this->getGuarantableAmount($guarantorId);
            if ($guarantableAmt >= $guaranteeamount) {
                //get the total amount guaranteed for this loan
                if (!$accepted = mysqli_query($this->link, "SELECT SUM(guaranteeamount) AS yote FROM guarantors WHERE loanapplicationId=$loanapplicationId AND status=1")) {
                    echo "Error " . mysqli_error($this->link);
                } else {
                    $acceptedrow = mysqli_fetch_array($accepted, MYSQLI_ASSOC);
                    $acceptedAmount = (isset($acceptedrow['yote']) ? $acceptedrow['yote'] : 0) + getSavingsBalance($guaranteeId);//take care of guarantee savings

                    //get loan details
                    if (!$loanDetails = mysqli_query($this->link, "SELECT * FROM loanapplications WHERE applicationid=$loanapplicationId")) {
                        echo "Error " . mysqli_error($this->link);
                    } else {
                        $loanDetailsrow = mysqli_fetch_array($loanDetails, MYSQLI_ASSOC);
                        $amount = $loanDetailsrow['amount'];
                        $loantype = $loanDetailsrow['loantype'];

                        $guaranteeBalance = $amount - $acceptedAmount;

                        if ($guaranteeBalance > 0) {
                            //if balance left is less than guaranteed amount --> get difference, update amount guaranteed to new value --> process loan
                            if ($guaranteeBalance < $guaranteeamount) {
                                $realAmountGuaranteed = $guaranteeamount - $guaranteeBalance;

                                //change status to 1 - success
                                if (!mysqli_query($this->link, "UPDATE guarantors SET status=1,dateapproved='$now',guaranteeamount=$guaranteeBalance WHERE status=0 AND guarantorshipId=$requestId")) {
                                    echo "Error " . mysqli_error($this->link);
                                } else {
                                    //process loan
                                    if ($this->processLoanToAccount($amount, $loantype, $guaranteeId, $loanapplicationId, $guaranteePhone) == true) {
                                        //update loan application as successful
                                        if (!mysqli_query($this->link, "UPDATE loanapplications SET approvalflag=1 WHERE applicationid=$loanapplicationId")) {
                                            echo "Error " . mysqli_error($this->link);
                                        }

                                        //notify applicant that success
                                        $message = $this->getUserName($guarantorId) . " guaranteed you Ksh " . number_format($guaranteeBalance, 2) . ". Your loan has been approved. Thank you.";
                                        $this->sendMobileSasa($guaranteePhone, $message, '');
                                    } else {
                                        //notify applicant that failed
                                        $message = "Your loan application failed. Please try again.";
                                        $this->sendMobileSasa($guaranteePhone, $message, '');
                                    }
                                }
                            } elseif ($guaranteeBalance == $guaranteeamount) {
                                //if balance left is equal to the guaranteed --> approve loan --> change guarantorship status
                                if (!mysqli_query($this->link, "UPDATE guarantors SET status=1,dateapproved='$now' WHERE status=0 AND guarantorshipId=$requestId")) {
                                    echo "Error " . mysqli_error($this->link);
                                } else {
                                    //process loan
                                    if ($this->processLoanToAccount($amount, $loantype, $guaranteeId, $loanapplicationId, $guaranteePhone) == true) {
                                        //update loan application as successful
                                        if (!mysqli_query($this->link, "UPDATE loanapplications SET approvalflag=1 WHERE applicationid=$loanapplicationId")) {
                                            echo "Error " . mysqli_error($this->link);
                                        }

                                        //notify applicant that success
                                        $message = $this->getUserName($guarantorId) . " guaranteed you Ksh " . number_format($guaranteeamount, 2) . ". Your loan has been approved. Thank you.";
                                        $this->sendMobileSasa($guaranteePhone, $message, '');
                                    } else {
                                        //notify applicant that failed
                                        $message = "Your loan application failed. Please try again.";
                                        $this->sendMobileSasa($guaranteePhone, $message, '');
                                    }
                                }
                            } elseif ($guaranteeBalance > $guaranteeamount) {
                                //if balance left is more than guaranteed amount
                                if (!mysqli_query($this->link, "UPDATE guarantors SET status=1,dateapproved='$now' WHERE status=0 AND guarantorshipId=$requestId")) {
                                    echo "Error " . mysqli_error($this->link);
                                } else {
                                    //notify applicant that success
                                    $message = $this->getUserName($guarantorId) . " guaranteed you Ksh " . number_format($guaranteeamount, 2) . ". Thank you.";
                                    $this->sendMobileSasa($guaranteePhone, $message, '');
                                }
                            }
                        } else {
                            //change status to 3 - amount already guaraneed
                            if (!mysqli_query($this->link, "UPDATE guarantors SET status=3,dateapproved='$now' WHERE status=0 AND guarantorshipId=$requestId")) {
                                echo "Error " . mysqli_error($this->link);
                            } else {
                                //notify user that amount had already been guaranteed
                                $message = "The loan for " . $this->getUserName($guarantorId) . " is already fully guaranteed. Thank you.";
                                $this->sendMobileSasa($guaranteePhone, $message, '');
                            }
                        }
                    }
                }
            } else {
                //notify guarantor that failed
                $message = "You can guarantee at most Ksh " . number_format($guarantableAmt, 2);
                $this->sendMobileSasa($guarantorPhone, $message, '');
            }
        }
    }

    function processLoanToAccount($amount, $loantype, $guaranteeId, $loanapplicationId, $guarantorPhone)
    {
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');

        $dateApplied = "";
        $loanTypeDesc = $this->getLoanType($loantype);

        $period = 0;
        $userPhone = "";

        //get date applied
        if (!$applied = mysqli_query($this->link, "SELECT * FROM loanapplications WHERE applicationid=$loanapplicationId")) {
            echo "Error " . mysqli_error($this->link);
        } else {
            $appliedrow = mysqli_fetch_array($applied, MYSQLI_ASSOC);
            $dateApplied = $appliedrow['dateapplied'];
            $userPhone = $this->getUserRegisteredPhone($appliedrow['userId']);
            $period = $this->getLoanRepaymentPeriodBasedOnAmount($appliedrow['amount']);
        }

        $return = false;

        $loanDueDate = date('Y-m-d H:i:s', strtotime($now) + ($period));

        if ($this->getUserLoanBalance($guaranteeId) <= 0)//process
        {
            $loanInterestVals = json_decode($this->getLoanInterest($loantype, $amount, $period), true);
            $interestAmount = $loanInterestVals['interest'];
            $monthlyrepayment = $loanInterestVals['monthlyrepayment'];

            $monthName = $this->getMonthName(date('m'));
            $interestMsg = "Loan Interest (" . substr($monthName, 0, 3) . "," . date('Y') . ")";

            //credit user loan account
            if (!mysqli_query($this->link, "INSERT INTO loans(loantype,amount,applidescription,dateapplied,dateapproved,userId,approvedflag,interestflag,loanapplicationId,correspondingloanId,period,monthlyrepayment,defaultstatus,defaultdate,useralert,guarantoralert) VALUES($loantype,$amount,'$loanTypeDesc','$dateApplied','$now',$guaranteeId,1,0,$loanapplicationId,0,$period,$monthlyrepayment,0,'$now',0,0)")) {
                echo "Error " . mysqli_error($this->link);
            } else {
                $lastLoanId = mysqli_insert_id($this->link);

                //add interest
                if (!mysqli_query($this->link, "INSERT INTO loans(loantype,amount,applidescription,dateapplied,dateapproved,userId,approvedflag,interestflag,loanapplicationId,correspondingloanId,period,monthlyrepayment,defaultstatus,defaultdate,useralert,guarantoralert) VALUES($loantype,$interestAmount,'$interestMsg','$dateApplied','$now',$guaranteeId,1,1,0,$lastLoanId,0,0,0,'$now',0,0)")) {
                    echo "Error " . mysqli_error($this->link);
                } else {
                    //credit amount to user a/c
                    $this->makeWithdrawableDeposit($amount, $guaranteeId, 5);
                    $return = true;

                    //alert user of loan due date
                    $message = "Your loan due date is $loanDueDate";
                    $this->sendMobileSasa($guarantorPhone, $message, '');
                }
            }
        }

        return $return;
    }

    //get user total loan balance
    function getUserLoanBalance($userid)
    {
        $totalLoans = 0;
        $totalPayments = 0;

        //get total loans
        if (!$loans = mysqli_query($this->link, "SELECT * FROM loans WHERE approvedflag=1 AND userId=$userid")) {
            echo "0~Error " . mysqli_error($this->link);
        } else {
            while ($loansrow = mysqli_fetch_array($loans, MYSQLI_ASSOC)) {
                $totalLoans += $loansrow['amount'];
            }
        }

        //get total payments
        if (!$loansPayments = mysqli_query($this->link, "SELECT * FROM loanpayments WHERE userId=$userid")) {
            echo "0~Error " . mysqli_error($this->link);
        } else {
            while ($loansPaymentsrow = mysqli_fetch_array($loansPayments, MYSQLI_ASSOC)) {
                $totalPayments += $loansPaymentsrow['amount'];
            }
        }

        return round($totalLoans - $totalPayments, 2);
    }

    public function get_user_details_for_loan($userId)
    {
        $return = "";
        if (!$users = DB::table('users')->where('id', $userId)->first()) {
            echo json_encode(array('status' => 501, 'error' => 'failed to get username'));
        } else {
            $collection = new stdClass();

            $collection->customer_id = $users->id;
            $collection->applicant_name = $users->fullname;
            $collection->phone_number = $users->phone;
            $collection->user_id_number = $users->idpass;
            $collection->dob = $users->dateofbirth;
            $collection->address = $users->town;

            $return = $collection;
        }

        return $return;
    }

    private function getSchoolFeesFinancing($userId)
    {
        $return = "";
        if (!$users = DB::table('accounts')->where('id', $userId)->where('actype', 11)->first()) {
            echo json_encode(array('status' => 501, 'error' => 'failed to fetch school fees finance holdings a/c'));
        } else {
            $return = $users->accountId;
        }

        return $return;
    }
}
// Functions done
//  26  -   withdrawable balance
//  31  -   transfer to savings

//  32  -   transfer to other member
//  30  -   buy airtime for line
//  35  -   mpesa withdrawal request
//  36  -   confirm mpesa withdrawal
//  37  -
//  38  -
//  39  -
//  40  -
//  41  -
//  42  -

/*  UPDATE
 * if ( ! DB::update( 'UPDATE usernotifications SET readflag = ? WHERE notificationId = ?', [
						1,
						$notiId
					] )
 */
