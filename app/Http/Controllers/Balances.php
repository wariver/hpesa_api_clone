<?php
/**
 * Created by Warui.
 * User: root
 * Date: 8/22/18
 * Time: 5:37 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Balances extends Controller {
	public $abi;
	public $api;

	/**
	 * DCF constructor.
	 */
	public function __construct() {
		$this->api = new Api();
		$this->abi = new Abi();
	}

	public function user_balance( Request $request ) {
		$user_id    = $request->get( 'user_id' );
		$ac_type    = $request->get( 'ac_type' );
		$ac_balance = 0;

		switch ( $ac_type ) {
			case 1:
				$ac_id      = $this->abi->get_user_account( $user_id, $this->abi->AC_TYPE_BANK );
				$ac_balance = $this->abi->get_account_balance( 'balance_bank', $ac_id );
				break;
			case 2:
				$ac_id         = $this->abi->get_user_account( $user_id, $this->abi->AC_TYPE_SAVINGS );
				$ac_id_holding = $this->abi->get_user_account( $user_id, $this->abi->AC_TYPE_HOLDING );
				$ac_holding    = $this->abi->get_account_balance( 'balance_holding', $ac_id_holding );
				$ac_balance    = $this->abi->get_account_balance( 'balance_savings', $ac_id );
				$ac_balance += $ac_holding;
				break;
			case 3:
				$ac_id      = $this->abi->get_user_account( $user_id, $this->abi->AC_TYPE_HOLDING );
				$ac_balance = $this->abi->get_account_balance( 'balance_holding', $ac_id );
				break;
			case 4:
				$ac_balance      = 0;
				$facility        = new Facility();
				$facilityAccount = $facility->has_unpaid_facility( $user_id );
				$ac_id      = $this->abi->get_user_account( $user_id, $this->abi->AC_TYPE_FACILITY );
				$ac_balance = $this->abi->get_account_balance( 'balance_loans', $ac_id );
				break;
			case 5:
				$ac_id      = $this->abi->get_user_account( $user_id, $this->abi->AC_TYPE_SCHOOL_FEES );
				$ac_balance = $this->abi->get_account_balance( 'balance_school_fees', $ac_id );
				break;
            case 10:
                $ac_id      = $this->abi->get_user_account( $user_id, $this->abi->AC_TYPE_MEMBERSHIPS );
                $ac_balance = $this->abi->get_account_balance( 'balance_memberships', $ac_id );


                break;
		}

		echo json_encode( array( 'status' => 200, 'balance' => 'KES. ' . number_format( $ac_balance, 2 ) ) );
	}

	public function user_balance_dcf( Request $request ) {
//		$user_id = $request->get( 'user_id' );
		$ac_id = $request->get( 'account_id' );

		$ac_balance = $this->abi->get_account_balance( 'balance_dcf', $ac_id );

		echo json_encode( array( 'status' => 200, 'balance' => 'KES. ' . number_format( $ac_balance, 2 ) ) );

	}

}
