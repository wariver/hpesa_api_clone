<?php
/**
 * Created by Warui.
 * User: root
 * Date: 12/25/18
 * Time: 5:04 PM
 */

namespace App\Http\Controllers;

use App\NewMember;
use App\WavesUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;

class Groups extends Controller
{
    public $abi;
    public $api;
    public $now;

    /**
     * DCF constructor.
     */
    public function __construct()
    {
        $this->api = new Api();
        $this->abi = new Abi();
        $this->now = $this->api->now;
    }

    public function create_group(Request $request)
    {
        $name = $request->get('username');
    }

    public function join_group(Request $request)
    {

    }

    public function make_contribution(Request $request)
    {
    }

    public function respond_to_invite(Request $request)
    {
        $groupId = $request->get('groupId');
        $status = $request->get('status');

        if (!isset($groupId) || !isset($status)) {
            echo json_encode(array('status' => 501, 'error' => 'There are missing parameters'));
            die;
        }
        $response = "";
        if (!DB::update('UPDATE groupmembers SET status = ? WHERE groupId = ?', [$status, $groupId])) {
            $response = json_encode(array('status' => 501, 'error' => 'database error'));
        } else {
            if ($status == 1) {
                $response = array('status' => 200, 'msg' => 'You are now a member of the group');
            } else if ($status == 2) {
                $response = array('status' => 200, 'msg' => 'You have successfully declined request');
            }
        }

        echo json_encode($response);
    }

    public function user_groups(Request $request)
    {
        $userId = $request->get('user_id');
//		$group_type = $request->get( 'group_type' );

        if (!isset($userId)) {
            echo json_encode(array('status' => 501, 'error' => 'missing params in request'));
            die;
        }
//			$response;
//			if ( ! $result = DB::table( 'groupmembers' )->where( 'userId', $userId )->where( 'status', $status )->get() ) {
        if (!$result = DB::table('groupmembers')->where('userId', $userId)->get()) {
            echo json_encode(array('status' => 501, 'error' => 'failed to fetch user groups'));
        } else {
            $count_result = count($result);
            for ($i = 0; $i < $count_result; $i++) {
                $id = $result[$i]->groupId;

                $user = WavesUser::where('id', $id)->first();

                $group = new stdClass();
                $group->pid = $id;
                $group->username = $user->fullname;
                $group->actype = $user->actype;
                $group->grp_idno = $user->idpass;
                $group->us_status = $result[$i]->status;

                $response[$i] = $group;
            }
            if (count($result) > 0) {
                $compile = array(
                    'status' => 200,
                    'count' => count($response),
                    'data' => (array)$response
                );
                $response = $compile;
            } else {
                $response = array('status' => 501, 'error' => 'YOU HAVEN\'T JOINED ANY GROUP YET');
            }
        }

        echo json_encode($response);
    }

    public function c2b_group_savings(Request $request)
    {

        $phoneno = (int)$request->get('phone_no');
        $amount = (int)$request->get('amount');
        $identifier = $request->get('identifier');
        $user_id = $request->get('user_id');
        $group_savings_id = $request->get('account_id');
        $paymenttype = (int)$request->get('paymenttype');

//		$dcf_account_id = $this->abi->get_user_account( $user_id, $this->abi->AC_TYPE_DCF );
        $dcf_balance = $this->abi->get_account_balance('balance_dcf', $dcf_account_id);
        $c2b_balance = $this->abi->get_account_balance('balance_general', $this->AC_C2B);


//		CREATE TRANSACTION
        $tx_id = $this->abi->create_transaction($amount, $this->TX_C2B_DCF, $user_id);
//		cr C2B account
        $this->abi->performTransactionEntry($tx_id, $this->AC_C2B, $this->abi->AC_TYPE_GENERAL, $amount, 1, $this->TX_C2B_DCF, $c2b_balance, ($c2b_balance + $amount), $this->abi->now);
//		cr DCF account
        $this->abi->performTransactionEntry($tx_id, $dcf_account_id, $this->abi->AC_TYPE_DCF, $amount, 1, $this->TX_C2B_DCF, $dcf_balance, ($dcf_balance + $amount), $this->abi->now);

        $reference = $this->api->getReference($paymenttype, $user_id);
        $paymentRequestResult = $this->api->stkPayment($amount, $phoneno, $paymenttype, $user_id, $identifier, $reference);

        $resultVals = json_decode($paymentRequestResult, true);

        if (!$this->abi->commit_transaction($tx_id)) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'Could not complete transaction at the moment.'
            ));
        } else {
            if ($resultVals['status'] == 1) {
                echo json_encode(array(
                    'status' => 200,
                    'msg' => 'A request has been sent to your phone.'
                ));
            } else {
                echo json_encode(array(
                    'status' => 501,
                    'msg' => 'Request failed.'
                ));
            }
        }
    }

    public function check_user_requests($user_id)
    {
    }

    public function get_user_laster_transactions($user_id, $group_id, $count)
    {
    }

    public function get_last_transactions($group_id, $count)
    {
    }

    public function user_group_facilities($group_id)
    {
    }

    public function apply_group_facility(Request $request)
    {
//        check user eligibility
    }

    public function get_total_group_savings(Request $request)
    {
    }

    public function get_user_group_savings(Request $request)
    {

    }

    public function getGroupDetails($group_id)
    {
        if (!$result = DB::table('groupmembers')->where('userId', $group_id)->get()) {
            echo json_encode(array('status' => 501, 'error' => 'failed to fetch user groups'));
        } else {
            return json_encode($result);
        }
    }

    public function isGroupMember($group_id, $member_id)
    {
        return DB::table('groupmembers')->where('groupId', $group_id)->where('userId', $member_id)->where('status', 1)->count();
    }

    //  57
    public function invite_to_group(Request $request)
    {
        $fullname = $request->get('fullname');
        $phone_number = $request->get('phone');
        $email_address = $request->get('email');
        $groupId = $request->get('group_id');
        $idno = $request->get('idpass');
        $new_existing = $request->get('new_existing');

        if ($new_existing == 0) {
            if (!isset($groupId) || !isset($idno) || !isset($fullname) || !isset($phone_number) || !isset($email_address)) {
                echo json_encode(array('status' => '501', 'error' => 'Please provide all the details required'));
                die;
            }
        }
        if ($new_existing == 1) {
            if (!isset($groupId) || !isset($idno)) {
                echo json_encode(array('status' => '501', 'error' => 'Request was rejected'));
                die;
            }
        }


        if($new_existing == 0){

            if ( DB::table('users')->where('idpass', $idno)->count() > 0){
                echo json_encode(array('status' => 502, 'error' => 'This is a waves user please invite as existing'));
                die;
            }

            if ( DB::table('groupmembers')->where('groupId', $groupId)->where('user_idpass', $idno)->count() > 0)//does not exist
            {
                echo json_encode(array('status' => 501, 'error' => 'Member has already been added in group'));
                die;
            }
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');

            $new_member = new NewMember();

            $new_member->full_name = $fullname;
            $new_member->idpass	 = $idno;
            $new_member->phone = $phone_number;
            $new_member->email = $email_address;
            $new_member->save();


            if (!mysqli_query($this->api->link, "INSERT INTO groupmembers(groupId,user_idpass,dateadded,dateaccepted,status) VALUES($groupId, $idno,'$now','$now',10)")) {
                $response = json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                $smsMessage = "You have been invite to join the group " . $this->api->getUserName($groupId) . ". Log into your account to confirm or decline membership.";

//                $smsPhone = $user->phone;
                $legitPhone = $this->api->phoneFormat($phone_number);
                if($legitPhone['status']){
                    $phone_number = $legitPhone['formatedPhone'];
                    $sms = new Sms();
                    $sms->sendSMS($phone_number, $smsMessage);
                }

                echo json_encode(array('status' => 200, 'msg' => 'Member has been successfully added'));
                die;
            }
        }
        if($new_existing == 1){
            if (!$user = WavesUser::where('idpass', $idno)->first()) {
                echo json_encode(array('status' => 501, 'error' => 'The id number provided does not exist'));
                die;
            }
            $user_id = $user->id;
            $user_idpass = $user->idpass;
            if ( DB::table('groupmembers')->where('groupId', $groupId)->where('userId', $user_id)->count() > 0)//does not exist
            {
                echo json_encode(array('status' => 501, 'error' => 'Member has already been added in group'));
                die;
            }
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');
            if (!mysqli_query($this->api->link, "INSERT INTO groupmembers(groupId,userId,user_idpass,dateadded,dateaccepted,status) VALUES($groupId,$user_id, $user_idpass,'$now','$now',0)")) {
                $response = json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                $smsMessage = "You have been invite to join the group " . $this->api->getUserName($groupId) . ". Log into your account to confirm or decline membership.";
                $smsPhone = $user->phone;
                $this->api->sendMobileSasa($smsPhone, $smsMessage);

                echo json_encode(array('status' => 200, 'error' => 'Member has been successfully added'));
                die;
            }
        }
    }

//	58
    public function add_member(Request $request)
    {
        $groupId = $request->get('groupId');
        $username = $request->get('username');
        $idpass = $request->get('idpass');
        $phone = $request->get('phone');
        $useremail = $request->get('useremail');

        if (!isset($groupId) || !isset($username) || !isset($_POST['username']) || !isset($idpass) || !isset($phone)) {
            echo json_encode(array('status' => 501, 'error' => 'Request was rejected'));
            die;
        }
        $name = $useremail;
        $idno = $idpass;
        $email = $useremail;


        //get member details
        if (!$getDetails = mysqli_query($this->api->link, "SELECT * FROM users WHERE idpass='$idno'")) {
            echo json_encode(array('status' => 501, 'error' => 'database error'));
            die;
        }

        if (count($getDetails) == 0)// does not exists
        {
            //check if email is registered
            if (!$getEmailDetails = mysqli_query($this->api->link, "SELECT * FROM users WHERE email='$email'")) {
                $response = json_encode(array('status' => 501, 'error' => 'database error'));
            } else {
                if (count($getEmailDetails) > 0)//does not exists
                {
                    echo json_encode(array('status' => 501, 'error' => 'This email is already in use. Kindly choose a different email'));
                    die;
                }

                //generate password
                $random = substr(md5(mt_rand()), 0, 7);
                $encrypted = md5($random);

                //insert member and get their id
                $userId = 0;
                if (!mysqli_query($this->api->link, "INSERT INTO users(fullname,idpass,phone,email,password,actype,acstatus,regfee,membershipfee,signuptime,timepaid,timeactivated,merchantId,passkey,passrecoverycode,signupoption,blacklist,phoneconfirmed,mpesaconfirmed,referredby) VALUES('$name','$idno','$phone','$email','$encrypted',1,0,0,0,'$now','$now','$now','','','',1,0,0,0,'')")) {
                    $response = json_encode(array('status' => 501, 'error' => 'query execution error'));
                } else {
                    $userId = mysqli_insert_id($this->api->link);
                }

                //add new user to group
                if (!mysqli_query($this->api->link, "INSERT INTO groupmembers(groupId,userId,dateadded,dateaccepted,status) VALUES($groupId,$userId,'$now','$now',0)")) {
                    echo json_encode(array('status' => 501, 'error' => 'query execution error'));
                    die;
                }

                $smsMessage = "You have been added to the group " . $this->api->getUserName($groupId) . ". Your temporary password is $random. Change it to enhance the security of your account. you will be required to approve or decline request. Log in at www.homepesasacco.com to complete request.";
                $smsPhone = $this->api->getUserRegisteredPhone($userId);
                $this->api->sendMobileSasa($smsPhone, $smsMessage);

                echo json_encode(array('status' => 200, 'msg' => 'Member has been successfully added. Inform the member the email registered'));
                die;
            }
        } else {
            $response = "0~The member is already registered by Homepesa Sacco. Select Existing Option";
        }

        echo json_encode($response);
    }

    public function get_all_group_members($group_id){
        return DB::table('groupmembers')->where('groupId', $group_id)->get();
    }
    public function get_pending_group_members($group_id){
       return DB::table('groupmembers')->where('groupId', $group_id)->where(function ($q) {$q->where('status', 0)->orWhere('status', 10);})->get();

    }
    public function get_active_group_members($group_id){
        return DB::table('groupmembers')->where('groupId', $group_id)->where('status', 1)->get();
    }
    public function get_declined_group_members($group_id){
        return DB::table('groupmembers')->where('groupId', $group_id)->where('status', 2)->get();
    }

    /*
    NEW WAVES MEMBER INVITES
    EXISTING WAVES MEMBER INVITES

    JOIN GROUP

    */
}


