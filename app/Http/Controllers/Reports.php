<?php
/**
 * Created by Warui.
 * User: root
 * Date: 8/22/18
 * Time: 5:37 PM
 */

namespace App\Http\Controllers;

use AfricasTalkingGateway;
use AfricasTalkingGatewayException;
use DateTime;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use PHPMailer;
use phpmailerException;
use stdClass;

class Reports extends Controller
{
    public $abi;
    public $api;

    /**
     * DCF constructor.
     */
    public function __construct()
    {
        $this->api = new Api();
        $this->abi = new Abi();
    }

    public function user_savings_reports(Request $request)
    {
        $user_id = (int)$request->get('user_id');
        $savings_ac = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_SAVINGS);

        if ($entry_result = mysqli_query($this->api->link, "SELECT * FROM transaction_entries WHERE account_id = $savings_ac ORDER BY time_created DESC")) {
            $entry_result_tmp = mysqli_fetch_all($entry_result, MYSQLI_ASSOC);

            return $entry_result_tmp;
        } else {
            return null;
        }

    }

    public function user_bank_reports(Request $request)
    {
        $user_id = (int)$request->get('user_id');
        $transactions = array();
        $bank_ac = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_BANK);
        if ($results = mysqli_query($this->api->link, "SELECT id FROM transactions WHERE user_id = $user_id AND tx_state = 'COMPLETE' AND (tx_type != 6 AND tx_type != 7 AND tx_type != 15) ORDER BY time_created DESC")) {
            $results_tmp = mysqli_fetch_array($results, MYSQLI_ASSOC);
            if ($results_tmp) {
                foreach ($results_tmp as $row) {

                    if (!$entry_result = mysqli_query($this->api->link, "SELECT * FROM transaction_entries WHERE transaction_id = $row")) {

                    } else {
                        $entry_result_tmp = mysqli_fetch_array($entry_result, MYSQLI_ASSOC);
                        var_dump($entry_result_tmp);
                    }
                }
            }
        }
    }

    public function user_facility_reports(Request $request)
    {
        $user_id = (int)$request->get('user_id');
        $transactions = array();
        $facility_ac = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_FACILITY);
        if ($results = mysqli_query($this->api->link, "SELECT id FROM transactions WHERE user_id = $user_id AND tx_state = 'COMPLETE' AND (tx_type = 3 OR tx_type = 13) ORDER BY time_created DESC")) {
            $results_tmp = mysqli_fetch_array($results, MYSQLI_ASSOC);
            if ($results_tmp) {
                foreach ($results_tmp as $row) {

                    if (!$entry_result = mysqli_query($this->api->link, "SELECT * FROM transaction_entries WHERE transaction_id = $row")) {

                    } else {
                        $entry_result_tmp = mysqli_fetch_array($entry_result, MYSQLI_ASSOC);
                        var_dump($entry_result_tmp);
                    }
                }
            }
        }
    }

    public function cbk_facility_reports()
    {
//		FACILITY REPORTS
        /*
         * Facility Repayments
         * DIB Bank Asset Account
         * Profits Account
         * Provisions Account
         * */
        $transactions = array();
        $ac_bank_asset = 1;
        $ac_profit = 4;
        $ac_provisions = 10;
        $ac_facility_repayments = 7;

        $ac_bank_asset_bal = $this->abi->get_account_balance('balance_general', 1);
        $ac_profit_bal = $this->abi->get_account_balance('balance_general', 4);
        $ac_provisions_bal = $this->abi->get_account_balance('balance_general', 10);
        $ac_facility_repayments_bal = $this->abi->get_account_balance('balance_general', 7);

        if ($results = mysqli_query($this->api->link, "SELECT id FROM transactions WHERE tx_state = 'COMPLETE' AND (tx_type = 2 OR tx_type = 3 OR tx_type = 13) ORDER BY time_created DESC")) {
            $results_tmp = mysqli_fetch_array($results, MYSQLI_ASSOC);
            if ($results_tmp) {
                foreach ($results_tmp as $row) {
                    if ($bank_asset_result = mysqli_query($this->api->link, "SELECT * FROM transaction_entries WHERE transaction_id = $row")) {
                        $bank_asset_result_tmp = mysqli_fetch_array($bank_asset_result, MYSQLI_ASSOC);
                        var_dump($bank_asset_result_tmp);
                    }
                    if ($profit_balance = mysqli_query($this->api->link, "SELECT * FROM transaction_entries WHERE transaction_id = $row")) {
                        $profit_balance_tmp = mysqli_fetch_array($profit_balance, MYSQLI_ASSOC);
                        var_dump($profit_balance_tmp);
                    }
                    if ($provisions_balance = mysqli_query($this->api->link, "SELECT * FROM transaction_entries WHERE transaction_id = $row")) {
                        $provisions_balance_tmp = mysqli_fetch_array($provisions_balance, MYSQLI_ASSOC);
                        var_dump($provisions_balance_tmp);
                    }
                    if ($facility_repayment = mysqli_query($this->api->link, "SELECT * FROM transaction_entries WHERE transaction_id = $row")) {
                        $facility_repayment_tmp = mysqli_fetch_array($facility_repayment, MYSQLI_ASSOC);
                        var_dump($facility_repayment_tmp);
                    }
                }
            }
            die;
        }
    }

    public function user_last_transactions($user_id, $count)
    {
        $collection = array();
        if ($results = mysqli_query($this->api->link, "SELECT tx.tx_state, tx.tx_type, tx.user_id, tx.amount, te.account_locale, te.debit_credit_flag FROM transactions AS tx, transaction_entries AS te WHERE tx.id = te.transaction_id AND tx.tx_state = 'COMPLETE' AND tx.user_id = $user_id AND te.account_locale != 7 AND te.account_locale != 3 AND te.account_locale != 4 GROUP BY tx.tx_state, tx.tx_type, tx.user_id, tx.amount, te.account_locale, te.debit_credit_flag LIMIT $count")) {
            $results_tmp = mysqli_fetch_all($results, MYSQLI_ASSOC);

            return $results_tmp;
        } else {
            return null;
        }
    }

    public function transactions_by_users_accounts($user_id, $count)
    {
//		get accounts list and put in or through tx entries

        $ac_bank = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_BANK);
        $ac_savings = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_SAVINGS);
        $ac_lipakaro = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_SCHOOL_FEES);
        $ac_facility = $this->abi->get_all_user_facility_accounts($user_id);
        $ac_dcf_d = $this->abi->get_all_user_dcf_accounts($user_id, 1);
        $ac_dcf_c = $this->abi->get_all_user_dcf_accounts($user_id, 2);
        $ac_dcf_f = $this->abi->get_all_user_dcf_accounts($user_id, 3);
        $ac_dcf_s = $this->abi->get_all_user_dcf_accounts($user_id, 4);

        $collection = array();
//		var_dump($ac_bank);echo '<br>';
        $collection[] = $ac_bank;

//		var_dump($ac_savings);echo '<br>';
        $collection[] = $ac_savings;

//		var_dump($ac_lipakaro);echo '<br>';
        $collection[] = $ac_lipakaro;


        if (count($ac_facility) > 0) {
            foreach ($ac_facility as $row) {
                $collection[] = $row['id'];
            }
        }

        if (count($ac_dcf_d) > 0) {
            foreach ($ac_dcf_d as $row) {
                $collection[] = (int)$row->account_id;
            }
        }

        if (count($ac_dcf_c) > 0) {
            foreach ($ac_dcf_c as $row) {
                $collection[] = (int)$row->account_id;
            }
        }

        if (count($ac_dcf_f) > 0) {
            foreach ($ac_dcf_f as $row) {
                $collection[] = (int)$row->account_id;
            }
        }

        if (count($ac_dcf_s) > 0) {
            foreach ($ac_dcf_s as $row) {
                $collection[] = (int)$row->account_id;
            }
        }

        $query = 'SELECT * FROM transaction_entries WHERE (account_id = ';

        $col_count = count($collection);
        foreach ($collection as $i => $iValue) {
            if ($i == 0) {
                $query = $query . $collection[0] . ' ';
            } else {
                $query = $query . 'OR account_id = ' . $iValue . ' ';
            }
        }
        $query_build = ") AND tx_type < 21 ";
        $query = $query . $query_build . "ORDER BY time_created DESC LIMIT $count";
        if ($results = mysqli_query($this->api->link, $query)) {
            return mysqli_fetch_all($results, MYSQLI_ASSOC);
        }

        return null;
    }

    public function user_dashboard_graph($user_id)
    {
        $collection = array();
        $back_ac = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_BANK);
        if ($dashboard_report = mysqli_query($this->api->link, "SELECT SUM(amount) total, MONTH(time_created) month FROM transaction_entries WHERE account_id = $back_ac GROUP BY month")) {
            $dashboard_report_tmp = mysqli_fetch_all($dashboard_report, MYSQLI_ASSOC);
            $dashboard_report_count = count($dashboard_report_tmp);
            for ($i = 1; $i <= 12; $i++) {
                for ($j = 0; $j < $dashboard_report_count; $j++) {
                    if ((string)$i == $dashboard_report_tmp[$j]['month']) {
                        array_push($collection, (int)$dashboard_report_tmp[$j]['total']);
                        break;
                    }
                    if ($j == (count($dashboard_report_tmp) - 1)) {
                        array_push($collection, 0);
                    }
                }
            }

            return $collection;
        }

        return null;
    }

    public function userShouldSee($tx)
    {
        switch ($tx) {
            case 21:
                return false;
                break;
            case 23:
                return false;
                break;
            case 24:
                return false;
                break;
            case 25:
                return false;
                break;
            case 26:
                return false;
                break;
            default:
                return true;
                break;
        }
    }
}
