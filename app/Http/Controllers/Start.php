<?php
/**
 * Created by Warui.
 * User: root
 * Date: 8/22/18
 * Time: 5:37 PM
 */

namespace App\Http\Controllers;

use App\Jobs\QueueGrails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class Start extends Controller
{
    public $now;
    public $abi;
    public $api;


    /**
     * Start constructor.
     */
    public function __construct()
    {
        $this->api = new Api();
        $this->abi = new Abi();
    }

    private function create_accounts($user_name, $user_id)
    {
//	number of accounts to make
//		account 1 ,2 ,3, 6

        $account_collection = array();

        if (!mysqli_query($this->api->link, "START TRANSACTION")) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'could not complete registration at the moment. Try again later' . mysqli_error($this->api->link)
            ));
            die;
        }

        $account_bank = array();
        $ac_name = $user_name . '_current';
        if (!mysqli_query($this->api->link, "INSERT INTO accounts_b (ac_name, extra_types, ac_type, branch_id) VALUES ('$ac_name', $user_id, 1, 1)")) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'Could not complete registration. Please try again later.' . mysqli_error($this->api->link)
            ));
            die;
        }

        $tx_id_result = mysqli_query($this->api->link, "SELECT MAX(id) AS tx_id FROM accounts_b");
        $tx_id_temp = mysqli_fetch_array($tx_id_result, MYSQLI_ASSOC);
        $tx_id_bank = $tx_id_temp['tx_id'];
        $account_bank = array('id' => (int)$tx_id_bank, 'ac_type' => 1);

        $account_savings = array();
        $ac_name = $user_name . '_savings';
        if (!mysqli_query($this->api->link, "INSERT INTO accounts_b (ac_name, extra_types, ac_type, branch_id) VALUES ('$ac_name', $user_id, 2, 1)")) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'Could not complete registration. Please try again later.' . mysqli_error($this->api->link)
            ));
            die;
        }

        $tx_id_result = mysqli_query($this->api->link, "SELECT MAX(id) AS tx_id FROM accounts_b");
        $tx_id_temp = mysqli_fetch_array($tx_id_result, MYSQLI_ASSOC);
        $tx_id_savings = $tx_id_temp['tx_id'];
        $account_savings = array('id' => (int)$tx_id_savings, 'ac_type' => 2);

        $account_holding = array();
        $ac_name = $user_name . '_holding';
        if (!mysqli_query($this->api->link, "INSERT INTO accounts_b (ac_name, extra_types, ac_type, branch_id) VALUES ('$ac_name', $user_id, 3, 1)")) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'Could not complete registration. Please try again later.' . mysqli_error($this->api->link)
            ));
            die;
        }

        $tx_id_result = mysqli_query($this->api->link, "SELECT MAX(id) AS tx_id FROM accounts_b");
        $tx_id_temp = mysqli_fetch_array($tx_id_result, MYSQLI_ASSOC);
        $tx_id_holding = $tx_id_temp['tx_id'];
        $account_holding = array('id' => (int)$tx_id_holding, 'ac_type' => 3);

        $account_school_fees = array();
        $ac_name = $user_name . '_school_fees';
        if (!mysqli_query($this->api->link, "INSERT INTO accounts_b (ac_name, extra_types, ac_type, branch_id) VALUES ('$ac_name', $user_id, 6, 1)")) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'Could not complete registration. Please try again later.' . mysqli_error($this->api->link)
            ));
            die;
        }

        $tx_id_result = mysqli_query($this->api->link, "SELECT MAX(id) AS tx_id FROM accounts_b");
        $tx_id_temp = mysqli_fetch_array($tx_id_result, MYSQLI_ASSOC);
        $tx_id_school_fees = $tx_id_temp['tx_id'];
        $account_school_fees = array('id' => (int)$tx_id_school_fees, 'ac_type' => 6);

        $account_collection[] = $account_bank;
        $account_collection[] = $account_savings;
        $account_collection[] = $account_holding;
        $account_collection[] = $account_school_fees;

        $postup = json_encode($account_collection);
        if (mysqli_query($this->api->link, "INSERT INTO account_holders (user_id, account_ids) VALUES ($user_id, '$postup')")) {

        } else {
            echo json_encode(array(
                'status' => 501,
                'error' => 'Could not complete registration. Please try again later.' . mysqli_error($this->api->link)
            ));
            die;
        }
        if (!mysqli_query($this->api->link, 'COMMIT')) {
            return false;
        }

        return true;
    }

    private function create_group_accounts($user_name, $group_id, $membership_type)
    {
        //	number of accounts to make
        //	account 10 ,11, 12 ,13, 14, 15
        $ac_type_savings = 0;
        $ac_type_holdings = 0;
        if ($membership_type == 'group') {
            $ac_type_savings = 10;
            $ac_type_holdings = 11;
        } elseif ($membership_type == 'business') {
            $ac_type_savings = 12;
            $ac_type_holdings = 13;
        } elseif ($membership_type == 'sacco') {
            $ac_type_savings = 14;
            $ac_type_holdings = 15;
        }

        $account_collection = array();

        if (!mysqli_query($this->api->link, "START TRANSACTION")) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'could not complete registration at the moment. Try again later' . mysqli_error($this->api->link)
            ));
            die;
        }


        $ac_name = $user_name . '_savings';
        if (!mysqli_query($this->api->link, "INSERT INTO accounts_b (ac_name, extra_types, extra_types2, ac_type, branch_id) VALUES ('$ac_name', $group_id, 1, $ac_type_savings, 1)")) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'Could not complete registration. Please try again later.' . mysqli_error($this->api->link)
            ));
            die;
        }

        $tx_id_result = mysqli_query($this->api->link, "SELECT MAX(id) AS tx_id FROM accounts_b");
        $tx_id_temp = mysqli_fetch_array($tx_id_result, MYSQLI_ASSOC);
        $tx_id_savings = $tx_id_temp['tx_id'];
        $account_group_savings = array('id' => (int)$tx_id_savings, 'ac_type' => 10);

        $ac_name = $user_name . '_holding';
        if (!mysqli_query($this->api->link, "INSERT INTO accounts_b (ac_name, extra_types, extra_types2, ac_type, branch_id) VALUES ('$ac_name', $group_id, 1, $ac_type_holdings, 1)")) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'Could not complete registration. Please try again later.' . mysqli_error($this->api->link)
            ));
            die;
        }

        $tx_id_result = mysqli_query($this->api->link, "SELECT MAX(id) AS tx_id FROM accounts_b");
        $tx_id_temp = mysqli_fetch_array($tx_id_result, MYSQLI_ASSOC);
        $tx_id_holding = $tx_id_temp['tx_id'];
        $account_group_holding = array('id' => (int)$tx_id_holding, 'ac_type' => 11);


        $account_collection[] = $account_group_savings;
        $account_collection[] = $account_group_holding;

        $postup = json_encode($account_collection);
        if (mysqli_query($this->api->link, "INSERT INTO account_holders (user_id, account_ids) VALUES ($group_id, '$postup')")) {

        } else {
            echo json_encode(array(
                'status' => 501,
                'error' => 'Could not complete registration. Please try again later.' . mysqli_error($this->api->link)
            ));
            die;
        }
        if (!mysqli_query($this->api->link, 'COMMIT')) {
            return false;
        }

        return true;
    }

    public function sign_up(Request $request)
    {
        $name = $request->get('username');
        $actype = $request->get('actype');
        $idpass = $request->get('idpass');
        $phone = $request->get('phone');
        $email = $request->get('useremail');
        $dob = $request->get('dob');
        $country = $request->get('country');
        $town = $request->get('town');
        $password = $request->get('password');
        $referal = $request->get('referal');

        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');

        //format phone
        if (substr($phone, 0, 2) === "07")//if phone starts with 07
        {
            $phone = substr_replace($phone, "254", 0, 1);
        } elseif (substr($phone, 0, 1) === "7")//if starts with 7
        {
            $phone = substr_replace($phone, "254", 0, 0);
        } elseif (substr($phone, 0, 4) === "+254") {
            $phone = substr_replace($phone, "", 0, 1);
        }
        if ($this->api->validatePhone($phone) == 1) {
            $response['phone'] = $phone;
        } else {
            echo json_encode(array('status' => 501, 'error' => 'phone number is invalid'));
            die;
        }
        if ($this->api->validateIdPass($idpass, $actype) == 1) {
            $response['id'] = $idpass;
            $response['actype'] = $actype;

            if (!$query = DB::table('users')->where('email', $email)->get()) {
                echo json_encode(array('status' => 501, 'error' => 'Failed to fetch email'));
                die;
            }

            if (count($query) > 0)//if email is in use
            {
                echo json_encode(array('status' => 501, 'error' => 'This email is already registered'));
                die;
            }

//check if ID/pas is registered
            try {
                $idpass = $actype >= 4 ? time() . random_int(1000, 9999) : $idpass;//generate random number for group
            } catch (\Exception $e) {
            }


            if (!$checkidpass = DB::table('users')->where('idpass', $idpass)->get()) {
                $response = array('status' => 501, 'error' => 'Could not find id number in database');
            } else {

                if (count($checkidpass) == 0) {
                    if ($actype == 4) {
                        $acStatusThis = 2;
                    } elseif ($actype == 5) {
                        $acStatusThis = 2;
                    } elseif ($actype == 6) {
                        $acStatusThis = 2;
                    } else {
                        $acStatusThis = 0;
                    }

                    if (!$user = DB::insert("INSERT INTO users (fullname,idpass,phone,email,password,actype,acstatus,passrecoverycode,signupoption,regfee,membershipfee,timepaid,timeactivated,merchantId,passkey,blacklist,phoneconfirmed,mpesaconfirmed,referredby,phone_iprs,gender,physicaladdress,dateofbirth,country,town) VALUES('$name','$idpass','$phone','$email','$password',$actype,$acStatusThis,'',2,0,0,'$now','$now','','',0,0,0,'','',0,'','$dob','$country','$town')"))//insert record
                    {
                        $response = array('status' => 501, 'error' => 'Failed to insert user ');
                    } else {
                        $userId = DB::table('users')->select(array('id', 'fullname'))->where('email', $email)->get();
                        //if group generate registration number
                        $userName = $userId[0]->fullname;
                        $userId = $userId[0]->id;

                        if ($actype == 1) {
                            if (!$this->create_accounts($userName, $userId)) {
                                echo json_encode(array('status' => 501, 'error' => 'Could not create accounts'));
                                die;
                            }

                            $sms_bundle = array('phone' => $phone, 'message' => "Hello $name and welcome to DIB WAVES, your details will be verified shortly");
                            QueueGrails::dispatch(1, $sms_bundle);

                        }
                        if ($actype == 4) {
                            $idno = "Group" . $userId;

                            if (!$this->api->crud_update($userId, $idno)) {

                                echo json_encode(array(
                                    'status' => 501,
                                    'error' => 'Failed to update gid'
                                ));
                            } else {
                                if (!$this->create_group_accounts($userName, $userId, 'group')) {
                                    echo json_encode(array('status' => 501, 'error' => 'Could not create accounts'));
                                    die;
                                }
                                $sms_bundle = array('phone' => $phone, 'message' => "$name's registration number is " . $idno);
                                QueueGrails::dispatch(1, $sms_bundle);
                            }
                        } elseif ($actype == 5) {
                            $idno = "Business" . $userId;

                            if (!$this->api->crud_update($userId, $idno)) {

                                echo json_encode(array(
                                    'status' => 501,
                                    'error' => 'Failed to update gid'
                                ));
                            } else {
                                if (!$this->create_group_accounts($userName, $userId, 'business')) {
                                    echo json_encode(array('status' => 501, 'error' => 'Could not create accounts'));
                                    die;
                                }
                                $sms_bundle = array('phone' => $phone, 'message' => "You have registered $name successfully as a business, registration number is " . $idno);
                                QueueGrails::dispatch(1, $sms_bundle);
                            }
                        } elseif ($actype == 6) {
                            $idno = "Sacco" . $userId;

                            if (!$this->api->crud_update($userId, $idno)) {

                                echo json_encode(array(
                                    'status' => 501,
                                    'error' => 'Failed to update gid'
                                ));
                            } else {
                                if (!$this->create_group_accounts($userName, $userId, 'sacco')) {
                                    echo json_encode(array('status' => 501, 'error' => 'Could not create accounts'));
                                    die;
                                }
                                $sms_bundle = array('phone' => $phone, 'message' => "Your Sacco $name has been created successfully. Your registration number is " . $idno);
                                QueueGrails::dispatch(1, $sms_bundle);
                            }
                        }
                        //send email
                        $message = $this->api->getSignUpEmailMessage($name);
                        $subject = "Welcome to DIB WAVES";
                        $files = public_path() . 'res/Loan_policy.pdf';

                        $email_bundle = array('subject' => $subject, 'message' => $message, 'email' => $email, 'files' => $files);
                        QueueGrails::dispatch(2, $email_bundle);

                        $response['user_id'] = $userId;
                        $response['actype'] = $actype;
                        $response['ac_status'] = $acStatusThis;
                    }
                } else {
                    echo json_encode(array(
                        'status' => 501,
                        'error' => 'The Id/Registration number you entered is already registered'
                    ));
                    die;
                }
            }
        } else {
            echo json_encode(array(
                'status' => 501,
                'error' => 'enter a valid / passport / registration number'
            ));
            die;
        }

        echo json_encode($response);
    }

    private function user_authentication($user_id, $password)
    {
        $salt_query = mysqli_query($this->api->link, "SELECT c_password FROM users WHERE id = $user_id");
        $fetcha = mysqli_fetch_array($salt_query, MYSQLI_ASSOC);
        $salt = $fetcha['c_password'];
        $password_hash = hash('sha256', $salt + $password);

//		$is_password_correct = $db->query( "SELECT password_hash = ? FROM Accounts WHERE account_name = ?",$password_hash, $input_account_name );
    }

    private function scoreUser($user_id)
    {

//	    get the /crb score
//      get the iprs and metropol

        $score = 0;
        $eligible = 0;
        if ($score > 200 && $score <= 400) {
        } elseif ($score > 400 && $score < 500) {
            $eligible = 0;
        } elseif ($score > 500 && $score < 600) {
            $eligible = 15000;
        } elseif ($score > 600 && $score < 650) {
            $eligible = 20000;
        } elseif ($score > 650 && $score < 700) {
            $eligible = 30000;
        } elseif ($score > 700 && $score < 750) {
            $eligible = 45000;
        } elseif ($score > 750 && $score < 800) {
            $eligible = 60000;
        } elseif ($score > 800 && $score < 850) {
            $eligible = 90000;
        } elseif ($score > 850 && $score < 900) {
            $eligible = 100000;
        }
        return $eligible;
//
    }
}
