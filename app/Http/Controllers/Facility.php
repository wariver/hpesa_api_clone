<?php
/**
 * Created by Warui.
 * User: root
 * Date: 8/22/18
 * Time: 5:37 PM
 */

namespace App\Http\Controllers;

use App\Jobs\QueueGrails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @property  TX_BANK_SAVINGS
 */
class Facility extends Controller
{
    public $link;
    public $now;
    public $api;
    public $abi;
    public $AC_TYPE_BANK;
    public $AC_TYPE_SAVINGS;
    public $AC_TYPE_HOLDING;
    public $AC_TYPE_FACILITY;
    public $AC_TYPE_DCF;
    public $AC_TYPE_SCHOOL_FEES;
    public $AC_TYPE_GENERAL;

    public $TX_TYPE_ASSET_PROFIT;
    public $TX_TYPE_FACILITY_DISBURSMENT;
    public $TX_TYPE_FACILITY_OFFSET;
    public $TX_TYPE_FACILITY_CREATION;
    public $TX_TYPE_BANK_SAVINGS;
    public $TX_TYPE_BANK_REPAYMENT;
    public $TX_TYPE_C2B_BANK;

    public $FACILITY_REPAYMENT_ACCOUNT_ID;
    public $C2B_ACCOUNT_ID;

    /**
     * Facility constructor.
     */
    public function __construct()
    {
        $this->AC_TYPE_BANK = 1;
        $this->AC_TYPE_SAVINGS = 2;
        $this->AC_TYPE_HOLDING = 3;
        $this->AC_TYPE_FACILITY = 4;
        $this->AC_TYPE_DCF = 5;
        $this->AC_TYPE_SCHOOL_FEES = 6;
        $this->AC_TYPE_GENERAL = 7;
        $this->FACILITY_REPAYMENT_ACCOUNT_ID = 7;
        $this->C2B_ACCOUNT_ID = 2;
        $this->TX_TYPE_FACILITY_DISBURSMENT = 2;
        $this->TX_TYPE_FACILITY_CREATION = 21;
        $this->TX_TYPE_BANK_REPAYMENT = 3;
        $this->TX_TYPE_ASSET_PROFIT = 20;
        $this->TX_TYPE_C2B_BANK = 22;
        $this->TX_TYPE_FACILITY_OFFSET = 25;
        $this->abi = new Abi();
        $this->api = new Api();

        $this->now = $this->api->now;
    }

    public function test()
    {
        echo 'mths';
    }

    public function eligibility(Request $request)
    {
        $user_id = $request->get('id');
        $loan_type = $request->get('loantype');
        $security = $request->get('security');
        $amount = $request->get('amount');
        $bank_checked = $request->get('bank_checked');
        $savings_checked = $request->get('savings_checked');
        $username = $request->get('username');
        $phone_number = $request->get('phone_number');
        $id_number = $request->get('id_number');
        $dob = $request->get('dob');
        $address = $request->get('address');
        $branch = $request->get('branch');
        $top_up = $request->get('top_up');
//		chuja on simple things like

        $unpaid_facility = $this->has_unpaid_facility($user_id);
        $duration_as_member = $this->api->getMembershipDuration($user_id);

        if (!isset($user_id) || !isset($loan_type) || !isset($security) || !isset($amount)) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'The request was incomplete'
            ));
            die;
        }

        if ($unpaid_facility['has_unpaid'] == true) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'Kindly pay your outstanding facility to access more'
            ));
            die;
        }
        if ($duration_as_member < 7) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'You must have been a member for more than a week to get a facility with DIB Waves'
            ));
            die;
        }
        $savings_ac = $this->abi->get_user_account($user_id, $this->AC_TYPE_SAVINGS);
        $member_savings = $this->abi->get_account_balance('balance_savings', $savings_ac);

        if ($security == 1) {
            if (($member_savings * 3) < $amount && $loan_type == 2) {
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'The amount you have applied is more than 3 times your savings balance. You can only get as much as 3 times your savings amount for a school fees facility '
                ));
                die;
            }
            if (($member_savings < $amount) && $loan_type == 3 && $savings_checked == 1) {
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'The amount you have applied is more than your savings balance. Increase your savings to get this facility'
                ));
                die;
            }
            if ($loan_type == 1) {
//		Check  for system scoring
                $amount_elig = $this->selfScoring($user_id);
                if ($amount > $amount_elig) {
                    echo json_encode(array(
                        'status' => 501,
                        'error' => 'You are not eligible to get that amount. Please check limit on finance limit tab on the mainmenu'
                    ));
                    die;
                }
            }
        }
        echo json_encode(array(
            'status' => 200,
            'msg' => 'You are eligible for a the facility you are applying for. Continue?'
        ));
        die;
    }

    public function check_and_apply(Request $request)
    {
        $user_id = htmlspecialchars($request->get('id'));
        $loan_type = (int)htmlspecialchars($request->get('loantype'));
        $security = htmlspecialchars($request->get('security'));
        $amount = htmlspecialchars($request->get('amount'));
        $username = htmlspecialchars($request->get('username'));
        $phone_number = htmlspecialchars($request->get('phone_number'));
        $id_number = htmlspecialchars($request->get('id_number'));
        $dob = date("d/m/Y", strtotime(htmlspecialchars($request->get('dob'))));
        $address = htmlspecialchars($request->get('address'));
        $branch = htmlspecialchars($request->get('branch'));
        $bank_checked = htmlspecialchars($request->get('bank_checked'));
        $savings_checked = htmlspecialchars($request->get('savings_checked'));
        $top_up = htmlspecialchars($request->get('top_up'));
        $auto = 1;
        $ever_applied = $request->get('ever_applied');

        $now = $this->now;
        $bank_checked = 1;
        $savings_checked = 1;
        $application_data = array(
            'user_id' => $user_id,
            'applicant_name' => (string)$username,
            'id_number' => (string)$id_number,
            'phone_number' => (string)$phone_number,
            'amount' => $amount,
            'approved' => 0,
            'customer_type' => $ever_applied,
            'dob' => (string)$dob,
            'branch_name' => 1,
            'product_type' => $loan_type,
            'status' => 0,
            'created_at' => (string)$now,
            'updated_at' => null
        );
        $case = 0;
        $customer_type = $this->abi->customer_type($user_id);
        $reason = '';
        $tx_id_result = -1;
        $tx_id = -1;
        $new_ac_id = -1;
        $ln_application_id = -1;
        $user_bank_account = -1;

        $reason = "pending";
        $application_data['reason'] = $reason;
        $application_data['status'] = 3;
        $this->record_facility_application($application_data);
        if (!isset($user_id) || !isset($loan_type) || !isset($amount)) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'There are some crucial missing parameters in your request'
            ));
            die;
        }

        $facility_limits = $this->api->getFacilityLimit($loan_type);
        $lower = (int)$facility_limits['lower'];
        $upper = (int)$facility_limits['upper'];
        $user_limit = (int)$this->abi->get_user_facility_limit_function($user_id)['murabaha_limit'];
        $user_limit_2 = (int)$this->abi->get_user_facility_limit_function($user_id)['school_fees_limit'];
        $user_limit_3 = (int)$this->abi->get_user_facility_limit_function($user_id)['business_limit'];
        $final_upper = min($user_limit, $upper);
        $final_upper_2 = min($user_limit_2, $upper);
        $final_upper_3 = min($user_limit_3, $upper);


        if ($loan_type == 1) {
            if ($amount < $lower || $amount > $final_upper) {
                $reason = "outside limit";
                $application_data['reason'] = $reason;
                $application_data['status'] = 3;
                $this->update_facility_application($this->get_last_application(), $application_data);
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'Finance amount should be within KES. ' . number_format($final_upper, 2)
                ));
                die;
            }
        }
        if ($loan_type == 2) {
            if ($amount < $lower || $amount > $final_upper_2) {
                $reason = "outside limit";
                $application_data['reason'] = $reason;
                $application_data['status'] = 3;
                $this->update_facility_application($this->get_last_application(), $application_data);

                if ($lower > $final_upper_2) {
                    echo json_encode(array(
                        'status' => 501,
                        'error' => 'Your financing limit is KES. ' . number_format($lower - $final_upper_2, 2) . ' below facility type lower limit of KES. ' . number_format($lower, 2)
                    ));
                    die;
                }

                $reason = "outside limit";
                $application_data['reason'] = $reason;
                $application_data['status'] = 3;
                $this->update_facility_application($this->get_last_application(), $application_data);
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'Your financing application amount should be within KES. ' . number_format($lower, 2)
                        . ' and KES. ' . number_format($final_upper_3, 2)
                ));
                die;

            }
        }
        if ($loan_type == 3) {
            if ($amount < $lower || $amount > $final_upper_3) {

                if ($lower > $final_upper_3) {
                    $reason = "outside limit";
                    $application_data['reason'] = $reason;
                    $application_data['status'] = 3;
                    $this->update_facility_application($this->get_last_application(), $application_data);
                    echo json_encode(array(
                        'status' => 501,
                        'error' => 'Your financing limit is KES. ' . number_format($lower - $final_upper_3, 2) . ' below facility type lower limit of KES. ' . number_format($lower, 2)
                    ));
                    die;
                }

                $reason = "outside limit";
                $application_data['reason'] = $reason;
                $application_data['status'] = 3;
                $this->update_facility_application($this->get_last_application(), $application_data);
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'Your financing application amount should be within KES. ' . number_format($lower, 2)
                        . ' and KES. ' . number_format($final_upper_3, 2)
                ));
                die;

            }
        }

        $outstanding_loan_balance = $this->has_unpaid_facility($user_id);
        if ($outstanding_loan_balance['has_unpaid'] == true) {
            $ac_id_outstanding = $outstanding_loan_balance['account'];
            $balance_outstanding = $this->abi->get_account_balance('balance_loans', $ac_id_outstanding);

            $reason = "outstanding facility";
            $application_data['reason'] = $reason;
            $application_data['status'] = 3;
            $this->update_facility_application($this->get_last_application(), $application_data);
            echo json_encode(array(
                'status' => 501,
                'error' => 'Kindly pay your outstanding finance balance of KES. ' . $balance_outstanding . ' to access more finance.'
            ));
            die;
        }
        if ($loan_type == 2) {
            $school_accounts = $this->abi->get_all_user_dcf_accounts($user_id, 4);
            $reason = "lacks a school account";
            $application_data['reason'] = $reason;
            $application_data['status'] = 3;
            $this->update_facility_application($this->get_last_application(), $application_data);
            if (!$school_accounts) {
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'Please create a school fees account on the app first.'
                ));
                die;
            }
        }
        $period = $this->api->getRepaymentPeriodBasedOnLoanType($loan_type, 'hours');
        $loanDueDate = date('Y-m-d H:i:s', strtotime($this->now) + ($period));
        $dateOfNonPerformance = date('Y-m-d H:i:s', strtotime($loanDueDate) + (60 * 60 * 1));

        /*Flexcube ledger entries*/
        $bankAssetBalance = $this->abi->get_account_balance('balance_general', 1);
        $profitAccountBalance = $this->abi->get_account_balance('balance_general', 4);

        $user_bank_account = $this->abi->get_user_account($user_id, $this->AC_TYPE_BANK);

        $user_savings_account = $this->abi->get_user_account($user_id, $this->AC_TYPE_SAVINGS);
        $user_holding_account = $this->abi->get_user_account($user_id, $this->AC_TYPE_HOLDING);
        $user_bank_balance = $this->abi->get_account_balance('balance_bank', $user_bank_account);
        $user_savings_balance = $this->abi->get_account_balance('balance_savings', $user_savings_account);
        $user_holding_balance = $this->abi->get_account_balance('balance_holding', $user_holding_account);
        $surplus = 0;
        /*API TWO*/
        /*sieve on if the user is qualified to get this loan*/
        if ($loan_type == 1) {
//		Check  for system scoring
            $amount_elig = $this->selfScoring($user_id);
            if ($amount > $amount_elig) {
                $reason = "low credit score";
                $application_data['reason'] = $reason;
                $this->update_facility_application($this->get_last_application(), $application_data);
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'You are not eligible to get that amount. Please check limit on finance limit tab on the mainmenu'
                ));
                die;
            }
        } elseif ($loan_type == 2) {
//		Check 3 times savings
            if ($amount > ($savings_checked * 3)) {
                $reason = "low savings";
                $application_data['reason'] = $reason;
                $this->update_facility_application($this->get_last_application(), $application_data);
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'You do not have enough savings to get this facility.'
                ));
                die;
            }
        } elseif ($loan_type == 3) {
//		Check for savings or bank account picked
            if (($savings_checked == 1) && ($bank_checked == 1)) {
                if ($amount > ($user_savings_balance + $user_bank_balance)) {
                    $reason = "low savings";
                    $application_data['reason'] = $reason;
                    $application_data['status'] = 3;
                    $this->update_facility_application($this->get_last_application(), $application_data);
                    echo json_encode(array(
                        'status' => 501,
                        'error' => 'You do not have enough security in both your accounts. Top up either to access this facility'
                    ));
                    die;
                }
                if (($amount > $user_savings_balance) && ($amount < ($user_savings_balance + $user_bank_balance))) {
                    $case = 4;
                    $surplus = $amount - $user_savings_balance;
                }

            } elseif (($savings_checked == 1) && ($bank_checked == 0)) {
                if ($amount > $user_savings_balance) {
                    $reason = "low savings";
                    $application_data['reason'] = $reason;
                    $application_data['status'] = 3;
                    $this->update_facility_application($this->get_last_application(), $application_data);
                    echo json_encode(array(
                        'status' => 501,
                        'error' => 'You do not have enough security in your savings to obtain this facility. Try including the funds in your DIB Waves Bank Account'
                    ));
                    die;
                }
                $case = 2;

            } elseif (($savings_checked == 0) && ($bank_checked == 1)) {
//				check if money in bank can sort out amount
                if ($amount > $user_savings_balance) {
                    $reason = "low savings";
                    $application_data['reason'] = $reason;
                    $application_data['status'] = 3;
                    $this->update_facility_application($this->get_last_application(), $application_data);
                    echo json_encode(array(
                        'status' => 501,
                        'error' => 'You do not have enough security in your DIB Waves Bank Account to obtain this facility. Perhaps include the savings option.'
                    ));
                    die;
                }
//				transfer funds from bank account to holding account
                $case = 3;
            }
            /*
                1. if money can be found in savings use it
                2. if the money cannot fit savings but can fit bank account + savings ...transfer bank account difference to savings
                3. if the money cannot fit either go home*/
        }

        $formatPhoneVals = json_decode($this->api->phoneFormat($phone_number), true);

        if ($formatPhoneVals['status'] == true) {
            $phone_number = $formatPhoneVals['formatedPhone'];
        }
        $customer_type = $this->facility_get_customer_type($user_id);
        $response = array();

//		dd( number_format( $blackList, 2 ) );
        /*NOTES
        IF SECURITY IS SAVINGS - SEND THE AMOUNT FROM SAVINGS TO HOLDING

        CHECK LOAN TYPE AND DO THE NEEDFUL TRANSACTION.
        */
        if ($security == 1) {
//		if amount asked is less than minimum amount or more than facility limit

            $loanInterestVals = json_decode($this->api->getLoanInterest($loan_type, $amount, $period), true);
//			calculate profit
            $loanInterest = $loanInterestVals['interest'];
            $monthlyrepayment = $loanInterestVals['monthlyrepayment'];
            $ln_application_id = $this->get_last_application();
            //			CREATE  TRANSACTION
            if (mysqli_query($this->api->link, "INSERT INTO transactions(amount, tx_type, tx_state,  user_id, time_created) VALUES ($amount, 2, 'PENDING', $user_id, '$this->now')")) {
                $tx_id_result = mysqli_query($this->api->link, "SELECT MAX(id) AS tx_id FROM transactions");
                $tx_id_temp = mysqli_fetch_array($tx_id_result, MYSQLI_ASSOC);
                $tx_id = $tx_id_temp['tx_id'];
            } else {
                echo json_encode(array('status' => 501, 'error' => mysqli_error($this->api->link)));
            }
            if (!mysqli_query($this->api->link, "START TRANSACTION")) {
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'could not complete transaction at the moment'
                ));
                $this->abi->mark_transaction_state($tx_id, 'FAILED');
                die;
            }

            /*CREATE NEW ACCOUNT AND NEW LOAN ACCOUNT */
            if (mysqli_query($this->api->link, "INSERT INTO accounts_b(ac_name, extra_types, extra_types2, ac_type, branch_id) VALUES ('$username', NULL , NULL, 4, 1)")) {
                $new_ac_id_results = mysqli_query($this->api->link, "SELECT MAX(id) AS ac_id FROM accounts_b");
                $new_ac_temp = mysqli_fetch_array($new_ac_id_results, MYSQLI_ASSOC);
                $new_ac_id = $new_ac_temp['ac_id'];
            } else {
                $this->abi->mark_transaction_state($tx_id, 'FAILED');
                die;
            }
            $peri = $this->periodFacilityType($loan_type);
            $installments = json_decode($this->get_installments($peri, $amount), true);
            $installments_count = $installments['count'];
            $installment_amount = $installments['each_installment'];
            $desc = $this->get_facility_desc($loan_type);

            if (!mysqli_query($this->api->link, "INSERT INTO accounts_loans(account_id, user_id, application_id, loan_type, principle_amount, applicant_name, number_of_installments, phone_number, national_id, customer_type, address, dob, branch_name, product_description, installments_amount, book_date, maturity_date, date_of_non_performance, credit_score) VALUES ($new_ac_id, $user_id, $ln_application_id, $loan_type, $amount, '$username', $installments_count, '$phone_number', '$id_number', $customer_type, '$address', '$dob', '$branch', '$desc', $installment_amount, '$this->now', '$loanDueDate', '$dateOfNonPerformance', 0)")) {
                $this->abi->mark_transaction_state($tx_id, 'FAILED');
                echo json_encode(array('status' => 501, 'error' => mysqli_error($this->api->link)));
                die;
            }
//			charge upfront
            $credit_to_bank = ($amount - $loanInterest);
//			works for loans type 2 and 3
            $debit_to_savings = ($user_savings_balance - $amount);
            $credit_to_holding = ($user_holding_balance + $amount);
//			DEBIT AND CREDIT NECESSARY ACCOUNTS
            if (!mysqli_query($this->api->link, "COMMIT")) {
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'could not complete transaction at the moment'
                ));
                die;
            }
            if (!mysqli_query($this->api->link, "START TRANSACTION")) {
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'could not complete transaction at the moment'
                ));
                $this->abi->mark_transaction_state($tx_id, 'FAILED');
                die;
            }
            if ($this->abi->add_account_to_user_holder($user_id, $new_ac_id, $this->AC_TYPE_FACILITY) == 0) {
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'could not complete transaction at the moment'
                ));
                $this->abi->mark_transaction_state($tx_id, 'FAILED');
                die;
            }
//			    dr bank asset account
            $this->abi->performTransactionEntry($tx_id, 1, $this->AC_TYPE_GENERAL, $amount, 2, $this->TX_TYPE_ASSET_PROFIT, $bankAssetBalance, ($bankAssetBalance - $amount), $this->now);
//			    cr profit account
            $this->abi->performTransactionEntry($tx_id, 4, $this->AC_TYPE_GENERAL, $loanInterest, 1, $this->TX_TYPE_ASSET_PROFIT, $profitAccountBalance, ($profitAccountBalance + $loanInterest), $this->now);
//				cr facility account
            $this->abi->performTransactionEntry($tx_id, $new_ac_id, $this->AC_TYPE_FACILITY, $amount, 1, $this->TX_TYPE_FACILITY_CREATION, 0, $amount, $this->now);

            if ($loan_type == 2) {
//              cr DIB-LIPAKARO if schools account
                $dib_lipa_karo = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_SCHOOL_FEES);
                $dib_lipa_karo_balance = $this->abi->get_account_balance('balance_school_fees', $dib_lipa_karo);
                $this->abi->performTransactionEntry($tx_id, $dib_lipa_karo, $this->AC_TYPE_SCHOOL_FEES, $credit_to_bank, 1, $this->TX_TYPE_FACILITY_DISBURSMENT, $dib_lipa_karo_balance, ($dib_lipa_karo_balance + $credit_to_bank), $this->now);
            } else {
//              cr bank account
                $this->abi->performTransactionEntry($tx_id, $user_bank_account, $this->AC_TYPE_BANK, $credit_to_bank, 1, $this->TX_TYPE_FACILITY_DISBURSMENT, $user_bank_balance, ($user_bank_balance + $credit_to_bank), $this->now);
            }

            if ($loan_type != 1) {
                if ($case == 3) {
//			    dr bank account
                    $this->abi->performTransactionEntry($tx_id, $user_bank_account, $this->AC_TYPE_BANK, $amount, 2, $this->TX_TYPE_FACILITY_DISBURSMENT, $user_bank_balance, ($user_bank_balance - $amount), $this->now);
//              cr holding account
                    $this->abi->performTransactionEntry($tx_id, $user_holding_account, $this->AC_TYPE_HOLDING, $amount, 1, $this->TX_TYPE_FACILITY_DISBURSMENT, $user_holding_balance, $credit_to_holding, $this->now);
                } elseif ($case == 2) {
//			    dr savings account
                    $this->abi->performTransactionEntry($tx_id, $user_savings_account, $this->AC_TYPE_SAVINGS, $amount, 2, $this->TX_TYPE_FACILITY_DISBURSMENT, $user_savings_balance, $debit_to_savings, $this->now);
//              cr holding account
                    $this->abi->performTransactionEntry($tx_id, $user_holding_account, $this->AC_TYPE_HOLDING, $amount, 1, $this->TX_TYPE_FACILITY_DISBURSMENT, $user_holding_balance, $credit_to_holding, $this->now);

                } elseif ($case == 4) {
//			    dr bank account
                    $this->abi->performTransactionEntry($tx_id, $user_savings_account, $this->AC_TYPE_SAVINGS, $surplus, 2, $this->TX_TYPE_BANK_SAVINGS, $user_bank_balance, ($user_bank_balance - $surplus), $this->now);
//			    cr savings account
                    $this->abi->performTransactionEntry($tx_id, $user_savings_account, $this->AC_TYPE_BANK, $surplus, 1, $this->TX_TYPE_BANK_SAVINGS, $user_savings_balance, ($user_savings_balance + $surplus), $this->now);

//			    dr savings account
                    $this->abi->performTransactionEntry($tx_id, $user_savings_account, $this->AC_TYPE_SAVINGS, $amount, 2, $this->TX_TYPE_FACILITY_DISBURSMENT, $user_savings_balance, $debit_to_savings, $this->now);
//              cr holding account
                    $this->abi->performTransactionEntry($tx_id, $user_holding_account, $this->AC_TYPE_HOLDING, $amount, 1, $this->TX_TYPE_FACILITY_DISBURSMENT, $user_holding_balance, $credit_to_holding, $this->now);
                } else {
//			    dr savings account
                    $this->abi->performTransactionEntry($tx_id, $user_savings_account, $this->AC_TYPE_SAVINGS, $amount, 2, $this->TX_TYPE_FACILITY_DISBURSMENT, $user_savings_balance, $debit_to_savings, $this->now);
//              cr holding account
                    $this->abi->performTransactionEntry($tx_id, $user_holding_account, $this->AC_TYPE_HOLDING, $amount, 1, $this->TX_TYPE_FACILITY_DISBURSMENT, $user_holding_balance, $credit_to_holding, $this->now);
                }
            }
            $extraMsg = "";
            if ($monthlyrepayment > 0) {
                $extraMsg = "Your monthly repayment is KES. " . number_format($installment_amount, 2) . ".";
            }
//			ADD USER TO ACCOUNT HOLDERS HAVING A LOAN
            if (!mysqli_query($this->api->link, "COMMIT")) {
                $application_data['reason'] = 'Failed Transaction';
                $application_data['status'] = 2;
                $this->update_facility_application($this->get_last_application(), $application_data);
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'could not complete transaction at the moment'
                ));
                die;
            } else {

                $this->abi->mark_transaction_state($tx_id, 'COMPLETE');
                $smsMessage = "Dear " . $username . ", your finance application of KES. " . number_format($amount, 2) . " is successful." . $extraMsg . " Check your Bank Account balance. Your finance is due on $loanDueDate.";
                $userPhone = $phone_number;
                $sms_bundle = array('phone' => $userPhone, 'message' => $smsMessage);
                QueueGrails::dispatch(1, $sms_bundle);
                $amount = $amount - $loanInterest;
                if ($loan_type == 2) {
                    $application_data['reason'] = 'Success';
                    $application_data['status'] = 1;
                    $application_data['approved'] = 1;
                    $this->update_facility_application($this->get_last_application(), $application_data);
                    echo json_encode(array(
                        'status' => 200,
                        'msg' => 'Your finance application is successful. Ksh ' . number_format($amount, 2) . ' has been credited to your DIB LIPAKARO Account.',
                        'loan_id' => $new_ac_id
                    ));
                    die;
                }
                $application_data['reason'] = 'Success';
                $application_data['status'] = 1;
                $application_data['approved'] = 1;
                $this->update_facility_application($this->get_last_application(), $application_data);

                echo json_encode(array(
                    'status' => 200,
                    'msg' => 'Your finance application is successful. Ksh ' . number_format($amount, 2) . ' has been credited to your Bank Account.',
                    'loan_id' => $new_ac_id
                ));
                die;
            }
        }
    }

    private function periodFacilityType($type)
    {
        $period = 0;
        if ($type == 1) {
            $period = 1;
        } elseif ($type == 2) {
            $period = 3;
        } elseif ($type == 3) {
            $period = 3;
        }

        return $period;
    }

    public function repayment(Request $request)
    {
        $user_id = $request->get('id');
        $amount = (int)$request->get('amount');
        $facility_account = (int)$request->get('facility_account');
        $source_account = $request->get('source_account');
        $channel = $request->get('channel');
        $phone = (int)$request->get('phone');
        $user_name = $request->get('username');

        if ($facility_account == -1) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'You do not have a pending facility.'
            ));
            die;
        }

        if (!is_numeric($amount) && $amount <= 0) {
            echo json_encode(array('status' => 501, 'error' => 'Please input an amount'));
            die;
        }
        if (!isset($user_id) && !isset($amount) && !isset($facility_account)) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'There were some missing parameters in your request'
            ));
            die;
        }
        $tx_type = 0;
        $source_account_balance = 0;
        $ac_type = 0;

        if ($channel == 'bank') {
            $ac_type = $this->abi->AC_TYPE_BANK;
            $source_account = $this->abi->get_user_account($user_id, $ac_type);
            $source_account_balance = $this->abi->get_account_balance('balance_bank', $source_account);
            $tx_type = $this->abi->TX_TYPE_BANK_REPAYMENT;

            if ($source_account_balance < $amount) {
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'You have insufficient funds.'
                ));
                die;
            }
        } elseif ($channel == 'c2b') {
            $tx_type = $this->abi->TX_TYPE_C2B_REPAYMENT;
            $ac_type = $this->abi->AC_TYPE_GENERAL;
            $source_account = $this->C2B_ACCOUNT_ID;
            $source_account_balance = $this->abi->get_account_balance('balance_general', $this->C2B_ACCOUNT_ID);
        }
//      fetch user accounts
        $holding_account_id = $this->abi->get_user_account($user_id, $this->AC_TYPE_HOLDING);
        $savings_account_id = $this->abi->get_user_account($user_id, $this->AC_TYPE_SAVINGS);
//		get balances
        $facility_account_balance = $this->abi->get_account_balance('balance_loans', $facility_account);
        $facility_repayment_account_balance = $this->abi->get_account_balance('balance_general', $this->FACILITY_REPAYMENT_ACCOUNT_ID);
        $holding_account_balance = $this->abi->get_account_balance('balance_holding', $holding_account_id);
        $savings_account_balance = $this->abi->get_account_balance('balance_savings', $savings_account_id);

        if ($facility_account_balance <= 0) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'You do not have any outstanding facility.'
            ));
            die;
        }
        /*if ( ( $facility_account_balance - $amount ) < 0 ) {
            $diff = ( $amount - $facility_account_balance );
            echo json_encode( array(
                'status'     => 502,
                'difference' => $diff,
                'info'       => 'The amount will surpass facility amount due. Do you wish to push it to savings?'
            ) );
            die;
        }*/

        $tx_id = $this->abi->create_transaction($amount, $tx_type, $user_id);
//		dd( $holding_account_balance );
//		dr source account

        if ($channel == 'bank') {
            if ($amount > $facility_account_balance) {
                $amount = $facility_account_balance;
            }
            $this->abi->performTransactionEntry($tx_id, $source_account, $ac_type, $amount, 2, $tx_type, $source_account_balance, ($source_account_balance - $amount), $this->now);
        } elseif ($channel == 'c2b') {
            $id_number = $this->api->getUserIdNo($user_id);
            $paymentRequestResult = $this->api->stkPayment($amount, $phone, $tx_type, $user_id, $tx_id, $id_number);
            $resultVals = json_decode($paymentRequestResult, true);
            if ($resultVals['status'] == 1) {
                $this->abi->performTransactionEntry($tx_id, $source_account, $ac_type, $amount, 2, $tx_type, $source_account_balance, ($source_account_balance - $amount), $this->now);
            } else {
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'Request failed. Please try again after a few minutes.'
                ));
                die;
            }

        }
        $surplus = 0;
        if ($amount > $facility_account_balance) {
//		dr loan account
            if ($channel == 'bank') {
                $this->abi->performTransactionEntry($tx_id, $facility_account, $this->AC_TYPE_FACILITY, $amount, 2, $this->TX_TYPE_FACILITY_OFFSET, $facility_account_balance, ($facility_account_balance - $facility_account_balance), $this->now);
            } elseif ($channel == 'c2b') {
                $this->abi->performTransactionEntry($tx_id, $facility_account, $this->AC_TYPE_FACILITY, $amount, 2, $this->TX_TYPE_FACILITY_OFFSET, $facility_account_balance, ($facility_account_balance - $facility_account_balance), $this->now);
//				cr bank account
                $surplus = $amount - $facility_account_balance;
                $bank_account = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_BANK);
                $bank_account_balance = $this->abi->get_account_balance('balance_bank', $bank_account);
                $this->abi->performTransactionEntry($tx_id, $bank_account, $this->AC_TYPE_BANK, $surplus, 1, $this->TX_TYPE_C2B_BANK, $bank_account_balance, ($bank_account_balance + $surplus), $this->now);
            }
//          cr bank account - with surplus

        } else {
//		dr loan account
            $this->abi->performTransactionEntry($tx_id, $facility_account, $this->AC_TYPE_FACILITY, $amount, 2, $this->TX_TYPE_FACILITY_OFFSET, $facility_account_balance, ($facility_account_balance - $amount), $this->now);
        }
//		cr facility repayment
        $this->abi->performTransactionEntry($tx_id, $this->FACILITY_REPAYMENT_ACCOUNT_ID, $this->AC_TYPE_GENERAL, $amount, 1, $tx_type, $facility_repayment_account_balance, ($facility_repayment_account_balance + $amount), $this->now);
        $loan_type = $this->get_loan_type($facility_account);

        if ($loan_type != -1 && $loan_type != '1') {
            if ($amount > $facility_account_balance) {
//		dr holding account
                $this->abi->performTransactionEntry($tx_id, $holding_account_id, $this->AC_TYPE_HOLDING, $amount, 2, $tx_type, $holding_account_balance, ($holding_account_balance - $facility_account_balance), $this->now);
//		cr savings account
                $this->abi->performTransactionEntry($tx_id, $savings_account_id, $this->AC_TYPE_SAVINGS, $amount, 1, $tx_type, $savings_account_balance, ($savings_account_balance + $facility_account_balance), $this->now);
            } else {
                //		dr holding account
                $this->abi->performTransactionEntry($tx_id, $holding_account_id, $this->AC_TYPE_HOLDING, $amount, 2, $tx_type, $holding_account_balance, ($holding_account_balance - $amount), $this->now);
//		cr savings account
                $this->abi->performTransactionEntry($tx_id, $savings_account_id, $this->AC_TYPE_SAVINGS, $amount, 1, $tx_type, $savings_account_balance, ($savings_account_balance + $amount), $this->now);
            }
        }

        if (!$this->abi->commit_transaction($tx_id)) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'could not complete transaction at the moment'
            ));
            die;
        } else {
            $this->abi->mark_transaction_state($tx_id, 'COMPLETE');
            echo json_encode(array(
                'status' => 200,
                'msg' => 'Finance repayment was successful.'
            ));
        }
    }

    private function get_installments($loan_period, $amount)
    {
        $count = 0;
        $count = ($loan_period * 4);

        return json_encode(array('count' => $count, 'each_installment' => ($amount / $count)));
    }

    private function get_facility_desc($ln_type)
    {
        $desc = '';

        if ($ln_type == 1) {
            $desc = 'Instant Facility';
        } elseif ($ln_type == 2) {
            $desc = 'School Fees Financing';
        } elseif ($ln_type == 3) {
            $desc = 'Business Funding';
        }

        return $desc;
    }

//	check whether customer is first time or has ever take a loan before
    private function facility_get_customer_type($user_id)
    {
        $id = $this->abi->get_user_account($user_id, $this->AC_TYPE_FACILITY);

        return $id == -1 ? 1 : 2;
    }

    private function user_facilities($user_id)
    {

    }

    public function has_unpaid_facility($user_id)
    {
        $has_unpaid = false;
        $facility_unpaid = array();

        $past_facility_accounts = $this->abi->get_all_user_facility_accounts($user_id);
//		loop through user account finding if any has balance greater than three
        foreach ($past_facility_accounts as $account) {
            $id = $account['id'];

            $balance = $this->abi->get_account_balance('balance_loans', $id);
            if ($balance > 0) {
                $has_unpaid = true;
                $facility_unpaid['account'] = $id;
                $facility_unpaid['type'] = $this->abi->get_facility_type($id);
                $facility_unpaid['balance'] = number_format($this->abi->get_account_balance('balance_loans', $id), 2);
                break;
            }
        }
        $facility_unpaid['has_unpaid'] = $has_unpaid;

        return $facility_unpaid;
    }

    private function update_user_limit_tp($user_id, $limitTP, $new_value)
    {
        $user_limit_updated = false;
        switch ($limitTP) {
            case 1:
                if (mysqli_query($this->api->link, "UPDATE user_facility_limits SET murabaha_limit = $new_value WHERE user_id = $user_id")) {
                    $user_limit_updated = true;
                } else {
                    $user_limit_updated = false;
                }
                break;
            case 2:
                if (mysqli_query($this->api->link, "UPDATE user_facility_limits SET school_fees_limit = $new_value WHERE user_id = $user_id")) {
                    $user_limit_updated = true;
                } else {
                    $user_limit_updated = false;
                }
                break;
            case 3:
                if (mysqli_query($this->api->link, "UPDATE user_facility_limits SET business_limit = $new_value WHERE user_id = $user_id")) {
                    $user_limit_updated = true;
                } else {
                    $user_limit_updated = false;
                }
                break;
            case 4:
                if (mysqli_query($this->api->link, "UPDATE user_facility_limits SET top_up_count = $new_value WHERE user_id = $user_id")) {
                    $user_limit_updated = true;
                } else {
                    $user_limit_updated = false;
                }
                break;
        }

        return $user_limit_updated;
    }

    private function offer_top_up_option($last_facility_ac_id)
    {
//		check top up limit and account limit for loan_type
        if (mysqli_query($this->api->link, "")) {

        }
    }

    public function selfScoring($user_id)
    {
        $return = 0;
        if (isset($user_id)) {
            if (!$getUserID = DB::table('users')->where('id', $user_id)->get()) {
                echo json_encode(array('status' => 501, 'error' => 'Error in fetching user'));
            } else {
                if (count($getUserID) > 0) {

                    $getUserID = $getUserID[0];
                    $idNumber = $getUserID->idpass;
//				dd($getUserID);

                    if (!$getScore = DB::table('creditdetails')->where('identity_number', $idNumber)->orderBy('creditId', 'DESC')->get()) {
                        echo json_encode(array('status' => 501, 'error' => 'Error in fetching user'));
                    } else {
                        if (DB::table('creditdetails')->where('identity_number', $idNumber)->orderBy('creditId', 'DESC')->count() > 0) {
                            $getScore = $getScore[0];
                            $score = $getScore->credit_score;
                        } else {
                            $score = 0;
                        }
                    }

                    if ($score < 50) {
                        $eligibleAmount = 0;
                    } elseif ($score >= 50 && $score <= 99) {
                        $return = 300;
                    } elseif ($score >= 100 && $score <= 199) {
                        $return = 500;
                    } elseif ($score >= 200 && $score <= 299) {
                        $return = 1000;
                    } elseif ($score >= 300 && $score <= 399) {
                        $return = 1500;
                    } elseif ($score >= 400 && $score <= 499) {
                        $return = 3000;
                    } elseif ($score >= 500 && $score <= 599) {
                        $return = 15000;
                    } elseif ($score >= 600 && $score <= 650) {
                        $return = 20000;
                    } elseif ($score >= 650 && $score <= 699) {
                        $return = 30000;
                    } elseif ($score >= 700 && $score <= 749) {
                        $return = 45000;
                    } elseif ($score >= 750 && $score <= 799) {
                        $return = 60000;
                    } elseif ($score >= 800 && $score <= 849) {
                        $return = 90000;
                    } elseif ($score >= 850 && $score <= 900) {
                        $return = 100000;
                    } else {
                        $return = 0;
                    }
                }
            }
            $response = array('status' => 200, 'instant_financing' => $return);
        } else {
            $response = array('status' => 501, 'error' => ' Missing parameters in your request');
        }

        return $response;
    }

    public function apply_top_up($user_id)
    {
//		check top up count
        if (mysqli_query($this->api->link, "SELECT top_up_count FROM user_facility_limits WHERE user_id = $user_id")) {
        }

    }

    public function get_user_facility_balance($user_id)
    {
        $temp = $this->has_unpaid_facility($user_id);
        echo json_encode($temp);
    }

    public function get_loan_type($loan_ac_id)
    {

        if ($result = mysqli_query($this->api->link, "SELECT loan_type FROM accounts_loans WHERE account_id = $loan_ac_id")) {
            $result_tmp = mysqli_fetch_array($result, MYSQLI_ASSOC);

            return $result_tmp['loan_type'];
        } else {
            return -1;
        }
    }

    public function push_excess_to_savings(Request $request)
    {
        $user_id = $request->get('id');
        $amount = $request->get('amount');
        $facility_id = $request->get('facility_id');

        $facility_balance = $this->abi->get_account_balance('balance_loans', $facility_id);

        $savings_ac_id = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_SAVINGS);
        $bank_ac_id = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_BANK);


        if (!isset($user_id) || !isset($amount) || !isset($savings_ac_id) || !isset($bank_ac_id)) {
            echo json_encode(array('status' => 501, 'There were some missing parameters in your request'));
            die;
        }
        $bank_ac_balance = $this->abi->get_account_balance('balance_bank', $bank_ac_id);
        $savings_ac_balance = $this->abi->get_account_balance('balance_savings', $savings_ac_id);

        if ($amount > $bank_ac_balance) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'You do not have enough funds to transfer this amount to your savings'
            ));
            die;
        }
        $tx_id = $this->abi->create_transaction($amount, $this->TX_BANK_SAVINGS, $user_id);
//		dr bank
        $this->abi->performTransactionEntry($tx_id, $bank_ac_id, $this->abi->AC_TYPE_BANK, $amount, 2, $this->TX_BANK_SAVINGS, $bank_ac_balance, ($bank_ac_balance - $amount), $this->abi->now);
//		cr savings
        $this->abi->performTransactionEntry($tx_id, $savings_ac_id, $this->abi->AC_TYPE_SAVINGS, $amount, 1, $this->TX_BANK_SAVINGS, $savings_ac_balance, ($savings_ac_balance + $amount), $this->abi->now);

        if (!$this->abi->commit_transaction($tx_id)) {
            echo json_encode(array('status' => 501, 'error' => 'could not complete transaction at the moment'));
            die;
        } else {
            echo json_encode(array(
                'status' => 200,
                'msg' => 'Amount KES. ' . $amount . ' transferred to savings successfully'
            ));
            die;
        }

    }

    public function get_defaulted_loans($status)
    {

        if ($status == 'defaulted') {
            $query = "SELECT * FROM accounts_loans AS a, balance_loans AS b
					WHERE a.account_id = b.account_id AND b.balance > 0 AND a.maturity_date < now() AND a.maturity_date + INTERVAL 30 DAY > now()";
        } elseif ($status == 'non-performing') {
            $query = "SELECT * FROM accounts_loans AS a, balance_loans AS b
					WHERE a.account_id = b.account_id AND b.balance > 0 AND a.maturity_date <  now() AND a.maturity_date > now() + INTERVAL 30 DAY";
        } elseif ($status == 'dormant') {
            $query = "SELECT * FROM accounts_loans AS a, balance_loans AS b
					WHERE a.account_id = b.account_id AND b.balance > 0 AND a.maturity_date < now() AND a.maturity_date > now() + INTERVAL 60 DAY";
//			charge 100 shillings per day
        } elseif ($status == 'dead') {
            $query = "SELECT * FROM accounts_loans AS a, balance_loans AS b
					WHERE a.account_id = b.account_id AND b.balance > 0 AND a.maturity_date < now() AND a.maturity_date > now() + INTERVAL 90 DAY";
//			kula mtu CRB
        }

        $defaulted_loans = mysqli_query($this->api->link, $query);
        $defaulted_loans_tmp = mysqli_fetch_array($defaulted_loans, MYSQLI_ASSOC);

        if ($defaulted_loans_tmp != null) {
            echo json_encode($defaulted_loans_tmp);
        } else {
            $defaulted_loans_tmp = 'no defaulted facilities';

            return $defaulted_loans_tmp;
        }
    }

    public function archive_serviced_loans()
    {
        $query = "SELECT * FROM accounts_loans AS a,balance_loans AS b WHERE a.account_id = b.account_id AND b.balance = 0";
        $serviced = mysqli_query($this->api->link, $query);
        $serviced_tmp = mysqli_fetch_all($serviced, MYSQLI_ASSOC);
        if ($serviced) {
            foreach ($serviced_tmp as $facility) {
                $account_id = $facility['account_id'];
                $user_id = $facility['user_id'];
                $application_id = $facility['application_id'];
                $loan_type = $facility['loan_type'];
                $principle_amount = $facility['principle_amount'];
                $applicant_name = $facility['applicant_name'];
                $phone_number = $facility['phone_number'];
                $national_id = $facility['national_id'];
                $customer_type = $facility['customer_type'];
                $address = $facility['address'];
                $dob = $facility['dob'];
                $branch_name = $facility['branch_name'];
                $product_description = $facility['product_description'];
                $book_date = $facility['book_date'];
                $maturity_date = $facility['maturity_date'];
                $date_of_non_performance = $facility['date_of_non_performance'];
                $credit_score = $facility['credit_score'];

                if (!mysqli_query($this->api->link, "START TRANSACTION")) {
                    echo json_encode(array(
                        'status' => 501,
                        'error' => 'could not complete transaction at the moment'
                    ));
                    die;
                }
                if (!mysqli_query($this->api->link, "INSERT INTO accounts_archive_loans_serviced (account_id, user_id, application_id, loan_type, principle_amount, applicant_name, phone_number, national_id, customer_type, address, dob, branch_name, product_description, book_date, maturity_date, date_of_non_performance, credit_score) VALUES ($account_id, $user_id, $application_id, $loan_type, $principle_amount, '$applicant_name', '$phone_number', '$national_id', $customer_type, '$address', '$dob', '$branch_name', '$product_description', '$book_date', '$maturity_date', '$date_of_non_performance', $credit_score)")) {
                    echo json_encode(array(
                        'status' => 530,
                        'error' => 'could not archive ' . $account_id . ' mysql : ' . mysqli_error($this->api->link)
                    ));
                    die;
                }
                if (!mysqli_query($this->api->link, "DELETE FROM balance_facility_provisions WHERE account_id = $account_id")) {
                    echo json_encode(array(
                        'status' => 530,
                        'error' => 'could not delete balance record ' . $account_id . ' mysql : ' . mysqli_error($this->api->link)
                    ));
                    die;
                }
                if (!mysqli_query($this->api->link, "DELETE FROM balance_loans WHERE account_id = $account_id")) {
                    echo json_encode(array(
                        'status' => 530,
                        'error' => 'could not delete balance record ' . $account_id . ' mysql : ' . mysqli_error($this->api->link)
                    ));
                    die;
                }
                if (!mysqli_query($this->api->link, "DELETE FROM accounts_loans WHERE account_id = $account_id")) {
                    echo json_encode(array(
                        'status' => 530,
                        'error' => 'could not delete account ' . $account_id . ' mysql : ' . mysqli_error($this->api->link)
                    ));
                    die;
                }
                if ($this->abi->remove_account_to_user_holder($user_id, $account_id) != 1) {
                    echo json_encode(array('status' => 530, 'error' => 'could not detach account ' . $account_id));
                    die;
                }

                if (!mysqli_query($this->api->link, "COMMIT")) {
                    echo json_encode(array(
                        'status' => 501,
                        'error' => 'could not complete transaction at the moment'
                    ));
                    die;
                } else {
                    echo $account_id . '<br>';
                }
            }
        } else {
            echo json_encode(array(
                'status' => 530,
                'error' => 'There are no serviced loans to clear'
            ));
            die;
        }
    }

    public function total_assets_disbursed_today()
    {
        $serviced = 0;
        $servicing = 0;

        if ($sumTotalServiced = mysqli_query($this->api->link, "SELECT SUM( principle_amount ) AS totalassets FROM accounts_archive_loans_serviced WHERE DATE(book_date) = CURDATE()")) {
            $sumTotalServiced_tmp = mysqli_fetch_array($sumTotalServiced, MYSQLI_ASSOC);
            $serviced = $sumTotalServiced_tmp['totalassets'];
        }
        if ($sumTotalPending = mysqli_query($this->api->link, "SELECT SUM( principle_amount ) AS totalassets FROM accounts_loans WHERE DATE(book_date) = CURDATE()")) {
            $sumTotalPending_tmp = mysqli_fetch_array($sumTotalPending, MYSQLI_ASSOC);
            $servicing = $sumTotalPending_tmp['totalassets'];
        }

        return ($serviced + $servicing);
    }

    public function total_assets_disbursed_month()
    {
        $serviced = 0;
        $servicing = 0;

        if ($sumTotalServiced = mysqli_query($this->api->link, "SELECT SUM( principle_amount ) AS totalassets FROM accounts_archive_loans_serviced WHERE DATE(book_date) > CURDATE() - 30")) {
            $sumTotalServiced_tmp = mysqli_fetch_array($sumTotalServiced, MYSQLI_ASSOC);
            $serviced = $sumTotalServiced_tmp['totalassets'];
        }
        if ($sumTotalPending = mysqli_query($this->api->link, "SELECT SUM( principle_amount ) AS totalassets FROM accounts_loans WHERE DATE(book_date) > CURDATE() - 30")) {
            $sumTotalPending_tmp = mysqli_fetch_array($sumTotalPending, MYSQLI_ASSOC);
            $servicing = $sumTotalPending_tmp['totalassets'];
        }

        return ($serviced + $servicing);
    }

    public function total_assets_disbursed($start = null, $end = null)
    {
        $serviced = 0;
        $servicing = 0;
        if (!isset($start)) {
            $start = "2018-06-12";
        }
        if (!isset($end)) {
            $end = date("Y-m-d", time());;
        }

        $queryServicing = "SELECT SUM( principle_amount ) AS totalassets FROM accounts_archive_loans_serviced WHERE DATE(book_date) BETWEEN DATE('$start') AND DATE('$end')";
        $queryServiced = "SELECT SUM( principle_amount ) AS totalassets FROM accounts_loans WHERE DATE(book_date) BETWEEN DATE('$start') AND DATE('$end')";
        if ($sumTotalServiced = mysqli_query($this->api->link, $queryServicing)) {
            $sumTotalServiced_tmp = mysqli_fetch_array($sumTotalServiced, MYSQLI_ASSOC);
            $serviced = $sumTotalServiced_tmp['totalassets'];
        }
        if ($sumTotalPending = mysqli_query($this->api->link, $queryServiced)) {
            $sumTotalPending_tmp = mysqli_fetch_array($sumTotalPending, MYSQLI_ASSOC);
            $servicing = $sumTotalPending_tmp['totalassets'];
        }

        return ($serviced + $servicing);
    }

    public function total_repayments_today()
    {
        $repay_today = 0;

        if ($sumTotalServiced = mysqli_query($this->api->link, "SELECT SUM(amount) AS balance FROM transaction_entries WHERE (tx_type = 3 OR tx_type = 13) AND account_locale = 4 AND DATE(time_created) = curdate()")) {
            $sumTotalServiced_tmp = mysqli_fetch_array($sumTotalServiced, MYSQLI_ASSOC);
            $repay_today = $sumTotalServiced_tmp['balance'];
        }

        return $repay_today;
    }

    public function total_repayments_yesterday()
    {
        $repay_yesterday = 0;

        if ($sumTotalServiced = mysqli_query($this->api->link, "SELECT SUM(amount) AS balance FROM transaction_entries WHERE (tx_type = 3 OR tx_type = 13) AND account_locale = 4 AND DATE(time_created) = curdate() - 1")) {
            $sumTotalServiced_tmp = mysqli_fetch_array($sumTotalServiced, MYSQLI_ASSOC);
            $repay_yesterday = $sumTotalServiced_tmp['balance'];
        }

        return $repay_yesterday;
    }

    public function total_repayments_month()
    {
        $repay_month = 0;

        if ($sumTotalServiced = mysqli_query($this->api->link, "SELECT SUM(amount) AS balance FROM transaction_entries WHERE (tx_type = 3 OR tx_type = 13) AND account_locale = 4 AND DATE(time_created) > curdate() - 30")) {
            $sumTotalServiced_tmp = mysqli_fetch_array($sumTotalServiced, MYSQLI_ASSOC);
            $repay_month = $sumTotalServiced_tmp['balance'];
        }

        if (isset($repay_month))
            return $repay_month;

        return 0;
    }

    public function total_repayments_total()
    {
        $repay_yesterday = 0;

        if ($sumTotalServiced = mysqli_query($this->api->link, "SELECT SUM(amount) AS balance FROM transaction_entries WHERE (tx_type = 3 OR tx_type = 13) AND account_locale = 4")) {
            $sumTotalServiced_tmp = mysqli_fetch_array($sumTotalServiced, MYSQLI_ASSOC);
            $repay_yesterday = $sumTotalServiced_tmp['balance'];
        }

        if (isset($repay_yesterday))
            return $repay_yesterday;

        return 0;
    }

    public function total_profits()
    {
        $profits_total = 0;

        if ($sumTotalServiced = mysqli_query($this->api->link, "SELECT balance FROM balance_general WHERE account_id = 4")) {
            $sumTotalServiced_tmp = mysqli_fetch_array($sumTotalServiced, MYSQLI_ASSOC);
            $profits_total = $sumTotalServiced_tmp['balance'];
        }

        if (isset($profits_total))
            return $profits_total;

        return 0;
    }

    public function facility_admin_numbers()
    {
        $total_assets_disbursed = $this->total_assets_disbursed();
        $number_of_assets_disbursed = $this->asset_count();
        $total_repayments = $this->total_repayments_total();
        $total_profits = $this->total_profits();
        $total_provisions = $this->total_provisions();

        $collection = array();

        $collection['total_assets'] = (int)$total_assets_disbursed;
        $collection['facility_count'] = (int)$number_of_assets_disbursed;
        $collection['total_repayments'] = (int)$total_repayments;
        $collection['total_profits'] = (int)$total_profits;
        $collection['total_provisions'] = (int)$total_provisions;

        echo json_encode($collection);
    }

    private function asset_count()
    {
        $serviced = 0;
        $servicing = 0;
//        if (!isset($start)) {
//            $start = "2018-06-12";
//        }
//        if (!isset($end)) {
//            $end = date("Y-m-d", time());;
//        }

        $queryServicing = 'SELECT COUNT(id) AS count FROM accounts_archive_loans_serviced';
        $queryServiced = 'SELECT COUNT(id) AS count FROM accounts_loans';
        if ($sumTotalServiced = mysqli_query($this->api->link, $queryServicing)) {
            $sumTotalServiced_tmp = mysqli_fetch_array($sumTotalServiced, MYSQLI_ASSOC);
            $serviced = $sumTotalServiced_tmp['count'];
        }
        if ($sumTotalPending = mysqli_query($this->api->link, $queryServiced)) {
            $sumTotalPending_tmp = mysqli_fetch_array($sumTotalPending, MYSQLI_ASSOC);
            $servicing = $sumTotalPending_tmp['count'];
        }

        return ($serviced + $servicing);
    }

    private function total_provisions()
    {
        $defaulted_total = 0;
        $non_performing_total = 0;
        $dormant_total = 0;
        $dead_total = 0;

        if ($sumTotalnon_performing = mysqli_query($this->api->link, 'SELECT SUM(balance) AS provisions FROM view_loans_overdue')) {
            $sumTotalnon_performing_tmp = mysqli_fetch_array($sumTotalnon_performing, MYSQLI_ASSOC);
            $non_performing_total = $sumTotalnon_performing_tmp['provisions'];
        }
        if ($sumTotaldefaulted = mysqli_query($this->api->link, 'SELECT SUM(balance) AS provisions FROM view_loans_defaulted')) {
            $sumTotaldefaulted_tmp = mysqli_fetch_array($sumTotaldefaulted, MYSQLI_ASSOC);
            $defaulted_total = $sumTotaldefaulted_tmp['provisions'];
        }
        if ($sumTotaldormant = mysqli_query($this->api->link, 'SELECT SUM(balance) AS provisions FROM view_loans_non_performing')) {
            $sumTotaldormant_tmp = mysqli_fetch_array($sumTotaldormant, MYSQLI_ASSOC);
            $dormant_total = $sumTotaldormant_tmp['provisions'];
        }
        if ($sumTotalDead = mysqli_query($this->api->link, 'SELECT SUM(balance) AS provisions FROM view_loans_baddebt')) {
            $sumTotalDead_tmp = mysqli_fetch_array($sumTotalDead, MYSQLI_ASSOC);
            $dead_total = $sumTotalDead_tmp['provisions'];
        }

        return ($dead_total + $defaulted_total + $non_performing_total + $dormant_total);
    }

    private function record_facility_application($params)
    {
        if (DB::table('facility_applications')->insert($params)) {
            return 1;
        } else {
            return -1;
        }
    }

    private function update_facility_application($id, $params)
    {
        if (DB::table('facility_applications')->where('id', $id)->update($params)) {
            return 1;
        } else {
            return -1;
        }
    }

    private function get_last_application()
    {
        if ($result = mysqli_query($this->api->link, "SELECT max(id) AS id FROM facility_applications LIMIT 1")) {
            $application_id = mysqli_fetch_array($result, MYSQLI_ASSOC);

            return $application_id['id'];
        } else {
            return -1;
        }
    }
    /*private function record_facility_application(){
        if(mysqli_query($this->api->link, "INSERT INTO loanapplications() VALUES ()")){

        };
    }*/
}