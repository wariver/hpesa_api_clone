<?php
/**
 * Created by Warui.
 * User: root
 * Date: 8/22/18
 * Time: 5:37 PM
 */

namespace App\Http\Controllers;

use App\Jobs\QueueGrails;
use App\WavesUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Bank extends Controller
{
    public $api;
    public $abi;
    public $TX_BANK_B2C;
    public $TX_BANK_SAVINGS;
    public $TX_BANK_BANK;
    public $TX_BANK_DCF;
    public $TX_LIPAKARO_SCHOOL_FEES;
    public $TX_CHARGES;
    public $TX_BANK_AIRTIME;
    public $AC_CHARGES_ID;
    public $AC_B2C;
    public $AC_C2B;
    public $AC_AIRTIME;
    public $TX_C2B_BANK;
    public $TX_TYPE_B2C;
    public $TX_GROUP_BANK;

    /**
     * Bank constructor.
     */
    public function __construct()
    {
        $this->TX_BANK_B2C = 8;
        $this->TX_BANK_SAVINGS = 4;
        $this->TX_BANK_BANK = 1;
        $this->TX_GROUP_BANK = 31;
        $this->TX_CHARGES = 19;
        $this->TX_BANK_DCF = 10;
        $this->TX_BANK_AIRTIME = 14;
        $this->TX_LIPAKARO_SCHOOL_FEES = 18;
        $this->TX_C2B_BANK = 9;
        $this->AC_CHARGES_ID = 8;
        $this->AC_AIRTIME = 9;
        $this->AC_B2C = 3;
        $this->AC_C2B = 2;
        $this->abi = new Abi();
        $this->api = new Api();
    }

    public function withdraw_to_phone(Request $request)
    {
        $user_id = $request->get('userId');
        $amount = $request->get('amount');
        $phone = $request->get('phone');

        $bank_account_id = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_BANK);

        $transaction_charges = 50;
        if (!isset($user_id) || !isset($amount) || !isset($bank_account_id)) {
            echo json_encode(array('status' => 501, 'There were some missing parameters in your request'));
            die;
        }
//		check balance of bank account
        $bank_balance = $this->abi->get_account_balance('balance_bank', $bank_account_id);
        $b2c_balance = $this->abi->get_account_balance('balance_general', $this->AC_B2C);
        $tx_charges_balance = $this->abi->get_account_balance('balance_general', $this->AC_CHARGES_ID);

        if ($bank_balance < ($amount)) {
            echo json_encode(array('status' => 501, 'You have insufficient funds'));
            die;
        } elseif ($bank_balance <= ($amount + $transaction_charges)) {
            echo json_encode(array(
                'status' => 501,
                'The have insufficient funds the transaction charge is KES' . number_format($transaction_charges, 2)
            ));
            die;
        }

        $tx_id = $this->abi->create_transaction($amount, $this->TX_BANK_B2C, $user_id);

//		dr bank account
        $this->abi->performTransactionEntry($tx_id, $bank_account_id, $this->abi->AC_TYPE_BANK, $transaction_charges, 2, $this->TX_CHARGES, $bank_balance, ($bank_balance - $transaction_charges), $this->abi->now);
//		cr charges account
        $this->abi->performTransactionEntry($tx_id, $this->AC_CHARGES_ID, $this->abi->AC_TYPE_GENERAL, $transaction_charges, 1, $this->TX_CHARGES, $tx_charges_balance, ($tx_charges_balance + $transaction_charges), $this->abi->now);

        $bank_balance = $this->abi->get_account_balance('balance_bank', $bank_account_id);
        //		dr bank account
        $this->abi->performTransactionEntry($tx_id, $bank_account_id, $this->abi->AC_TYPE_BANK, $amount, 2, $this->TX_BANK_B2C, $bank_balance, ($bank_balance - $amount), $this->abi->now);
//		cr B2C account
        $this->abi->performTransactionEntry($tx_id, $this->AC_B2C, $this->abi->AC_TYPE_GENERAL, $amount, 1, $this->TX_BANK_B2C, $b2c_balance, ($this->AC_B2C + $amount), $this->abi->now);
//		perform the B2C transaction

        if (!$this->abi->commit_transaction($tx_id)) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'could not complete transaction at the moment'
            ));
            die;
        } else {
//		pending B2C transaction
//			if B2C success
            echo json_encode(array('status' => 200, 'msg' => 'The money has been sent to your phone'));
            die;
//			else not success full
//			mysqli_query(link, "ROLLBACK");
        }
    }

    public function bank_savings(Request $request)
    {
        $user_id_two = $request->get('userId');
        $user_id = $request->get('id');
        $amount = $request->get('amount');

        if (isset($user_id_two)) {
            $user_id = $user_id_two;
        }
//		$savings_ac_id = $request->get( 'savings_ac_id' );
//		$bank_ac_id    = $request->get( 'bank_ac_id' );
        $savings_ac_id = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_SAVINGS);
        $bank_ac_id = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_BANK);


        if (!isset($user_id) || !isset($amount) || !isset($savings_ac_id) || !isset($bank_ac_id)) {
            echo json_encode(array('status' => 501, 'There were some missing parameters in your request'));
            die;
        }
        $bank_ac_balance = $this->abi->get_account_balance('balance_bank', $bank_ac_id);
        $savings_ac_balance = $this->abi->get_account_balance('balance_savings', $savings_ac_id);

        if ($amount > $bank_ac_balance) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'You do not have enough funds to transfer this amount to your savings'
            ));
            die;
        }
        $tx_id = $this->abi->create_transaction($amount, $this->TX_BANK_SAVINGS, $user_id);
//		dr bank
        $this->abi->performTransactionEntry($tx_id, $bank_ac_id, $this->abi->AC_TYPE_BANK, $amount, 2, $this->TX_BANK_SAVINGS, $bank_ac_balance, ($bank_ac_balance - $amount), $this->abi->now);
//		cr savings
        $this->abi->performTransactionEntry($tx_id, $savings_ac_id, $this->abi->AC_TYPE_SAVINGS, $amount, 1, $this->TX_BANK_SAVINGS, $savings_ac_balance, ($savings_ac_balance + $amount), $this->abi->now);

        if (!$this->abi->commit_transaction($tx_id)) {
            echo json_encode(array('status' => 501, 'error' => 'could not complete transaction at the moment'));
            die;
        } else {
            echo json_encode(array(
                'status' => 200,
                'msg' => 'Amount KES. ' . $amount . ' transferred to savings successfully'
            ));
            die;
        }
    }

    public function bank_bank(Request $request)
    {
        $user_id = $request->get('id');
        $amount = $request->get('amount');
        $member_id_number = $request->get('member_id_number');
        $payment = $request->get('payment_id');

        $user = WavesUser::where('id', $user_id)->first();

        $toId = -1;
        if (!isset($user_id) || !isset($amount) || !isset($member_id_number) || !isset($payment)) {
            echo json_encode(array('status' => 501, 'There were some missing parameters in your request'));
            die;
        }
        if($payment == 1){
            $bank_ac_id = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_BANK);
            $bank_ac_balance = $this->abi->get_account_balance('balance_bank', $bank_ac_id);

        }

        if($payment == 10){
            $bank_ac_id = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_MEMBERSHIPS);
            $bank_ac_balance = $this->abi->get_account_balance('balance_memberships', $bank_ac_id);
        }

        $transaction_fees = $this->calculate_charges($amount);
        if ($amount > ($bank_ac_balance + $transaction_fees)) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'You do not have enough funds to transfer this amount'
            ));
            die;
        }
        if (!$check = DB::table('users')->where('idpass', $member_id_number)->first()) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'cannot not fetch user at the moment'
            ));
            die;
        }

        $recepientName = $check->fullname;
        $recepientPhone = $check->phone;
        if (empty($check)) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'The national id/passport does not exist'
            ));
            die;
        }

        $toId = $check->id;

        if ($toId == $user_id) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'You cannot transfer to your own account.'
            ));
            die;
        }
        if ($toId == -1) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'Could not get user with the provided ID number in our system.'
            ));
            die;
        }
        $grp = new Groups();
        if(!$grp->isGroupMember($user_id, $toId )){
            echo json_encode(array(
                'status' => 501,
                'error' => 'Is this ID '.$user_id.' ' . $toId .' a group member?' . ' : '.$grp->isGroupMember($user_id, $toId )
            ));
            die;
        }



        $member_bank_ac_id = $this->abi->get_user_account($toId, $this->abi->AC_TYPE_BANK);
        $member_bank_ac_balance = $this->abi->get_account_balance('balance_bank', $member_bank_ac_id);

        $charges_ac_balance = $this->abi->get_account_balance('balance_general', $this->AC_CHARGES_ID);

//		CREATE TRANSACTION
        if($payment == 1){
            $tx_id = $this->abi->create_transaction($amount, $this->TX_BANK_BANK, $user_id);
            //		dr bank
            $this->abi->performTransactionEntry($tx_id, $bank_ac_id, $this->abi->AC_TYPE_BANK, $transaction_fees, 2, $this->TX_CHARGES, $bank_ac_balance, ($bank_ac_balance - $transaction_fees), $this->abi->now);
//		cr charges
            $this->abi->performTransactionEntry($tx_id, $this->AC_CHARGES_ID, $this->abi->AC_TYPE_GENERAL, $transaction_fees, 1, $this->TX_CHARGES, $charges_ac_balance, ($charges_ac_balance + $transaction_fees), $this->abi->now);

            $bank_ac_balance = $this->abi->get_account_balance('balance_bank', $bank_ac_id);

//		dr bank
            $this->abi->performTransactionEntry($tx_id, $bank_ac_id, $this->abi->AC_TYPE_BANK, $amount, 2, $this->TX_BANK_BANK, $bank_ac_balance, ($bank_ac_balance - $amount), $this->abi->now);
//		cr member bank
            $this->abi->performTransactionEntry($tx_id, $member_bank_ac_id, $this->abi->AC_TYPE_BANK, $amount, 1, $this->TX_BANK_BANK, $member_bank_ac_balance, ($member_bank_ac_balance + $amount), $this->abi->now);

            $RecepientSmsMessage = 'Confirmed, you have received KES. ' . number_format($amount, 2) . ' from Waves Member Check your Bank Account balance.';

        }

        if($payment == 10){
            $tx_id = $this->abi->create_transaction($amount, $this->TX_GROUP_BANK, $user_id);
//		dr bank
            $this->abi->performTransactionEntry($tx_id, $bank_ac_id, $this->abi->AC_TYPE_MEMBERSHIPS, $transaction_fees, 2, $this->TX_CHARGES, $bank_ac_balance, ($bank_ac_balance - $transaction_fees), $this->abi->now);
//		cr charges
            $this->abi->performTransactionEntry($tx_id, $this->AC_CHARGES_ID, $this->abi->AC_TYPE_GENERAL, $transaction_fees, 1, $this->TX_CHARGES, $charges_ac_balance, ($charges_ac_balance + $transaction_fees), $this->abi->now);

            $bank_ac_balance = $this->abi->get_account_balance('balance_memberships', $bank_ac_id);

//		dr bank
            $this->abi->performTransactionEntry($tx_id, $bank_ac_id, $this->abi->AC_TYPE_MEMBERSHIPS, $amount, 2, $this->TX_GROUP_BANK, $bank_ac_balance, ($bank_ac_balance - $amount), $this->abi->now);
//		cr member bank
            $this->abi->performTransactionEntry($tx_id, $member_bank_ac_id, $this->abi->AC_TYPE_BANK, $amount, 1, $this->TX_GROUP_BANK, $member_bank_ac_balance, ($member_bank_ac_balance + $amount), $this->abi->now);

            $RecepientSmsMessage = 'Confirmed, you have received KES. ' . number_format($amount, 2) . ' from '. $user->fullname.' Check your Bank Account balance.';

        }


        if (!$this->abi->commit_transaction($tx_id)) {
            echo json_encode(array('status' => 501, 'error' => 'could not complete transaction at the moment'));
            die;
        }

        $sms_bundle = array('phone' => $recepientPhone, 'message' => $RecepientSmsMessage);
        QueueGrails::dispatch(1, $sms_bundle);

        /*$SenderSmsMessage = "Confirmed , you have sent KES. " . number_format( $amount, 2 ) . " to " . $recepientName . " Check your Bank Account balance. Your finance is due on $loanDueDate.";
    $senderPhone  = $recepientPhone;
    $sms_bundle_sender = array( 'phone' => $userPhone, 'message' => $smsMessage );
    QueueGrails::dispatch( 1, $sms_bundle_sender );*/
        echo json_encode(array(
            'status' => 200,
            'msg' => 'Amount KES. ' . $amount . ' transferred to ' . $recepientName . ' successfully'
        ));
        die;

    }

    public function bank_airtime(Request $request)
    {
        $phone = $request->get('phone');
        $amount = $request->get('amount');
        $user_id = $request->get('user_id');
        $operator = $request->get('operator');

        $bank_ac_id = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_BANK);
        $bank_balance = $this->abi->get_account_balance('balance_bank', $bank_ac_id);

        $airtime_balance = $this->abi->get_account_balance('balance_general', $this->AC_AIRTIME);
        if (!isset($user_id) || !isset($phone) || !isset($amount)) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'Could not complete request some parameters are missing'
            ));
            die;
        }
        if ($amount <= 10) {
            echo json_encode(array('status' => 501, 'error' => 'Minimum for airtime is KES. 10'));
            die;
        }
        $formatPhoneVals = json_decode($this->api->phoneFormat($phone), true);
        if (!$formatPhoneVals['status'] == true) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'The phone number appears to be invalid'
            ));
            die;
        }
        $phone = $formatPhoneVals['formatedPhone'];
        if (!is_numeric($amount)) {
            echo json_encode(array('status' => 501, 'error' => 'Please use only numbers'));
            die;
        }
        if ($amount > $bank_balance) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'You do not have sufficient funds to buy airtime worth this amount. Deposit enough cash into your account and proceed to buy airtime.'
            ));
            die;
        }

//		CREATE TRANSACTION
        $tx_id = $this->abi->create_transaction($amount, $this->TX_BANK_AIRTIME, $user_id);
//		dr bank account
        $this->abi->performTransactionEntry($tx_id, $bank_ac_id, $this->abi->AC_TYPE_BANK, $amount, 2, $this->TX_BANK_AIRTIME, $bank_balance, ($bank_balance - $amount), $this->abi->now);
//		cr airtime account
        $this->abi->performTransactionEntry($tx_id, $this->AC_AIRTIME, $this->abi->AC_TYPE_GENERAL, $amount, 1, $this->TX_BANK_AIRTIME, $airtime_balance, ($airtime_balance + $amount), $this->abi->now);

        if (!$this->abi->commit_transaction($tx_id)) {
            array(
                'status' => 501,
                'msg' => 'Could not complete transaction at the moment.'
            );
        } else {
            array(
                'status' => 200,
                'msg' => 'Airtime request has been successfully sent. Please wait for response. Please note that you cannot buy airtime to the same number within 5 minutes.'
            );
        }
    }

    public function bank_dcf(Request $request)
    {
        $user_id = $request->get('user_id');
        $amount = $request->get('amount');
        $dcf_ac_id = $request->get('account_id');
        $dcf_type = $request->get('dcfs_type');
        $lipa_karo = (int)$request->get('lipakaro');

        $bank_ac_id = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_BANK);
        $dcf_ac_balance = $this->abi->get_account_balance('balance_dcf', $dcf_ac_id);

        $toId = -1;
        if (!isset($user_id) || !isset($amount) || !isset($dcf_ac_id)) {
            echo json_encode(array('status' => 501, 'There were some missing parameters in your request'));
            die;
        }

//		CREATE TRANSACTION
        if ($lipa_karo == 1) {
            $dib_lipakaro_ac = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_SCHOOL_FEES);
            $dib_lipakaro_balance = $this->abi->get_account_balance('balance_school_fees', $dib_lipakaro_ac);
            if ($amount > ($dib_lipakaro_balance)) {
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'You do not have enough funds in dib lipakaro account to transfer this amount. DIB LIPAKARO is loaded by applying for a facility of type: school fees'
                ));
                die;
            }
            $tx_id = $this->abi->create_transaction($amount, $this->TX_BANK_DCF, $user_id);
            //		dr lipa karo account
            $this->abi->performTransactionEntry($tx_id, $dib_lipakaro_ac, $this->abi->AC_TYPE_SCHOOL_FEES, $amount, 2, $this->TX_LIPAKARO_SCHOOL_FEES, $dib_lipakaro_balance, ($dib_lipakaro_balance - $amount), $this->abi->now);
//		cr dcf account
            $this->abi->performTransactionEntry($tx_id, $dcf_ac_id, $this->abi->AC_TYPE_DCF, $amount, 1, $this->TX_LIPAKARO_SCHOOL_FEES, $dcf_ac_balance, ($dcf_ac_balance + $amount), $this->abi->now);

        } else {
            $bank_ac_balance = $this->abi->get_account_balance('balance_bank', $bank_ac_id);
            if ($amount > ($bank_ac_balance)) {
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'You do not have enough funds to transfer this amount'
                ));
                die;
            }
            $tx_id = $this->abi->create_transaction($amount, $this->TX_BANK_DCF, $user_id);
            //		dr bank account
            $this->abi->performTransactionEntry($tx_id, $bank_ac_id, $this->abi->AC_TYPE_BANK, $amount, 2, $this->TX_BANK_DCF, $bank_ac_balance, ($bank_ac_balance - $amount), $this->abi->now);
//		cr dcf account
            $this->abi->performTransactionEntry($tx_id, $dcf_ac_id, $this->abi->AC_TYPE_DCF, $amount, 1, $this->TX_BANK_DCF, $dcf_ac_balance, ($dcf_ac_balance + $amount), $this->abi->now);

        }


        if (!$this->abi->commit_transaction($tx_id)) {
            echo json_encode(array('status' => 501, 'error' => 'could not complete transaction at the moment'));
            die;
        } else {
            echo json_encode(array(
                'status' => 200,
                'msg' => 'Amount KES. ' . $amount . ' transferred successfully'
            ));
            die;
        }

    }

    public function C2B_bank(Request $request)
    {
        $phoneno = (int)$request->get('phoneno');
        $amount = (int)$request->get('amount');
        $identifier = $request->get('identifier');
        $user_id = $request->get('userId');
        $paymenttype = (int)$request->get('paymenttype');
        $paymenttype = 1;
        $identifier = 1;
        $phoneno = 706129749;

        if ($amount > 70000) {
            echo json_encode(array('status' => 501, 'error' => 'The amount input exceeds transaction limit'));
            die;
        }

        $bank_account_id = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_BANK);
        $bank_balance = $this->abi->get_account_balance('balance_bank', $bank_account_id);
        $c2b_balance = $this->abi->get_account_balance('balance_general', $this->AC_C2B);

//		CREATE TRANSACTION
        $tx_id = $this->abi->create_transaction($amount, $this->TX_C2B_BANK, $user_id);
//		dr c2b account
        $this->abi->performTransactionEntry($tx_id, $this->AC_C2B, $this->abi->AC_TYPE_GENERAL, $amount, 1, $this->TX_C2B_BANK, $c2b_balance, ($c2b_balance - $amount), $this->abi->now);
//		cr bank account
        $this->abi->performTransactionEntry($tx_id, $bank_account_id, $this->abi->AC_TYPE_BANK, $amount, 1, $this->TX_C2B_BANK, $bank_balance, ($bank_balance + $amount), $this->abi->now);

//		run stk push notification
        $paymentRequestResult = $this->api->stkPayment($amount, $phoneno, $paymenttype, $user_id, $identifier, 'C2B to Savings');
        $resultVals = json_decode($paymentRequestResult, true);
        if ($resultVals['status'] == 1) {
            if (!$this->abi->commit_transaction($tx_id)) {
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'Could not complete transaction at the moment.'
                ));
            } else {
                echo json_encode(array(
                    'status' => 200,
                    'msg' => 'A request has been sent to your phone.'
                ));
            }
        } else {
            echo json_encode(array(
                'status' => 501,
                'msg' => 'Request failed. Please try again after a few minutes.'
            ));
        }
    }

    private function calculate_charges($amount)
    {
        $transaction_fee = 0;
        if ($amount > 0 && $amount <= 100) {
            $transaction_fee = 0;
        } elseif ($amount >= 101 && $amount <= 500) {
            $transaction_fee = 50;
        } elseif ($amount >= 501 && $amount <= 2500) {
            $transaction_fee = 50;
        } elseif ($amount >= 2501 && $amount <= 5000) {
            $transaction_fee = 50;
        } elseif ($amount >= 5001 && $amount <= 10000) {
            $transaction_fee = 50;
        } elseif ($amount >= 20001 && $amount <= 20000) {
            $transaction_fee = 100;
        } elseif ($amount >= 30001 && $amount <= 30000) {
            $transaction_fee = 150;
        } elseif ($amount >= 40001 && $amount <= 40000) {
            $transaction_fee = 180;
        } elseif ($amount >= 50001 && $amount <= 50000) {
            $transaction_fee = 200;
        } elseif ($amount >= 60001 && $amount <= 60000) {
            $transaction_fee = 250;
        } elseif ($amount >= 70001 && $amount <= 70000) {
            $transaction_fee = 300;
        } elseif ($amount >= 80001 && $amount <= 80000) {
            $transaction_fee = 350;
        } elseif ($amount >= 90001 && $amount <= 90000) {
            $transaction_fee = 400;
        } elseif ($amount >= 100001) {
            $transaction_fee = 500;
        }

        return $transaction_fee;
    }
}