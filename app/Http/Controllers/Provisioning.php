<?php
/**
 * Created by Warui.
 * User: root
 * Date: 8/22/18
 * Time: 5:37 PM
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

/**
 * @property int TX_TYPE_BANK_REPAYMENT
 */
class Provisioning extends Controller {
	public $link;
	public $now;
	public $api;
	public $abi;
	public $AC_TYPE_BANK;
	public $AC_TYPE_SAVINGS;
	public $AC_TYPE_HOLDING;
	public $AC_TYPE_FACILITY;
	public $AC_TYPE_DCF;
	public $AC_TYPE_SCHOOL_FEES;
	public $AC_TYPE_GENERAL;
	public $AC_TYPE_PROVISION;
	public $AC_TYPE_PENALTIES;

	public $AC_ID_PROVISIONS;
	public $AC_ID_PENALTIES;
	public $AC_ID_PROFITS;
	public $TX_MAKE_PROVISION;
	public $TX_REVERSE_PROVISION;
	public $TX_MAKE_PENALTY;

	/**
	 * PROVISIONING Constructor.
	 * NARRATIVE:
	 * Each day that the balance of a loan is defaulted we shall be provisioning
	 */
	public function __construct() {
//		$this->api->link = mysqli_connect( '127.0.0.1', 'root', '!@#$1234ASDFasdf', 'dibwaves' );
		date_default_timezone_set( 'Africa/Nairobi' );
		$this->now                 = date( 'Y-m-d H:i:s' );
		$this->AC_TYPE_BANK        = 1;
		$this->AC_TYPE_SAVINGS     = 2;
		$this->AC_TYPE_HOLDING     = 3;
		$this->AC_TYPE_FACILITY    = 4;
		$this->AC_TYPE_DCF         = 5;
		$this->AC_TYPE_SCHOOL_FEES = 6;
		$this->AC_TYPE_GENERAL     = 7;
		$this->AC_TYPE_PROVISION   = 8;
		$this->AC_TYPE_PENALTIES   = 9;

		$this->AC_ID_PROVISIONS = 10;
		$this->AC_ID_PENALTIES  = 11;
		$this->AC_ID_PROFITS    = 4;

		$this->TX_TYPE_C2B_REPAYMENT  = 13;
		$this->TX_TYPE_BANK_REPAYMENT = 3;
		$this->TX_MAKE_PROVISION      = 23;
		$this->TX_REVERSE_PROVISION   = 24;
		$this->TX_MAKE_PENALTY        = 26;

		$this->abi = new Abi();

	}

	public function get_loan_age( $ac_id ) {
//		first check if there is a balance on the loan serviced
		$facility_account_balance = $this->abi->get_account_balance( 'balance_loans', $ac_id );
		if ( $facility_account_balance == 0 ) {
			return array( 'status' => 'serviced' );
		}
		$servicing      = mysqli_query( $this->abi->api->link, "SELECT * FROM view_loans_servicing WHERE account_id = $ac_id" );
		$overdue        = mysqli_query( $this->abi->api->link, "SELECT * FROM view_loans_overdue WHERE account_id = $ac_id" );
		$defaulted      = mysqli_query( $this->abi->api->link, "SELECT * FROM view_loans_defaulted WHERE account_id = $ac_id" );
		$non_performing = mysqli_query( $this->abi->api->link, "SELECT * FROM view_loans_non_performing WHERE account_id = $ac_id" );
		$bad_debt       = mysqli_query( $this->abi->api->link, "SELECT * FROM view_loans_baddebt WHERE account_id = $ac_id" );

		if ( mysqli_num_rows( $servicing ) > 0 ) {
			return array( 'status' => 'servicing', 'balance' => $facility_account_balance );
		} elseif ( mysqli_num_rows( $overdue ) > 0 ) {
			return array( 'status' => 'overdue', 'balance' => $facility_account_balance );
		} elseif ( mysqli_num_rows( $defaulted ) > 0 ) {
			return array( 'status' => 'dafaulted', 'balance' => $facility_account_balance );
		} elseif ( mysqli_num_rows( $non_performing ) > 0 ) {
			return array( 'status' => 'non-performing', 'balance' => $facility_account_balance );
		} elseif ( mysqli_num_rows( $bad_debt ) > 0 ) {
			return array( 'status' => 'bad_debt', 'balance' => $facility_account_balance );
		} else {
			return array( 'status' => 'stale', 'balance' => $facility_account_balance );
		}

	}

	public function reverse_provision( $account_id, $amount, $user_id ) {
//		get provision paid for balance
		$total_provisions_balance   = $this->abi->get_account_balance( 'balance_general', $this->AC_ID_PROVISIONS );
		$total_profits_balance      = $this->abi->get_account_balance( 'balance_general', $this->AC_ID_PROFITS );
		$facility_provision_balance = $this->abi->get_account_balance( 'balance_facility_provisions', $account_id );
		$tx_id                      = $this->abi->create_transaction( $amount, $this->TX_REVERSE_PROVISION, $user_id );

//		to reverse a provision we need to pass the excess balance of provision for loan balance
//      debit total provisions
		$this->abi->performTransactionEntry( $tx_id, $this->AC_ID_PROVISIONS, $this->AC_TYPE_GENERAL, $amount, 2, $this->TX_REVERSE_PROVISION, $total_provisions_balance, ( $total_provisions_balance - $amount ), $this->now );
//      debit facility provisions
		$this->abi->performTransactionEntry( $tx_id, $account_id, $this->AC_TYPE_PROVISION, $amount, 2, $this->TX_REVERSE_PROVISION, $facility_provision_balance, ( $facility_provision_balance - $amount ), $this->now );
//		credit profits account
		$this->abi->performTransactionEntry( $tx_id, $this->AC_ID_PROFITS, $this->AC_TYPE_GENERAL, $amount, 1, $this->TX_REVERSE_PROVISION, $total_profits_balance, ( $total_profits_balance + $amount ), $this->now );

		if ( $this->abi->commit_transaction( $tx_id ) ) {
			$this->abi->mark_transaction_state( $tx_id, 'COMPLETE' );
		} else {
			$this->abi->mark_transaction_state( $tx_id, 'FAILED' );
		}


	}

	public function make_provision( $account_id, $percentage, $user_id, $facility_balance, $facility_provision_balance ) {
//		check if provision account exists

//		calculate the provisioning amount = percentage of total outstanding
		$amount                   = ( $facility_balance * $percentage / 100 );
		$total_provisions_balance = $this->abi->get_account_balance( 'balance_general', $this->AC_ID_PROVISIONS );
		$total_profits_balance    = $this->abi->get_account_balance( 'balance_general', $this->AC_ID_PROFITS );
		$tx_id                    = $this->abi->create_transaction( $amount, $this->TX_MAKE_PROVISION, $user_id );

		//		debit profits account
		$this->abi->performTransactionEntry( $tx_id, $this->AC_ID_PROFITS, $this->AC_TYPE_GENERAL, $amount, 2, $this->TX_MAKE_PROVISION, $total_profits_balance, ( $total_profits_balance - $amount ), $this->now );
//      credit total provisions
		$this->abi->performTransactionEntry( $tx_id, $this->AC_ID_PROVISIONS, $this->AC_TYPE_GENERAL, $amount, 1, $this->TX_MAKE_PROVISION, $total_provisions_balance, ( $total_provisions_balance + $amount ), $this->now );
//      credit facility provisions
		$this->abi->performTransactionEntry( $tx_id, $account_id, $this->AC_TYPE_PROVISION, $amount, 1, $this->TX_MAKE_PROVISION, $facility_provision_balance, ( $facility_provision_balance + $amount ), $this->now );

		if ( $this->abi->commit_transaction( $tx_id ) ) {
			$this->abi->mark_transaction_state( $tx_id, 'COMPLETE' );
		} else {
			$this->abi->mark_transaction_state( $tx_id, 'FAILED' );
		}


	}

	public function kula_sup( $account_id, $percentage, $user_id, $principle ) {
//		check if provision account exists
//		calculate the provisioning amount = percentage of total outstanding
		$amount                     = ( $principle * $percentage / 100 );
		$total_penalties_balance    = $this->abi->get_account_balance( 'balance_general', $this->AC_ID_PENALTIES );
		$facility_liability_balance = $this->abi->get_account_balance( 'balance_loans', $account_id );
		$facility_penalty_balance   = $this->abi->get_account_balance( 'balance_penalties', $account_id );
		$tx_id                      = $this->abi->create_transaction( $amount, $this->TX_MAKE_PENALTY, $user_id );


//		credit facility liability account
		$this->abi->performTransactionEntry( $tx_id, $account_id, $this->AC_TYPE_FACILITY, $amount, 1, $this->TX_MAKE_PENALTY, $facility_liability_balance, ( $facility_liability_balance + $amount ), $this->now );

//		credit penalties account
		$this->abi->performTransactionEntry( $tx_id, $account_id, $this->AC_TYPE_PENALTIES, $amount, 1, $this->TX_MAKE_PENALTY, $facility_penalty_balance, ( $facility_penalty_balance + $amount ), $this->now );

//      credit total provisions
		$this->abi->performTransactionEntry( $tx_id, $this->AC_ID_PENALTIES, $this->AC_TYPE_GENERAL, $amount, 1, $this->TX_MAKE_PENALTY, $total_penalties_balance, ( $total_penalties_balance + $amount ), $this->now );

		if ( $this->abi->commit_transaction( $tx_id ) ) {
			$this->abi->mark_transaction_state( $tx_id, 'COMPLETE' );
		} else {
			$this->abi->mark_transaction_state( $tx_id, 'FAILED' );
		}
	}

	public function create_facility_provision( $account_id ) {
		$exist = mysqli_query( $this->abi->api->link, "SELECT account_id FROM balance_facility_provisions WHERE account_id = $account_id" );
		if ( mysqli_num_rows( $exist ) > 0 ) {
			return 0;
		} else {
			if ( mysqli_query( $this->abi->api->link, "INSERT INTO balance_facility_provisions(account_id, balance) VALUES ($account_id, 0)" ) ) {
				mysqli_close( $this->abi->api->link );

				return 1;
			} else {
				mysqli_close( $this->abi->api->link );
				echo json_encode( array( 'status' => 500, 'error' => 'could not create provision for facility' ) );
				die;
			}
		}

	}

	public function create_facility_penalty( $account_id ) {
		$exist = mysqli_query( $this->abi->api->link, "SELECT account_id FROM balance_penalties WHERE account_id = $account_id" );

		if ( mysqli_num_rows( $exist ) > 0 ) {

			return 0;
		} else {
			if ( mysqli_query( $this->abi->api->link, "INSERT INTO balance_penalties(account_id, balance) VALUES ($account_id, 0)" ) ) {

				return 1;
			} else {
				echo json_encode( array(
					'status' => 500,
					'error'  => mysqli_error( $this->abi->api->link ) . 'could not create penalty for facility'
				) );
				mysqli_close( $this->abi->api->link );
				die;
			}
		}
	}

	/*
	 * Penalties at 2% for all facility accounts that have a balance > 0
	 * Sum up penalty balances with initial facility account balances...search for all that apply
	 * make penalty included in amount payable at repayment time
	 *
	 */
	public function do_penalties() {
//		check if the penalty balances exist
		$penalty_accounts = $this->get_facilities_overdue();
		$arr_penalties    = json_decode( $penalty_accounts, true );

		foreach ( $arr_penalties as $arr_penalty ) {
			$account_id = $arr_penalty['account_id'];
			$this->create_facility_penalty( $account_id );

			$user_id                      = $arr_penalty['user_id'];
			$principle                    = $arr_penalty['principle_amount'];
			$facility_outstanding_balance = $this->abi->get_account_balance( 'balance_loans', $account_id );
			$facility_pena_balance        = $this->abi->get_account_balance( 'balance_penalties', $account_id );

			$this->kula_sup( $account_id, 2, $user_id, $principle );
		}
	}

	public function should_provision_or_reverse( $account_id, $user_id ) {
		$this->create_facility_provision( $account_id );
//		check balance and total provisioning and decide
		$total_penas                  = $this->abi->get_account_balance( 'balance_penalties', $account_id );
		$facility_outstanding_balance = $this->abi->get_account_balance( 'balance_loans', $account_id );
		$facility_provision           = $this->abi->get_account_balance( 'balance_facility_provisions', $account_id );
		$facility_outstanding_balance -= $total_penas;
		//		if provision is greater than the outstanding balance reverse the outstanding part
		if ( $facility_provision > $facility_outstanding_balance ) {
			$amt_reverse = $facility_provision - $facility_outstanding_balance;
			$this->reverse_provision( $account_id, $amt_reverse, $user_id );
		} elseif ( $facility_provision == $facility_outstanding_balance ) {
//			do nothing;
		} else {
//			continue to provision at the percentage given
			$this->make_provision( $account_id, 5, $user_id, $facility_outstanding_balance, $facility_provision );
		}
	}

	public function provision_zote() {
		$provisions = new Provisioning();

		if ( $result = mysqli_query( $this->abi->api->link, "SELECT account_id FROM view_all_overdue WHERE balance > 0" ) ) {
			if ( mysqli_num_rows( $result ) > 0 ) {
				$result_tmp = mysqli_fetch_all( $result, MYSQLI_ASSOC );
				foreach ( $result_tmp as $account_id ) {
					$ac_id   = $account_id['account_id'];
					$id      = DB::table( 'accounts_loans' )->select( [ 'user_id' ] )->where( 'account_id', $ac_id )->first();
					$user_id = $id->user_id;
					$provisions->should_provision_or_reverse( $ac_id, $user_id );
				}
			}
		}
		mysqli_close( $this->abi->api->link );
	}

	public function get_facilities_overdue() {
		$facility_overdue = DB::table( 'view_all_overdue' )->get();

		return json_encode( $facility_overdue );
//		return json_decode( $facility_overdue, true );
	}

	public function get_facility_provisioning( $id ) {

		$facility_provisions = DB::table( 'transaction_entries' )->select( [
			'account_id',
			'account_locale',
			'debit_credit_flag',
			'tx_type',
			'balance_before',
			'balance_after'
		] )->where( 'account_id', $id )->where( function ( $query ) {
			$query->where( 'tx_type', 23 )
			      ->orWhere( 'tx_type', 24 )
			      ->orWhere( 'tx_type', 25 );
		} )->where( function ( $query ) {
			$query->where( 'account_locale', 4 )
			      ->orWhere( 'account_locale', 8 );
		} )->get();

		return json_encode( $facility_provisions );
//		return json_decode( $facility_provisions, true );

	}


}