<?php
/**
 * Created by Warui.
 * User: root
 * Date: 8/22/18
 * Time: 5:37 PM
 */

namespace App\Http\Controllers;

use App\Jobs\QueueGrails;
use Converter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use stdClass;

class Abi extends Controller
{
    public $link;
    public $now;
    public $api;
    public $AC_TYPE_BANK;
    public $AC_TYPE_SAVINGS;
    public $AC_TYPE_HOLDING;
    public $AC_TYPE_FACILITY;
    public $AC_TYPE_DCF;
    public $AC_TYPE_SCHOOL_FEES;
    public $AC_TYPE_GENERAL;
    public $AC_TYPE_MEMBERSHIPS;

    public $TX_TYPE_C2B_REPAYMENT;
    public $TX_TYPE_BANK_REPAYMENT;

    /**
     * Abi constructor.
     */
    public function __construct()
    {
//		$this->api->link = mysqli_connect( '127.0.0.1', 'root', '!@#$1234ASDFasdf', 'dibwaves' );
        date_default_timezone_set('Africa/Nairobi');
        $this->now = date('Y-m-d H:i:s');
        $this->AC_TYPE_BANK = 1;
        $this->AC_TYPE_SAVINGS = 2;
        $this->AC_TYPE_HOLDING = 3;
        $this->AC_TYPE_FACILITY = 4;
        $this->AC_TYPE_DCF = 5;
        $this->AC_TYPE_SCHOOL_FEES = 6;
        $this->AC_TYPE_GENERAL = 7;
        $this->AC_TYPE_MEMBERSHIPS = 10;

        $this->TX_TYPE_C2B_REPAYMENT = 13;
        $this->TX_TYPE_BANK_REPAYMENT = 3;
        $this->api = new Api();

    }

    public function get_installments($loan_period, $amount)
    {
        $count = ($loan_period * 4);

        return json_encode(array('count' => $count, 'each_installment' => number_format($amount / $count, 2)));
    }

//	check whether customer is first time or has ever take a loan before
    public function get_account_balance($balance_table, $account_id)
    {
        $balance = 0;
        $balance_result = mysqli_query($this->api->link, "SELECT balance FROM $balance_table WHERE account_id = $account_id");
        if ($balance_result) {
            $balance_tmp = mysqli_fetch_array($balance_result, MYSQLI_ASSOC);
            $balance = $balance_tmp['balance'];
        } else {
            echo json_encode(array('status' => 501, 'error' => mysqli_error($this->api->link)));
            mysqli_close($this->api->link);
            die;
        }

        return $balance;
    }

    public function get_user_facility_limit($user_id)
    {
        $limit_collection = array();
//		get savings and balance * 3;
        $savings_ac = $this->get_user_account($user_id, $this->AC_TYPE_SAVINGS);
        $savings_ac_balance = $this->get_account_balance('balance_savings', $savings_ac);

        $bank_ac = $this->get_user_account($user_id, $this->AC_TYPE_BANK);
        $bank_ac_balance = $this->get_account_balance('balance_bank', $bank_ac);

        if ($result = mysqli_query($this->api->link, "SELECT murabaha_limit, school_fees_limit, business_limit, top_up_count FROM user_facility_limits WHERE user_id = $user_id")) {
            $limit_collection = mysqli_fetch_array($result, MYSQLI_ASSOC);
            $limit_collection['murabaha_limit'] = number_format($limit_collection['murabaha_limit'], 2);
            $limit_collection['school_fees_limit'] = number_format(($savings_ac_balance * 3), 2);
            $limit_collection['business_limit'] = number_format(($savings_ac_balance + $bank_ac_balance), 2);

            $limit_collection['status'] = 200;
            echo json_encode($limit_collection);
        } else {
            echo -1;
        }
    }

    public function get_user_facility_limit_function($user_id)
    {
        $limit_collection = array();
//		get savings and balance * 3;
        $savings_ac = $this->get_user_account($user_id, $this->AC_TYPE_SAVINGS);
        $savings_ac_balance = $this->get_account_balance('balance_savings', $savings_ac);

        $bank_ac = $this->get_user_account($user_id, $this->AC_TYPE_BANK);
        $bank_ac_balance = $this->get_account_balance('balance_bank', $bank_ac);

        if ($result = mysqli_query($this->api->link, "SELECT murabaha_limit, school_fees_limit, business_limit, top_up_count FROM user_facility_limits WHERE user_id = $user_id")) {
            $limit_collection = mysqli_fetch_array($result, MYSQLI_ASSOC);
            $limit_collection['murabaha_limit'] = $limit_collection['murabaha_limit'];
            $limit_collection['school_fees_limit'] = ($savings_ac_balance * 3);
            $limit_collection['business_limit'] = ($savings_ac_balance + $bank_ac_balance);

            $limit_collection['status'] = 200;

            return $limit_collection;
        } else {
            return -1;
        }
    }

    public function performTransactionEntry($transaction_id, $account_id, $account_locale, $amount, $debit_credit_flag, $tx_type, $balance_before, $balance_after, $time_created)
    {
        if (mysqli_query($this->api->link, "INSERT INTO transaction_entries(transaction_id, account_id, account_locale, amount, debit_credit_flag, tx_type, balance_before, balance_after, time_created) VALUES ($transaction_id, $account_id, $account_locale, $amount, $debit_credit_flag, $tx_type, $balance_before, $balance_after, '$time_created')")) {
            $this->update_balance($account_locale, $account_id, $balance_after);
        } else {
            $this->mark_transaction_state($transaction_id, 'FAILED');
            echo json_encode(array('status' => 501, 'error' => mysqli_error($this->api->link)));
            die;
        }
    }

    private function update_balance($locale, $ac_id, $latest_value)
    {
        $tbl = '';
        date_default_timezone_set('Africa/Nairobi');
        $time = date('Y-m-d H:i:s');

        if ($locale == 1) {
            $tbl = 'balance_bank';
        } elseif ($locale == 2) {
            $tbl = 'balance_savings';
        } elseif ($locale == 3) {
            $tbl = 'balance_holding';
        } elseif ($locale == 4) {
            $tbl = 'balance_loans';
        } elseif ($locale == 5) {
            $tbl = 'balance_dcf';
        } elseif ($locale == 6) {
            $tbl = 'balance_school_fees';
        } elseif ($locale == 7) {
            $tbl = 'balance_general';
        } elseif ($locale == 8) {
            $tbl = 'balance_facility_provisions';
        } elseif ($locale == 9) {
            $tbl = 'balance_penalties';
        } elseif ($locale > 9 && $locale < 16) {
            $tbl = 'balance_memberships';
        }
        if (mysqli_query($this->api->link, "UPDATE $tbl SET balance = $latest_value, last_updated = '$time' WHERE account_id = $ac_id")) {
            return;
        }

        echo json_encode(array('status' => 501, 'error' => mysqli_error($this->api->link)));
        die;
    }

    public function get_user_account($user_id, $account_type)
    {
        if ($json_result = mysqli_query($this->api->link, "SELECT account_ids FROM account_holders WHERE user_id = $user_id")) {

        } else {
            echo json_encode(array('status' => 501, 'error' => mysqli_error($this->api->link)));
            die;
        }

        $id = -1;
        $json_result_tmp = mysqli_fetch_array($json_result, MYSQLI_ASSOC);

        $json_result_tmp = $json_result_tmp['account_ids'];
        $json = json_decode($json_result_tmp, true);
        if ($json) {
            foreach ($json as $account) {
                if ($account['ac_type'] == $account_type) {
                    $id = $account['id'];
                    break;
                }
            }
        }

        return $id;
    }

    public function get_all_user_facility_accounts($user_id)
    {
        if ($json_result = mysqli_query($this->api->link, "SELECT account_ids FROM account_holders WHERE user_id = $user_id")) {

        } else {
            echo json_encode(array('status' => 501, 'error' => mysqli_error($this->api->link)));
            die;
        }

        $account_collection = array();
        $id = -1;
        $json_result_tmp = mysqli_fetch_array($json_result, MYSQLI_ASSOC);
        $json_result_tmp = $json_result_tmp['account_ids'];
        $json = json_decode($json_result_tmp, true);
        if ($json != null) {
            foreach ($json as $account) {
                if ($account['ac_type'] == $this->AC_TYPE_FACILITY) {
                    array_push($account_collection, $account);
                }
            }
        }

        return $account_collection;
    }

    public function get_all_user_dcf_accounts($user_id, $dcf_type)
    {
        if ($json_result = mysqli_query($this->api->link, "SELECT account_ids FROM account_holders WHERE user_id = $user_id")) {

        } else {
            echo json_encode(array('status' => 501, 'error' => mysqli_error($this->api->link)));
            die;
        }

        $account_collection = array();
        $id = -1;
        $json_result_tmp = mysqli_fetch_array($json_result, MYSQLI_ASSOC);
        $json_result_tmp = $json_result_tmp['account_ids'];
        $json = json_decode($json_result_tmp, true);

        foreach ($json as $account) {
            if ($account['ac_type'] == $this->AC_TYPE_DCF) {
                $ac_id = $account['id'];
                $ac_balance = $this->get_account_balance('balance_dcf', $ac_id);
                if ($dcf_account = mysqli_query($this->api->link, "SELECT * FROM accounts_dcf WHERE account_id = $ac_id")) {
                    $dcf_account_tmp = mysqli_fetch_array($dcf_account, MYSQLI_ASSOC);
                    $final_account_type = $dcf_account_tmp['ac_type'];
                    if ($final_account_type == $dcf_type) {
                        $dcf_account_tmp['balance'] = $ac_balance;
                        array_push($account_collection, (object)$dcf_account_tmp);
                    }
                }
            }
        }

        return $account_collection;
    }

    public function mark_transaction_state($tx_id, $state)
    {
        if (mysqli_query($this->api->link, "UPDATE transactions SET tx_state = '$state' WHERE id = $tx_id;")) {
            return null;
        }

        echo json_encode(array('status' => 501, 'error' => mysqli_error($this->api->link)));
        mysqli_close($this->api->link);
        die;
    }

    public function create_transaction($amount, $tx_type, $user_id)
    {
        date_default_timezone_set('Africa/Nairobi');
        $time = date('Y-m-d H:i:s');
        if (mysqli_query($this->api->link, "INSERT INTO transactions(amount, tx_type, user_id, tx_state, time_created) VALUES ($amount, $tx_type, $user_id, 'PENDING', '$time')")) {
            $tx_id_result = mysqli_query($this->api->link, "SELECT MAX(id) AS tx_id FROM transactions");
            $tx_id_temp = mysqli_fetch_array($tx_id_result, MYSQLI_ASSOC);
            $tx_id = $tx_id_temp['tx_id'];

            if (!mysqli_query($this->api->link, "START TRANSACTION")) {
                $this->mark_transaction_state($tx_id, 'FAILED');
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'could not complete transaction at the moment'
                ));
                die;
            }
        } else {
            echo json_encode(array('status' => 501, 'error' => mysqli_error($this->api->link)));
            die;
        }

        return $tx_id;
    }

    public function commit_transaction($tx_id)
    {
        if (!mysqli_query($this->api->link, "COMMIT")) {
            return false;
        }

        $this->mark_transaction_state($tx_id, 'COMPLETE');

        return true;
    }

    public function create_account($ac_name, $user_id, $ac_type)
    {
        if (!$result = mysqli_query($this->api->link, "INSERT INTO accounts_b (ac_name, extra_types, ac_type, branch_id) VALUES ('$ac_name', $user_id, $ac_type, 1)")) {
            return -1;
        }

        $tx_id_result = mysqli_query($this->api->link, "SELECT MAX(id) AS tx_id FROM accounts_b");
        $tx_id_temp = mysqli_fetch_array($tx_id_result, MYSQLI_ASSOC);
        $ac_id = $tx_id_temp['tx_id'];
        $this->add_account_to_user_holder($user_id, $ac_id, $ac_type);

        return $ac_id;
    }

    public function add_account_to_user_holder($user_id, $account_id, $account_type)
    {
//		GETS USERS AND ACCOUNTS UNDER THEIR NAME
        if ($json_result = mysqli_query($this->api->link, "SELECT account_ids FROM account_holders WHERE user_id = $user_id")) {

        } else {
            echo json_encode(array('status' => 501, 'error' => mysqli_error($this->api->link)));
            die;
        }
        $account_to_add = array('id' => (int)$account_id, 'ac_type' => $account_type);
        $accounts_collection = array();
        $id = -1;
        $json_result_tmp = mysqli_fetch_array($json_result, MYSQLI_ASSOC);

        $json_result_tmp = $json_result_tmp['account_ids'];
        $json = json_decode($json_result_tmp, true);
        foreach ($json as $account) {
            array_push($accounts_collection, $account);
        }
        array_push($accounts_collection, $account_to_add);
        $update_accounts = json_encode($accounts_collection);
        if (mysqli_query($this->api->link, "UPDATE account_holders SET account_ids = '$update_accounts' WHERE user_id = $user_id")) {
            return 1;
        } else {
            return 0;
        }
    }

    public function remove_account_to_user_holder($user_id, $account_id)
    {
//		GETS USERS AND ACCOUNTS UNDER THEIR NAME
        if ($json_result = mysqli_query($this->api->link, "SELECT account_ids FROM account_holders WHERE user_id = $user_id")) {

        } else {
            echo json_encode(array('status' => 501, 'error' => mysqli_error($this->api->link)));
            die;
        }

        $accounts_collection = array();
        $id = -1;
        $json_result_tmp = mysqli_fetch_array($json_result, MYSQLI_ASSOC);

        $json_result_tmp = $json_result_tmp['account_ids'];
        $json = json_decode($json_result_tmp, true);

        foreach ($json as $account) {
            if ($account['id'] != $account_id) {
                array_push($accounts_collection, $account);
            }
        }
//		dd($accounts_collection);
        $update_accounts = json_encode($accounts_collection);
        if (mysqli_query($this->api->link, "UPDATE account_holders SET account_ids = '$update_accounts' WHERE user_id = $user_id")) {
            return 1;
        } else {
            return 0;
        }
    }

    public function get_facility_type($ac_id)
    {
        if ($type_result = mysqli_query($this->api->link, "SELECT loan_type FROM accounts_loans WHERE account_id = $ac_id")) {
            $type_result_temp = mysqli_fetch_array($type_result, MYSQLI_ASSOC);
            $type = $type_result_temp['loan_type'];

            return $type;

        } else {
            return -1;
        }
    }

    public function function_tester()
    {
        echo $this->get_user_account(7, $this->AC_TYPE_BANK);
    }

    public function create_facility_limits_new_user($user_id)
    {
        $score = 0;
        $return = 0;
        if ($score < 50) {
            $eligibleAmount = 0;
        } elseif ($score >= 50 && $score <= 99) {
            $return = 300;
        } elseif ($score >= 100 && $score <= 199) {
            $return = 500;
        } elseif ($score >= 200 && $score <= 299) {
            $return = 1000;
        } elseif ($score >= 300 && $score <= 399) {
            $return = 1500;
        } elseif ($score >= 400 && $score <= 499) {
            $return = 3000;
        } elseif ($score >= 500 && $score <= 599) {
            $return = 15000;
        } elseif ($score >= 600 && $score <= 650) {
            $return = 20000;
        } elseif ($score >= 650 && $score <= 699) {
            $return = 30000;
        } elseif ($score >= 700 && $score <= 749) {
            $return = 45000;
        } elseif ($score >= 750 && $score <= 799) {
            $return = 60000;
        } elseif ($score >= 800 && $score <= 849) {
            $return = 90000;
        } elseif ($score >= 850 && $score <= 900) {
            $return = 100000;
        } else {
            $return = 0;
        }
        date_default_timezone_set('Africa/Nairobi');
        $time = date('Y-m-d H:i:s');
        $updated_at = $time;
        $created_at = $time;
        if (mysqli_query($this->api->link, "INSERT INTO user_facility_limits (user_id, murabaha_limit, school_fees_limit, business_limit, top_up_count, credit_score, created_at, updated_at) 
			VALUES ($user_id, $return,0,0,0,$score,'$created_at', '$updated_at')")) {
            return 1;
        } else {
            return -1;
        }
    }

    public function get_reports(Request $request)
    {
        $user_id = (int)$request->get('user_id');
        $ac_type = (int)$request->get('ac_type');
        $dcfs_account_id = (int)$request->get('dcfs_account');

        if (!isset($user_id) || !isset($ac_type)) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'Request could not provide sufficient authentication documents'
            ));
            die;
        }
        switch ($ac_type) {
            case $this->AC_TYPE_SAVINGS:
                $ac_id = $this->get_user_account($user_id, $this->AC_TYPE_SAVINGS);
                break;
            case $this->AC_TYPE_BANK:
                $ac_id = $this->get_user_account($user_id, $this->AC_TYPE_BANK);
                break;
            case $this->AC_TYPE_DCF:
                $ac_id = $dcfs_account_id;
                break;
            case $this->AC_TYPE_FACILITY:
                $ac_id = $this->get_user_account($user_id, $this->AC_TYPE_FACILITY);
                break;
        }
//		get the last 20 transactions
        $tx_collection = array();
        $transaction_entries = DB::table('transaction_entries')->where('account_id', $ac_id)->orderBy('time_created', 'DESC')->limit(20)->get();

        echo json_encode($transaction_entries);
    }

    public function tx_type_in_words($tx_type)
    {
        $query = "SELECT * FROM transaction_types WHERE id = $tx_type LIMIT 1";
        $transaction_types = mysqli_query($this->api->link, $query);
        $transaction_types_tmp = mysqli_fetch_array($transaction_types, MYSQLI_ASSOC);

        dd($transaction_types_tmp);

        return $transaction_types_tmp;
    }

    public function tx_master_piece($src_ac, $dest_ac, $amount, $extra_charges, $tx_type, $user_id)
    {
        /*SWITCH OUT ALL ACCOUNTS AND DO ALL TRANSACTIONS THROUGH THIS FUNCTION...DEFINE ALL THE RULE AND TRANSACTIONS HERE*/
    }

    public function customer_type($user_id)
    {
        $one = 0;
        $two = 0;
        if ($loan_before = mysqli_query($this->api->link, "SELECT id FROM accounts_loans WHERE user_id = $user_id LIMIT 1")) {
            $loan_before_tmp = mysqli_fetch_array($loan_before, MYSQLI_ASSOC);
            $one = $loan_before_tmp['id'];
        }
        if ($loan_before_arch = mysqli_query($this->api->link, "SELECT id FROM accounts_archive_loans_serviced WHERE user_id = $user_id LIMIT 1")) {
            $loan_before_arch_tmp = mysqli_fetch_array($loan_before_arch, MYSQLI_ASSOC);
            $two = $loan_before_arch_tmp['id'];
        }

        if ($one > 0 || $two > 0) {
            return 2;
        } else {
            return 1;
        }

    }

    public function update_score()
    {

    }

    public function test_queuing()
    {
        $userPhone = '0706129749';
        $smsMessage = 'Testing queues';
        $sms_bundle = array('phone' => $userPhone, 'message' => $smsMessage);
        QueueGrails::dispatch(1, $sms_bundle);
        echo 'complete';
    }

    public function registration_sms_mail(Request $request)
    {
        $data = $request->get('data');
        $json = json_decode($data, true);
        $sms = $json['sms'];
        $email = $json['email'];
        QueueGrails::dispatch(1, $sms);
        QueueGrails::dispatch(2, $email);
        echo 'complete';
    }

    public function flex_cube_post_transaction($transaction)
    {
//		$transaction['transaction_nature']
//		$transaction['account_debited']
//		$transaction['account_credited']
    }

    public function flex_cube_open_new_facility_account($data)
    {
//		$data...KYCs
//		$data...amount and dates
//
    }

    public function test_schedule()
    {

        DB::update("UPDATE creditdetails SET credit_score = credit_score+1 WHERE creditId = 5");

        $date = date('Y-m-d H:i:s');
        Storage::append("test.txt", "update schedule" . $date);
    }

    public function get_last_account_transactions($account, $count)
    {
        if (!$lastTxResult = mysqli_query($this->api->link, "SELECT te.*, te.debit_credit_flag, ty.name, te.time_created  FROM transaction_entries te, transaction_types ty WHERE te.account_id = $account AND te.tx_type = ty.id ORDER BY te.time_created DESC LIMIT $count")) {
            echo "Error " . mysqli_error($this->api->link);

            return null;
        }

        $gather = array();
        while ($row = mysqli_fetch_array($lastTxResult, MYSQLI_ASSOC)) {
            $collection = new stdClass();
            $collection->time = $row['time_created'];
            $collection->method = $row['tx_type'];
            $collection->dr_cr = $row['debit_credit_flag'];
            if ($row['debit_credit_flag'] == 1) {
                $collection->credit = $row['amount'];
                $collection->debit = null;
            } elseif ($row['debit_credit_flag'] == 2) {
                $collection->credit = null;
                $collection->debit = $row['amount'];
            }
            $collection->tx_narrative = $row['name'];

            $collection->balance = $row['balance_after'];
            $gather[] = $collection;
        }

        return json_encode($gather);
    }

    public function get_dcf_details($account_no, $user_id)
    {
        return DB::table('accounts_dcf')->select(['ac_name', 'ac_type'])->where('account_id', $account_no)->where('user_id', $user_id)->first();
    }
}