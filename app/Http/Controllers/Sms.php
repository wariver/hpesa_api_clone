<?php
/**
 * Created by Warui.
 * User: root
 * Date: 8/22/18
 * Time: 5:37 PM
 */

namespace App\Http\Controllers;

use App\Jobs\QueueGrails;
use Converter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use stdClass;

class Sms extends Controller
{

    /**
     * Abi constructor.
     */
    public function __construct()
    {

    }

    public function sendSMS($phone, $msg){
        $sms_bundle = array('phone' => $phone, 'message' => $msg);
        QueueGrails::dispatch(1, $sms_bundle);
    }
}