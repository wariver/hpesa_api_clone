<?php
/**
 * Created by Warui.
 * User: root
 * Date: 8/22/18
 * Time: 5:37 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Savings extends Controller {
	public $api;
	public $abi;
	public $TX_BANK_B2C;
	public $TX_BANK_SAVINGS;
	public $TX_SAVINGS_BANK;
	public $TX_BANK_BANK;
	public $TX_BANK_AIRTIME;
	public $TX_B2C;
	public $TX_C2B_SAVINGS;
	public $TX_CHARGES;
	public $AC_CHARGES_ID;
	public $AC_B2C;
	public $AC_C2B;
	public $AC_AIRTIME;

	/**
	 * Savings constructor.
	 */
	public function __construct() {
		$this->TX_BANK_B2C     = 8;
		$this->TX_BANK_SAVINGS = 4;
		$this->TX_SAVINGS_BANK = 5;
		$this->TX_BANK_BANK    = 1;
		$this->TX_BANK_AIRTIME = 14;
		$this->TX_C2B_SAVINGS  = 15;
		$this->TX_CHARGES       = 19;

        $this->TX_C2B_GROUP_SAVINGS = 30;
		$this->AC_CHARGES_ID = 8;
		$this->AC_AIRTIME    = 9;
		$this->AC_C2B        = 2;
		$this->AC_B2C        = 3;
		$this->api           = new Api();
		$this->abi           = new Abi();
	}

	public function C2B_savings( Request $request ) {
		$phoneno            = (int) $request->get( 'phoneno' );
		$amount             = (int) $request->get( 'amount' );
		$identifier         = $request->get( 'identifier' );
		$user_id            = $request->get( 'userId' );
		$savings_account_id = $request->get('account_id');
		$paymenttype        = (int) $request->get( 'paymenttype' );

		$identifier         = 1;
		$phoneno            = 254706129749;
        $savings_account_id = 0;
        $c2b_balance        = $this->abi->get_account_balance( 'balance_general', $this->AC_C2B );
        $savings_balance    = 0;

        if($paymenttype == 1) {
            $savings_account_id = $this->abi->get_user_account( $user_id, $this->abi->AC_TYPE_SAVINGS );
            $savings_balance = $this->abi->get_account_balance( 'balance_savings', $savings_account_id );
//		CREATE TRANSACTION
            $tx_id = $this->abi->create_transaction($amount, $this->TX_C2B_SAVINGS, $user_id);
//		cr B2C account
            $this->abi->performTransactionEntry($tx_id, $this->AC_C2B, $this->abi->AC_TYPE_GENERAL, $amount, 1, $this->TX_C2B_SAVINGS, $c2b_balance, ($c2b_balance + $amount), $this->abi->now);
//		cr savings account
            $this->abi->performTransactionEntry($tx_id, $savings_account_id, $this->abi->AC_TYPE_SAVINGS, $amount, 1, $this->TX_C2B_SAVINGS, $savings_balance, ($savings_balance + $amount), $this->abi->now);

//		run stk push notification
            $paymentRequestResult = $this->api->stkPayment($amount, $phoneno, $paymenttype, $user_id, $identifier, 'C2B to Savings');

            $resultVals = json_decode($paymentRequestResult, true);
        }
        if($paymenttype == 4){
            $savings_account_id = $this->abi->get_user_account( $user_id, $this->abi->AC_TYPE_MEMBERSHIPS );
            $savings_balance = $this->abi->get_account_balance( 'balance_memberships', $savings_account_id );
//		CREATE TRANSACTION
            $tx_id = $this->abi->create_transaction( $amount, $this->TX_C2B_GROUP_SAVINGS, $user_id );

//		cr group account
            $this->abi->performTransactionEntry( $tx_id, $this->AC_C2B, $this->abi->AC_TYPE_MEMBERSHIPS, $amount, 1, $this->TX_C2B_GROUP_SAVINGS, $c2b_balance, ( $c2b_balance + $amount ), $this->abi->now );

//		cr savings account
            $this->abi->performTransactionEntry( $tx_id, $savings_account_id, $this->abi->AC_TYPE_MEMBERSHIPS, $amount, 1, $this->TX_C2B_GROUP_SAVINGS, $savings_balance, ( $savings_balance + $amount ), $this->abi->now );

//		run stk push notification
            $paymentRequestResult = $this->api->stkPayment( $amount, $phoneno, $paymenttype, $user_id, $identifier, 'C2B to Group Savings' );

            $resultVals = json_decode( $paymentRequestResult, true );
        }

		if ( $resultVals['status'] == 1 ) {
		if ( ! $this->abi->commit_transaction( $tx_id ) ) {
			echo json_encode( array(
				'status' => 501,
				'error'  => 'Could not complete transaction at the moment.'
			) );
		} else {
			echo json_encode( array(
				'status' => 200,
				'msg'    => 'A request has been sent to your phone.'
			) );
		}
		} else {
			echo json_encode( array(
				'status' => 501,
				'msg'    => 'Request failed. Please try again after a few minutes.'
			) );
		}
	}

	public function savings_bank( Request $request ) {
		$user_id       = $request->get( 'userId' );
		$amount        = $request->get( 'amount' );
		$savings_ac_id = $request->get( 'savings_ac_id' );
		$bank_ac_id    = $request->get( 'bank_ac_id' );

		if ( ! isset( $user_id ) || ! isset( $amount ) || ! isset( $savings_ac_id ) || ! isset( $bank_ac_id ) ) {
			echo json_encode( array( 'status' => 501, 'There were some missing parameters in your request' ) );
			die;
		}
		$bank_ac_balance    = $this->abi->get_account_balance( 'balance_bank', $bank_ac_id );
		$savings_ac_balance = $this->abi->get_account_balance( 'balance_savings', $savings_ac_id );

		if ( $amount > $savings_ac_balance ) {
			echo json_encode( array(
				'status' => 501,
				'error'  => 'You do not have enough funds to transfer this amount to your savings. You can access up to '.$savings_ac_balance.' of your total savings'
			) );
			die;
		}
		$tx_id = $this->abi->create_transaction( $amount, $this->TX_SAVINGS_BANK, $user_id );

		//		dr savings
		$this->abi->performTransactionEntry( $tx_id, $savings_ac_id, $this->abi->AC_TYPE_SAVINGS, $amount, 2, $this->TX_SAVINGS_BANK, $savings_ac_balance, ( $savings_ac_balance - $amount ), $this->abi->now );
		//		cr bank
		$this->abi->performTransactionEntry( $tx_id, $bank_ac_id, $this->abi->AC_TYPE_BANK, $amount, 1, $this->TX_SAVINGS_BANK, $bank_ac_balance, ( $bank_ac_balance + $amount ), $this->abi->now );

		if ( ! $this->abi->commit_transaction( $tx_id ) ) {
			echo json_encode( array( 'status' => 501, 'error' => 'could not complete transaction at the moment' ) );
			die;
		} else {
			echo json_encode( array(
				'status' => 200,
				'msg'    => 'Amount KES. ' . $amount . ' transferred to savings successfully'
			) );
			die;
		}
	}

}