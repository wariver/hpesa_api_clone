<?php
/**
 * Created by Warui.
 * User: root
 * Date: 8/22/18
 * Time: 5:37 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DCF extends Controller
{
    public $abi;
    public $api;
    public $TX_DCF_BANK;
    public $AC_C2B;
    public $TX_C2B_DCF;
    public $now;

    /**
     * DCF constructor.
     */
    public function __construct()
    {
        $this->TX_DCF_BANK = 11;
        $this->TX_C2B_DCF = 16;
        $this->AC_C2B = 2;
        $this->api = new Api();
        $this->abi = new Abi();
        $this->now = $this->api->now;
    }

    public function sign_up(Request $request)
    {

        $name = $request->get('username');
        $actype = $request->get('actype');
        $idpass = $request->get('idpass');
        $phone = $request->get('phone');
        $email = $request->get('useremail');
        $dob = $request->get('dob');
        $country = $request->get('country');
        $town = $request->get('town');
        $password = $request->get('password');
        $referal = $request->get('referal');

        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');

        //format phone
        if (substr($phone, 0, 2) === "07")//if phone starts with 07
        {
            $phone = substr_replace($phone, "254", 0, 1);
        } elseif (substr($phone, 0, 1) === "7")//if starts with 7
        {
            $phone = substr_replace($phone, "254", 0, 0);
        } elseif (substr($phone, 0, 4) === "+254") {
            $phone = substr_replace($phone, "", 0, 1);
        }
        if ($this->api->validatePhone($phone) == 1) {
            $response['phone'] = $phone;
            if ($this->api->validateIdPass($idpass, $actype) == 1) {
                $response['id'] = $idpass;
                $response['actype'] = $actype;

                if (!$query = DB::table('users')->where('email', $email)->get()) {
                    $response = array('status' => 501, 'error' => 'Failed to fetch email');
                } else {

                    if (count($query) > 0)//if email is in use
                    {
                        $response = array('status' => 501, 'error' => 'This email is already registered');
                    } else {

                        //check if ID/pas is registered
                        $idpass = $actype == 4 ? time() . rand(1000, 9999) : $idpass;//generate random number for group


                        if (!$checkidpass = DB::table('users')->where('idpass', $idpass)->get()) {
                            $response = array('status' => 501, 'error' => 'Could not find id number in database');
                        } else {

                            if (count($checkidpass) == 0) {
                                if ($actype == 4) {
                                    $acStatusThis = 2;
                                } elseif ($actype == 5) {
                                    $acStatusThis = 2;
                                } elseif ($actype == 6) {
                                    $acStatusThis = 2;
                                } else {
                                    $acStatusThis = 0;
                                }

                                if (!$user = DB::insert("INSERT INTO users (fullname,idpass,phone,email,password,actype,acstatus,signuptime,passrecoverycode,signupoption,regfee,membershipfee,timepaid,timeactivated,merchantId,passkey,blacklist,phoneconfirmed,mpesaconfirmed,referredby,phone_iprs,gender,physicaladdress,dateofbirth,country,town) VALUES('$name','$idpass','$phone','$email','$password',$actype,$acStatusThis,'$now','',2,0,0,'$now','$now','','',0,0,0,'','',0,'','$dob','$country','$town')"))//insert record
                                {
                                    $response = array('status' => 501, 'error' => 'Failed to insert user ');
                                } else {
                                    $userId = DB::table('users')->select(array(
                                        'id',
                                        'fullname'
                                    ))->where('email', $email)->get();
                                    //if group generate registration number

                                    $userName = $userId[0]->fullname;
                                    $userId = $userId[0]->id;

                                    if (!$this->create_accounts($userName, $userId)) {
                                        echo json_encode(array(
                                            'status' => 501,
                                            'error' => 'Could not create accounts'
                                        ));
                                        die;
                                    }

                                    if ($actype == 4) {
                                        $idno = "Group" . $userId;

                                        if (!$this->api->crud_update($userId, $idno)) {

                                            echo json_encode(array(
                                                'status' => 501,
                                                'error' => 'Failed to update gid'
                                            ));
                                        } else {

                                            $message = "$name's registration number is " . $idno;
                                            $this->api->sendMobileSasa($phone, $message);
                                        }
                                    } elseif ($actype == 5) {
                                        $idno = "Business" . $userId;

                                        if (!$this->api->crud_update($userId, $idno)) {

                                            echo json_encode(array(
                                                'status' => 501,
                                                'error' => 'Failed to update gid'
                                            ));
                                        } else {

                                            $message = "You have registered $name successfully as a business, registration number is " . $idno;
                                            $this->api->sendMobileSasa($phone, $message);
                                        }
                                    } elseif ($actype == 6) {
                                        $idno = "Sacco" . $userId;

                                        if (!$this->api->crud_update($userId, $idno)) {

                                            echo json_encode(array(
                                                'status' => 501,
                                                'error' => 'Failed to update gid'
                                            ));
                                        } else {

                                            $message = "Your Sacco $name has been created successfully. Your registration number is " . $idno;
                                            $this->api->sendMobileSasa($phone, $message);
                                        }
                                    }
                                    //send email
                                    $message = $this->api->getSignUpEmailMessage($name);
                                    $subject = "Welcome to DIB WAVES";
                                    $files = "Loan_policy.pdf";
                                    $this->api->sendEmail($subject, $message, $email, $files);

                                    $response['user_id'] = $userId;
                                    $response['actype'] = $actype;
                                    $response['ac_status'] = $acStatusThis;
                                }
                            } else {
                                echo json_encode(array(
                                    'status' => 501,
                                    'error' => 'The Id/Registration number you entered is already registered'
                                ));
                                die;
                            }
                        }
                    }
                }
            } else {
                echo json_encode(array(
                    'status' => 501,
                    'error' => 'enter a valid / passport / registration number'
                ));
                die;
            }
        } else {
            echo json_encode(array('status' => 501, 'error' => 'phone number is invalid'));
            die;
        }
        echo json_encode($response);
    }

    public function create_dcf(Request $request)
    {
        $response = 0;
        $userId = $request->get('userId');
        $date = $request->get('date');
        $accountname = $request->get('accountname');
        $projectprice = $request->get('projectprice');
        $description = $request->get('description');
        $type = $request->get('type');
        $targetsavings = $request->get('targetsavings');

//		create real real account
        //validate target date
        $today = date('d/m/Y');
        //must be at least 1 week
        if (strtotime($this->api->convertDate($date)) - strtotime($this->api->convertDate($today)) <= (7 * 24 * 60 * 60)) {
            json_encode(array(
                'status' => 506,
                'error' => 'Target date must be at least after one week'
            ));
            die;
        }

        //validate savings target
        if (!is_numeric($targetsavings) || $targetsavings <= 1000) {
            echo json_encode(array(
                'status' => 504,
                'error' => 'Enter a valid savings target. It must be at least Ksh 1,000'
            ));
            die;
        }

        //validate savings target
        if (!is_numeric($projectprice) || $projectprice <= 1000) {
            echo json_encode(array(
                'status' => 504,
                'error' => 'Enter a valid project price. It must be at least Ksh 1,000'
            ));
        }
        /*$userId
$accountname
$targetsavings
$projectprice
$description
$date
$type*/


        //check if account name exists for this user
        if ($check = DB::table('accounts_dcf')->where('user_id', $userId)->where('ac_name', $accountname)->where('ac_status', 1)->get()->first()) {
            $response = array('status' => 501, 'error' => 'account name does not exist');
        } else {
            if ($check != null) {
                $response = array(
                    'status' => 501,
                    'error' => 'you have an active account with the same name'
                );
            } else {

                date_default_timezone_set('Africa/Nairobi');
                $now = date('Y-m-d H:i:s');
                $date = $this->api->convertDate($date);

                if (!mysqli_query($this->api->link, "START TRANSACTION")) {
                    echo json_encode(array(
                        'status' => 501,
                        'error' => 'Account failed to create'
                    ));
                    die;
                }
                $ac_id = $this->abi->create_account($accountname, $userId, 5);
                $bank_ac_id = $this->abi->get_user_account($userId, $this->abi->AC_TYPE_BANK);

                if ($ac_id == -1) {
                    echo json_encode(array(
                        'status' => 501,
                        'error' => 'Could not create account'
                    ));
                    die;
                }
                if (DB::insert("INSERT INTO accounts_dcf (user_id, account_id, bank_account_id, ac_name, description, savings_target, project_price, target_date, date_created, date_closed, ac_type, ac_status, last_seen) VALUES ($userId, $ac_id, $bank_ac_id, '$accountname', '$description', $targetsavings, $projectprice, '$date', '$now', 'NULL',$type, 1,'$now')")) {
                    mysqli_query($this->api->link, "COMMIT");
                    if (!mysqli_query($this->api->link, "COMMIT")) {
                        echo json_encode(array(
                            'status' => 501,
                            'error' => 'Account could not be created'
                        ));
                        die;
                    } else {
                        echo json_encode(array(
                            'status' => 200,
                            'msg' => 'Your account has been created'
                        ));
                        die;
                    }
                } else {
                    echo json_encode(array(
                        'status' => 501,
                        'error' => 'Account could not be created'
                    ));
                    die;
                }
            }


        }

        return json_encode($response);
    }

    public function create_school_fees(Request $request)
    {
        $response = 0;
        $user_id = $request->get('user_id');
        $child_name = $request->get('child_name');
        $adm_number = $request->get('adm_number');
        $school_name = $request->get('school_name');
        $bank_name = $request->get('bank_name');
        $branch_name = $request->get('branch_name');
        $cp_phone = $request->get('cp_phone');
        $ac_number = $request->get('account_number');
        $description = "$child_name" . "\'" . 's School fees account';
        $type = 4;
//		dd($description);
        //validate target date
        $today = date('d/m/Y');

        $account_exists = DB::table('school_fees_accounts')->where('user_id', $user_id)->where('adm_number', $adm_number)->where('status', 1)->get()->first();
        if ($account_exists != null) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'you have an active account with the same admission number'
            ));
            die;
        }
        //check if account name exists for this user
        $name_exists = DB::table('school_fees_accounts')->where('user_id', $user_id)->where('student_name', $child_name)->where('status', 1)->get()->first();
        if ($name_exists != null) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'you have an active account with the same name'
            ));
            die;
        }
        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');
        $date = $this->api->convertDate($today);

        $ac_id = $this->abi->create_account($child_name, $user_id, 5);
        $bank_ac_id = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_BANK);

//		CREATE ACCOUNT
        if ($ac_id != -1) {
            if (DB::insert("INSERT INTO accounts_dcf (user_id, account_id, bank_account_id, ac_name, description, savings_target, project_price, target_date, date_created, date_closed, ac_type, ac_status, last_seen) 
							VALUES ($user_id, $ac_id, $bank_ac_id, '$child_name', '$description', 0, 0, '$date', '$this->now', 'NULL',$type, 1,'$this->now')")) {
                if (DB::insert("INSERT INTO school_fees_accounts  (account_id, student_name, adm_number, school_name, bank_name, bank_branch, ac_number, status, amount, bursar_phone, user_id, date_created, date_updated) 
							VALUES($ac_id, '$child_name', '$adm_number', '$school_name', '$bank_name', '$branch_name', $ac_number, 1, 0, '$cp_phone', $user_id, '$this->now', '$this->now')")) {
                    $response = array(
                        'status' => 200,
                        'msg' => 'Your account has been created!'
                    );

                } else {
                    $response = array(
                        'status' => 501,
                        'error' => 'failed to add school fees account'
                    );
                }
            }
        }

        echo json_encode($response);
    }

    public function dcf_bank(Request $request)
    {
        $user_id = $request->get('user_id');
        $amount = $request->get('amount');
        $dcf_ac_id = $request->get('dcf_ac_id');

        $bank_ac_id = $this->abi->get_user_account($user_id, $this->abi->AC_TYPE_BANK);

        $toId = -1;
        if (!isset($user_id) || !isset($amount) || !isset($dcf_ac_id)) {
            echo json_encode(array('status' => 501, 'There were some missing parameters in your request'));
            die;
        }
        $bank_ac_balance = $this->abi->get_account_balance('balance_bank', $bank_ac_id);
        $dcf_ac_balance = $this->abi->get_account_balance('balance_dcf', $dcf_ac_id);

        if ($amount > $dcf_ac_balance) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'You do not have enough funds to transfer this amount'
            ));
            die;
        }

//		CREATE TRANSACTION
        $tx_id = $this->abi->create_transaction($amount, $this->TX_DCF_BANK, $user_id);

//		dr dcf account
        $this->abi->performTransactionEntry($tx_id, $dcf_ac_id, $this->abi->AC_TYPE_DCF, $amount, 2, $this->TX_DCF_BANK, $dcf_ac_balance, ($dcf_ac_balance - $amount), $this->abi->now);
//		cr bank account
        $this->abi->performTransactionEntry($tx_id, $bank_ac_id, $this->abi->AC_TYPE_BANK, $amount, 1, $this->TX_DCF_BANK, $bank_ac_balance, ($bank_ac_balance + $amount), $this->abi->now);
        if (!$this->abi->commit_transaction($tx_id)) {
            echo json_encode(array('status' => 501, 'error' => 'could not complete transaction at the moment'));
            die;
        } else {
            echo json_encode(array(
                'status' => 200,
                'msg' => 'Amount KES. ' . $amount . ' transferred to you account successfully'
            ));
            die;
        }

    }

    public function c2b_dcf(Request $request)
    {
        $phoneno = (int)$request->get('phoneno');
        $amount = (int)$request->get('amount');
        $identifier = $request->get('identifier');
        $user_id = $request->get('userId');
        $dcf_account_id = $request->get('account_id');
        $paymenttype = (int)$request->get('paymenttype');
        $paymenttype = 1;
        $phoneno = 254706129749;

//		$dcf_account_id = $this->abi->get_user_account( $user_id, $this->abi->AC_TYPE_DCF );
        $dcf_balance = $this->abi->get_account_balance('balance_dcf', $dcf_account_id);
        $c2b_balance = $this->abi->get_account_balance('balance_general', $this->AC_C2B);


//		CREATE TRANSACTION
        $tx_id = $this->abi->create_transaction($amount, $this->TX_C2B_DCF, $user_id);
//		cr C2B account
        $this->abi->performTransactionEntry($tx_id, $this->AC_C2B, $this->abi->AC_TYPE_GENERAL, $amount, 1, $this->TX_C2B_DCF, $c2b_balance, ($c2b_balance + $amount), $this->abi->now);
//		cr DCF account
        $this->abi->performTransactionEntry($tx_id, $dcf_account_id, $this->abi->AC_TYPE_DCF, $amount, 1, $this->TX_C2B_DCF, $dcf_balance, ($dcf_balance + $amount), $this->abi->now);

        $reference = $this->api->getReference($paymenttype, $user_id);
        $paymentRequestResult = $this->api->stkPayment($amount, $phoneno, $paymenttype, $user_id, $identifier, $reference);

        $resultVals = json_decode($paymentRequestResult, true);

        if (!$this->abi->commit_transaction($tx_id)) {
            echo json_encode(array(
                'status' => 501,
                'error' => 'Could not complete transaction at the moment.'
            ));
        } else {
            if ($resultVals['status'] == 1) {
                echo json_encode(array(
                    'status' => 200,
                    'msg' => 'A request has been sent to your phone.'
                ));
            } else {
                echo json_encode(array(
                    'status' => 501,
                    'msg' => 'Request failed.'
                ));
            }
        }
    }

    public function dcf_list(Request $request)
    {
        $user_id = (int)$request->get('user_id');
        $dcf_type = (int)$request->get('dcf_type');
        $dcf_list = $this->abi->get_all_user_dcf_accounts($user_id, $dcf_type);
        if (count($dcf_list) > 0) {
            echo json_encode($dcf_list);
        } else {
            return json_encode(array(
                'status' => 501,
                'error' => 'You haven\'t yet created such an account'
            ));
        }
    }

    public function balance_sum_dcfs(Request $request)
    {
        $user_id = $request->get('user_id');
        $dcf_type = $request->get('dcfs_type');

        if ($json_result = mysqli_query($this->api->link, "SELECT account_ids FROM account_holders WHERE user_id = $user_id")) {

        } else {
            echo json_encode(array('status' => 501, 'error' => mysqli_error($this->api->link)));
            die;
        }
        $total = 0;
        $json_result_tmp = mysqli_fetch_array($json_result, MYSQLI_ASSOC);
        $json_result_tmp = $json_result_tmp['account_ids'];
        $json = json_decode($json_result_tmp, true);

        foreach ($json as $account) {
            if ($account['ac_type'] == $this->abi->AC_TYPE_DCF) {
                $ac_id = $account['id'];
                if ($dcf_account = mysqli_query($this->api->link, "SELECT SUM(balance) as balance FROM balance_dcf,accounts_dcf WHERE accounts_dcf.user_id = $user_id AND accounts_dcf.ac_type = $dcf_type AND balance_dcf.account_id = accounts_dcf.account_id")) {
                    $dcf_account_tmp = mysqli_fetch_array($dcf_account, MYSQLI_ASSOC);
                    $final_account_type = $dcf_account_tmp['balance'];
                    $total = $final_account_type;

                }
            }
        }

        echo json_encode(array('status' => 200, 'total' => 'KES. ' . number_format($total, 2)));
    }

    public function dcf_details(Request $request)
    {
        $user_id = $request->get('user_id');
        $account_no = $request->get('account_number');

        if (!isset($user_id) || !isset($account_no)) {
            echo json_encode(array('status' => 501, 'error' => 'Method not allowed'));
            die;
        }


        $abi = new Abi();
        $account_balance = $abi->get_account_balance('balance_dcf', $account_no);
        $dcf_details = $abi->get_dcf_details($account_no, $user_id);

        if (isset($dcf_details)) {
            $dcf_details->balance = 'KES ' . number_format($account_balance, 2);
            $dcf_details->last_transactions = $abi->get_last_account_transactions($account_no, 20);
        }

        return json_encode($dcf_details);
    }

}
