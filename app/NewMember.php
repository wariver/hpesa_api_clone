<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewMember extends Model
{
    //
    //
    protected $table = 'new_members_invited';
    public const CREATED_AT = 'creation_date';
    public const UPDATED_AT = 'last_update';
}
