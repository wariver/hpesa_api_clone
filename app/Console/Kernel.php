<?php

namespace App\Console;

use App\Console\Commands\CleanUpServiced;
use App\Console\Commands\FacilityPenalties;
use App\Console\Commands\ScheduleLoans;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		ScheduleLoans::class,
		CleanUpServiced::class,
		FacilityPenalties::class
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule $schedule
	 *
	 * @return void
	 */
	protected function schedule( Schedule $schedule ) {
		$schedule->command( ScheduleLoans::class )->everyFiveMinutes();
		$schedule->command( CleanUpServiced::class )->everyFifteenMinutes();
		$schedule->command( FacilityPenalties::class )->hourly( 0 );
		/*$schedule->call( function () {
			$this->facility_do_provisioning();
		} )->everyFiveMinutes();*/
	}

	/**
	 * Register the Closure based commands for the application.
	 *
	 * @return void
	 */
	protected function commands() {
		require base_path( 'routes/console.php' );
	}


}
