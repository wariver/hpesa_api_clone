<?php

namespace App\Console\Commands;

use App\Http\Controllers\Provisioning;
use Illuminate\Console\Command;

class FacilityPenalties extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'FacilityPenalties:sup';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command to set penalties on overdue loans';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	private $provisions;
	public function __construct() {
		parent::__construct();
		$this->provisions = new Provisioning();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$this->do_penalties();
	}

	private function kula_sup( $account_id, $percentage, $user_id, $principle ) {
//		check if provision account exists
//		calculate the provisioning amount = percentage of total outstanding
		$amount                     = ( $principle * $percentage / 100 );
		$total_penalties_balance    = $this->provisions->abi->get_account_balance( 'balance_general', $this->provisions->AC_ID_PENALTIES );
		$facility_liability_balance = $this->provisions->abi->get_account_balance( 'balance_loans', $account_id );
		$facility_penalty_balance   = $this->provisions->abi->get_account_balance( 'balance_penalties', $account_id );
		$tx_id                      = $this->provisions->abi->create_transaction( $amount, $this->provisions->TX_MAKE_PENALTY, $user_id );


//		credit facility liability account
		$this->provisions->abi->performTransactionEntry( $tx_id, $account_id, $this->provisions->AC_TYPE_FACILITY, $amount, 1, $this->provisions->TX_MAKE_PENALTY, $facility_liability_balance, ( $facility_liability_balance + $amount ), $this->provisions->now );

//		credit penalties account
		$this->provisions->abi->performTransactionEntry( $tx_id, $account_id, $this->provisions->AC_TYPE_PENALTIES, $amount, 1, $this->provisions->TX_MAKE_PENALTY, $facility_penalty_balance, ( $facility_penalty_balance + $amount ), $this->provisions->now );

//      credit total provisions
		$this->provisions->abi->performTransactionEntry( $tx_id, $this->provisions->AC_ID_PENALTIES, $this->provisions->AC_TYPE_GENERAL, $amount, 1, $this->provisions->TX_MAKE_PENALTY, $total_penalties_balance, ( $total_penalties_balance + $amount ), $this->provisions->now );

		if ( $this->provisions->abi->commit_transaction( $tx_id ) ) {
//			send penalty SMS

			$this->provisions->abi->mark_transaction_state( $tx_id, 'COMPLETE' );
		} else {
			$this->provisions->abi->mark_transaction_state( $tx_id, 'FAILED' );
		}
	}
	private function create_facility_penalty( $account_id ) {
		$exist = mysqli_query( $this->provisions->abi->api->link, "SELECT account_id FROM balance_penalties WHERE account_id = $account_id" );
		if ( mysqli_num_rows( $exist ) > 0 ) {
			return 0;
		} else {
			if ( mysqli_query( $this->provisions->abi->api->link, "INSERT INTO balance_penalties(account_id, balance) VALUES ($account_id, 0)" ) ) {
				return 1;
			} else {
				echo json_encode( array(
					'status' => 500,
					'error'  => mysqli_error( $this->provisions->abi->api->link ) . 'could not create penalty for facility'
				) );
				mysqli_close( $this->provisions->abi->api->link );
				die;
			}
		}
	}
	private function do_penalties() {
		$penalty_accounts = $this->provisions->get_facilities_overdue();
		$arr_penalties    = json_decode( $penalty_accounts, true );

		foreach ( $arr_penalties as $arr_penalty ) {
			$account_id = $arr_penalty['account_id'];
			$this->create_facility_penalty( $account_id );
			$user_id                      = $arr_penalty['user_id'];
			$principle                    = $arr_penalty['principle_amount'];
			$this->kula_sup( $account_id, 2, $user_id, $principle );
		}
	}
}
