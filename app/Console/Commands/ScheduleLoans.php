<?php

namespace App\Console\Commands;

use App\Http\Controllers\Abi;
use App\Http\Controllers\Provisioning;
use function GuzzleHttp\Promise\exception_for;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ScheduleLoans extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'ScheduleLoans:provisioning';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$this->facility_do_provisioning();
		return null;
	}

	private function test_file() {
		print 'Kill proc';
//		$new_abi = new Abi();
//		$new_abi->test_schedule();
		$exec = DB::update( "UPDATE creditdetails SET credit_score = credit_score+1 WHERE creditId = 5" );
		if ( $exec ) {
			return 1;
		} else {
			return 0;
		}

	}
	private function facility_do_provisioning() {
		$provisions = new Provisioning();

		if ( $result = mysqli_query( $provisions->abi->api->link, "SELECT account_id FROM view_all_overdue WHERE balance > 0" ) ) {
			if ( mysqli_num_rows( $result ) > 0 ) {
				$result_tmp = mysqli_fetch_all( $result, MYSQLI_ASSOC );
				foreach ( $result_tmp as $account_id ) {
					$ac_id   = $account_id['account_id'];
					$id      = DB::table( 'accounts_loans' )->select( [ 'user_id' ] )->where( 'account_id', $ac_id )->first();
					$user_id = $id->user_id;
					$provisions->should_provision_or_reverse( $ac_id, $user_id );
				}
			}
			mysqli_close( $provisions->abi->api->link );
		}
	}
}
