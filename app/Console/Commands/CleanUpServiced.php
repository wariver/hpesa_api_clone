<?php

namespace App\Console\Commands;

use App\Http\Controllers\Abi;
use Illuminate\Console\Command;

class CleanUpServiced extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'CleanUp:serviced_facilities';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Cleans up serviced facilities';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$this->archive_serviced_loans();

		return null;
	}

	public function archive_serviced_loans() {
		$abi          = new Abi();
		$query        = "SELECT * FROM accounts_loans AS a,balance_loans AS b WHERE a.account_id = b.account_id AND b.balance = 0";
		$serviced     = mysqli_query( $abi->api->link, $query );
		$serviced_tmp = mysqli_fetch_all( $serviced, MYSQLI_ASSOC );
		if ( $serviced ) {
			foreach ( $serviced_tmp as $facility ) {
				$account_id              = $facility['account_id'];
				$user_id                 = $facility['user_id'];
				$application_id          = $facility['application_id'];
				$loan_type               = $facility['loan_type'];
				$principle_amount        = $facility['principle_amount'];
				$applicant_name          = $facility['applicant_name'];
				$phone_number            = $facility['phone_number'];
				$national_id             = $facility['national_id'];
				$customer_type           = $facility['customer_type'];
				$address                 = $facility['address'];
				$dob                     = $facility['dob'];
				$branch_name             = $facility['branch_name'];
				$product_description     = $facility['product_description'];
				$book_date               = $facility['book_date'];
				$maturity_date           = $facility['maturity_date'];
				$date_of_non_performance = $facility['date_of_non_performance'];
				$credit_score            = $facility['credit_score'];

				mysqli_query( $abi->api->link, "START TRANSACTION" );
				mysqli_query( $abi->api->link, "INSERT INTO accounts_archive_loans_serviced (account_id, user_id, application_id, loan_type, principle_amount, applicant_name, phone_number, national_id, customer_type, address, dob, branch_name, product_description, book_date, maturity_date, date_of_non_performance, credit_score) VALUES ($account_id, $user_id, $application_id, $loan_type, $principle_amount, '$applicant_name', '$phone_number', '$national_id', $customer_type, '$address', '$dob', '$branch_name', '$product_description', '$book_date', '$maturity_date', '$date_of_non_performance', $credit_score)" );
				mysqli_query( $abi->api->link, "DELETE FROM balance_facility_provisions WHERE account_id = $account_id" );
				mysqli_query( $abi->api->link, "DELETE FROM balance_loans WHERE account_id = $account_id" );
				mysqli_query( $abi->api->link, "DELETE FROM accounts_loans WHERE account_id = $account_id" );
				if ( $abi->remove_account_to_user_holder( $user_id, $account_id ) != 1 ) {

				}
				mysqli_query( $abi->api->link, "COMMIT" );
			}
			mysqli_close( $abi->api->link );
		}
	}
}
