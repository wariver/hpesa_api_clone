<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WavesUser extends Model
{
    //
    protected $table = 'users';
    public const CREATED_AT = 'creation_date';
    public const UPDATED_AT = 'last_update';
}
